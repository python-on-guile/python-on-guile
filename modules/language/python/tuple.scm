;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python tuple)
  #:use-module (oop goops)
  #:use-module (oop pf-objects)
  #:use-module (language python hash)
  #:use-module (language python for)
  #:use-module (language python yield)
  #:use-module (language python bool)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:export (tuple <py-tuple> defpair #;tuple-ackumulator))


(define-class <py-tuple> () l)
(name-object <py-tuple>)
(cpit <py-tuple>
      (o (lambda (o l)
	   (slot-set! o 'l (map (lambda (x) x) l)))
	 (list
	  (slot-ref o 'l))))

(define-method (py-hash   (o  <py-tuple>)) (py-hash (slot-ref o 'l)))
(define-method (py-class  (o  <py-tuple>)) tuple)
(define-method (bool      (o  <py-tuple>)) (if (pair? (slot-ref o 'l)) o #f))

(define-method (py-equal? (o1 <py-tuple>) o2) (equal? (slot-ref o1 'l) o2))
(define-method (py-equal? o1 (o2 <py-tuple>)) (equal? o1 (slot-ref o2 'l)))

(define-method (wrap-in (o <py-tuple>))
  (wrap-in (slot-ref o 'l)))

(define-python-class tuple (object <py-tuple>)
  (define __new__
    (case-lambda      
      ((cls it er)
       ((ref object '__new__) cls))

      ((cls)
       '())
      
      ((cls it)
       (catch #t
         (lambda ()
           (for-in ((x : it)) ((r '()))
	      (cons x r)
	      #:final (list->vector (reverse! r))))
         (lambda x
	   (pk x)
           (raise (TypeError (+ "'"
                                (pyname it)
                                "' object is not iterable"))))))))

      
  (define __iter__
    (lambda (self)
      ((make-generator
	(lambda (yield)
	  (for-in ((k : (slot-ref self 'l))) ()
		  (yield k)
		  (values)))))))

  (define __repr__
    (lambda (self) (format #f "~a" (slot-ref self 'l)))))

(name-object tuple)

(define-syntax-rule (defpair (f o . u) code ...)
  (begin
    (define-method (f (o <pair>)     . u)
      code ...)
    (define-method (f (o <py-tuple>) . l)
      (let ((o (slot-ref o 'l)))
        (apply f o l)))))

(define-method (scheme? (o <pair>))     tuple)
(define-method (scheme? (o <py-tuple>)) tuple)

(define null (tuple '()))

(define-method (ref (o <null>) . l)
  (apply ref null l))
(define-method (set (o <null>) . l)
  (apply set null l))

(set! (@ (oop dict) slask-tuple) tuple)


(define* (tuple-ackumulator #:optional (n 32))
  (let* ((i     0)
	 (v     (make-vector n)))
    (case-lambda
     ((x)
      (when (>= i n)
	(let* ((nn (* n 2))
	       (v2 (make-vector nn)))
	  (vector-copy! v2 0 v 0 n)
	  (set! v v2)
	  (set! n nn)))
	             
      (vector-set! v i x)
      (set! i (+ i 1)))
     
     (()
      (if (= i n)
	  v
	  (let ((r (make-vector i)))
	    (vector-copy! r 0 v 0 i)
	    r))))))

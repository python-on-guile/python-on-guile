;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python for)
  #:use-module (language python type)
  #:use-module (language python yield)
  #:use-module (language python arg)
  #:use-module (oop pf-objects)
  #:use-module (persist persistance)
  #:use-module (ice-9 stis stis)
  #:use-module (language python exceptions)
  #:use-module (language python def)
  #:use-module (oop goops)
  #:use-module (ice-9 control)
  #:use-module (ice-9 stis-hash)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs bytevectors)
  #:use-module (language python persist)
  #:export (for break next next-sielent define-next wrap-in for-in-py
		for-in <scm-list> <scm-string> empty->stop
		stis-next-l)
                
  #:re-export (arg-empty))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-syntax-rule (pp x ... y)
  (begin
    ;(pretty-print (list x ... (syntax->datum y)))
    y))



(eval-when (compile eval load)
  (begin
    (define stis #f)
    (when (and #t
	       (module-defined? (resolve-module '(ice-9 stis)) 'make-generator))
      (set! stis (resolve-module '(ice-9 stis)))
      (process-use-modules '(((ice-9 stis) #:select ((next . stis-next0))))))))

(define-syntax stis-next
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (if stis
	   #'(stis-next0 x #f)
	   #'"stis"))
      ((_ x y)
       (if stis
	   #'(stis-next0 x y )
	   #'"stis")))))

(define (next-struct ret list)
  (if (eq? (class-of ret) <arg>)
      (if (eq? (struct-ref ret 0) arg-empty)
	  (if (pair? list)
	      (cdr list)
	      #f)
	  #f)
      #f))

(define (stis? ret)
  (let* ((addr (stis-address ret))
	 (tc3 (logand addr   7)))
    (if (= tc3 0)
	(let* ((addr (lookup-1 ret 0)))
	  (= 127 (logand addr 127)))
	#f)))

(define-syntax-rule (stis-next-l list stack x ...)
  (let lp ()
    (let ((ret (stis-next stack x ...)))
      (cond
       ((struct? ret)
	(aif new-stack (next-struct ret list)
	    (begin
	      (set! stack new-stack)
	      (set! list (cdr list))
	      (lp))
	    ret))
       
       ((stis? ret)
	(set! list  (cons ret list))
	(set! stack ret)
	(lp))

       (else
	ret)))))
  

(eval-when (compile eval load)
  (define (generate-temporaries2 x)
    (map (lambda (x) (generate-temporaries x)) x)))

(define-syntax-parameter break (lambda (x) #f))

  
(define (pylist? x)
  (and (struct? x)
       (eq? (ref (ref x '__class__) '__name__)) 'list))


(define-syntax for
  (lambda (x)
    (pp x)
    (syntax-case x (:)
      ((_ ((x ... : #f It ((@@ q m) G (#:apply b ...)))) . v)
       (and (eq? (syntax->datum #'m) 'ref-x)
	    (eq? (syntax->datum #'G) 'zip))
       #'(for ((x : b) ...) . v))

      ((_ a ((x ... : #f It ((@@ q m) G (#:apply b ...)))) . v)
       (and (eq? (syntax->datum #'m) 'ref-x)
	    (eq? (syntax->datum #'G) 'zip))
       #'(for a ((x : b) ...) . v))

      ((for (a ...) . l)
       #'(for #f (a ...) . l))
      
      ((for A (G ...) . l)
       (and
        (syntax-case #'A ()
          ((_ . _) #f)
          (_ #t))
        
        (or-map (lambda (x)
                  (let lp ((x x))
                    (syntax-case x (:)
                      ((: G)     #t)
                      ((: I G)   #t)
		      ((: P I G) #f)
                      ((_ . l)  (lp #'l))
                      (()       #f))))                      
                #'(G ...)))
       
       (with-syntax ((((I ...) ...)
                      (map (lambda (x)
                             (let lp ((x x) (r '()))
                               (syntax-case x (:)
                                 ((: I G)
                                  (append (reverse r)  (list #': #t #'I #'G)))
				 
                                 ((: G)
                                  (append (reverse r)
                                          (list #': #f
						(with-syntax
						    (((I)
						      (generate-temporaries
						       (list #'G))))
						  #'I)
						#'G)))
                                 ((X . L)
                                  (lp #'L (cons #'X r))))))
                           #'(G ...))))
	 
         #'(for A ((I ...) ...) . l)))

      ((for A ((x ... : P I G) ...) ((c n) ...) code ... #:final fin)
       #'(for-work A ((x ... : P I G) ...)
		   ((c n) ...) (code ...) fin values))
    
      ((for A ((x ... : P I G) ...) ((c n) ...) code ... #:finally fin)
       #'(for-work A ((x ... : P I G) ...)
		   ((c n) ...) (code ...) fin values))

      ((for A ((x ... : P I G) ...) ((c n) ...) code ... #:else fin)
       #'(for-work A ((x ... : P I G) ...) ((c n) ...) (code ...) (values)
                   (lambda () fin)))
      
      ((for A ((x ... : P I G) ...) ((c n)) code ...)
       #'(for A ((x ... : P I G) ...) ((c n)) code ... #:final c))

      ((for A ((x ... : P I G) ...) ((c n) ...) code ...)
       #'(for-work A ((x ... : P I G) ...) ((c n) ...) (code ...)
                   (values) values)))))

(define-syntax lambi2
  (syntax-rules ()
    ((_ () () lam) lam)
    
    ((_ (xx . yy) ((x ...) . y) lam)
     (match xx
      ((x ...) (lambi2 yy y lam))))

    ((_ (xx . yy) (#(x ...) . y) lam)
     (match xx
       (#(x ...) (lambi2 yy y lam))))
    
    ((_ (xx . yy) (x . y) lam)
     (let ((x xx))
       (lambi2 yy y lam)))))

(define (slusk g . x) (apply g x))

(define (stopper xx)
  (throw 'python StopIteration
	 (StopIteration xx) None))
	
(define-syntax lambi
  (lambda (x)
    (syntax-case x ()
      ((_ (x    ) lam)
       #'(lambda (xx)
	   (let ((xxx (handle-arg xx 1 stopper)))
	     (lambi2 (xxx) (x) lam))))
      
      ((_ (x ...) lam)
       (with-syntax (((xx ...) (generate-temporaries #'(x ...)))
		     (n        (length #'(x ...))))
	 #'(letrec ((g (lambda (xx ...) (lambi2 (xx ...) (x ...) lam)))

		    (f 
		     (case-lambda
		      ((xx ...    ) (g xx ...))
		      ((xx ... . y) (slusk g xx ...))
		      ((xq)
		       (if (vector? xq)
			   (apply f (vector->list xq))
			   (apply f xq)))))

		    (h
		     (case-lambda
		      ((xxx)
		       (call-with-values
			   (lambda ()
			     (handle-arg xxx n stopper))
			 f))		      
		      ((xx ...)
		       (g xx ...))
		      (xxx (apply f xxx)))))
	     
	     h))))))

(define-syntax o
  (lambda (x)
    (syntax-case x ()
      ((_ code)
       #'code)

      ((_ (next x ...) y . z)
       #'(call-with-values
	     (lambda () next)
	   (lambi (x ...) (o y . z)))))))
	 
(define range (lambda x x))
(define (make-vec v n i d) (values))

(define-syntax-rule (for-work a b . c) (for-set #t a b () . c))

(eval-when (compile eval load)
  (define in-stuff-*   '(ingen ingen2 inlist inscmlist invector))
  (define in-stuff-1   '(instring inbytes inbytearray inrange inbytevector))
  (define in-stuff-2   '(inassoc inassocl indict intable inhash))
  
  (define in-stuff     (append in-stuff-1
			       in-stuff-*))
  
  (define in-stuff2    (append in-stuff-2
			       in-stuff-*))
  
  (define in-stuff-all (append in-stuff-1 in-stuff-2 in-stuff-*)))


(define (simple? x)
  (define (simple0 x)
    (syntax-case x ()
      ((x . l) #f)
      (_ #t)))

  (and-map simple0 x))
  
(define-syntax for-set
  (lambda (x)
    (pp 1 x)
    (syntax-case x (@@)
      ((_ #f a () u . v)
       #'(for-work0 a u . v))
      
      ((_ #t a () u . v)
       #'(for-work-in a u . v))

      ((_ p a ((x ... : P It ((@@ q m) G (#:apply . b))) . w) (u ...)  . v)
       (and (eq?  (syntax->datum #'m) 'ref-x)
	    (simple? #'(x ...))
	    (memq (syntax->datum #'G) in-stuff-all))
       #'(for-set p a w (u ... (x ... : P It (G . b))) . v))

      ((_ p a ((x ... : P It (G . b)) . w) (u ...)  . v)
       (and
	(memq (syntax->datum #'G) in-stuff-all)
	(simple? #'(x ...)))
       
       #'(for-set p a w (u ... (x ... : P It (G . b))) . v))

      ((_ p a ((x ... : P It (@@ q m) H (#:apply G)) . w) (u ...)  . v)
       (and (eq?  (syntax->datum #'m) 'ref-x)
	    (simple? #'(x ...))
	    (memq (syntax->datum #'H) in-stuff-all))      
       #'(for-set #f a w (u ... (x ... : #f It G)) . v))
      
      ((_ p a ((x ... : P It (H G)) . w) (u ...)  . v)
       (memq (syntax->datum #'H) in-stuff-all)
       #'(for-set #f a w (u ... (x ... : #f It G)) . v))
      
      ((_ p a ((x ... : P It G) . w) (u ...)  . v)
       #'(for-set #f a w (u ... (x ... : #f It G)) . v)))))

(define-syntax for-work0
  (lambda (z)
    (pp z)
    (syntax-case z (: @@)
      ((_ a ((x ... : #f It ((@@ q m) G (#:apply . b)))) . v)
       (and (eq?  (syntax->datum #'m) 'ref-x)
	    (memq (syntax->datum #'G) '(range arange)))
       #'(for-work-range ((x ... : #f It (G . b))) . v))
      
      ((_ _ ((x ... : #f It (G . b)))  . v)
       (memq (syntax->datum #'G) '(range arange))
       #'(for-work-range ((x ... : #f It (G . b))) . v))

      ((_ . l)
       #'(for-work00 . l)))))

(define-syntax for-work00
  (lambda (z)
    (pp z)
    (define (wrap-continue lp code)
      (if (syntax->datum lp)
          #`(lambda () (let/ec lp code))
          #`(lambda ()              #,@code)))
    
    (syntax-case z (:)      
      ((for continue ((x ... : P It G) ...) ((c n) ...) (code ...) fin er)
       (with-syntax (((cc     ...) (generate-temporaries #'(c ...)))
                     ((f-next ...) (generate-temporaries #'(It ...)))
		     ((nr     ...) (map length #'((x ...) ...))))
	 #`(let/ec lp-break                                  
	    (let* ((It     (wrap-in G))
		    ...
		    
		    (f-next (resolve-method-g next (list (class-of It) '_)))
		    ...)
	       
	       (syntax-parameterize ((break (lambda (z)
					      (syntax-case z ()
						((_ . l)
						 #'(lp-break . l))
						(_ #'lp-break)))))
                                                     
		 
		 (let ((finale   (lambda (c ...) fin))
		       (cc       None)
		       ...)
                       
		   (catch 'python
		       (lambda ()
			 #,(cond
			    ((pair? #'(c ...))
			     (cond
			      ((pair? (cdr #'(c ...)))
			       #`(let lp ((c n) ...)
				   (set! cc c) ...
				   (o ((f-next It nr) x ...) ...
				      (call-with-values
					  (lambda ()
					    #,(if (syntax->datum #'continue)
						  #'(let/ec continue code ...)
						  #'(begin code ...)))
					(lambda (cc ...)
					  (lp cc ...))))))

			      (else
			       #`(let lp ((c n) ...)
				   (set! cc c) ...
				   (o ((f-next It nr) x ...) ...
				     
				      #,(if (syntax->datum #'continue)
					    #'(let/ec continue
						(lp (begin code ...)))
					    #'(lp (begin code ...))))))))
			   
			    (else
			     #`(let lp ()
				 (o ((f-next It nr) x ...) ...
				    #,(if (syntax->datum #'continue)
					  #'(let/ec continue code ... (lp))
					  #'(begin code ... (lp))))))))
		   

			    
		       (lambda (tag E . u)
			 (if (eq? E StopIteration)
			     (begin (er) (finale cc ...))
			     (apply throw tag E u)))))))))))))

(define-syntax-parameter
  escape (lambda (x) (error "escape in non loop context")))

(define-syntax-rule (stuff code ...)
  (let/ec lp-break
    (syntax-parameterize ((break  (lambda (z)
				    (syntax-case z ()
				      ((_ . l)
				       #'(lp-break . l))
				      (_ #'lp-break)))))
      code ...)))


(define-syntax-rule (stuff2 e f code ...)
  (syntax-parameterize ((escape  (syntax-rules ()
				   ((_)
				    (apply break
					   (begin (e)
						  (call-with-values
						      (lambda () f)
						    (lambda x x))))))))   
					     
    code ...))

(define-syntax lp-it
  (syntax-rules ()
    ((_ (lp a ...) () code ...)
     (begin code ... (lp a ...)))
        
    ((_ (lp a ...) (s ...) code ...)
     (call-with-values (lambda () code ...)
       (lambda (s ...)
	 (lp a ... s ...))))))

(define-syntax for-work-dict
  (lambda (z)
    (pp z)
    (syntax-case z (:)
      ((for ((k v : #f It G)) ((c n) ...) (code ...) fin er)
       (with-syntax (((cc ...) (generate-temporaries #'(c ...))))
         #'(let ((c n) ...)
	      (stis-for-each
	       (lambda (k v)
		 (call-with-values (lambda () (stuff2 fin er code ...))
		   (lambda (cc ...)
		     (set! c cc) ...
		     (values))))
	       G)))))))

(define-syntax-rule (vec-iterator ref vec i n)
  (lambda ()
    (set! i (+ i 1))
    (if (< i n)
	(ref vec i)
	(escape))))

(define-syntax-rule (range-iter It i n d code ...)
  (let ((It (lambda ()
	      (set! i (+ i d))
	      (if (< d 0)
		  (if (> i n)
		      i
		      (escape))
		  (if (< i n)
		      i
		      (escape)))))) code ...))

(define-syntax for-work-range
  (lambda (z)
    (pp z)
    (syntax-case z (:)
      ((for ((x : _ It (ra a))) ((c n) ...) (code ...) fin er)
       (memq (syntax->datum #'ra) '(range arange))
       (begin
	 (add-type-num #'x (get-type-num #'a))
	 (add-type-one #'x #t)
	 #'(let ((aa a))
	     (let lp ((x 0) (c n) ...)
	       (if (< x aa)
		   (lp-it (lp (+ x 1)) (c ...)
			  (stuff2 fin er (range-iter It x aa 1 code ...)))
		   (begin (er) fin))))))

      ((for ((x : _ It (ra a1 a2))) ((c n) ...) (code ...) fin er)
       (memq (syntax->datum #'ra) '(range arange))
       (let* ((aa1 (syntax->datum #'a1))
	      (t1  (get-type-num #'a1)))
	 (add-type-num #'x t1)
	 (add-type-one #'x #t)
	 #'(let ((aa1 a1) (aa2 a2)) 
	      (let lp ((x aa1) (c n) ...)
		(if (< x aa2)
		    (lp-it (lp (+ x 1)) (c ...)
			   (stuff2 fin er
				  (range-iter It x aa2 1 code ...)))
		    (begin (er) fin))))))
      
      ((for ((x : _ It (ra a1 a2 -1))) ((c n) ...) (code ...) fin er)
       (memq (syntax->datum #'ra) '(range arange))
       (let* ((aa1 (syntax->datum #'a1))
	      (t1  (get-type-num #'a1)))
	 (add-type-num #'x t1)
	 (add-type-one #'x #t)
	 #'(let ((aa1 a1) (aa2 a2))
	      (let lp ((x aa1) (c n) ...)
		(if (> x aa2)
		    (lp-it (lp (- x 1)) (c ...)
			   (stuff2 fin er (range-iter It x aa2 -1 code ...)))
		    (begin (er) fin))))))

      ((for ((x : _ It (ra a1 a2 a3))) ((c n) ...) (code ...) fin er)
       (memq (syntax->datum #'ra) '(range arange))
       (let* ((aa1 (syntax->datum #'a1))
	      (aa2 (syntax->datum #'a3))
	      (aa3 (syntax->datum #'a3))
	      (t1  (get-type-num #'a1))
	      (t3  (get-type-num #'a3))
	      (t   (max t1 t3)))
	 
	 (add-type-num #'x t)
	 (add-type-one #'x #t)
	 
	 #`(let* ((aa1 a1) (aa2 a2) (aa3 a3)		   
		  (d       (if (> aa3 0) 1 -1))
		  (aa2     (* d aa2)))
	      (let lp ((x aa1) (c n) ...)	       
		(if (< (if (> d 0) x (- x))  aa2)
		    (lp-it (lp (+ x aa3)) (c ...)
			   (stuff2 fin er (range-iter It x aa2 aa3 code ...)))
		    (begin (er) fin))))))
      
      ((for . l)
       #'(for-work00 . l)))))

(define-syntax-rule (s-string-ref s i)
  (let ((x (make-string 1)))
    (string-set! x 0 (string-ref s i))
    x))
  
(define (false-1 x)
  (syntax-case x (:)
    ((x : #f It (ra a nn))
     (case (syntax->datum #'ra)
       ((inbytevector)
	#'((begin)
	   (let* ((ve  a)
		  (n   (let ((N (bytevector-length ve)))
			 (if nn (min nn N) N)))))
	   (lp  (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x (bytevector-u8-ref ve i))))
	   (begin)	
	   (lp  (+ i 1))))
       (else #f)))

    ((x : #f It (ra a))
     (case (syntax->datum #'ra)
       ((inrange)
	#'((begin)
	   (let* ((aa a)
		  (n1 (ref aa '_a))
		  (n2 (ref aa '_b))
		  (n3 (ref aa '_c))
		  (d  (if (> n3 0) 1 -1))))
	   (lp  (x n1))
	   (let ())
	   (if  (< (* d x) (* d n2)))
	   (let ())
	   (begin)
	   (lp  (+ x n3))))

       ((instring)
	#'((begin)
	   (let* ((s a)
		  (n (string-length s))))
     
	   (lp  (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x (make-string 1))))
	   (begin (string-set! x 0 (string-ref s i)))
	   (lp  (+ i 1))))

       ((invector)
	#'((begin)
	   (let* ((ve  a)
		  (n   (vector-length ve))))
	   (lp  (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x (vector-ref ve i))))
	   (begin)
	   (lp  (+ i 1))))
       
       ((inlist)
	#'((begin)
	   (let* ((aa  a)
		  (vec (slot-ref aa 'vec))
		  (nn  (slot-ref aa 'nn))))
	   (lp  (i 0))
	   (let ())
	   (if  (< i nn))
	   (let ((x (vector-ref vec i))))
	   (begin)
	   (lp  (+ i 1))))

       ((inscmlist)
	#'((begin)
	   (let  ((aa a)))
	   (lp   (xx aa))
	   (let ())
	   (if   (pair? xx))
	   (let  ((x (car xx))))
	   (begin)
	   (lp   (cdr xx))))

       ((ingen2)
	(if stis
	    #'((begin)
	       (let ((aa (slot-ref a 'x))))
	       (lp  (x  (handle-arg (stis-next aa 'query-stis-stack) 1)))
	       (let ())
	       (if  (not (eq? x arg-empty)))
	       (let ())
	       (begin)
	       (lp  (handle-arg (stis-next aa) 1)))
	    
	    (false-1 #'(x : #f It (ingen a)))))

       ((ingen)
	(if stis
	    #'((begin)
	       (let* ((aa (struct-ref a 0))
		      (li (struct-ref a 1))))
	       (lp)
	       (let ((x  (handle-arg (stis-next-l li aa) 1))))
	       (if  (not (eq? x arg-empty)))
	       (let ())
	       (begin)       
	       (lp))
	    
	    #'((begin)
	       (let* ((aa     a)
		      (f-next (resolve-method-g next-sielent
					     (list (class-of aa) '_)))))
	       (lp  (x  (f-next aa 1)))
	       (let ())
	       (if  (not (eq? x arg-empty)))
	       (let ())
	       (begin)       
	       (lp  (f-next aa 1)))))

       (else
	#f)))))
       

(define (true-1 x)
  (syntax-case x (:)
    ((x : #t It (ra a nn))
     (case (syntax->datum #'ra)
       ((inbytevector)
	#'((begin)
	   (let* ((ve  a)
		  (n   (let ((N (bytevector-length ve)))
			 (if nn (min nn N) N)))))
	   (lp (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x  (bytevector-u8-ref ve i))
		 (It (vec-iterator bytevector-u8-ref ve i n))))
	   (begin)
	   (lp)))
       
       (else
	#f)))

    ((x : #t It (ra a))
     (case (syntax->datum #'ra)
       ((instring)
	#'((begin)
	   (let* ((s   a)
		  (n   (string-length s))))
	   (lp (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x  (make-string 1))
		 (It (vec-iterator s-string-ref s i n))))
	   (begin (string-set! x 0 (string-ref s i)))
	   (lp (+ i 1))))

       ((invector)
	#'((begin)
	   (let* ((ve  a)
		  (n   (vector-length ve))))
	   (lp (i 0))
	   (let ())
	   (if  (< i n))
	   (let ((x  (vector-ref ve i))
		 (It (vec-iterator vector-ref ve i n))))
	   (begin)
	   (lp (+ i 1))))
            
       ((inlist)
	#'((begin)
	   (let* ((aa  a)
		  (vec (slot-ref aa 'vec))
		  (nn  (slot-ref aa 'nn))))

	   (lp (i 0))
	   (let ())
	   (if   (< i nn))
	   (let* ((x  (vector-ref vec i))
		  (It (vec-iterator vector-ref vec i nn))))
	   (begin)
	   (lp (+ i 1))))

       ((ingen)
	(if stis
	    #'((begin)
	       (let* ((aa (struct-ref a 0))
		      (li (struct-ref a 1))))
	       (lp)  
	       (let ((x (handle-arg (stis-next-l li aa) 1))))
	       (if  (not (eq? x arg-empty)))
	       (let ((It (lambda ()
			   (let ((xxx (handle-arg (stis-next-l li aa) 1)))
			     (if (eq? xxx arg-empty)
				 (escape)
				 xxx))))))
	       (begin)       
	       (lp))
	    
	    #'((begin)
	       (let ((It  a)))	      
	       (lp  (x (next It 1)))
	       (let ())
	       (if  (not (eq? xx arg-empty)))
	       (let ())
	       (begin)       
	       (lp  (let ((k (struct-ref It 1)))
		      (if k
			  (k (lambda (g)
			       (if g
				   (let ((xxx (next-sielent g)))
				     (if (eq? xxx arg-empty)
					 arg-empty
					 xxx))
				   'None)))
			  arg-empty))))))

       (else
	#f)))))


(define (false-2 z)
  (syntax-case z ()
    ((k v : #f It (ra a))
     (case (syntax->datum #'ra)     
       ((ingen)
	(if stis
	    #'((begin)
	       (let ((aa (struct-ref a 0))
		     (li (struct-ref a 1))))
	       (lp)  
	       (let ((xx (stis-next-l li aa))))
	       (if  (not (empty-arg? xx)))
	       (let-values (((k v) (handle-arg xx 2))))
	       (begin)
	       (lp))
	    
	    #'((begin)
	       (let* ((aa     a)
		      (f-next (resolve-method-g next-sielent
					     (list (class-of aa) '_)))))
	       (lp)
	       (let-values (((k v) (f-next aa 2))))
	       (if  (not (eq? xx arg-empty)))
	       (let ())
	       (begin)       
	       (lp))))

       ((indict)
	#'((expand-a (a : ar n))
	   (let ())
	   (lp  (i 0))
	   (let ((xx (let lp2 ()
		       (if (< i n)
			   (let ((xx (vector-ref ar i)))
			     (if (empty-hash-slot? xx)
				 (begin
				   (set! i (+ i 1))
				   (lp2))
				 xx))
			   #f)))))
	   (if  xx)
	   (let* ((k  (car xx))
		  (v  (cdr xx))))
	   (begin)
	   (lp  (+ i 1))))

       ((intable)
	#'((begin)
	   (let ((l (hash-fold (lambda (k v s) (cons (cons k v) s)) '() a))))
	   (lp  (l l))
	   (let ())
	   (if (pair? l))
	   (let* ((xx (car l ))
		  (k  (car xx))
		  (v  (cdr xx))))
	   (begin)
	   (lp  (cdr l))))
		  

       ((inassoc)
	#'((begin)
	   (let ())
	   (lp  (aa a))
	   (let ())
	   (if  (pair? aa))
	   (let* ((p (car aa))
		  (k (car p))
		  (v (cdr p))))
	   (begin)
	   (lp (cdr aa))))

       ((inassocl)
	#'((begin)
	   (let ())
	   (lp  (aa a))
	   (let ())
	   (if  (pair? aa))
	   (let* ((p (car aa))
		  (k (car p))
		  (v (cadr p))))
	   (begin)
	   (lp (cdr aa))))

       (else
	#f)))))

(define (true-2 z) #f)

(define (f-in z)
  (syntax-case z (:)
    ((x : P It (ra a))
     (eq? (syntax->datum #'ra) 'inbytevector)
     (f-in #'(x : P It (ra a #f))))

    ((x : P It (ra a))
     (eq? (syntax->datum #'ra) 'inbytes)
     (with-syntax ((bv (datum->syntax #'a 'abv)))
       (with-syntax (((b (let (l ...)) . u)
		      (f-in #'(x : P It (inbytevector bv #f)))))
	 #'(b
	    (let* ((aa a)
		   (bv (slot-ref aa 'bytes))
		   l ...))
	    . u))))

    ((x : P It (ra a))
     (eq? (syntax->datum #'ra) 'inbytearray)
     (with-syntax ((bv (datum->syntax #'a 'abv)))
       (with-syntax (((b (let (l ...)) . u)
		      (f-in #'(x : P It (inbytevector bv nn)))))

	 #'(b
	    (let* ((aa a)
		   (bv (slot-ref aa 'vec))
		   (nn (slot-ref aa 'nn))
		   l ...))
	    . u))))
   
    ((x : #f It (ra a . l))
     (false-1 z))

    ((x : #t It (ra a . l))
     (true-1 z))

    ((k v : #f It (ra . a))
     (false-2 z))

    ((k v : #t It (ra . a))
     (true-2 z))

    (_
     #f)))


(define-syntax letter
  (syntax-rules ()
    ((_ () code ...)
     (begin code ...))
    ((_ ((lett a) . l) . c)
     (lett a (letter l . c)))))

(define-syntax cup
  (syntax-rules ()
    ((_ () code ...)
     (begin code ...))
    ((_ ((b ...)) code ...)
     (b ... code ...))
    ((_ ((b ...) . l) . c)
     (b ... (cup l . c)))))

(define-syntax for-work-in
  (lambda (z)
    (pp z)
    (syntax-case z (:)
      ((for #f ((x ... : a b c)  ...) ((s v) ...) (code ...) fin er)
       (let ((anal (map f-in #'((x ... : a b c) ...))))
	 (if (and-map (lambda (x) x) anal)
	     (with-syntax (((((beg    co ...)
			      (let1   ((a1 v1) ...))
			      (lp1    (a2 v2) ...)
			      (let2   ((a3 v3) ...))
			      (if1    p)			
			      (let3   ((a4 v4) ...))
			      (begin1 codex ...)
			      (lp2   a5 ...))
			     ...) anal))
		      
	       
	       (pp #'(stuff
		     (cup ((beg co ...) ...)
		       (letter ((let1 ((a1 v1) ...)) ...)
		         (let lp ((a2 v2) ... ... (s v) ...)
			  (letter ((let2 ((a3 v3) ...)) ...)
			    (if (and p ...)
				(stuff2 er fin
				  (letter ((let3 ((a4 v4) ...)) ...)
				    codex ... ...
				    (lp-it (lp a5 ... ...) (s ...)
					   code ...)))
				(begin (er) fin)))))))))
	     #f)))

      ((for a ((x ... : P It (ra G . _)) ...) . l)
       #'(for-work00 a ((x ... : P It G) ...) . l)))))


(define licls  #f)
(define btcls  #f)
(define btacls #f)
(define rncls  #f)
(define dicls  <HH>)
(define gncls  <stis-iter>)
(define gncls2 <yield>)
;;; Expensive but fast!

(define-syntax for-in
  (lambda (x)
    (pp x)
    (syntax-case x ()
      ((_ ((x : (G . u))) s . l)
       (memq (syntax->datum #'G) '(range arange))
       #'(for ((x : (G . u))) s . l))

      ((_ ((x ... : (G . u))) s . l)
       (memq (syntax->datum #'G) in-stuff-all)
       #'(for ((x ... : (G . u))) s . l))

      ((_ ((x : ((@@ a b) G . u))) s . l)
       (and
	(eq? (syntax->datum #'b) 'ref-x)
	(memq (syntax->datum #'G) '(range arange)))
       #'(for ((x : ((@@ a b) G . u))) s . l))

      ((_ ((x ... : ((@@ a b) G . u))) s . l)
       (and
	(eq? (syntax->datum #'b) 'ref-x)
	(memq (syntax->datum #'G) '(range arange)))
       #'(for ((x ... : ((@@ a b) G . u))) s . l))

      ((_ ((k v : G)) s . l)
       #'(let ((GG G))
	   (define (f)
	     (for ((k v : GG)) s . l))
	 
	   (cond
	    ((struct? GG)
	     (let ((cls (class-of GG)))
	       (cond
		((eq? cls dicls)
		 (for ((k v : (indict GG))) s . l))
		
		((eq? cls gncls)
		 (for ((k v : (ingen GG))) s . l))

		(else
		 (f)))))
		      
	    ((pair? GG)
	     (if (eq? #:stis (car GG))
		 (for ((k v : (ingen GG))) s . l)
		 (if (not pair? (cdr GG))
		     (for ((k v : (inassoc  GG))) s . l)
		     (for ((k v : (inassocl GG))) s . l))))

	    ((hash-table? GG)
	     (for ((k v : (intable GG))) s . l))

	    (else
	     (f)))))

	  
	 
      ((_ ((x : G)) s . l)
       #'(let ((GG G))
	   (define (f)
	     (for ((x : GG)) s . l))
	   
	   (cond
	    ((struct? GG)
	     (let ((cls (class-of GG)))
	       (cond
		((eq? cls licls)
		 (for ((x : (inlist GG))) s . l))
						
		((eq? cls gncls)
		 (for ((x : (ingen GG))) s . l))

		((eq? cls gncls2)
		 (for ((x : (ingen GG))) s . l))

		((eq? cls btcls)
		 (for ((x : (inbytes GG))) s . l))

		((eq? cls rncls)
		 (for ((x : (inrange GG))) s . l))

		((eq? cls btacls)
		 (for ((x : (inbytearray GG))) s . l))		    

		(else
		 (f)))))

	    ((pair? GG)
	     (if (eq? (car GG) #:stis)
		 (for ((x :            GG)) s . l)
		 (for ((x : (inscmlist GG))) s . l)))

	    ((vector? GG)
	     (for ((x : (invector GG))) s . l))
	    
	    ((string? GG)
	     (for ((x : (instring GG))) s . l))
     
	    ((bytevector? GG)
	     (for ((x  : (inbytevector GG))) s . l))

	    ((null? GG)
	     (for ((x : (inscmlist GG))) s . l))
	    
	    (else
	     (f)))))

      ((_ . l)
       #'(for . l)))))

(define-syntax for-in-py
  (lambda (x)
    (pp x)
    (syntax-case x ()
      ((_ ((x : (G . u))) s . l)
       (memq (syntax->datum #'G) '(range arange))
       #'(for ((x : (G . u))) s . l))

      ((_ ((x ... : (G . u))) s . l)
       (memq (syntax->datum #'G) in-stuff-all)
       #'(for ((x ... : (G . u))) s . l))

      ((_ ((x : ((@@ a b) G . u))) s . l)
       (and
	(eq? (syntax->datum #'b) 'ref-x)
	(memq (syntax->datum #'G) '(range arange)))
       #'(for ((x : ((@@ a b) G . u))) s . l))

      ((_ ((x ... : ((@@ a b) G . u))) s . l)
       (and
	(eq? (syntax->datum #'b) 'ref-x)
	(memq (syntax->datum #'G) '(range arange)))
       #'(for ((x ... : ((@@ a b) G . u))) s . l))

      ((_ ((k v : G)) s . l)
       #'(let ((GG G))
	   (define (f)
	     (for ((k v : GG)) s . l))
	 
	   (cond
	    ((struct? GG)
	     (let ((cls (class-of GG)))
	       (cond
		((eq? cls dicls)
		 (for ((k v : (indict GG))) s . l))
		
		((eq? cls gncls)
		 (for ((k v : (ingen GG))) s . l))

		(else
		 (f)))))

	    (else
	     (f)))))

	  
	 
      ((_ ((x : G)) s . l)
       #'(let ((GG G))
	   (define (f)
	     (for ((x : GG)) s . l))
	   
	   (cond
	    ((struct? GG)
	     (let ((cls (class-of GG)))
	       (cond
		((eq? cls licls)
		 (for ((x : (inlist GG))) s . l))
						
		((or (eq? cls gncls) (eq? cls gncls2))
		 (for ((x : (ingen GG))) s . l))
		
		(else
		 (f)))))
	    
	    (else
	     (f)))))

      ((_ . l)
       #'(for . l)))))

(define-method (wrap-in x)
  (cond
   ((pair? x)
    (if (eq? (car x) #:stis)
	(cdr x)
	(let ((oo (make <scm-list>)))
	  (slot-set! oo 'l x)
	  oo)))

   
   ((null? x)
    (let ((o (make <scm-list>)))
      (slot-set! o 'l x)
      o))
   
   ((string? x)
    (let ((o (make <scm-string>)))
      (slot-set! o 's x)
      (slot-set! o 'i 0)
      o))
   
   (else
    (throw 'python
           TypeError
           (TypeError (format #f"'~a' is not iterable"
                              (ref (type x) '__name__)))
           #f))))

(name-object wrap-in)

(define-syntax-rule (empty->stop code ...)
  (call-with-values (lambda () (begin code ...))
    (case-lambda
     (()
      (values))
     ((ret . l)
      (if (eq? ret arg-empty)
	  (throw 'python StopIteration (StopIteration) #f)
	  (apply values ret l))))))

(define-syntax define-next
  (lambda (x)
    (syntax-case x ()
      ((_ arg code ...)
       (with-syntax ((name1 (datum->syntax x 'next-sielent))
                     (name2 (datum->syntax x 'next)))         
         #'(begin
             (define-method (name1 . arg) code ...)
             (define-method (name2 . arg) (empty->stop code ...))))))))

(define-next (x n) (handle-arg arg-empty))
(define-method (next x) (next x 1))
(define-next ((l <procedure>) n) (l))

(name-object next)

(define-class <scm-list>   () l)
(define-class <scm-string> () s i)

(name-object <scm-list>)
(name-object <scm-string>)

(cpit <scm-list> (o (lambda (o l) (slot-set! o 'l l))
		    (list (slot-ref o 'l))))
(cpit <scm-string> (o (lambda (o s i)
			(slot-set! o 's s)
			(slot-set! o 'i i))
		      (list
		       (slot-ref o 's)
		       (slot-ref o 'i))))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python number)
  #:use-module (language python type)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module ((oop dict) #:select (classtranslator py-mod-))
  #:use-module (rnrs bytevectors)
  #:use-module (language python hash)
  #:use-module (language python list)
  #:use-module (language python try)
  #:use-module (language python for)
  #:use-module (language python def)
  #:use-module (language python exceptions)
  #:use-module (language python bytes)
  #:use-module (language python persist)
  #:export (py-int py-float py-complex
                   py-/ py-logand py-logior py-logxor py-abs py-trunc
                   py-lshift py-rshift py-mod py-floordiv py-round py-iadd
		   py-lognot py-matmul
                   <py-int> <py-float> <py-complex> 
                   py-divmod pyfloat-listing pyint-listing pycomplex-listing
                   py-as-integer-ratio py-conjugate py-imag
                   py-is-integer py-real hex py-bin py-index
		   py-ifloordiv py-ilshift py-imod py-imul py-imatmul
		   py-bit-length py-to-bytes
		   py-ilogior py-ilogand py-ipow py-isub py-i/
		   py-irshift py-ilogxor py-rmatmul
                   -= /= //= &= ^/ **= <<= >>= or= %= @=
                   py-= py/= py//= py&= py^= py**= py<<=
                   py>>= py-or= py%= py@=
                   fast/ fast% fast& fast-or fast^ fast// fast~ fast<< fast>>
                   fast+= fast-= fast*= fast/= fast%= fast&=
                   fast-or= fast^= fast**= fast<<= fast>>= fast//=))

(define-method (< x y)
  (backtrace)
  (raise (ValueError (format #f "can't evaluate (< ~a ~a)" x y))))
(define-method (> x y)
  (raise (ValueError (format #f "can't evaluate (> ~a ~a)" x y))))
(define-method (<= x y)
  (raise (ValueError (format #f "can't evaluate (<= ~a ~a)" x y))))
(define-method (>= x y)
  (raise (ValueError (format #f "can't evaluate (>= ~a ~a)" x y))))

(define-method (> (o <boolean>) x)
  (> (if o 1 0) x))
(define-method (> x (o <boolean>))
  (> x (if o 1 0)))
(define-method (>= (o <boolean>) x)
  (>= (if o 1 0) x))
(define-method (>= x (o <boolean>))
  (>= x (if o 1 0)))
(define-method (< (o <boolean>) x)
  (< (if o 1 0) x))
(define-method (< x (o <boolean>))
  (< x (if o 1 0)))
(define-method (<= (o <boolean>) x)
  (<= (if o 1 0) x))
(define-method (<= x (o <boolean>))
  (<= x (if o 1 0)))
(name-object <boolean>)

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-syntax-rule (mki py-iadd __iadd__)
  (define (py-iadd x y)
    ((ref x '__iadd__) y)))

(mki py-iadd __iadd__)
(mki py-ifloordiv __ifloordiv__)
(mki py-ilshift   __ilshift__)
(mki py-imod      __imod__)
(mki py-imul      __imul__)
(mki py-imatmul   __imatmul__)
(mki py-ilogior   __ior__)
(mki py-ilogand   __iand__)
(mki py-ipow      __ipow__)
(mki py-isub      __isub__)
(mki py-irshift   __irshift__)
(mki py-ilogxor   __ixor__)
(mki py-i/        __itruediv__)


(define-class <py-int>     () x)
(define-class <py-float>   () x)
(define-class <py-complex> () x)

(name-object <py-int>)
(name-object <py-float>)
(name-object <py-complex>)

(define-syntax-rule (mk <py-int>)
  (cpit <py-int> (o (lambda (o x) (slot-set! o 'x x)) (list (slot-ref o 'x)))))

(mk <py-int>)
(mk <py-float>)
(mk <py-complex>)

(define-syntax-rule (b0 op)
  (begin
    (define-method (op (o1 <py-int>) o2)
      (op (slot-ref o1 'x) o2))    
    (define-method (op (o1 <py-float>) o2)
      (op (slot-ref o1 'x) o2))
    (define-method (op (o1 <py-complex>) o2)
      (op (slot-ref o1 'x) o2))
    (define-method (op o2 (o1 <py-int>))
      (op o2 (slot-ref o1 'x)))    
    (define-method (op o2 (o1 <py-complex>))
      (op o2 (slot-ref o1 'x)))
    (define-method (op o2 (o1 <py-float>))
      (op o2 (slot-ref o1 'x)))))

(define-syntax-rule (mk-biop1 mk-biop0 op r1)
  (begin
    (mk-biop0 op)
    (define-method (op (o <py>) v)
      (aif it (ref-in-class o 'r1)
           (it o v)
           (next-method)))))

(define-syntax-rule (mk-biop2 mk-biop0 rop op r1 r2)
  (begin
    (define-syntax-rule (rop x y) (op y x))
    (mk-biop1 mk-biop0 op r1)
    (define-method (op v (o <py>))
      (aif it (ref-in-class o 'r2)
           (it o v)
           (next-method)))))

(define-syntax-rule (i0 op)
  (begin
    (define-method (op (o1 <py-int>) o2)
      (op (slot-ref o1 'x) o2))    
    (define-method (op o2 (o1 <py-int>))
      (op o2 (slot-ref o1 'x)))))

      
(mk-biop2 b0 r+ + __add__ __radd__)
(mk-biop2 b0 r- - __sub__ __rsub__)
(mk-biop2 b0 r* * __mul__ __rmul__)

(define-method (- (n <integer>) (o <py>))
  (if (= n 0)
      (aif it (ref-in-class o '__neg__)
           (it o)
           (aif it (ref-in-class o '__rsub__)
                (it o n)
                (next-method)))
      (aif it (ref-in-class o '__rsub__)
           (it o n)
           (next-method))))

(mk-biop1 b0 <  __le__)
(mk-biop1 b0 >  __ge__)
(mk-biop1 b0 <= __lt__)
(mk-biop1 b0 >= __gt__)
(mk-biop2 b0 rexpt expt __pow__ __rpow__)
(b0 py-equal?)


(define-method (py-lshift (o1 <integer>) (o2 <integer>))
  (ash o1 o2))
(define-method (py-rshift (o1 <integer>) (o2 <integer>))
  (ash o1 (- o2)))
(name-object py-lshift)
(name-object py-rshift)

(mk-biop2 i0 py-rlshift py-lshift __lshift__ __rlshift__)
(mk-biop2 i0 py-rrshift py-rshift __rshift__ __rrshift__)

(define-method (py-logand (o1 <integer>) (o2 <integer>))
  (logand o1 o2))
(name-object py-logand)

(define-method (py-logior (o1 <integer>) (o2 <integer>))
  (logior o1 o2))
(name-object py-logior)

(define-method (py-logxor (o1 <integer>) (o2 <integer>))
  (logxor o1 o2))
(name-object py-logxor)

(define-method (py-lognot (o1 <integer>))
  (lognot o1))
(name-object py-lognot)

(define-method (py-logand o1 (o2 <py-int>))
  (py-logand o1 (slot-ref o2 'x)))

(define-method (py-logand (o1 <py-int>) o2)
  (py-logand (slot-ref o1 'x) o2))

(define-method (py-logior o1 (o2 <py-int>))
  (py-logior o1 (slot-ref o2 'x)))

(define-method (py-logior (o1 <py-int>) o2)
  (py-logior (slot-ref o1 'x) o2))

(define-method (py-logxor o1 (o2 <py-int>))
  (py-logxor o1 (slot-ref o2 'x)))

(define-method (py-logxor (o1 <py-int>) o2)
  (py-logxor (slot-ref o1 'x) o2))

(define-method (py-lognot (o1 <py-int>))
  (lognot (slot-ref o1 'x)))

(define-method (py-logand (o1 <py>) o2)
  (aif it (ref-in-class o1 '__and__)
       (it o1 o2)
       (next-method)))

(define-method (py-logand o1 (o2 <py>))
  (aif it (ref-in-class o1 '__rand__)
       (it o1 o2)
       (next-method)))

(define-method (py-logior (o1 <py>) o2)
  (aif it (ref-in-class o1 '__or__)
       (it o1 o2)
       (next-method)))

(define-method (py-logior o1 (o2 <py>))
  (aif it (ref-in-class o2 '__ror__)
       (it o2 o1)
       (next-method)))

(define-method (py-logxor (o1 <py>) o2)
  (aif it (ref-in-class o1 '__xor__)
       (it o1 o2)
       (next-method)))

(define-method (py-logxor o1 (o2 <py>))
  (aif it (ref-in-class o2 '__rxor__)
       (it o2 o1)
       (next-method)))

(define-method (py-lognot (o1 <py>))
  (aif it (ref-in-class o1 '__not__)
       (it o1)
       (next-method)))


(define-method (py-/ (o1 <number>) (o2 <integer>))
  (when (= o2 0)
    (raise (ZeroDivisionError "division by zero")))
  (/ o1 (exact->inexact o2)))

(define-method (py-/ (o1 <number>) (o2 <number>))
  (when (= o2 0)
    (raise (ZeroDivisionError "division by zero")))
  (/ o1 o2))

(name-object py-/)

(define-method (py-divmod (o1 <integer>) (o2 <integer>))
  (values
   (floor-quotient o1 o2)
   (modulo o1 o2)))
(name-object py-divmod)

(define-method (py-divmod (o1 <number>) (o2 <number>))
  (values
   (floor-quotient o1 o2)
   (floor-remainder o1 o2)))

(define-method (py-floordiv (o1 <number>) (o2 <number>))
  (floor-quotient o1 o2))
(name-object py-floordiv)

(define-method (py-matmul (o1 <number>) (o2 <number>))
  (* o1 o2))

(mk-biop2 b0 py-rfloordiv py-floordiv __floordiv__ __rfloordiv__)
(mk-biop2 b0 py-rdivmod py-divmod __divmod__  __rdivmod__)
(mk-biop2 b0 py-r/      py-/      __truediv__ __rtruediv__)
(mk-biop2 b0 py-rmatmul py-matmul __matmul__  __rmatmul__)

(mk-biop2 i0 py-rlogand py-logand __and__ __rand__)
(mk-biop2 i0 py-rlogior py-logior __or__  __ror__)
(mk-biop2 i0 py-rlogxor py-logxor __xor__ __rxor__)

(define-method (py-mod (o1 <integer>) (o2 <integer>))
  (modulo o1 o2))
(define-method (py-mod (o1 <real>)    (o2 <real>))
  (floor-remainder o1 o2))
(name-object py-mod)

(mk-biop2 i0 py-rmod py-mod __mod__ __rmod__)


(define-method (py-floor (o1 <integer>)) o1)
(define-method (py-floor (o1 <number> )) (inexact->exact (floor o1)))
(define-method (py-trunc (o1 <integer>)) (exact->inexact o1))
(define-method (py-trunc (o1 <number> ))
  (floor o1))
(name-object py-floor)

(define-syntax-rule (u0 f)
  (begin
    (define-method (f (o <py-int>  ))   (f (slot-ref o 'x)))
    (name-object f)
    (define-method (f (o <py-float>))   (f (slot-ref o 'x)))
    (define-method (f (o <py-complex>)) (f (slot-ref o 'x)))))

(define-syntax-rule (i0 f)
  (begin
    (define-method (f (o <py-int>  )) (f (slot-ref o 'x)))
    (name-object f)))

(define-syntax-rule (mk-unop u0 f r)
  (begin
    (u0 f)
    (define-method (f (o <py>))
      (aif it (ref-in-class o 'r)
           (it o)
           (next-method)))
    (name-object f)))

(define-syntax-rule (mk-unop0 u0 f r)
  (begin
    (u0 f)
    (name-object f)))

(u0 py-hash )
(mk-unop u0 -         __neg__   )
(mk-unop u0 py-trunc  __trunc__ )
(mk-unop i0 py-lognot __invert__)

(define-syntax-rule (fast~ x)
  (let ((xx x))
    (if (number? xx)
        (lognot xx)
        (py-lognot xx))))

(define-method (py-bit-length (i <integer>))
  (integer-length (abs i)))
(name-object py-bit-length)

(define-method (py-conjugate (i <complex>))
  (make-rectangular (real-part i) (- (imag-part i))))
(define-method (py-conjugate (i <number>)) i)
(name-object py-conjugate)

(define-method (py-imag (i <complex>)) (imag-part i))
(define-method (py-imag (i <number>)) i)
(name-object py-imag)

(define-method (py-real (i <complex>)) (real-part i))
(define-method (py-real (i <number>)) i)
(name-object py-real)

(define-method (py-denominator (o <integer>)) 0)
(define-method (py-denominator (o <real>))
  (denominator (inexact->exact o)))
(name-object py-denominator)

(define-method (py-numerator (o <integer>)) o)
(define-method (py-numerator (o <real>   ))
  (numerator (inexact->exact o)))
(name-object py-numerator)

(define-method (py-as-integer-ratio (o <integer>))
  (list o 0))
(define-method (py-as-integer-ratio (o <real>))
  (let ((r (inexact->exact o)))
    (list (numerator r) (denominator r))))
(name-object py-as-integer-ratio)

(define-method (py-fromhex     (o <real>) (s <string>))
  (fromhex- s))
(define-method (py-fromhex    (s <string>))
  (fromhex- s))

(name-object py-fromhex)

(define (hex x)
  (hex- x))
                               
(define-method (py-is-integer  (o <real>))
  (= 1 (denominator (inexact->exact o))))
(define-method (py-is-integer  (o <integer>)) #t)
(name-object py-is-integer)

(define-method (py-hex (o <real>))
  (hex- o))
(define-method (py-hex (o <integer>))
  (+ "0x" (number->string o 16)))
(name-object py-hex)

(define-method (py-abs (o <complex>))
  (magnitude o))
(define-method (py-abs (o <number>))
  (abs o))
(name-object py-abs)

(define-method (py-index  (o <integer>)) o)
(name-object py-index)

(mk-unop u0 py-abs       __abs__)
(mk-unop u0 py-conjugate conjugate)
(mk-unop u0 py-imag imag)
(mk-unop u0 py-real real)
(mk-unop u0 py-denominator denominator)
(mk-unop u0 py-numerator   numerator)
(mk-unop u0 py-as-integer-ratio as_integer_ratio)
(mk-unop0 u0 py-fromhex fromhex)
(mk-unop0 i0 py-hex __hex__)
(mk-unop u0 py-is-integer is_integer)
(mk-unop u0 py-index __index__)

(define-method (write (o <py-float>) . l)
  (apply write (slot-ref o 'x) l))
(define-method (write (o <py-int>) . l)
  (apply write (slot-ref o 'x) l))

(define-method (py-from-bytes (o <py>) . l)
  (aif it (ref-in-class o 'from_bytes)
       (apply it o l)
       (next-method)))
(name-object py-from-bytes)

(define-method (py-from-bytes (o <integer>) . l)
  (apply py-from-bytes int l))

(define-method (py-to-bytes (o <py>) . l)
  (aif it (ref-in-class o 'to_bytes)
       (apply it o l)
       (aif it (ref-in-class o '__int__)
            (apply py-to-bytes (it o) l)
            (next-method))))
(name-object py-to-bytes)

(define-method (py-to-bytes (o <integer>) . l)
  (apply
   (lam (length (= byteorder "big") (= signed #f))
        (let ((big? (cond
                     ((equal? byteorder "little")
                      #f)
                     ((equal? byteorder "big")
                      #t)
                     (else
                      (raise (ValueError "to_bytes with wrong byteorder"))))))
          
          (if (and (< o 0) (not signed))
              (raise (OverflowError
                      "to_byted, integer negative but not signed")))
          
          (if signed
              (let ((mask (ash 1 (- (* 8 length) 1))))
                (set! o (+ mask o))))
          
          (let lp ((o o) (l '()))
            (if (= o 0)
                (let ((n (len l)))
                  (if (> n length)
                      (raise (OverflowError
                              "to bytes number larger than size")))
                  (let lp ((i (len l)) (l l))
                    (if (< i length)
                        (lp (+ i 1) (cons 0 l))
                        (begin
                          (if signed
                              (let ((x (car l)))
                                (if (> (logand x #x80) 0)
                                    (raise
                                     "OverflowError to large number compared to size in to_bytes"))
                                (set-car! l (logior #x80 x))))
                          
                          (bytes
                            (if big?
                                l
                                (reverse l)))))))
                (lp (ash o -8) (cons (logand o #xff) l))))))
   l))

                        
        
  

(define-syntax-rule (mkint int p?)
(define-python-class int (object <py-int>)
  (define from_bytes
    (class-method
     (lam (self bytes byteorder (= signed #f))
	  (for ((x : bytes)) ((l '()))
	       (cons
		(let ((i (if (and (number? x) (integer? x))
			     x
			     (list-ref (bv-scm x) 0))))
		  (if (not (and (number? i) (integer? i)
				(>= i 0) (<= i 356)))
		      (raise (ValueError "wrong bytevector in from_bytes"))
		      i))
		l)
	       #:final
	       (begin
		 (if (equal? byteorder "little")
		     (set! l (reverse l)))
		 (let lp ((s 0) (i 0) (l l))
		   (if (pair? l)
		       (let ((x (car l)))
			 (if (null? (cdr l))
			     (if (and signed (not (= 0 (logand x #x80))))
				 (set! x (logand x #x7f))
				 (set! signed #f)))
			 (lp (logior s (ash x i)) (+ i 8) (cdr l)))
		       (if signed
			   (let ((mask (ash 1 (- i 1))))
			     (- s mask))
			   s))))))))
  (define __newobj__
    (lambda (cls n)
      (let ((obj ((rawref object '__new__) cls)))
        (slot-set! obj 'x (__new__ cls n))
        obj)))
       
  (define __new__
    (lambda x
      (define (lp x)
        (apply
         (case-lambda
           ((self)
            0)
           
           ((self n)
            (let lp2 ((n n))
              (cond
               ((and (number? n) (integer? n))
                (inexact->exact n))
               ((boolean? n)
                (if n 1 0))
               ((number? n)
                (lp2 (py-floor n)))
               
               ((string? n)
                (lp2 (aif it (string->number n)
                         it
                         (raise
                          (ValueError
                           "invalid literal for int() with base 10")))))
               (else
                (catch #t
                  (lambda ()
                    (aif it (ref n '__int__)
                         (lp2 (it))
                         (raise (ValueError
                                 (py-mod "could not make int from %r"
                                         (list n))))))
                  (lambda z
                    (raise
                     (ValueError (py-mod "could not make int from %r"
                                         (list n))))))))))
      
           ((self n k)
            (if k
                (lp (list self (string->number n k)))
                (let ((res (onew self)))
                  (slot-set! res 'x (lp (list self n)))
                  res)))

           ((self n base k)
            (when (not (eq? k #:base))
              (raise (ValueError "int keword is not 'base'")))
            
            (lp (list n k))))
          
         x))

      (if p?
          (apply
           (case-lambda
             ((self)
              (lp (list self 0 #f)))
             ((self x)
              (lp (list self x #f)))
             ((self x n)
              (if n
                  (lp (list self (lp (list self x n)) #f))
                  (lp (list self x n)))))
           x)
          (lp x))))))

(mkint int  #f)
(mkint int- #t)
(hashq-set! classtranslator int int-)

(name-object int)
(name-object int-)

(define (proj? x)
  (if (number? x)
      x
      (and
       (or (is-a? x <py-complex>)
           (is-a? x <py-int>)
           (is-a? x <py-float>))
       (slot-ref x 'x))))

(define (projc? x)
  (if (number? x)
      (cond
       ((or (integer? x) (rational? x))
        (exact->inexact x))
       ((real? x)
        x)
       (raise (ValueError "cannot make a float out of a complex")))
      (and
       (or (is-a? x <py-complex>)
           (is-a? x <py-int>)
           (is-a? x <py-float>))
       (let ((ret (slot-ref x 'x)))
         (if (not (complex? ret))
             ret
             #f)))))

(define onew (rawref object '__new__))

(define-syntax-rule (mk-float2 float p?)
(define-python-class float (object <py-float>)
  (define hex
    (lambda (s) (hex- (slot-ref s 'x))))
  (define fromhex
    (static-method
     (lambda (x) (fromhex- x))))
  (define __new__
    (lambda (cls a . l)
      (define __init__
        (lambda (n)
          (let lp ((n n))
            (cond
             ((projc? n) =>
              (lambda (x) x))
             ((string? n)
              (cond
               ((equal? n "nan")
                (nan))
               ((equal? n "inf")
                (inf))
               ((equal? n "-inf")
                (- (inf)))            
               (else
                (string->number n))))
             ((is-a? n <py-float>)
              (slot-ref n 'x))))))

      (if p?
          (let ((res (onew cls)))
            (slot-set! res 'x (__init__ a))
            res)          
          (if (pair? l)
              (let ((res (onew cls)))
                (slot-set! res 'x (__init__ a))
                res)
              (__init__ a)))))))


(mk-float2 float  #f)
(mk-float2 float- #t)
(hashq-set! classtranslator float float-)

(name-object float)

(define-syntax-rule (mk-complex py-complex p?)
(define-python-class py-complex (object <py-complex>)
  (define __new__
    (lambda x
      (define __init__
        (case-lambda
          ((self n)
           (cond
            ((proj? n) =>
             (lambda (n)
               n))
            (else
             (raise ValueError "could not make complex from 1" n))))
          ((self n m)
           (cond
            ((projc? n) =>
             (lambda (n)        
               (cond
                ((projc? m) =>
                 (lambda (m)
                   (make-rectangular n m)))
                (#t
                 (raise ValueError "could not make complex from 2" n m)))))
            (#t
             (raise ValueError "could not make complex from 3" n m))))))

      (if p?
          (apply
           (case-lambda
             ((cls n)
              (__init__ cls n #f))
             ((cls n m)          
              (let ((ret (onew cls)))
                (slot-set! ret 'x (__init__ cls n m))
                ret)))
           x)
          (apply
           (case-lambda
             ((cls n)
              (__init__ cls n))
             ((cls n m)
              (if m
                  (__init__ cls n m)
                  (let ((ret (onew cls)))
                    (slot-set! ret 'x (__init__ cls n))
                    ret))))
           x))))))

(mk-complex py-complex  #f)
(mk-complex py-complex- #t)
(hashq-set! classtranslator py-complex py-complex-)
(name-object py-complex)

(define-method (py-class (o <integer>    )) int)
(define-method (py-class (o <real>       )) float)
(u0 py-class)
           
(define py-int   int)
(define py-float float)

(define-method (mk-int   (o <boolean>))
  (if o 1 0))

(define-method (mk-int   (o <number>)) (slot-ref (py-int o)   'x))
(define-method (mk-float (o <number>)) (slot-ref (py-float o) 'x))

(mk-unop u0 mk-int   __int__)
(mk-unop u0 mk-float __float__)

(define (pyint-listing)
  (let ((l
         (to-pylist
          (map symbol->string
               '(__abs__ __add__ __and__ __class__ __cmp__ __coerce__
                         __delattr__ __div__ __divmod__ __doc__ __float__
                         __floordiv__ __format__ __getattribute__
                         __getnewargs__ __hash__ __hex__ __index__ __init__
                         __int__ __invert__ __long__ __lshift__ __mod__
                         __mul__ __neg__ __new__ __nonzero__ __oct__ __or__
                         __pos__ __pow__ __radd__ __rand__ __rdiv__
                         __rdivmod__ __reduce__ __reduce_ex__ __repr__
                         __rfloordiv__ __rlshift__ __rmod__ __rmul__ __ror__
                         __rpow__ __rrshift__ __rshift__ __rsub__ __rtruediv__
                         __rxor__ __setattr__ __sizeof__ __str__ __sub__
                         __subclasshook__ __truediv__ __trunc__ __xor__
                         bit_length conjugate denominator imag numerator from_bytes to_bytes
                         real)))))
    (pylist-sort! l)
    l))

(define (pyfloat-listing)
  (let ((l
         (to-pylist
          (map symbol->string
               '(__abs__ __add__ __class__ __coerce__ __delattr__ __div__
                         __divmod__ __doc__ __eq__ __float__ __floordiv__
                         __format__ __ge__ __getattribute__ __getformat__
                         __getnewargs__ __gt__ __hash__ __init__ __int__
                         __le__ __long__ __lt__ __mod__ __mul__ __ne__
                         __neg__ __new__ __nonzero__ __pos__ __pow__
                         __radd__ __rdiv__ __rdivmod__ __reduce__
                         __reduce_ex__ __repr__ __rfloordiv__ __rmod__
                         __rmul__ __rpow__ __rsub__ __rtruediv__
                         __setattr__ __setformat__ __sizeof__ __str__
                         __sub__ __subclasshook__ __truediv__ __trunc__
                         as_integer_ratio conjugate fromhex hex imag
                         is_integer real)))))
    (pylist-sort! l)
    l))

(define (pycomplex-listing)
  (let ((l
         (to-pylist
          (map symbol->string
               '(__abs__ __add__ __class__ __coerce__ __delattr__ __div__
                         __divmod__ __doc__ __eq__ __float__ __floordiv__
                         __format__ __ge__ __getattribute__ __getnewargs__
                         __gt__ __hash__ __init__ __int__ __le__ __long__
                         __lt__ __mod__ __mul__ __ne__ __neg__ __new__
                         __nonzero__ __pos__ __pow__ __radd__ __rdiv__
                         __rdivmod__ __reduce__ __reduce_ex__ __repr__
                         __rfloordiv__ __rmod__ __rmul__ __rpow__ __rsub__
                         __rtruediv__ __setattr__ __sizeof__ __str__
                         __sub__ __subclasshook__ __truediv__
                         conjugate imag real)))))
    (pylist-sort! l)
    l))

(define* (py-round x #:optional (digits 0))
  (let* ((f (expt 10.0 digits)))
    (inexact->exact
     (if (equal? digits 0)
         (round x)
         (/ (round (* x f)) f)))))
         
(define-method (py-bin (o <integer>))
  (number->string o 2))
(define-method (py-bin (o <py-int>))
  (number->string (slot-ref o 'x) 2))
(define-method (py-bin o)
  (+ "0b" (number->string (py-index o) 2)))

(define-method (scheme? (o <integer>   )) py-int)
(define-method (scheme? (o <py-int>    )) py-int)
(define-method (scheme? (o <complex>   )) py-complex)
(define-method (scheme? (o <py-complex>)) py-complex)
(define-method (scheme? (o <real>      )) py-float)
(define-method (scheme? (o <py-float>  )) py-float)

  
(set! (@@ (oop dict) int) int)

(define-method (py@= var val set) (set (py-matmul var val)))
(define-method (py@= (var <p>) val set)
  (aif it (ref var '__idiv__)
       (it val)
       (next-method)))

(define-method (py-= var val set) (set (- var val)))
(define-method (py-= (var <p>) val set)
  (aif it (ref var '__isub__)
       (it val)
       (next-method)))

(define-method (py/= var val set) (set (py-/ var val)))
(define-method (py/= (var <p>) val set)
  (aif it (ref var '__itruediv__)
       (it val)
       (next-method)))

(define-method (py%= var val set) (set (modulo var val)))
(define-method (py%= (var <p>) val set)
  (aif it (ref var '__imod__)
       (it val)
       (next-method)))

(define-method (py&= var val set) (set (py-logand var val)))
(define-method (py&= (var <p>) val set)
  (aif it (ref var '__iand__)
       (it val)
       (next-method)))

(define-method (py-or= var val set) (set (py-logior var val)))
(define-method (py-or= (var <p>) val set)
  (aif it (ref var '__ior__)
       (it val)
       (next-method)))

(define-method (py^= var val set) (set (py-logxor var val)))
(define-method (py^= (var <p>) val set)
  (aif it (ref var '__ixor__)
       (it val)
       (next-method)))

(define-method (py**= var val set) (set (expt var val)))
(define-method (py**= (var <p>) val set)
  (aif it (ref var '__ipow__)
       (it val)
       (next-method)))

(define-method (py<<= var val set) (set (py-lshift var val)))
(define-method (py<<= (var <p>) val set)
  (aif it (ref var '__ilshift__)
       (it val)
       (next-method)))

(define-method (py>>= var val set) (set (py-rshift var val)))
(define-method (py>>= (var <p>) val set)
  (aif it (ref var '__irshift__)
       (it val)
       (next-method)))

(define-method (py//= var val set) (set (py-floordiv var val)))
(define-method (py//= (var <p>) val set)
  (aif it (ref var '__ifloordiv__)
       (it val)
       (next-method)))

(define-syntax-rule  (make-= += py+=)
  (define-syntax +=
    (syntax-rules ()
      ((_ var val)     (py+= var val (lambda (v) (set! var v))))
      ((_ var val act) (py+= var val act)))))

(make-=  /=  py/=)
(make-=  -=  py-=)
(make-=  %=  py%=)
(make-=  &=  py&=)
(make-=  ^=  py^=)
(make-=  &=  py&=)
(make-= or=  py-or=)
(make-= <<=  py<<=)
(make-= >>=  py>>=)
(make-= //=  py//=)
(make-= **=  py**=)

(set! py-mod- py-mod)

(define (fromhex- s)
  (let lp ((l (string->list s)) (sign 1) (base 16))
    (define (frac x fr)
      (let lp ((x x) (ll (string->list fr)) (f (/ 1 base)))
        (if (pair? ll)
            (lp (+ x
                   (* f
                      (string->number
                       (list->string (list (car ll))) base)))
                (cdr ll)
                (* f (/ 1 base)))
            x)))

    (match l
      ((#\+ . l)
       (lp l sign base))
      ((#\- . l)
       (lp l (- sign) base))
      ((#\0 #\x . l)
       (lp l sign 16))
      ((#\0 #\o . l)
       (lp l sign 8))
      ((#\0 #\b . l)
       (lp l sign 2))
      ((#\. . _)
       (lp (cons #\0 l) sign base))
      (_
       (let* ((s  (list->string l))
              (l2 (string-split s #\.)))
         (if (> (length l2) 1) 
             (let ((p1 (car l2))
                   (l3 (string-split (cadr l2) #\p)))
                (if (> (length l3) 1)
                    (let* ((x1 (string->number p1 base))
                           (x2 (frac x1 (car l3))))
                      (exact->inexact
                       (* x2 sign (expt 2 (string->number (cadr l3))))))
                    (let* ((x1 (string->number p1 base))
                           (x2 (frac x1 (car l3))))
                      (exact->inexact (* sign x2)))))
             (let ((l3 (string-split s #\p)))
               (if (> (length l3) 1)
                   (let* ((x1 (string->number (car l3)  base)))
                      (exact->inexact
                       (* x1 sign (expt 2 (string->number (cadr l3))))))
                   (* sign (string->number s base))))))))))
                      
(define (hex- x)
  (let ((x1 (number->string x))
        (x2 (number->string x 16)))
    (string-append
     "0x"
     (if (in "e" x1)
         (let lp ((l (reverse (string->list x2))) (r '()))
           (match l
             ((#\e . l)
              (list->string (reverse (append (reverse (cons #\p r)) l))))
             ((x . l)
              (lp l (cons x r)))
             (() (list->string r))))
         x2))))

(define-syntax-rule (mk+ py+ fast+ +)
  (define-syntax fast+
    (lambda (x)
      (syntax-case x ()
	((fast+ a b)
	 (if (numeric-expr #'a)
	     (if (numeric-expr #'b)
		 #'(+ a b)
		 #'(let ((aa a)
			 (bb b))
		     (if (number? b)
			 (+ a b)
			 (py+ a b))))
	     (if (numeric-expr #'b)
		 #'(let ((aa a)
			 (bb b))
		     (if (number? a)
			 (+ a b)
			 (py+ a b)))
		 #'(let ((aa a)
			 (bb b))
		     (if (and (number? a) (number? b))
			 (+ a b)
			 (py+ a b))))))))))

(define-syntax-rule (rash x y) (ash x (- y)))

(mk+ py-mod      fast%   modulo        )
(mk+ py-logand   fast&   logand        )
(mk+ py-logior   fast-or logior        )
(mk+ py-logxor   fast^   logxor        )
(mk+ py-floordiv fast//  floor-quotient)
(mk+ py-lshift   fast<<  ash           )
(mk+ py-rshift   fast>>  rash          )

  
(define-syntax fast/
  (lambda (x)
    (syntax-case x ()
      ((_ a b)
	 (cond
	  ((and (rational-expr #'a)
		(rational-expr #'b))
	   #'(let ((r (/ a b)))
	       (if (integer? r)
		   r
		   (exact->inexact r))))

	  ((and (numeric-expr #'a)
		(numeric-expr #'b))
	   #'(/ a b))

	  (else	   
	   #'(let ((aa a)
		   (bb b))
	       (if (and (number? aa) (number? bb))
		   (if (or (inexact? aa) (inexact? bb))
		       (/ aa bb)
		       (let ((r (/ aa bb)))
			 (if (integer? r)
			     r
			     (exact->inexact r))))
		   (py-/ aa bb)))))))))

(define-syntax-rule (mk+= py+= fast+= +)
  (define-syntax fast+=
    (lambda (x)
      (syntax-case x ()
        ((_ a b)
	 (let ((t1 (get-sym-type-num #'a)))
	   (cond
	    ((and (<= t1 1) (numeric-expr #'b))
	     #'(set! a (+ a b)))
	    
	    (else
	     #'(fast+= a b (lambda (v) (set! a v)))))))
      
        ((_ a b c)
	 (let ((t1 (get-sym-type-num #'a)))
	   (cond
	    ((and (<= t1 2) (numeric-expr #'b))
	     #'(set! a (+ a b)))
	  
	    ((symbol? (syntax->datum #'a))
	     #'(let ((bb b))
		 (if (and (number? a) (number? bb))
		     (set! a (+ a bb))
		     (py+= a bb c))))
	    (else
	     #'(py+= a b c)))))))))

(mk+= py+=   fast+=   +)
(mk+= py-=   fast-=   -)
(mk+= py*=   fast*=   *)
(mk+= py**=  fast**=  expt)
(mk+= py/=   fast/=   fast/)
(mk+= py%=   fast%=   modulo)
(mk+= py&=   fast&=   logand)
(mk+= py^=   fast^=   logxor)
(mk+= py-or= fast-or= logior)
(mk+= py<<=  fast<<=  ash)
(mk+= py>>=  fast>>=  rash)
(mk+= py//=  fast//=  floor-quotient)

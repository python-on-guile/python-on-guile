;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python match)
  #:use-module (ice-9 match)
  #:use-module (oop pf-objects)
  #:use-module (language python inspect)
  #:use-module (language python dict)
  #:use-module (language python list)
  #:use-module (language python exceptions)
  #:export (compile-match py-match))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define (G sym) `(@ (guile) ,sym))
(define (M sym) `(@@ (language python match) ,sym))
(define pkold pk)
(define (pkk . l) (car (reverse l)))

(define (union as vs)
  (let lp ((as as) (vs vs))
    (match as
      (((x) . u)
       (lp (cons x u) vs))
      ((x . as)
       (if (member x vs)
           (lp as vs)
           (lp as (cons x vs))))
      (()
       vs))))

(define (get-id-e x)
  (match x
    ((#:identifier id . _)
     (string->symbol id))))

(define (get-id x) x)

(define (get-vars pat)
  (define (get-vars-l pats)
    (match (pkk 'g-v-l pats)
      ((x . l)
       (union (get-vars x) (get-vars-l l)))
      (() '())))
  (pkk 'g-v-pats pat
  (match (pkk 'get-var pat)
    ((#:lit key pat)
     (get-vars pat))
    ((#:key key pat)
     (get-vars pat))
    ((#:** #:capture . id)
     (list (get-id-e id)))    
    ((#:seq . pats)
     (get-vars-l pats))
    ((#:pat pat)
     (get-vars pat))
    ((#:star id)
     (list (get-id-e id)))
    ((#:as (#:or . ps) (#:capture . id))
     (union (list (get-id-e id)) (get-vars-l ps)))
    ((#:or . ps)
     (get-vars-l ps))
    ((#:literal . _)
     '())
    ((#:capture . id)
     (let ((x (get-id-e id)))
       (if (equal? x '_)
           '()
           (list x))))
    ((#:wildcard)
     '())
    ((#:value . _)
     '())
    ((#:group p)
     (get-vars p))
    ((#:seq . l)
     (get-vars-l l))
    ((#:mapping . l)
     (get-vars-l l))
    ((#:class ids (#:key . l))
     (get-vars-l l))
    ((#:class ids (#:pos . l))
     (get-vars-l l))
    ((#:class (#:pos . l1) (#:key . l2))
     (union (get-vars-l l1) (get-vars-l l2))))))


(define (compile-named exp e vs)
  (match e
    ((#:named id expr)
     `(,(G 'begin)
       (set! ,id ,(exp vs expr))
       ,id))

    ((#:expr expr)
     (exp vs expr))))

(define (compile-star-named exp e vs)
  (match e
    ((#:bor x)
     `(#:star ,(exp vs x)))
    (_
     (compile-named exp e vs))))

(define-syntax listify
  (syntax-rules ()
    ((listify)
     '())
    ((listify (#:star x) . l)
     (append (to-list x) (listify l)))
    ((listify x . l)
     (cons x (listify . l)))))

(define (compile-lit exp e vs)
  (match e
    (#:None  None)
    (#:True  #t)
    (#:False #f)
    ((#:number . x)
     (exp vs x))
    ((#:string x)
     (exp vs x))
    ((#:add n1 n2)
     (+ (exp vs n1) (exp vs n2)))
    ((#:sub n1 n2)
     (- (exp vs n1) (exp vs n2)))))

(define (compile-key-val exp e vs)
  (match (pkk 'key-val e)
    ((#:key (#:identifier x . l) pat)
     `(#:key ,(string->symbol x)
             ,(compile-pat exp pat vs)))

    ((#:key (#:literal . key) pat)
     `(#:key ,(compile-lit exp key vs)
             ,(compile-pat exp pat vs)))
    ((#:** #:capture . id)
     `(#:** ,(get-id-e id)))))

(define (compile-class-pat exp e vs)
  (define (r f) (lambda (x) (f exp x vs)))
  (match e
    ((#f) #f)
    (((#:key . l))
     `(#:key ,@(map (r compile-key-val) l)))
    (((#:pos . l))
     `(#:pos ,@(map (r compile-pat) l)))
    (((#:pos . l1) (#:key . l2))
     `(#:poskey ,(map (r compile-pat) l1)
                ,(map (r compile-key-val) l2)))))
    
(define (compile-closed-pat exp e vs)
  (define (r f) (lambda (x) (f exp x vs)))
  (match (pkk 'clp e)
    ((#:literal . x)
     `(#:lit ,(compile-lit exp x vs)))
    ((#:capture . id)
     (let ((x (get-id-e id)))
       (if (eq? x '_)
           #:wildcard
           `(#:cap ,x))))
    ((#:wildcard)
     #:wildcard)
    ((#:value . x)
     `(#:ref ,@(map get-id-e x)))
    ((#:group p)
     `(#:group ,(compile-pat exp p vs)))
    ((#:seq . l)
     `(#:seq ,@(map (r compile-maybe-star-pat) l)))
    ((#:mapping . l)
     `(#:map ,@(map (r compile-key-val) l)))
    ((#:class ids . v)
     `(#:class ,(map get-id-e ids)
               ,(compile-class-pat exp v vs)))))
  
(define (compile-pat exp e vs)
  (define (r f) (lambda (x) (f exp x vs)))
  
  (match (pkk 'pat e)
    ((#:as (#:or . ps) (#:capture . id))
     `(#:as ,(map (r compile-closed-pat) ps) ,(get-id-e id)))
    ((#:or . ps)
     `(#:or ,@(map (r compile-closed-pat) ps)))))
    

(define (compile-maybe-star-pat exp e vs)
  (match (pkk 'msp e)
    ((#:star pat)
     `(#:star ,(compile-pat exp pat vs)))
    ((#:pat pat)
     `(#:pat ,(compile-pat exp pat vs)))
    (pat (compile-pat exp pat vs))))

(define (compile-pats exp pats vs)
  (define (r f) (lambda (x) (f exp x vs)))
  
  (match (pkk 'pats pats)
    ((#:seq . pats)
     `(#:list ,@(map (r compile-maybe-star-pat) pats)))
    (_ `(#:pat ,(compile-pat exp pats vs)))))

(define (compile-case exp e vs)
  (match (pkk 'case e)
    ((pats #f code)
     (let* ((v   (get-vars pats))
            (vs2 (pkk 0 (union vs v))))
       `(,v ,(pkk 1 (compile-pats exp pats vs)) #t ,(pkk 2 (exp vs2 code)))))
    ((pats if code)
     (let* ((v   (get-vars pats))
            (vs2 (union vs v)))
       `(,v
         ,(compile-pats exp pats vs)
         ,(exp vs2 if)
         ,(exp vs2 code))))))

(define-syntax reffer
  (syntax-rules ()
    ((_ e)
     e)
    ((_ e x . l)
     (reffer (ref e 'x) . l))))

(define er (list 'error))
(define-syntax handle-map
  (syntax-rules ()
    ((_ e keys)
     #t)
    
    ((_ e keys (#:key key pat) . l)
     (let ((e2 (py-get e key er)))
       (and
        (not (eq? e2 er))
        (handle-pattern e2 pat)
        (handle-map e (key . keys) . l))))
    
    ((_ e keys (#:** id))
     (begin
       (let ((m (dict e)))
         (set! id m)
         (let lp ((l 'keys))
           (if (pair? l)
               (begin
                 (pylist-delete! m (car l))
                 (lp (cdr l))))))
       #t))))

(define-syntax handle-class-keys
  (lambda (x)
    (syntax-case x ()
      ((_ e seen keys)
       #'#t)
      
      ((_ e seen keys (#:key key pat)  . l)
       #'(let ((x (ref e 'key er)))
           (if (eq? er x)
               (raise (TypeError "class has not the attribute (in match"))
               (and
                (handle-pattern x pat)
                (handle-class-keys e seen (key . keys) . l)))))
      
      ((_ e seen keys (#:** id))
       #'(let ((h (get-h e))
               (m (make-hash-table))
               (d (dict)))
           (fold (k v s) (hash-set! s k #t) keys)
           (fold (k v s) (hash-set! s k #t) seen)
           (hash-for-each
            (lambda (k v)
              (let ((x (hash-ref m k #f)))
                (when (not x)
                  (pylist-set! d (symbol->string k) v))))
            h)
           (set! id d)
           #t))
      
      ((_ e seen keys (#:** id) . _)
       (error "double star pattren not last in match statement")))))



(define-syntax-rule (empty . l) #t)

(define-syntax handle-class-pos
  (syntax-rules ()
    ((_ e keys seen () (a . l))
     (a e seen . l))
    ((_ e keys seen (p . pats) u)
     (if (pair? keys)
         (let* ((k  (car keys))
                (ks (cdr keys))
                (se (cons k seen))
                (x  (ref e k)))
           (and
            (handle-pattern x p)
            (handle-class-pos e ks se pats u)))))))

    
(define-syntax handle-class
  (syntax-rules ()
    ((_)
     #t)
    
    ((_ e (#:key . l))
     (handle-class-keys e '() () . l))

    ((_ e (#:pos . l))
     (aif it (ref e '__match_args__)
          (let ((u (map string->symbol (to-list it))))
            (handle-class-pos e u '() l (empty)))
          (error "positional patterns can't be hanlded")))

    ((_ e (#:poskey pos key))
     (aif it (ref e '__match_args__)
          (let ((u (map string->symbol (to-list it))))
            (handle-class-pos e u '() pos (handle-class-keys () key)))
          (error "positional patterns can't be hanlded")))))
    

(define-syntax handle-pattern0
  (syntax-rules ()    
    ((_ e (#:lit x))
     (equal? e x))
    
    ((_ e (#:cap id))
     (begin
       (set! id e)
       #t))
    
    ((_ e #:wildcard)
     #t)
    
    ((_ e (#:ref a . l))
     (equal? e (reffer a . l)))
    
    ((_ e (#:group pat))
     (handle-pattern e pat))
    
    ((_ e (#:seq . l))
     (if (sequence? e)
         (let ((ee (if (pair? e) e (to-list e))))
           (handle-pats-list ee . l))
         #f))
    
    ((_ e (#:map . l))
     (if (mapping? e)
         (handle-map e () . l)
         #f))

    ((_ e (#:class (a . l) v))
     (if (isinstance e (reffer a . l))
         (handle-class e v)
         #f))))

(define-syntax handle-pattern
  (syntax-rules ()
    ((_ e (#:pat p))
     (handle-pattern e p))
    
    ((_ e (#:as (p ...) id))
     (if (or (handle-pattern0 e p) ...)
         (begin
           (set! id e)
           #t)
         #f))
    ((_ e (#:or p ...))
     (or (handle-pattern0 e p) ...))))
    

(define-syntax handle-pats-list
  (lambda (x)
    (syntax-case x ()
      ((_ e)
       #'(null? e))
      ((_ e (#:star p))
       #'(handle-pattern e p))
      ((_ e (#:star p) . l)
       (error "Can't have a star expression as a pattern in non last position"))
      ((_ e p . l)
       #'(and (pair? e)
              (handle-pattern (car e) p)
              (handle-pats-list (cdr e) . l))))))


(define-syntax handle-pats
  (syntax-rules ()
    ((_ e (#:seq . l))
     (if (sequence? e)
         (handle-pats-list (to-list e) . l)
         #f))
    ((_ e p)
     (handle-pattern e p))))             

(define-syntax handle-cases
  (syntax-rules ()
    ((_ e)
     (values))
    ((_ e ((vs ...) pat if-guard code) . l)
     (let ((vs None) ...)
       (if (and (handle-pats e pat) if-guard)
           code
           (handle-cases e . l))))))

(define-syntax py-match
  (syntax-rules ()
    ((_ x case ...)
     (let ((e x)) (handle-cases e case ...)))))

(define (compile-match exp e vs)
  (define (r f) (lambda (x) (f exp x vs)))
  (match e
    (((#:star . l) . cases)
     `(,(M 'py-match) (,(M 'listify) ,@(map (r compile-star-named) l))
       ,@(map (r compile-case) cases)))
    ((x . cases)
     `(,(M 'py-match) ,(compile-named exp x vs)
        ,@(map (r compile-case) cases)))))

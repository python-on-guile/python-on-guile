;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python dict)
  #:use-module (language python type)
  #:use-module (language python list)
  #:use-module (language python try)
  #:use-module (language python hash)
  #:use-module (language python yield)
  #:use-module (language python def)
  #:use-module (language python for)
  #:use-module (language python bool)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:use-module (ice-9 stis-hash)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 control)
  #:use-module (rnrs arithmetic fixnums)
  #:use-module (oop goops)
  #:use-module ((oop dict)  #:renamer
                (lambda (x)
                  (cond
                   ((eq? x 'format)  'usp-format)
                   ((eq? x 'to-list) 'usp-to-list)
                   (else x))))
  #:use-module (oop pf-objects)

  
  #:re-export (py-get dictNs dictRNs py-clear <HH>)
  #:export (make-py-hashtable <py-hashtable>
            py-copy py-fromkeys py-has_key py-items py-iteritems
            py-iterkeys py-itervalues py-keys py-values
            py-popitem py-setdefault py-update
            py-hash-ref dict pyhash-listing
	    weak-key-dict weak-value-dict
	    py-hash-ref py-hash-set! 
	    make-py-weak-key-hashtable
	    make-py-weak-value-hashtable
	    dict-ackumulator
            ))

(define (special? x)
  (struct? x))

  
(define miss  (list 'miss))
(define miss2 (list 'miss2))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(make-stis-hash-type 'python-hash
  (lambda (x)
    (cond
     ((fixnum? x)
      x)
	 
     ((or (symbol? x) (keyword? x))
      (object-address x))
	 
     (else
      (py-hash x))))

  (lambda (x y)
    (cond
    ((number? x)
     (cond
      ((number? y)
       (= x y))
      ((string? y)
       #f)
      (else
       (py-equal? x y))))

    ((string? x)
     (cond
      ((string? y)
       (equal? x y))
      ((number? y)
       #f)
      (else
       (py-equal? x y))))

    (else
     (py-equal? x y)))))
       


(define py-hash-ref     stis-hash-ref     )
(define py-hash-set!    stis-hash-set!    )
(define py-hash-remove! stis-hash-remove! )

(set! (@@ (oop dict) hset!) py-hash-set!)

(define H (hash 1333674836 complexity))

(define-class <py-hashtable> () t hash nn)

(name-object <py-hashtable>)

(cpit <py-hashtable>
      (o (lambda (o h n a)
	   (slot-set! o 'hash h)
	   (slot-set! o 'nn n)
	   (slot-set! o 't
		      (let ((t (make-hash-table)))
			(let lp ((a a))
			  (if (pair? a)
			      (begin
				(py-hash-set! t (caar a) (cdar a))
				(lp (cdr a)))))
			t)))
	 (let ((t (slot-ref o 't)))
	   (list
	    (slot-ref o 'hash)
	    (slot-ref o 'nn)
	    (hash-fold (lambda (k v s) (cons (cons k v) s)) '() t)))))

(define (make-py-hashtable) (dict))

(define (make-py-weak-key-hashtable)
  (let* ((o (make <py-hashtable>))
         (t (make-weak-key-hash-table))
         (h H))
    (slot-set! o 't    t)
    (slot-set! o 'hash h)
    (slot-set! o 'nn   0)
    o))

(define (make-py-weak-value-hashtable)
  (let* ((o (make <py-hashtable>))
         (t (make-weak-value-hash-table))
         (h H))
    (slot-set! o 't    t)
    (slot-set! o 'hash h)
    (slot-set! o 'nn   0)
    o))

(define-method (pylist-ref (o <hashtable>) x)
  (let ((r (hash-ref o x miss)))
    (if (eq? r miss)
        (raise (KeyError "missing key in <hashtable>[key]"))
        r)))
(name-object <hashtable>)

(define-method (pylist-ref (o <HH>) x)
  (let ((r (stis-hash-ref o x miss)))
    (if (eq? r miss)
        (raise (KeyError "missing key in <hashtable>[key]"))
        r)))
(name-object <HH>)

(define-method (pylist-ref (oo <py-hashtable>) x)
  (let ((r (hash-ref (slot-ref oo 't) x miss)))
    (if (eq? r miss)
	(aif it (ref oo '__missing__)
	     (it x)
	     (raise KeyError x))
        r)))

(define-method (pylist-remove! (o <py-hashtable>) k)
  (hash-remove! (slot-ref o 't) k))

(define-method (pylist-remove! (o <hashtable>) k)
  (hash-remove! o k))

(define-method (pylist-remove! (o <HH>) k)
  (stis-hash-remove! o k))

(define-method (pylist-delete! (o <py-hashtable>) k)
  (hash-remove! (slot-ref o 't) k))

(define-method (pylist-delete! (o <hashtable>) k)
  (hash-remove! o k))

(define-method (pylist-delete! (o <HH>) k)
  (stis-hash-remove! o k))

(define-method (py-hash (o <hashtable>))
  (hash-fold
   (lambda (k v s)
     (logxor
      (fast-hash k)
      s))
   0 o))

(define-method (py-hash (o <HH>))
  (stis-hash-fold
   (lambda (k v s)
     (logxor
      (fast-hash k)
      s))
   0 o))

(define-method (py-hash (o <py-hashtable>))
  (slot-ref o 'hash))

(define-method (len (o <hashtable>))
  (hash-fold (lambda l
               (apply (lambda ll
                        (+ (caddr ll) 1))
                      l))
             0 o))

(define-method (len (o <py-hashtable>))
  (slot-ref o 'nn))

(define-method (len (o <HH>))
  (- (slot-ref o 'nm) (slot-ref o 'na)))

(define-method (pylist-pop! (o <hashtable>) k . l)
  (match l
    ((v)
     (let ((ret (hash-ref o k v)))
       (hash-remove! o k)
       ret))
    (()
     (let ((ret (hash-ref o k miss)))
       (if (eq? ret miss)
           (raise KeyError k)
           (begin
             (hash-remove! o k)
             ret))))))

(define-method (pylist-pop! (o <HH>) k . l)
  (match l
    ((v)
     (let ((ret (stis-hash-ref o k v)))
       (stis-hash-remove! o k)
       ret))
    (()
     (let ((ret (stis-hash-ref o k miss)))
       (if (eq? ret miss)
           (raise KeyError k)
           (begin
             (stis-hash-remove! o k)
             ret))))))

(define-method (pyhash-rem! (o <hashtable>) k)
  (hash-remove! o k)
  (values))

(define-method (pyhash-rem! (o <HH>) k)
  (stis-hash-remove! o k)
  (values))

(name-object pyhash-rem!)

(define-method (pyhash-rem! (o <py-hashtable>) k)
  (let ((t (slot-ref o 't))
        (n (slot-ref o 'nn))
        (h (slot-ref o 'hash)))
    (let ((ret (py-hash-ref t k miss)))
      (if (eq? ret miss)
          (values)
          (begin
            (py-hash-remove! t k)
            (slot-set! o 'nn   (- n 1))
            (slot-set! o 'hash (logxor h (xy (fast-hash k) (fast-hash ret))))
            (values))))))

(define-method (pylist-pop! (o <py-hashtable>) k . l)
  (let ((t (slot-ref o 't)))
    (match l
      ((v)
       (let ((ret (py-hash-ref t k miss)))
         (if (eq? ret miss)
             v
             (begin
               (pyhash-rem! o k)
               ret))))
      (()
       (let ((ret (hash-ref o k miss)))
         (if (eq? ret miss)
             (raise KeyError k)
             (begin
               (pyhash-rem! o k)
               ret)))))))

(define-method (pylist-set! (o <hashtable>) key val)
  (hash-set! o key val)
  (values))

(define-method (pylist-set! (o <HH>) key val)
  (stis-hash-set! o key val)
  (values))

(define-method (pylist-set! (o <py-hashtable>) key val)
  (let ((t (slot-ref o 't))
        (n (slot-ref o 'nn))
        (h (slot-ref o 'hash)))
    (let ((ret (py-hash-ref t key miss)))
      (if (eq? ret miss)
          (begin
            (py-hash-set! t key val)
            (slot-set! o 'nn (+ n 1))
            (slot-set! o 'hash (logxor (xy (fast-hash key) (fast-hash val)) h)))
          (begin
            (py-hash-set! t key val)
            (slot-set! o 'hash
                       (logxor (fast-hash key)
                               h))))))
  (values))
  
(define-method (py-get x y . l)
  (raise (AttributeError "Attribyte 'get' is not available for object")))

(define goops (rawref object '__goops__))
(define-syntax define-py
  (syntax-rules ()
    ((_ (nm n o l ...) (class code ...) ...)
     (begin
       (define-method (nm (o class) l ...) code ...)
       ...
       (name-object nm)
       (define-method (nm (o <py>) . v)
         (aif it (ref-in-class o 'n)
              (apply it o v)
              (aif it (ref o 'n #f)
                   (apply it v)
                   (next-method))))))
    
    ((_ (nm n o l ... . u) (class code ...) ...)
     (begin
       (define-method (nm (o class) l ... . u) code ...)
       ...
       (define-method (nm (o <py>) . v)
         (aif it (ref-in-class o 'n) 
              (apply it o v)
              (aif it (ref o 'n #f)
                   (apply it v)
                   (next-method))))))))


(define-method (bool (o <hashtable>   )) (> (len o) 0))
(define-method (bool (o <HH>            )) (> (len o) 0))
(define-method (bool (o <py-hashtable>)) (> (len o) 0))

(define-py (py-copy copy o)
  (<HH>
   (let ((h (make-stis-hash-table 'python-hash)))
     (with-stis-hash ((o : _ set))
       (hash-for-each set h))
     h))
  
  (<hashtable>
   (hash-fold
    (lambda (k v h)
      (hash-set! h k v)
      h)
    (make-hash-table)
    o))

  (<py-hashtable>
   (let ((r (make <py-hashtable>)))
     (slot-set! r 'hash (slot-ref o 'hash))
     (slot-set! r 'nn   (slot-ref o 'nn))
     (slot-set! r 't    (py-copy (slot-ref o 't)))
     r)))

(define-py (py-fromkeys fromkeys o . l)
  (<HH>
   (let ((h (make-stis-hash-table 'python-hash))
	 (newval (match l
                   (()  None)
                   ((v) v))))
     (with-stis-hash ((o : _ set))
       (stis-hash-for-each (lambda (k v) (set h newval)) o))
     
     h))
  
  (<hashtable>
   (let ((newval (match l
                   (()  None)
                   ((v) v))))
     (hash-fold
      (lambda (k v h)
        (py-hash-set! h k newval)
        h)
      (make-hash-table)
      o)))

  (<py-hashtable>
   (let ((newval (match l
                   (()  None)
                   ((v) v))))
     (hash-fold
      (lambda (k v h)
        (pylist-set! h k newval)
	h)
      (dict)
      (slot-ref o 't)))))

(define-py (py-get get o k . l)
  (<HH>
   (let ((elseval (match l
			 (()  None)
			 ((v) v))))
     (let ((ret (stis-hash-ref o k miss)))
       (if (eq? ret miss)
	   elseval
	   ret))))
    
  (<hashtable>
   (let ((elseval (match l
                    (()  None)
                    ((v) v))))
     (let ((ret (hash-ref o k miss)))
       (if (eq? ret miss)
           elseval
           ret))))

  (<py-hashtable>
   (let ((elseval (match l
                    (()  None)
                    ((v) v))))
     (let ((ret (py-hash-ref (slot-ref o 't) k miss)))
       (if (eq? ret miss)
           elseval
           ret)))))

(define-syntax-rule (mk-get <vector> vector-length vector-ref mk)
  (define-method (py-get (o <vector>) i . l)
    (let* ((i (index-ref i #t))
           (N (vector-length o))
           (i (if (< i 0) (+ N i) i)))
        (if (and (< i N) (>= i 0))
            (mk (vector-ref o i))
            (let ((elseval (match l
                             (()  None)
                             ((v) v))))
              (if (eq? elseval None)
                  (raise (IndexError "index out of range in sequence"))
                  elseval))))))

(define (id x) x)
(mk-get <vector> vector-length vector-ref id)
(mk-get <string> string-length string-ref
        (lambda (x)
          (let ((s (make-string 1)))
            (string-set! s 0 x)
            s)))
(mk-get <pair>   length        list-ref   id)
        
(define-py (py-has_key has_key o k . l)
  (<HH>
   (let ((elseval (match l
                    (()  None)
                    ((v) v))))
     (let ((ret (stis-hash-ref o k miss)))
       (if (eq? ret miss)
           elseval
           #t))))
   
  (<hashtable>
   (let ((elseval (match l
                    (()  None)
                    ((v) v))))
     (let ((ret (hash-ref o k miss)))
       (if (eq? ret miss)
           elseval
           #t))))

  (<py-hashtable>
   (let ((elseval (match l
                   (()  None)
                   ((v) v))))
     (let ((ret (py-hash-ref (slot-ref o 't) k miss)))
       (if (eq? ret miss)
           #f
           #t)))))

(define-py (py-items items o)
  (<module>
   (to-pylist
    (let ((l '()))
      (module-for-each
       (lambda (k v)
	 (set! l (cons (list (symbol->string k) (variable-ref v)) l)))
       o)
      l)))

  (<HH>
   o)
   
  (<hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l)
       (cons (list k v) l))
     '() o)))
   
  (<py-hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l)
       (cons (list k v) l))
     '() (slot-ref o 't)))))
  
(define-generator (hash-item-gen yield hash-table)
  (let lp ((l (hash-fold cons* '() hash-table)))
    (match l
      ((k v . l)
       (yield k v)
       (lp l))
      (()
       #t))))

(define-generator (hash-values-gen yield hash-table)
  (let lp ((l (hash-fold cons* '() hash-table)))
    (match l
      ((k v . l)
       (yield v)
       (lp l))
      (()
       #t))))

(define-generator (hash-keys-gen yield hash-table)
  (let lp ((l (hash-fold cons* '() hash-table)))
    (match l
      ((k v . l)
       (yield k)
       (lp l))
      (()
       #t))))

(define-generator (stis-hash-item-gen yield hash-table)
  (stis-hash-for-each
   (lambda (k v) (yield k v)) hash-table))

(define-generator (stis-hash-values-gen yield hash-table)
  (stis-hash-for-each
   (lambda (k v) (yield v)) hash-table))

(define-generator (stis-hash-keys-gen yield hash-table)
  (stis-hash-for-each
   (lambda (k v) (yield k)) hash-table))
      
(define-py (py-iteritems iteritems o)
  (<HH>
   (stis-hash-item-gen o))
  
  (<hashtable>
   (hash-item-gen o))
  
  (<py-hashtable>
   (hash-item-gen (slot-ref o 't))))

(define-py (py-iterkeys iterkeys o)
  (<HH>
   (stis-hash-keys-gen o))
  
  (<hashtable>
   (hash-keys-gen o))
  
  (<py-hashtable>
   (hash-keys-gen (slot-ref o 't))))

(define-py (py-itervalues itervalues o)
  (<HH>
   (stis-hash-values-gen o))

  (<hashtable>
   (hash-values-gen o))
  
  (<py-hashtable>
   (hash-values-gen (slot-ref o 't))))

(define-py (py-keys keys o)
  (<HH>
   ((make-generator
     (lambda (yield)
       (stis-hash-for-each
	(lambda (k v) (yield k))
	o)))))
   
  (<hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l) (cons k l))
     '()
     o)))

  (<py-hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l) (cons k l))
     '()
     (slot-ref o 't)))))

(define-py (py-values values o)
  (<HH>
   ((make-generator
     (lambda (yield)
       (stis-hash-for-each       
	(lambda (k v) (yield v))
	o)))))

  (<hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l) (cons v l))
     '()
     o)))

  (<py-hashtable>
   (to-pylist
    (hash-fold
     (lambda (k v l) (cons v l))
     '()
     (slot-ref o 't)))))

(define-py (py-popitem popitem o)
  (<HH>
   (let ((k.v (let/ec ret
                (stis-hash-for-each
                 (lambda (k v)
                   (ret (cons k v)))
                 o)
                #f)))
     (if k.v
         (begin (stis-hash-remove! o (car k.v)) k.v)
         (raise KeyError "No elements in hash"))))

  (<hashtable>
   (let ((k.v (let/ec ret
                (hash-for-each
                 (lambda (k v)
                   (ret (cons k v)))
                 o)
                #f)))
     (if k.v
         (begin (hash-remove! o (car k.v)) k.v)
         (raise KeyError "No elements in hash"))))
     
  (<py-hashtable>
      (let ((k.v (let/ec ret
                   (hash-for-each
                    (lambda (k v)
                      (ret (cons k v)))
                    (slot-ref o 't))
                   #f)))
        (if k.v
            (begin (pyhash-rem! o (car k.v)) k.v)
            (raise KeyError "No elements in hash")))))

(define-py (py-setdefault setdefault o k . l)
  (<HH>
   (stis-hash-set! o k (apply py-get o k l))
   (apply py-get o k l))

  (<hashtable>
   (hash-set! o k (apply py-get o k l))
   (apply py-get o k l))
                
  (<py-hashtable>
   (pylist-set! o k (apply py-get o k l))
   (apply py-get o k l)))

(define update0
  (lam0 (o (* L) (** K))
       (match L
         ((L)
          (for-in ((k v : L)) ()
		  (pylist-set! o k v)))
         (_ #f))

       (when (bool K)
         (for-in ((k v : K)) ()
		 (pylist-set! o k v)
		 (values)))))

(define-py (py-update update o . l)
  (<HH>
   (apply update0 o l))
  (<hashtable>
   (apply update0 o l))
  (<py-hashtable>
   (apply update0 o l)))

(define-py (py-clear clear o)
  (<HH>
   (stis-hash-clear! o))
  (<hashtable>
   (hash-clear! o))
  (<py-hashtable>
   (let ((t (slot-ref o 't)))
     (hash-clear! t)
     (slot-set! o 'nn 0)
     (slot-set! o 'hash H)
     (values))))

#|        
'viewitems'
'viewkeys'
'viewvalues'
|#

(define-syntax-rule (top <)
  (begin
    (define-method (< (o1 <hashtable>) o2)
      (< (len o1) (len o2)))
    (define-method (< (o1 <HH>) o2)
      (< (len o1) (len o2)))
    (define-method (< (o1 <py-hashtable>) o2))))


(top <)
(top >)
(top <=)
(top >=)

(define (fold f s l)
  (if (pair? l)
      (f (car l) (fold f s (cdr l)))
      s))

(define-method (write (o <py-hashtable>) . l)
  (define port (match l (() #f) ((p) p)))
  (define li (map
              (lambda (x)
                (print-scm x write))
              (hash-fold cons* '() (slot-ref o 't))))
  
  (if (null? li)
      (format port "{}") 
      (format port "{~a: ~a~{, ~a: ~a~}}" (car li) (cadr li) (cddr li))))

(define (write- o . l)
  (define port (match l (() #f) ((p) p)))
  
  (format port "{")

  (for-in ((k v : o)) ((first #t) (n 0))
    (when (not first)
      (format port ", "))
    
    (if (< n 100)
	(format port "~a:~a" k v)
	(begin
	  (format port "...")
	  (break)))
    
    (values #f (+ n 1)))
  
  (format port "}"))
     
(define-method (write (o <HH>) p) (write- o p))
(define-method (write (o <HH>))   (write- o))

(define (write-dict o write . l)
  (define port (if (pair? l) (car l) #f))
  (define l
    (let lp ((u o))
      (if (pair? u)
          (cons* (caar u) (cadar u) (lp (cdr u)))
          '())))
  
  (if (null? l)
      (format port "{}") 
      (format port "{~a: ~a~{, ~a: ~a~}}" (car l) (cadr l) (cddr l))))

(define-method (display (o <py-hashtable>) . l)
  (define port (match l (() #f) ((p) p)))
  (define li (map
             (lambda (x)
                (print-scm x display))
              (hash-fold cons* '() (slot-ref o 't))))
  (if (null? li)
      (format port "{}") 
      (format port "{~a: ~a~{, ~a: ~a~}}" (car li) (cadr li) (cddr li))))

(define-method (display (o <HH>) x) (write o x))
(define-method (display (o <HH>)  ) (write o  ))

(define-method (py-equal? (o1 <HH>) (o2 <HH>))
  (let/ec ret
    (with-stis-hash ((o1 : ref))
      (stis-hash-for-each
        (lambda (k2 v2)
	  (let ((v1 (ref k2 miss)))
	    (if (eq? v1 miss)
		(ret #f)
		(if (not (fast-equal? v1 v2))
		    (ret #f)))))
	o2))
    #t))
	       
  
(define-method (py-equal? (o1 <py-hashtable>) (o2 <py-hashtable>))  
  (and   
   (equal? (slot-ref o1 'nn)   (slot-ref o2 'nn))
   (equal? (slot-ref o1 'hash) (slot-ref o2 'hash))
   (e?     2 2 (slot-ref o1 't)    (slot-ref o2 't))))

(define (e? n1 n2 t1 t2)
  (define-syntax-rule (mk py-hash-ref)
    (let/ec ret
      (hash-fold
       (lambda (k v s)
         (let ((r (py-hash-ref t2 k miss)))
           (if (eq? r miss)
               (ret #f)
               (if (fast-equal? v r)
                   #t
                   (ret #f)))))
       #t
       t1)))
  (and
   (eq? (max n1 1) (max n2 1))
   (case n1
     ((0)
      (if (eq? n2 1)
          (e? n2 n1 t2 t1)
          (let lp ((l t1))
            (if (pair? l)
                (let* ((x (car l))
                       (k (car x)))
                  (aif it (assoc k t2)
                       (begin
                         (fast-equal? (cdr x) (cdr it))
                         (lp (cdr l)))
                       #f))
                #t))))
     
     ((1)
      (if (eq? t2 0)
          (mk (lambda (h k m) (aif it (assoc k h)
                                   (cdr it)
                                   m)))
          (mk hash-ref)))
     
     ((2) (mk py-hash-ref)))))


(define-class <hashiter> () l)
(name-object <hashiter>)
(cpit <hashiter> (o (lambda (o l) (slot-set! o 'l l))
		    (list (slot-ref o 'l))))


(define-method (wrap-in (t <hashtable>))
  (let ((o (make <hashiter>)))
    (slot-set! o 'l (to-list (py-items t)))
    o))

(define-method (wrap-in (t <HH>))
  (wrap-in (stis-hash-item-gen t)))

(define-method (wrap-in (t <py-hashtable>))
  (let ((o (make <hashiter>)))
    (slot-set! o 'l (to-list (py-items t)))
    o))

(define-next ((o <hashiter>) n)
  (let ((l (slot-ref o 'l)))
    (if (pair? l)
        (let ((k (caar  l))
              (v (cadar l))
              (l (cdr   l)))
          (slot-set! o 'l l)
          (produce-arg k v))
        (produce-arg arg-empty))))
           

(define-method (in key (o <hashtable>))
  (py-has_key o key))

(define-method (in key (o <py-hashtable>))
  (py-has_key o key))

(define-method (in key (o <HH>))
  (py-has_key o key))

(define dict-start-type 0)

(define (simple? key) 
  (or
   (string? key)
   (number? key)
   (vector? key)
   (not (or (struct? key)
	    (pair? key)
	    (null? key)))))
 

(define-python-class dict ()  
  (define __init__
    (letrec ((__init__              
              (case-lambda
	       ((self)
		(let ((x (make-stis-hash-table 'python-hash)))
		  x))
	       
	       ((self . l)
		(let ((self (__init__ self)))
		  (with-stis-hash ((self : _ set))
		    (let lp ((l l))
		      (if (and (pair? l)
			       (not (keyword? (car l))))
                        
			  (begin
			    (catch #t
			      (lambda ()
				(for-in ((k v : (car l))) () 
				  (set k v)))
			      (lambda x
				(for-in ((k : (car l))) ()
				  (if (pair? k)
				      (set (car k) (cdr k))
				      (set (fastlist-ref k 0)
					   (fastlist-ref k 1))))))
                            
			    (lp (cdr l)))

			
			  (let lp ((l l))
			    (match l
			      ((x y . l)
			       (when (not (keyword? x))
				 (raise (ValueError "not a keywrod arg")))
			       (set (symbol->string
				     (keyword->symbol x))
				    y)
			       (lp l))
			      (() ((@ (guile) values))))))))
		  self)))))
      __init__))

  (define __new__
    (case-lambda
      ((cls . x)
       (let* ((o ((rawref object '__new__) cls))
	      (s (apply __init__ o x)))
	 s)))))
       
    
                
(define (renorm k)
  (if (symbol? k)
      k
      (string->symbol k)))

(define (norm k)
  (if (symbol? k)
      (symbol->string k)
      k))

(define fail (list 'fail))

(define-syntax-rule (mkwrap dictNs norm renorm)
(define-python-class dictNs ()
  (define __getitem__
    (lambda (self k)
      (pylist-ref (ref self '_dict) (norm k))))
  
  (define __setitem__
    (lambda (self k v)
      (pylist-set! (ref self '_dict) (norm k) v)))

  (define __delitem__
    (lambda (self k)
      (pylist-delete! (ref self '_dict) (norm k))))
  
  (define __iter__
    (lambda (self)
      ((make-generator
        (lambda (yield)
          (for-in ((k v : (ref self '_dict))) ()
		  (yield (renorm k) v)))))))
             
  (define pop
    (lambda (self k . l)
      (apply pylist-pop! (ref self '_dict) (norm k) l)))
  
  (define clear
    (lambda (self)
      (py-clear (ref self '_dict))))
  
  (define get
    (lambda (self key . l)
      (apply py-get (ref self '_dict) (norm key) l)))
  
  (define __len__
    (lambda (self)
      (len (ref self '_dict))))
  
  (define __bool__
    (lambda (self)
      (bool (ref self '_dict))))
  
  (define __contains__
    (lambda (self x)
      (in (norm x) (ref self '_dict))))

  (define items
    (lambda (self)
      (for-in ((k v : (ref self '_dict))) ((l '()))
	      (cons (list (renorm k) v) l)
	      #:final (reverse l))))

  (define keys
    (lambda (self)
      (for-in ((k v : self)) ((l '()))
	      (cons (renorm k) l)
	      #:final
	      l)))

  (define values
    (lambda (self)
      (for-in ((k v : self)) ((l '()))
	      (cons v l)
	      #:final
	      l)))

  (define __repr__
    (lambda (self)
      (for-in ((k v : (ref self '_dict))) ((l '()))
	      (cons (format #f "~a:~a"
			    (print-scm k write) (print-scm v write))
		    l)
	      #:final
	      (aif it (ref (ref self '_dict) '__name__)
		   (format #f "Ns-~a: ~a" it (reverse l))
                (format #f "Ns: ~a" (reverse l))))))

  (define __str__
    (lambda (self)
      (for-in ((k v : (ref self '_dict))) ((l '()))
	      (cons (format #f "~a:~a"
			    (print-scm k display) (print-scm v display))
		    l)
	      #:final
	      (aif it (ref (ref self '_dict) '__name__)
		   (format #f "Ns-~a: ~a" it (reverse l))
		   (format #f "Ns: ~a" (reverse l))))))

  
  (define __getattr__
    (lambda (self key)
      (let ((r (ref (ref self '_dict) key fail)))
        (if (eq? r fail)
            (raise (AttributeError "No attribute ~a" key))
            r))))
  
  (define __init__
    (lambda (self d) (set self '_dict d)))))

(mkwrap dictNs  norm   renorm)
(mkwrap dictRNs renorm norm)

(set! (@@ (oop dict) dictNs)  dictNs)
(set! (@@ (oop dict) dictRNs) dictRNs)

(define-python-class weak-key-dict (object <py-hashtable>)
  (define __init__
    (letrec ((__init__
              (case-lambda
                ((self)
                 (let ((r (make-weak-key-hash-table)))
                   (slot-set! self 't    r)
                   (slot-set! self 'hash H)
                   (slot-set! self 'nn   0)))
                
                ((self x)
                 (__init__ self)
                 (if (is-a? x <py-hashtable>)
                     (hash-for-each
                      (lambda (k v)
                        (pylist-set! self k v))
                      (slot-ref x 't)))))))
      __init__)))

(define-python-class weak-value-dict (object <py-hashtable>)
  (define __init__
    (letrec ((__init__
              (case-lambda
                ((self)
                 (let ((r (make-weak-value-hash-table)))
                   (slot-set! self 't    r)
                   (slot-set! self 'hash H)
                   (slot-set! self 'nn   0)))

                ((self x)
                 (__init__ self)
                 (if (is-a? x <py-hashtable>)
                     (hash-for-each
                      (lambda (k v)
                        (pylist-set! self k v))
                      (slot-ref x 't)))))))
      __init__)))

(define (pyhash-listing)
  (let ((l (to-pylist
            (map symbol->string
                 '(__class__ __cmp__ __contains__ __delattr__
                             __delitem__ __doc__ __eq__ __format__
                             __ge__ __getattribute__ __getitem__
                             __gt__ __hash__ __init__ __iter__
                             __le__ __len__ __lt__ __ne__ __new__
                             __reduce__ __reduce_ex__ __repr__
                             __setattr__ __setitem__ __sizeof__
                             __str__ __subclasshook__
                             clear copy fromkeys get has_key
                             items iteritems iterkeys itervalues
                             keys pop popitem setdefault update
                             values viewitems viewkeys viewvalues)))))
    (pylist-sort! l)
    l))

(set! (@@ (oop dict) hash-for-each*)
  (lambda (f dict)
    (for-in ((k v : dict)) ()
	 (f k v))))

(define-method (py-class (o <hashtable>))    dict)
(define-method (py-class (o <py-hashtable>)) dict)

(set! (@ (oop dict) def-dict) dict)

(define-method (py-copy (o <py-list>))
  (to-pylist o))

(define-method (py-copy (o <pair>))
  o)

(define-method (py-copy (o <string>))
  o)

(define <g-dict> (ref dict '__goops__))

(define (dict-ackumulator)
  (let ((d (dict)))
    (with-stis-hash ((d : _ set))
      (case-lambda
       ((k v) (set k v))
       (()
	d)))))
    

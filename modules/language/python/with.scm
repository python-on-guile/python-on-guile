;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python with)
  #:use-module (language python try)
  #:use-module (language python yield)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:use-module (language python bool)
  #:use-module (language python for)
  #:use-module (language python list)
  #:use-module (language python bool)
  #:use-module (oop pf-objects)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  
  #:export (with enter exit))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-method (enter o)
  (pk '(no enter message) o)
  (raise (TypeError "no __enter__ member")))
(define-method (enter (o <yield>))
  (next o))
  
(define-method (enter (o <p>))
  (aif it (ref o '__enter__)
       (it)
       (next-method)))

(define-method (exit . l)
  (raise (TypeError "no __exit__ member")))

(define-method (exit (o <p>) . l)
  (aif it (ref o '__exit__)
       (apply it l)
       (next-method)))

(name-object enter)
(name-object exit)

(define-syntax with
  (syntax-rules ()
    ((_ () . code)
     (begin . code))
    ((_ (x . l) . code)
     (with0 x (with l . code)))))

(define-syntax-rule (keyerr o att code)
  (let ((r
         (catch #t
           (lambda ()
             code)
           (lambda x
             (raise (AttributeError
                     (format #f
                             "Could not find attribute ~a for object ~a"
                             'att o)))))))
    (if (not (procedure? r))
        (raise (AttributeError
                (format #f
                        "Could not find attribute ~a for object ~a"
                        'att o)))
        r)))

(define-syntax-rule (m code)
  (call-with-values
      (lambda () code)
    (case-lambda
      ((x) x)
      (x   x))))

(define-syntax with0
  (syntax-rules ()
    ((_ ((id ...) exp) . code)
     (let* ((e        exp)
            (enter-f  (keyerr e __enter__
                       (if (is-a? e <py>)
                           (ref-in-class e '__enter__)
                           (resolve-method-g
                            enter                            
                            (list (class-of e))))))
            
            (exit-f  (keyerr e __exit__
                      (if (is-a? e <py>)
                          (ref-in-class e '__exit__)
                          (resolve-method-g
                           exit
                           (list (class-of e) '_ '_ '_)))))
            
            (v       (m (enter-f e)))
            (hit     #f))
       (try
        (lambda ()
          (match (to-list v) ((id ...) . code)))
              
        (#:except #t =>
          (lambda (tag l)
            (set! hit #t)              
            (let ((type  (car   l))
                  (value (cadr  l))
                  (trace (caddr l)))
              (if (not (exit-f e type value trace))
                  (apply throw tag l)))))
          
        #:finally
        (lambda ()
          (if (not hit)
              (exit-f e None None None))))))

    ((_ (id exp) . code)
     (let* ((e        exp)
            (enter-f  (keyerr e __enter__
                       (if (is-a? e <py>)
                           (ref-in-class e '__enter__)
                           (resolve-method-g
                            enter
                            (list (class-of e))))))
            (exit-f  (keyerr e __exit__
                      (if (is-a? e <py>)
                          (ref-in-class e '__exit__)
                          (resolve-method-g
                           exit
                           (list (class-of e) '_ '_ '_)))))
            
            (v       (m (enter-f e)))
            (hit     #f))
       (try
        (lambda ()
          (let ((id v)) . code))
              
        (#:except #t =>
          (lambda (tag l)
            (set! hit #t)              
            (let ((type  (car   l))
                  (value (cadr  l))
                  (trace (caddr l)))
              (if (not (bool (exit-f e type value trace)))
                  (apply throw tag l)))))
          
        #:finally
        (lambda ()
          (if (not hit)
              (exit-f e None None  None))))))
         
    ((_ (exp) . code)
     (with0 (id exp) . code))))

  

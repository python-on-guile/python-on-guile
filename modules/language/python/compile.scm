;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python compile)
  #:use-module (language python librarys)
  #:use-module (ice-9 match)  
  #:use-module (ice-9 format)
  #:use-module (ice-9 control)
  #:use-module (oop pf-objects)
  #:use-module (language python type)
  #:use-module (oop goops)
  #:use-module ((oop dict) #:select (set-procedure-property!- slask-it))
  #:use-module (system syntax internal)
  #:use-module (system syntax)
  #:use-module (rnrs bytevectors)
  #:use-module ((language python guilemod) #:select
                ())
  #:use-module (language python adress)
  #:use-module (language python match)
  #:use-module (language python dict)
  #:use-module (language python exceptions)
  #:use-module (language python yield)
  #:use-module (language python for)
  #:use-module (language python for2)
  #:use-module (language python try)
  #:use-module (language python list)
  #:use-module (language python string)
  #:use-module (language python bytes)
  #:use-module (language python number)
  #:use-module (language python def)
  #:use-module (language python module)
  #:use-module (language python dir)
  #:use-module (language python procedure)
  #:use-module (language python bool)
  #:use-module (language python fastlib)
  #:use-module (language python matrix)
  #:use-module (language python inspect)
  #:use-module ((language python format2) #:select (fnm (format . fmt)))
  #:use-module ((language python with) #:select ())
  #:use-module (ice-9 pretty-print)

  #:export (python-comp exit-fluid exit-prompt pks do-pr o_a *main* do_not_warn
                        Ni Mi Filei- Modulei- Ni-f Filei-f Modulei-f))

(define-syntax-rule (aif it p . x) (let ((it p)) (if it . x)))

(define do-pr #t)

(define (pr . x)
  (if do-pr
      (let ()
        (define port (open-file "log.txt" "a"))
        (with-output-to-port port
          (lambda ()
            (pretty-print (syntax->datum x))))
        (close port)))
  (car (reverse x)))

(define (pf x)
  (define port (open-file "compile.log" "a"))
  (with-output-to-port port
    (lambda () (pretty-print (syntax->datum x)) x))
  (close port)
  x)

(define (pp . x)
  (pretty-print (syntax->datum x))
  (car (reverse x)))

(define-inlinable (C   x) `(@@ (language python compile) ,x))
(define-inlinable (F2  x) `(@@ (language python format2) ,x))
(define-inlinable (N   x) `(@@ (language python number)  ,x))
(define-inlinable (Y   x) `(@@ (language python yield)   ,x))
(define-inlinable (T   x) `(@@ (language python try)     ,x))
(define-inlinable (Ty  x) `(@@ (language python type)    ,x))
(define-inlinable (F   x) `(@@ (language python for)     ,x))
(define-inlinable (E   x) `(@@ (language python exceptions) ,x))
(define-inlinable (L   x) `(@@ (language python list)    ,x))
(define-inlinable (S   x) `(@@ (language python string)  ,x))
(define-inlinable (Fa  x) `(@@ (language python fastlib) ,x))
(define-inlinable (B   x) `(@@ (language python bytes)   ,x))
(define-inlinable (Se  x) `(@@ (language python set)     ,x))
(define-inlinable (D   x) `(@@ (language python def)     ,x))
(define-inlinable (Di  x) `(@@ (language python dict)    ,x))
(define-inlinable (Mo  x) `(@@ (language python module)  ,x))
(define-inlinable (O   x) `(@@ (oop pf-objects)          ,x))
(define-inlinable (G   x) `(@  (guile)                   ,x))
(define-inlinable (H   x) `(@  (language python hash)    ,x))
(define-inlinable (W   x) `(@  (language python with)    ,x))

(define (NRR name n m f x)
  (set-procedure-property!- x '__file__  f)
  (set-procedure-property!- x 'line      (car  n))
  (set-procedure-property!- x 'column    (cadr n))
  (set-procedure-property!- x 'line2     (car  m))
  (set-procedure-property!- x 'column2   (cadr m))
  (if name (set-procedure-property! x 'name name))
  
  (set-procedure-property!-- x 'filename f)
  (set-procedure-property!-- x 'line      (car  n))
  (set-procedure-property!-- x 'column    (cadr n))
  (set-procedure-property!-- x 'line2     (car  m))
  (set-procedure-property!-- x 'column2   (cadr m))
  (if name (set-procedure-property!-- x 'name name))
  x)

(define (unexp exp l)
  (let lp ((l l))
    (define (f x)
      (define (g x)
        (match x
          ((#:identifier x . _)
           (string->symbol x))))
      
      (match x
        ((x . #f)
         (let ((x (g x)))
           (cons x x)))
        ((x . y)
         (cons (g x) (g y)))))
        
    (match l      
      ((x . l)
       (cons (f x) (lp l)))
      (x x))))

(define (unexp2 l)
  (let lp ((l l))
    (match l
      ((#:identifier x . _)
       (string->symbol x))
      ((x . l)
       (cons (lp x) (lp l)))
      (x x))))

(define (stmt-wrap e ni mi)
 `(,(C 'with-ni) ,ni ,mi ,e))


(define get-macro-names
  (case-lambda
    ((as mod)
     (get-macro-names as #f mod))
    ((as p? mod)
     (catch #t
       (lambda ()
         (let ((M (__import__ mod)))
           (for ((k : M)) ((l '()))
                (let ((v (ref M (string->symbol k))))
                  (if (macro? v)
                      (if p?
                          (let* ((mod (if as (if (symbol? as)
                                                as
                                                (string->symbol as))
                                          mod))
                                 (s
                                  (string->symbol
                                   (string-append
                                    (if (symbol? mod) (symbol->string mod) mod)
                                    (string-append "." k)))))
                            (cons (cons (string->symbol k) s) l))
                          (let ((s (string->symbol k)))
                            (cons (cons s s) l)))
                      l))
         
                #:final l)))
       (lambda x
         '())))))
         
(define (fslask x) x)
(define ann-fluid (make-fluid #f))

(define (boolit-op- n op x y)
  (values
   (memq (syntax->datum op) '(fast-equal-mac? eq? equal? < <= > >=))
   (let ((op (syntax->datum op)))
     (cond         
      ((eq? op '<)      #`(<            #,x #,@y))
      ((eq? op '<=)     #`(<=           #,x #,@y)) 
      ((eq? op '>)      #`(>            #,x #,@y))
      ((eq? op '>=)     #`(>=           #,x #,@y))
      ((eq? op 'eq?)    #`(eq?          #,x #,@y))
      ((eq? op 'fast-equal-mac?) #`(fast-equal-mac?       #,x #,@y))
      ((eq? op 'equal?) #`(fast-equal-mac?       #,x #,@y))
      (else
       #f)))))

(define (boolit-op1 n op x y)
  (call-with-values (lambda () (boolit-op- n op x y))
    (lambda (x y) x)))

(define (boolit-op2 n op x y)
  (call-with-values (lambda () (boolit-op- n op x y))
    (lambda (x y) y)))

(define (andit-op- n op x y)
  (values
   (memq (syntax->datum op) '(orx andx))
   (let ((op (syntax->datum op)))
     (cond         
      ((eq? op 'orx)    #`(orx    #,n + #,@y))
      ((eq? op 'andx)   #`(andx   #,n + #,@y))
      (else
       #f)))))

(define (andit-op1 n op x y)
  (call-with-values (lambda () (andit-op- n op x y))
    (lambda (x y) x)))

(define (andit-op2 n op x y)
  (call-with-values (lambda () (andit-op- n op x y))
    (lambda (x y) y)))

(define-syntax test-it
  (lambda (x)
     (syntax-case x (@ @@)
      ((_ #t n ((@ _ op) x . y) cont)
       (boolit-op1 #'n #'op #'x #'y)
       #`(if #,(boolit-op2 #'n #'op #'x #'y)
             cont
             #f))

      ((_ #t #f ((@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(if #,(andit-op2 #f #'op #'x #'y)
             cont
             #f))

      ((_ #t #t ((@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(let ((r #,(andit-op2 #t #'op #'x #'y)))
           (if (boolit #f r)
               cont
               r)))

      ((_ #t n ((@@ _ op) x . y) cont)
       (boolit-op1 #'n #'op #'x #'y)
       #`(if #,(boolit-op2 #'n #'op #'x #'y)
             cont
             #f))

      ((_ #t #f ((@@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(if #,(andit-op2 #f #'op #'x #'y)
             cont
             #f))

      ((_ #t #t ((@@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(let ((r #,(andit-op2 #t #'op #'x #'y)))
           (if (boolit #f r)
               cont
               r)))

      ((_ #t #t x cont)
       #'(let ((xx x))
           (if (boolit #f xx)
               cont
               xx)))
      
      ((_ #t #f x cont)
       #'(if (boolit #f x)
             cont
             #f))
      
      ((_ #f n ((@ _ op) x . y) cont)
       (boolit-op1 #'n #'op #'x #'y)
       #`(if #,(boolit-op2 #'n #'op #'x #'y)
             #t
             cont))

      ((_ #f #f ((@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(if #,(andit-op2 #f #'op #'x #'y)
             #t
             cont))

      ((_ #f #t ((@ _ op) x . y) cont)
       (andit-op1 #'n #'op #'x #'y)
       #`(let ((r #,(andit-op2 #t #'op #'x #'y)))
           (if (boolit #f r)
               r
               cont)))

      ((_ #f #t x cont)
       #'(let ((xx x))
           (if (boolit #f xx)
               xx
               cont)))

      ((_ #f #f x cont)
       #'(if (boolit #f x)
             #t
             cont)))))

(define-syntax orx
  (lambda (x)
    (syntax-case x (+)
      ((_ _ +)
       #f)

      ((_ +)
       #f)

      ((_ + . l)
       #'(orx #t + . l))
        
      ((_ n + x)
       #'(boolit n x))
    
      ((_ n + x . y)
       #'(test-it #f n x (orx n + . y))))))

(define-syntax andx
  (lambda (x)
     (syntax-case x (+)
      ((_ _ +)
       #t)
    
      ((_ +)
       #t)
    
      ((_ + . l)
       #'(andx #t + . l))
    
      ((_ n + x)
       #'(boolit n x))
        
      ((_ n + x . l)
       #'(test-it #t n x (andx n + . l))))))

(define-syntax boolit
  (lambda (x)
     (syntax-case x (@ and eq? equal? or not < <= > >= andx orx)
      ((_ n (and  . l))   #'(andx n + . l))
      ((_ n (or   . l))   #'(orx  n + . l))
      ((_ n (not    x))   #'(not x))
      ((_ n (<      x y)) #'(<  x y))
      ((_ n (<=     x y)) #'(<= x y))
      ((_ n (>      x y)) #'(>  x y))
      ((_ n (>=     x y)) #'(>= x y))
      ((_ n (eq?    x y)) #'(eq? x y))
      ((_ n (equal? x y)) #'(equal? x y))

      ((_ n ((@ A Equal?) x y))
       (eq? (syntax->datum #'Equal?) 'equal?)
       #'(-equal? x y))

      ((_ n ((@@ A Equal?) x y))
       (eq? (syntax->datum #'Equal?) 'equal?)
       #'(-equal? x y))

      ((_ n ((@  A Eq?   ) x . y))
       (boolit-op1 #'n #'Eq? #'x #'y)
       (boolit-op2 #'n #'Eq? #'x #'y))

      ((_ n ((@@  A Eq?   ) x . y))
       (boolit-op1 #'n #'Eq? #'x #'y)
       (boolit-op2 #'n #'Eq? #'x #'y))

      ((_ n ((@  A And   ) x . y))
       (andit-op1 #'n #'And #'x #'y)
       (andit-op2 #'n #'And #'x #'y))

      ((_ n ((@@  A And   ) x . y))
       (andit-op1 #'n #'And #'x #'y)
       (andit-op2 #'n #'And #'x #'y))

      
      ((_ n ((@  A Not   ) x  ))
       (eq? (syntax->datum #'Not) 'not)
       #'(not x))

      ((_ n ((@@  A Not   ) x  ))
       (eq? (syntax->datum #'Not) 'not)
       #'(not x))

      ((_ _  #t)  #t)
      ((_ _  #f)  #f)
      ((_ #f  0)  #f)
      ((_ #t  0)  0)
      ((_ #f "")  #f)
      ((_ #t "")  "")
      ((_ #f x )
       (let ((xx (syntax->datum #'x)))
         (cond
          ((number? xx)
           #t)
          ((string? xx)
           #t)
          (else
           #'(bool- x)))))
      
      ((_ #t x )
       #'x))))

(define-syntax source-in-stx?
  (lambda (x)
    (syntax-case x ()
      ((_ code1 code2)
       (if (pk (module-defined? (resolve-module '(system syntax)) 'syntax-sourcev))
           #'code1
           #'code2)))))

(define (nr-verbatim? stx)
  (syntax-case stx (@@)
    (((@@ (language python compile) x) . l)
     (if (member (syntax->datum #'x) '(NR- NR-- with-ni))
         #t
         #f))
    
    ((x . l)
     (if (member (syntax->datum #'x) '(NR- NR-- with-ni))
         #t
         #f))
    (_
     #f)))
      
(cond-expand
 (guile-2.2
  (define-syntax NR-
    (lambda (x)
      (syntax-case x ()
        ((_ name (n nn ...) (m mm ...) y)
         #t
         (source-in-stx?
          ;; New in 3.1
          (let* ((xx (if (syntax? x) (syntax-expression x) #f))
                 (yy (if xx (source-properties xx) #f)))
            
            (define newstx
              (let lp ((stx #'y))
                (let ((v (if (syntax? stx) (syntax-sourcev stx) #f)))
                  (define (mk datum)
                    (datum->syntax stx datum
                                   #:source
                                   (vector
                                    (if v
                                        (vector-ref v 0)
                                        (fluid-ref
                                         (@@ (system base compile) %in-file)))
                                    (syntax->datum #'n)
                                    (syntax->datum #'m))))
                  (cond
                   ((nr-verbatim? stx)
                    stx)
                   ((pair? stx)
                    (cons (lp (car stx)) (lp (cdr stx))))
                   (else
                    (if (syntax? stx)                          
                        (mk (syntax-expression stx))
                        stx))))))

            (with-syntax ((yyy newstx))
              #`(NRR name (list n nn ...) (list m mm ...)
                     #,(if yy (aif it
                                   (assoc 'filename yy) (cdr it)
                                   "python-shell")
                           "python-shell")
                     yyy)))

          ;; 2.2 and 3.0
          (let* ((xx (if (syntax? x) (syntax-expression x) #f))
                 (yy (if xx (source-properties xx) #f)))
            #`(NRR name (list n nn ...) (list m mm ...)
                   #,(if yy (aif it
                                 (assoc 'filename yy) (cdr it)
                                 "python-shell")
                         "python-shell")
                   y))))))))
  
 (guile-2.0
  (define-syntax NR
    (lambda (x)
      (syntax-case x ()
        ((_ name line col stx)
         #t
         #'stx))))))

(define (putte x) x #;(pk 'Nr-- (syntax->datum x) x))
(define (dutte x) x #;(pk 'qset (syntax->datum x) x))

(cond-expand
 ((or guile-2.2 guile-3.0)
  (define-syntax NR--
    (lambda (x)
      (putte
      (syntax-case x ()
        ((_ (n nn ...) (m mm ...) y)
         #t
         (source-in-stx?
          ;; New in 3.1
          (let lp ((stx #'y))
            (let ((v (if (syntax? stx) (syntax-sourcev stx) #f)))
              (define (mk datum)
                (datum->syntax stx datum
                               #:source
                               (vector
                                (if v
                                    (vector-ref v 0)
                                    (fluid-ref
                                     (@@ (system base compile) %in-file)))
                                (syntax->datum #'n)
                                (syntax->datum #'m))))
              (cond
               ((nr-verbatim? stx)
                stx)
               ((pair? stx)
                (cons (lp (car stx)) (lp (cdr stx))))
               (else
                (if (syntax? stx)                          
                    (mk (syntax-expression stx))
                    stx)))))

          ;; 2.2 and 3.0
          #'y)))))))
 
 (guile-2.0
  (define-syntax NR--
    (lambda (x)
      (syntax-case x ()
        ((_ line col stx)
         #t
         #'stx))))))

(define (get-pos y)
  (aif it (object-property y 'position)
       (match it
         ((a b c d . _)
          (list a b c d))
         (_ (list 0 0 0 0)))
       (list 0 0 0 0)))
 
(cond-expand
  (guile-3.0
   (define (not-inline x) (slask-it x)))
  (guile-2.0
   (define-syntax not-inline (syntax-rules () ((_ x) x)))))

(define in-dec   (make-fluid #f))
(define class-nm (make-fluid ""))

(define-syntax-rule (mk/ec x) x)

(define (letec-f f) (let/ec x (f x)))

(define-syntax  letec
  (lambda (x)
    (syntax-case x ()
      ((_ f)
       (if (and (=  (major-version) 3)
		(=  (minor-version) 0)
		(<  (micro-version) 7))
	   #'(letec-f f)
	   #'(let/ec xx (f xx)))))))
(define-syntax-rule (letec f)
  (let/ec x (f x)))

(define-syntax-rule (let/ecx c a ...)
  (letec (lambda (c) a ...)))

(define-syntax-rule (let/ect c a ...)
  (letec (lambda (c) a ...)))

(eval-when (expand eval load)
  (if (equal? (effective-version) "3.0")
      (module-set! (current-module) 'let/ecx
                   (module-ref (current-module) 'let/ect))))

(define (to-matcher vs x)
  (match x
    ((#:power #f (#:tuple . l) . _)
     (map (lambda (x) (to-matcher vs x)) l))
    (x (exp vs x))))

(define (flatten x)
  (if (pair? x)
      (append (flatten (car x)) (flatten (cdr x)))
      (if (null? x)
          '()
          (list x))))

(define exit-prompt (make-prompt-tag))
(define exit-fluid  (make-fluid #f))

(define (formatter . x) "<missing>")

(define (get-class-name name)
  (let ((pre- (fluid-ref class-nm)))
    (if (equal? pre- "")
        name                 
        (string->symbol
         (string-append
          pre-
          "."
          (symbol->string
           name))))))

(define (mk-string vs l)
  (define (mk-string2 x)
    (if (string? x)
        x
        (let ((l (let lp ((l x))
                   (match l
                     ((x . l)
                      (cons
                       (if (string? x)
                           x
                           (match x
                             ((#:field tag a b)
                              `(,(C 'formatter) ,(exp vs tag) ,a ,b))))
                       (lp l)))
                     (() '())))))
          (match l
            ((x) x)
            ((x . l) (cons* '+ x l))))))
                              
  (let ((r
         (let lp ((l l))
           (match l
             ((x . l)
              (let ((x (mk-string2 x))
                    (l (lp l)))
                (if (and (string? x) (= (length l) 1) (string? (car l)))
                    (list (+ x (car l)))
                    (cons x l))))
             (() (list ""))))))
    (if (string? r)
        r
        (cons '+ r))))
  
             

(define-syntax-rule (with-exit code ...)
  (syntax-parameterize ((var-define
                           (syntax-rules ()
                             ((_ v x)
                              (define! 'v (if (module-defined?
                                               (current-module) 'v)
                                              (module-ref (current-module) 'v)
                                              x))))))
   (with-fluids ((exit-fluid #t))
     (call-with-prompt exit-prompt
       (lambda ()
         (catch
	  #t
	  (lambda ()
	    (call-with-values (lambda () code ...)
	      (case-lambda
	       ((x)
		(if (eq? x None)
		    (values)
		    x))
	       (x x))))
		
           (lambda (tag . l)
             (if (eq? tag 'python)
                 (begin
                   (when (issubclass (car l) SystemExit)
                     (let ((arg (ref (cadr l) 'value)))
                       ((@ (guile) primitive-exit) arg)))
                 
                   ((@ (guile) format) #t
                    "~%Caught python exception ~%~%   ~a: ~a~%"
                    (rawref (car l) '__name__) (ref (cadr l) 'value)))
                 
                 (if (string? (car l))
                     ((@ (guile) format) #t
                      "~%Caught scheme exception ~%   ~a~%"
                      (apply (@ (guile) format) #f (car l) (cdr l)))
                     (if (and (pair? (cdr l)) (string? (cadr l)))
                         ((@ (guile) format) #t
                          "~%Caught scheme exception ~%    ~a~%"
                          (apply (@ (guile) format) #f
                                 (cadr l) (cddr l)))
                         ((@ (guile) format) #t
                          "~%Caught scheme exception ~%    ~a~%"
                          l))))
             (values))))
       (lambda (k val)
         (unless (zero? val)
           (format #t "exit with error ~a~%" val)))))))

(define (get-exported-symbols name)
  (catch #t
    (lambda ()
      (or (and=> (and=> (resolve-module name) module-public-interface)
                 (lambda (interface)
                   (module-map (lambda (name var) name)
                               interface)))
          '()))
    (lambda x '())))


(define cvalues (G 'values))

(define-syntax-rule (wth code)
  (let ((old s/d))
    (set! s/d (C 'qset!))
    (let ((r code))
      (set! s/d old)
      r)))


(define-syntax use-modules--
  (lambda (x)
    (define (keyword-like? stx)
      (let ((dat (syntax->datum stx)))
        (and (symbol? dat)
             (eqv? (string-ref (symbol->string dat) 0) #\:))))
    (define (->keyword sym)
      (symbol->keyword (string->symbol (substring (symbol->string sym) 1))))
    
    (define (quotify-iface args)
      (let loop ((in args) (out '()))
        (syntax-case in ()
          (() (reverse! out))
          ;; The user wanted #:foo, but wrote :foo. Fix it.
          ((sym . in) (keyword-like? #'sym)
           (loop #`(#,(->keyword (syntax->datum #'sym)) . in) out))
          ((kw . in) (not (keyword? (syntax->datum #'kw)))
           (syntax-violation 'define-module "expected keyword arg" x #'kw))
          ((#:renamer renamer . in)
           (loop #'in (cons* #'renamer #:renamer out)))
          ((kw val . in)
           (loop #'in (cons* #''val #'kw out))))))

    (define (quotify specs)
      (let lp ((in specs) (out '()))
        (syntax-case in ()
          (() (reverse out))
          (((name name* ...) . in)
           (and-map symbol? (syntax->datum #'(name name* ...)))
           (lp #'in (cons #''((name name* ...)) out)))
          ((((name name* ...) arg ...) . in)
           (and-map symbol? (syntax->datum #'(name name* ...)))
           (with-syntax (((quoted-arg ...) (quotify-iface #'(arg ...))))
             (lp #'in (cons #`(list '(name name* ...) quoted-arg ...)
                            out)))))))
    
    (syntax-case x ()
      ((_ spec ...)
       (with-syntax (((quoted-args ...) (quotify #'(spec ...))))
         #'(eval-when (expand)
             (process-use-modules (list quoted-args ...))
             *unspecified*))))))

(define-syntax use-modules-
  (lambda (x)
    (define (keyword-like? stx)
      (let ((dat (syntax->datum stx)))
        (and (symbol? dat)
             (eqv? (string-ref (symbol->string dat) 0) #\:))))
    (define (->keyword sym)
      (symbol->keyword (string->symbol (substring (symbol->string sym) 1))))
    
    (define (quotify-iface args)
      (let loop ((in args) (out '()))
        (syntax-case in ()
          (() (reverse! out))
          ;; The user wanted #:foo, but wrote :foo. Fix it.
          ((sym . in) (keyword-like? #'sym)
           (loop #`(#,(->keyword (syntax->datum #'sym)) . in) out))
          ((kw . in) (not (keyword? (syntax->datum #'kw)))
           (syntax-violation 'define-module "expected keyword arg" x #'kw))
          ((#:renamer renamer . in)
           (loop #'in (cons* #'renamer #:renamer out)))
          ((kw val . in)
           (loop #'in (cons* #''val #'kw out))))))

    (define (quotify specs)
      (let lp ((in specs) (out '()))
        (syntax-case in ()
          (() (reverse out))
          (((name name* ...) . in)
           (and-map symbol? (syntax->datum #'(name name* ...)))
           (lp #'in (cons #''((name name* ...)) out)))
          ((((name name* ...) arg ...) . in)
           (and-map symbol? (syntax->datum #'(name name* ...)))
           (with-syntax (((quoted-arg ...) (quotify-iface #'(arg ...))))
             (lp #'in (cons #`(list '(name name* ...) quoted-arg ...)
                            out)))))))
    
    (syntax-case x ()
      ((_ spec ...)
       (with-syntax (((quoted-args ...) (quotify #'(spec ...))))
         #'(eval-when (eval load)
	    (process-use-modules (list quoted-args ...))
	    *unspecified*))))))

(define (remove-tilde x)
  (let lp ((l (string->list x)) (r '()))
    (if (pair? l)
        (if (eqv? (car l) #\~)
            (lp (cdr l) (cons #\% r))
            (lp (cdr l) (cons (car l) r)))
        (list->string (reverse r)))))

(define-syntax-rule (use l a ...)
  (begin
    (eval-when (expand compile)
      (catch #t
        (lambda ()          
          (use-modules a ...)          
          (values))
        (const #f)))
    (eval-when (eval load)
      (catch #t
        (lambda ()
          (use-modules a ...)
          (values))
        (lambda x
          ;;(backtrace)
          (catch #t
            (lambda ()
              (raise (ImportError
                      (remove-tilde
                       ((@ (ice-9 format) format)
                        #f "failed to import '~a' ~a" '(a ...) x)))))
            (lambda xx
              (raise
               (ImportError
                ((@ (ice-9 format) format)
                 #f "failed to import '~a'" '(a ...)))))))))))

(define level (make-fluid 0))

(define (flat x)
  (let lp ((x (list x)))
    (if (pair? x)
        (let ((e (car x)))
          (if (pair? e)
              (let ((ee (car e)))
                (if (equal? ee '(@ (guile) cons))
                    (append (lp (list (cadr  e)))
                            (lp (list (caddr e)))
                            (lp (cdr x)))
                    (lp (cdr x))))
              (if (symbol? e)
                  (cons e (lp (cdr x)))
                  '())))
        '())))
                    
(define s/d (C 'qset!))

(define (pre) (warn "Patching guile will lead to way better experience use 'python.patch' on guile-2.2 e.g. (use-modules (language python guilemod))"))

(define (dont-warn v)
  (values))

(define (dont-warn* v)
  (values))

(define-syntax call
  (syntax-rules ()
    ((_ (f) . l) (f . l))))

(define (fold f init l)
  (if (pair? l)
      (fold f (f (car l) init) (cdr l))
      init))


(define (gv x)
  (if (equal? x '_)
      (gensym "_")
      x))

(define (unwrap-match vs op x)
  (let lp ((x x) (n 0))
    (match x
      ((#:test (#:power #f (#:tuple . l) . _) . _)
       (lp l 0))

      ((#:test (#:power #f (#:list . l) . _) . _)
       (lp l 0))
      
      ((#:test . l)
       (list #:set (make-set vs op x '())))
      
      (((#:starexpr _ _ x . _) . l)
       (if (= n 1)
           (raise (SyntaxError "two stared expressions in assignement"))
           (cons
            (list #:* (lp x 1))
            (lp l (+ n 1)))))

      ((#:identifier x)
       (string->symbol x))
      
      ((x . l)
       (cons (lp x 0) (lp l n)))
      
      (x x))))

(define (explode x)
  (match x
    (((#:test (#:power #f (#:tuple . l) . _) . _))
     l)

    (((#:test (#:power #f (#:list . l) . _) . _))
     l)

    (l l)))

(define (gen-sel vs e item fail ll else)
  (match e
    (#f item)
    ((#:cfor for-e in-e cont)
     (let lp ((for-e for-e))
       (match for-e
         (((#:sub l))
          `(,(F 'for-in-py) ((,@(map (lambda (x) (gv ((g vs exp) x))) l)
			: ,(exp vs in-e))) ,ll
                        ,(gen-sel vs cont item fail '() '(#f))
			,@else))

	  (_
	   `(,(F 'for-in-py) ((,@(map (lambda (x) (gv ((g vs exp) x))) for-e)
			 : ,(exp vs in-e))) ,ll
			 ,(gen-sel vs cont item fail '() '(#f))
			 ,@else)))))
        
    ((#:cif cif cont)
     `(,(G 'if) ,(exp vs cif)          
         ,(gen-sel vs cont item fail ll else)
         ,fail))))

(define (union as vs)
  (let lp ((as as) (vs vs))
    (match as
      ((x . as)
       (if (member x vs)
           (lp as vs)
           (lp as (cons x vs))))
      (()
       vs))))

(define (diff as vs)
  (let lp ((as as) (rs '()))
    (match as
      ((x . as)
       (if (member x vs)
           (lp as rs)
           (lp as (cons x rs))))
      (()
       rs))))

(define (intersect as vs)
  (let lp ((as as) (rs '()))
    (match as
      ((x . as)
       (if (member x vs)
           (lp as (cons x rs))
           (lp as rs)))
      (()
       rs))))

(define (member-s x l)
  (if (pair? x) (set! x (car x)))
  
  (let lp ((l l))
    (match l
      (((y) . l)
       (if (equal? y x)
           #t
           (lp l)))
      ((y . l)
       (if (equal? y x)
           #t
           (lp l)))
      (() #f))))

(define (get-globals code)
  (let lp ((vs (glob code '())) (rs (scope #t code '())))
    (match vs
      ((x . l)
       (if (member-s x rs)
           (lp l rs)
           (lp l (cons x rs))))
      (()
       rs))))

(define (glob x vs)
  (match x
    (((or #:nonlocal #:global) . l)
     (let lp ((l l) (vs vs))
       (match l
         (((#:identifier v . _) . l)
          (let ((s (string->symbol v)))            
            (if (member s vs)
                (lp l vs)
                (lp l (cons s vs)))))
         (()
          vs))))
    (((or #:def #:classdef) . l)
     vs)

    ((#:import (#:from (() () . nm) . #f))
     (let* ((xl (map (lambda (nm) (exp vs nm)) nm))
            (l  `(language python module ,@xl)))
       (union vs (get-exported-symbols l))))

    ((#:import (#:from (() () . nm)  l))
     ;; Make sure to load the module in
     (let* ((xl   (map (lambda (nm) (exp vs nm)) nm))
            (ss   (find-symbols xl l)))
       (union (map cdr ss) vs)))
    
    ((#:import (#:from (".")  l))
     (let* ((u    (get-current-module))
            (u    (reverse (list-cdr-ref (reverse (u-it u)) (length '()))))
            (xl   (append u (map (lambda (nm) (exp vs nm)) '())))
            (ss   (find-symbols xl l)))
       (union vs (map cdr ss))))

    
    ((#:import (#:from (("." . nn) . nm)  l))
     ;; Make sure to load the module in
     (let* ((u    (get-current-module))
            (u    (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
            (xl   (append u (map (lambda (nm) (exp vs nm)) (cdr nm))))
            (ss   (find-symbols xl l)))
       (union vs (map cdr ss))))
    
    ((x . y)
     (glob y (glob x vs)))
    (x vs)))

(define (folder f start . l)
  (let lp ((l l) (s start))
    (if (and-map pair? l)
        (lp
         (map cdr l)
         (let ((ll (map car l)))
           (apply f s ll)))
        s)))

(define get-module-macro-defs
  (case-lambda
    ((dots ids as)
     (get-module-macro-defs dots ids as #f))
    
    ((dots ids as p?)
     (define (getsym x)
       (match x
         ((#:identifier x . _)
          x)))

     (folder
      (lambda (s dots ids as)
        (union
         s
         (folder
          (lambda (s dots ids as)
            (union
             s
             (let* ((u   (get-current-module))
                    (u   (if (null? dots)
                             '()
                             (reverse (list-cdr-ref
                                       (reverse (u-it u))
                                       (- (length dots) 1)))))
                   
                    (path (append (if (null? dots) '() u)
                                  (map getsym ids)))
                       
                    (ml  (get-macro-names as p? (module->py-module path))))
               (map cdr ml))))
          s dots ids as)))
      '()
      dots ids as))))
    

(define scope
  (case-lambda
    ((x vs)
     (scope #f x vs))
    ((p x vs)
     (let scope ((x x) (vs vs))
       (match x
         ((#:wal id x)
          (scope x (cons (exp vs id) vs)))
         
         ((#:del . x)
          (let lp ((x x) (vs vs))
            (if (pair? x)
                (match x
                  ((#:power #f base () . #f)
                   (lp (cdr x) (union vs (list (exp vs base)))))
                  (_
                   (lp (cdr x) vs)))
                vs)))
   
  
         ((#:def f . _)
          (if p
              (union (list (list (exp '() f))) vs)
              (union (list (exp '() f)) vs)))

         ((#:lambdef . _)
          vs)

         ((#:comma a)
          (scope a vs))

         ((#:comma a . l)
          (union
           (scope a vs)
           (scope (cons #:comma l) vs)))

    
         ((#:with (l ...) code)
          (scope code (union vs
                             (let lp ((l l))
                               (match l
                                 (((a b) . l)
                                  (union (flatten (to-matcher '() b)) (lp l)))
                                 ((x . l) (lp l))
                                 (() '()))))))
    
         ((#:classdef f . _)
          (if p
              (union (list (list (exp '() f))) vs)
              (union (list (exp '() f)) vs)))
          
         (((or #:global #:nonlocal) . _)
          vs)
         
         ((#:import (#:name ((dots ids ...) . as) ...) ...)
          (union
           (get-module-macro-defs dots ids (unexp2 as) #t)
           (let lp ((ids ids) (as as) (vs vs))
             (if (pair? ids)
                 (let lp2 ((ids2 (car ids)) (as2 (car as)) (vs vs))
                   (if (pair? as2)
                       (lp2 (cdr ids2) (cdr as2)
                            (let ((as2  (car as2))
                                  (ids2 (car ids2)))
                              (union vs (list (exp '()
                                                   (if as2 as2 (car ids2)))))))
                       (lp (cdr ids) (cdr as) vs)))
                 vs))))
  
         ((#:import (#:from (() () . nm)  l))
          ;; Make sure to load the module in
          (let* ((xl   (map (lambda (nm) (exp vs nm)) nm))
                 (ss   (find-symbols xl l)))
            (if p
                (union vs (map list (map cdr ss)))                
                (union vs (map cdr  ss)))))
                

         ((#:import (#:from (".")  l))
          (let* ((u    (get-current-module))
                 (u    (reverse (list-cdr-ref (reverse (u-it u)) (length '()))))
                 (xl   (append u (map (lambda (nm) (exp vs nm)) '())))
                 (ss   (find-symbols xl l)))
            (if p
                (union vs (map cdr ss))
                (union vs (map cdr ss)))))
         
         ((#:import (#:from (("." . nn) . nm)  l))
          ;; Make sure to load the module in
          (let* ((u    (get-current-module))
                 (u    (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
                 (xl   (append u (map (lambda (nm) (exp vs nm)) (cdr nm))))
                 (ss   (find-symbols xl l)))
            (if p
                (union vs (map cdr ss))
                (union vs (map cdr ss)))))

         ((#:expr-stmt l (#:assign-typed u v w))
          (scope (list #:expr-stmt l (list #:assign v)) vs))
         
         ((#:expr-stmt l (#:assign u ... v))
          (union
           (fold (lambda (l s)
                   (union
                    s
                    (fold (letrec
                              ((f
                                (lambda (x s)
                                  (match x
                                    ((#:test (#:power #f
                                                      (#:tuple . l) . _) . _)
                                     (union (fold f vs l) s))

                                    ((#:test (#:power #f
                                                      (#:list . l) . _) . _)
                                     (union (fold f vs l) s))
                                      
                               
                                    ((#:test (#:power v2 v1 () . _) . _)
                                     (if v2
                                         (union
                                          (union (flat (exp '() v1))
                                                 (flat (exp '() v2)))
                                          s)
                                         (union (flat (exp '() v1)) s)))
                              
                                    ((#:starexpr #:power _ v1 . _)
                                     (union
                                      (flat (exp '() v1))
                                      s))
                              
                                    (_ s)))))
                            f)
                          
                          '()              
                          l)))     
                 '()
                 (cons l u))
          vs))

         ((#:for es in code . final)
          (let ((vs (union
                     vs
                     (let lp ((es es))
                       (match es
			 (((#:sub . l) . u)
                          (union (lp l) (lp u))) 
                         (((#:power #f (#:tuple . l) . _) . u)
                          (union (lp l) (lp u)))
                         (((and (#:power . _) x) . u)
                          (union (list (exp vs x)) (lp u)))
                         ((e . es)
                          (union (lp e) (lp es)))
                         (() '()))))))
            (scope final (scope code vs))))
         
         ((#:expr-stmt l (#:assign-typed u v w))
          (scope (list #:expr-stmt l (list #:assign v)) vs))
         
         ((#:expr-stmt l (#:assign k . u))
          (union
           (union (fold (lambda (x s)
                          (match x
                            ((#:test (#:power v2 v1 () . _) . _)
                             (if v2
                                 (union
                                  (union (flat (exp '() v1))
                                         (flat (exp '() v2)))
                                  s)
                                 (union (flat (exp '() v1)) s)))
                            (_ s)))
                        '()              
                        l)
                  vs)
           (scope `(#:expr-stmt ,k (#:asignvs . ,u)) vs)))
         
         ((x . y)
          (scope y (scope x vs)))
         (_ vs))))))
     
(define ignore (make-fluid '()))

(define (defs x vs)
  (match x
    ((#:def  (#:identifier f) . _)
     (union (list (string->symbol f)) vs))

    ((#:classdef  (#:identifier f . _) . _)
     (union (list (string->symbol f)) vs))

    ((#:import (#:name ((dots ids ...) . as) ...) ...)
     (union
      (get-module-macro-defs dots ids (unexp2 as) #t)
      (let lp ((ids ids) (as as) (vs vs))
        (if (pair? as)
            (lp (cdr ids) (cdr as)
                (let ((as  (car as))
                      (ids (car ids)))
                  (union vs (list (exp '() (if as as (car ids)))))))
            vs))))

    ((#:import (#:from (() () . nm)  l))
     ;; Make sure to load the module in
     (let* ((xl  (map (lambda (nm) (exp vs nm)) nm)))
       (let ((ss (find-symbols xl l)))
         (union vs (map cdr ss)))))

    ((#:import (#:from (("." . nn) . nm)  l))
     ;; Make sure to load the module in
     (let* ((u   (get-current-module))
            (u   (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
            (xl  (append u (map (lambda (nm)
                                  (exp vs (if (pair? nm)
                                              (cdr nm)
                                              nm)))
                                nm))))
       (let ((ss (find-symbols xl l)))
         (union vs (map cdr ss)))))

    ((#:import (#:from (".")  l))
     ;; Make sure to load the module in
     (let* ((u   (get-current-module))
            (u   (reverse (list-cdr-ref (reverse (u-it u)) 0)))
            (xl  (append u (map (lambda (nm) (exp vs (cdr nm))) '()))))
     
       (let ((ss (find-symbols xl l)))
         (union vs (map cdr ss)))))

    ((x . y)
     (defs y (defs x vs)))
    
    (_ vs)))

(define (gen-yield f)
  (string->symbol
   (string-append
    (symbol->string f)
    ".yield")))

(define (g vs e)
  (lambda (x) (e vs x)))

(define return (make-fluid 'error-return))

(define-syntax-rule (<< x y) (ash x y))
(define-syntax-rule (>> x y) (ash x (- y)))

(define-syntax-rule (mkfast ((a) v) ...)
  (let ((h (make-hash-table)))
    (hash-set! h 'a v)
    ...
    h))

(define (fast-ref x)
  (and=> (assoc x `((__class__ . ,(O 'py-class)))) cdr))

(define fasthash
  (mkfast
   ;; General
   ((__init__)    (O 'py-init))
   ((__ne__)      (O 'ne))
   ((__eq__)      (Fa 'fast-equal?))
   ((__repr__)    (O 'repr-))
   
   ;;iterators
   ((__iter__)      (F 'wrap-in))
   ((__next__)      (F 'next))
   ((__send__)      (Y 'send))
   ((throw)         (Y 'sendException))
   ((__close__)     (Y 'sendClose))
   
   ;; Numerics
   ((__index__)    (N 'py-index))
   ((__add__ )     (N '+))
   ((__mul__ )     (N '*))
   ((__matmul__ )  (N 'py-matmul))
   ((__sub__ )     (N '-))
   ((__radd__ )    (N 'r+))
   ((__rmul__ )    (N 'r*))
   ((__rmatmul__ ) (N 'py-rmatmul))
   ((__rsub__ )    (N 'r-))
   ((__neg__ )     (N '-))
   ((__le__  )     (N '<))
   ((__lt__  )     (N '<=))
   ((__ge__  )     (N '>))
   ((__gt__  )     (N '>=))
   ((__abs__ )     (N 'py-abs))
   ((__pow__ )     (N 'expt))
   ((__rpow__ )    (N 'rexpt))
   ((__truediv__)  (N 'py-/))
   ((__rtruediv__) (N 'py-r/))
   ((__and__)      (N 'py-logand))   
   ((__or__)       (N 'py-logior))
   ((__xor__)      (N 'py-logxor))
   ((__rand__)     (N 'py-rlogand))   
   ((__ror__)      (N 'py-rlogior))
   ((__rxor__)     (N 'py-rlogxor))
   ((__divmod__)   (N 'py-divmod))
   ((__rdivmod__)  (N 'py-rdivmod))
   ((__invert__)   (N 'py-lognot))
   ((__int__)      (N 'mk-int))
   ((__float__)    (N 'mk-float))
   ((__lshift__)   (N 'py-lshift))
   ((__rshift__)   (N 'py-rshift))
   ((__rlshift__)  (N 'py-rlshift))
   ((__rrshift__)  (N 'py-rrshift))
   ((bit_length)   (N 'py-bit-length))
   ((as_integer_ratio) (N 'py-as-integer-ratio))
   ((conjugate)    (N 'py-conjugate))
   ((denominator)  (N 'py-denominator))
   ((numerator)    (N 'py-numerator))
   ((to_bytes)     (N 'py-to-bytes))
   ((fromhex)      (B 'py-fromhex))
   ((hex)          (B 'py-hex))
   ((imag)         (N 'py-imag))
   ((is_integer)   (N 'py-is-integer))
   ((real)         (N 'py-real))
   ((__mod__)      (N 'py-mod))
   ((__rmod__)     (N 'py-rmod))
   ((__floordiv__) (N 'py-floordiv))
   ((__rfloordiv__)(N 'py-rfloordiv))
   ((__hex__)      (N 'py-hex))
   
   ;; Lists
   ((append)       (Fa 'fastlist-append!))
   ((count)        (L  'pylist-count))
   ((extend)       (L  'pylist-extend!))
   ((index)        (L  'pylist-index))
   ((pop)          (Fa 'fastlist-pop!))
   ((insert)       (L  'pylist-insert!))
   ((remove)       (L  'pylist-remove!))
   ((reverse)      (L  'pylist-reverse!))
   ((sort)         (L  'pylist-sort!))
   ((__len__)      (Fa 'fastlist-len))
   ((__contains__) (Fa 'fastlist-in))
   ((__delitem__)  (L  'pylist-delete!))
   ((__delslice__) (L  'pylist-delslice))
   ((__setitem__)  (Fa 'fastlist-set!))
   ((__getitem__)  (Fa 'fastlist-get))
   
   ;; String
   ((format)     (S  'py-strformat))
   ((format_map) (S  'py-format-map))
   ((capitalize) (S  'py-capitalize))
   ((center)     (S  'py-center ))
   ((endswith)   (Fa 'fast-endswith))
   ((expandtabs) (S  'py-expandtabs))
   ((find)       (S  'py-find   ))
   ((rfind)      (S  'py-rfind  ))
   ((isalnum)    (S  'py-isalnum))
   ((isalpha)    (S  'py-isalpha))
   ((isdigit)    (S  'py-isdigit))
   ((islower)    (S  'py-islower))
   ((isspace)    (S  'py-isspace))
   ((isupper)    (S  'py-isupper))
   ((istitle)    (S  'py-istitle))
   ((isidentifier) (S 'py-identifier?))
   ((join)       (S  'py-join   ))
   ((ljust)      (S  'py-join   ))
   ((rljust)     (S  'py-rljust ))
   ((lower)      (S  'py-lower  ))
   ((upper)      (S  'py-upper  ))
   ((lstrip)     (S  'py-lstrip ))
   ((rstrip)     (S  'py-rstrip ))
   ((partition)  (S  'py-partition))
   ((replace)    (S  'py-replace))
   ((strip)      (S  'py-strip  ))
   ((title)      (S  'py-title  ))
   ((rpartition) (S  'py-rpartition))
   ((rindex)     (S  'py-rindex ))
   ((split)      (S  'py-split  ))
   ((rsplit)     (S  'py-rsplit ))
   ((splitlines) (S  'py-splitlines))
   ((startswith) (Fa 'fast-startswith))
   ((removeprefix) (S 'py-removeprefix))
   ((removesuffix) (S 'py-removesuffix))
   ((swapcase)   (S  'py-swapcase))
   ((translate)  (S  'py-translate))
   ((zfill)      (S  'py-zfill))
   ((encode)     (S  'py-encode))
  
   ;;Bytevectors   
   ((decode)     (B 'py-decode))
   ((isascii)    (B 'py-isascii))
  
   ;;DICTS
   ((copy)       (Di 'py-copy))
   ((fromkeys)   (Di 'py-fromkeys))
   ((get)        (Fa 'fast-get))
   ((has_key)    (Di 'py-has_key))
   ((items)      (Di 'py-items))
   ((iteritems)  (Di 'py-iteritems))
   ((iterkeys)   (Di 'py-iterkeys))
   ((itervalues) (Di 'py-itervalues))
   ((keys)       (Di 'py-keys))
   ((values)     (Di 'py-values))
   ((popitem)    (Di 'py-popitem))
   ((setdefault) (Di 'py-setdefault))
   ((update)     (Di 'py-update))
   ((clear)      (Fa 'fast-clear!))
   ((__hash__)   (Fa 'fast-hash))))
  

(define (fastfkn x) (hash-ref fasthash x))

(define (get-kwarg vs arg)
  (let lp ((arg arg))
    (match arg
      (((#:* a) . arg)
       (cons `(* ,(exp vs a)) (lp arg)))
      (((#:** a) . arg)
       (cons `(** ,(exp vs a)) (lp arg)))
      (((#:= a b) . arg)
       (cons `(= ,(exp vs a) ,(exp vs b)) (lp arg)))
      ((x . arg)
       (cons (exp vs x) (lp arg)))
      (()
       '()))))

(define (getarg x)
  (match x
    ((#:tp x . l)
     x)
    (x x)))

(define (get-args_ vs arg)  
  (let lp ((arg arg))
    (match arg
      (((#:arg x) . arg)
       (cons (exp vs (getarg x))
             (lp arg)))
      ((x . args)
       (lp args))
      
      (()
       '()))))

(define (get-args= vs arg)
  (let lp ((arg arg))
    (match arg
      (((#:= x v) . arg)
       (cons (list '= (exp vs (getarg x)) (exp vs v))
             (lp arg)))
      
      ((x . args)
       (lp args))
      
      (()
       '()))))

(define (get-args* vs arg)
  (let lp ((arg arg))
    (match arg
      (((#:* x) . arg)
       (cons (list '* (exp vs (getarg x)))
             (lp arg)))
      
      ((x . args)
       (lp args))
      
      (()
       '()))))

(define (get-args** vs arg)
  (let lp ((arg arg))
    (match arg
      (((#:** x) . arg)
       (cons (list '** (exp vs (getarg x)))
             (lp arg)))

      ((x . args)
       (lp args))
      
      (()
       '()))))

(define (kw->li dict)
  (for ((k v : dict)) ((l '()))
    (cons* v (symbol->keyword (string->symbol k)) l)
    #:final
    (reverse l)))

(define (arglist->pkw  l)
  (let lp ((l l) (r '()))
    (if (pair? l)
        (let ((x (car l)))
          (if  (keyword? x)
               (list (G 'cons) `(,(G 'list) ,@(reverse r)) `(,(G 'list) ,@l))
               (lp (cdr l) (cons x r))))
        (list (G 'cons)  `(,(G 'list) ,@(reverse r)) `(,(G 'quote) ())))))

(define (get-addings vs x fast?)
  (match x
    (() '())
    ((x . l)
     (let ((is-fkn? (match l
                      ((#f) #t)
                      (((#:arglist . _) . _)
                       #t)
                      (_
                       #f))))
       
       (cons
        (match x
          ((#:identifier . _)
           (let* ((tag     (exp vs x))
                  (xs      (gensym "xs"))
                  (fast    (fastfkn tag))
                  (is-fkn? (and=> (and fast? is-fkn? fast)
                                  (lambda (it)
                                    `(#:call-obj ,it)))))
             (cond
              (is-fkn? is-fkn?)
              ((and fast? fast)
               `(#:fastfkn-ref ,fast (,(G 'quote) ,tag)))
              ((and fast? (fast-ref tag)) =>
               (lambda (it)
                 `(#:fast-id ,it (,(G 'quote) ,tag))))
              (else
               `(#:identifier (,(G 'quote) ,tag))))))
          
          ((#:arglist args)
           `(#:apply ,@(get-kwarg vs args)))
          
          ((#:subscripts (n #f #f))
           `(#:vecref ,(exp vs n)))
        
          ((#:subscripts (n1 n2 n3))
           (let ((w (lambda (x) (if (eq? x None) (E 'None) x))))
             `(#:vecsub            
               ,(w (exp vs n1)) ,(w (exp vs n2)) ,(w (exp vs n3)))))
        
          ((#:subscripts (n #f #f) ...)
           `(#:array-ref ((@ (guile) list) ,@(map (lambda (n)
                                                    (exp vs n))
                                                  n))))
        
          ((#:subscripts (n1 n2 n3) ...)
           (let ((w (lambda (x) (if (eq? x None) (E 'None) x))))
             `(#:array-ref
               ((@ (guile) list)
                ,@(map (lambda (x y z)
                         `((@ (guile) vector)
                           ,(exp vs x) ,(exp vs y) ,(exp vs z)))
                       n1 n2 n3)))))
        
          (_ (error "unhandled addings")))
        (get-addings vs l fast?))))))


(define (sv? x)
  (define (ref-sv? x)
    (syntax-case x ()
      ((a ... (x . _))
       (member (syntax->datum #'x)
               '(#:fastfkn-ref
                 #:call-obj
                 #:call
                 #:apply))
       #f)
      (_
       #t)))

  (syntax-case x (@ @@)
    ((x  . _)
     (one-expr #'x)
     #t)
    
    ((x  . _)
     (or
      (not (syntax? #'x))
      (member
       (syntax->datum #'x)
       '(+ - * / @ @@ < > <= >= and or not)))
      #t)
    (((@ _ x) . _)
     (member (syntax->datum #'x) '(+ - * / @ @@ < > <= >= and or not logior lognao logxor logan modulo))
     #t)
    (((@@ _ x) . _)
     (member (syntax->datum #'x) '())
     #t)
    (((@@ _ x) v . l)
     (eq? (syntax->datum #'x) 'ref-x)
     (ref-sv? #'l))
    ((_ . _)
     #f)
    (_ #t)))

(define wrapper-f
  (case-lambda
    ((x) x)
    (()  None)
    (x   x)))

(define-syntax setwrap
  (lambda (x)
    (syntax-case x ()
      ((_ u)
       (if (sv? #'u)
           #'u
           #'(call-with-values (lambda () u) wrapper-f))))))
               

(define (make-set vs op x u)
  (define (tr-op op)
    (match op
      ("+="  (N 'fast+=))
      ("-="  (N 'fast-=))
      ("*="  (N 'fast*=))
      ("@="  (N '@=))
      ("/="  (N 'fast/=))
      ("%="  (N 'fast%=))
      ("&="  (N 'fast&=))
      ("|="  (N 'fast-or=))
      ("^="  (N 'fast^=))
      ("**=" (N 'fast**=))
      ("<<=" (N 'fast<<=))
      (">>=" (N 'fast>>=))
      ("//=" (N 'fast//=))))
  
  (match x
    ((#:verb x) x)
    ((#:test (#:power kind v addings . _) . _)
     (let* ((v       (exp vs v))
            (fast?   (not (eq? v 'super)))
            (addings (get-addings vs addings fast?))
            (p.a     (match kind
                       (#f (cons #f '()))
                       ((v add)
                        (cons (exp vs v) add))))
            (p      (car p.a))
            (pa     (cdr p.a))
            (pa     (get-addings vs pa fast?)))
       (define q (lambda (x) `',x))
       (if kind
           (if (not p)
               (if (null? addings)                   
                   (if op
                       `(,(C 'assign=) ,(tr-op op) ,v ,@u)
                       `(,(C 'assign=) ,s/d        ,v ,@u))
                   (if op
                       `(,(C 'oper) ,(tr-op op) ,v ,addings ,@u)
                       `(,s/d ,(exp vs kind)
                              (,(C 'fset-x) ,v ,addings ,@u))))
               
               (let ((pre (if (equal? p v)
                              (let lp ((pa pa) (ad addings) (r '()))
                                (if (and (pair? pa) (pair? ad))
                                    (let ((px (car pa)) (ax (car ad)))
                                      (if (equal? px ax)
                                          (lp (cdr pa) (cdr ad)
                                              (cons px r))
                                          #f))
                                    (if (pair? pa)
                                        #f
                                        (reverse r))))
                              #f)))
                 (if (null? addings)                   
                     (if op
                         `(,(tr-op op) ,v ,@u)
                         `(,s/d ,v ,@u))
                     (if op
                         `(,(C 'oper2) ,(tr-op op) ,v ,pre ,p ,pa ,addings ,@u)
                         `(,(C 'set-x) ,v ,pre ,p ,pa ,addings ,@u)))))
     
           (if (null? addings)
               (if op
                   `(,(tr-op op) ,v ,@u)
                   `(,s/d ,v ,@u))
               (if op
                   `(,(C 'oper) ,(tr-op op) ,v ,addings ,@u)
                   `(,(C 'set-x) ,v ,addings ,@u))))))))


(define is-class? (make-fluid #f))
(define (gen-yargs vs x)
  (match x
    ((#:list args)
     (map (g vs exp) args))))

(define inhibit-finally #f)
(define decorations (make-fluid '()))
(define tagis (make-hash-table))

(define (lr as)
  (lambda (vs x)
    (define (eval p a b) ((cdr (assoc p as)) a b))
    (define (expit x)
      (match x
	((#:e e) e)
	(x (exp vs x))))     
    (let lp ((x x))
      (match x
       ((p a b)
	(if (assoc p as)
	    (match b
	      ((q c d)
	       (if (assoc q as)
		   (lp (list q (list #:e (lp (list p a c))) d))
		   (eval p (expit a) (expit b))))
	      (_ (eval p (expit a) (expit b))))
	    (expit x)))
       (_ (expit x))))))
    
(define (mklr x)
  (lambda (a b)
    (list x a b)))

(define-syntax fmod
  (lambda (x)
    (syntax-case x ()
      ((fmod s a)
       (let ((t (get-type-num #'s)))
	 (cond
	  ((and (< t 2) (one-expr #'a))
	   #'(modulo s a))
	  (else
	   #'(let ((ss s) (aa a))
	       (cond
		((string? ss) (fmt ss aa))
		((and (number? ss) (number? aa))
		 (modulo ss aa))
		(else
		 (py-mod ss aa)))))))))))

(define (f% s a)
  (let ((ss s))
    (cond
     ((string? ss)
      (list (F2 'format) ss a))
     (else
      (list (C  'fmod  ) ss a)))))

(define lr+ (lr	`((#:+ . ,(mklr (G '+))) (#:-  . ,(mklr (G '-))))))
(define lr* (lr `((#:* . ,(mklr (G '*))) (#:/  . ,(mklr (N 'fast/)))
                  (#:@ . ,(mklr (N 'py-matmul)))
		  (#:% . ,f%)            (#:// . ,(mklr (N 'fast//))))))

(define lr-or  (lr `((#:bor  . ,(mklr (N 'fast-or))))))
(define lr-and (lr `((#:band . ,(mklr (N 'fast&))))))
(define lr-xor (lr `((#:bxor . ,(mklr (N 'fast^))))))

(define-syntax-parameter Modulei- (lambda (x) "__main__"))
(define-syntax-parameter Filei-   (lambda (x) ""))
(define-syntax-parameter Ni  (lambda (x) 0))
(define-syntax-parameter Mi  (lambda (x) 0))
(define-syntax-parameter Ni2 (lambda (x) 0))
(define-syntax-parameter Mi2 (lambda (x) 0))
(define-syntax-rule (with-ni N M Code)
  (syntax-parameterize ((Ni (lambda (x) N))
                        (Mi (lambda (x) M)))
    (NR-- (N N) (M M) Code)))

(define Ni-f      (make-fluid 0))
(define Filei-f   (make-fluid ""))
(define Modulei-f (make-fluid ""))
(define-syntax-rule (gen-table x vs (tag code ...) ...)
  (begin
    (hash-set! tagis tag
               (lambda (x vs n m n2 m2)
                 (syntax-parameterize
                  ((Ni  (lambda (x) #'n))
                   (Mi  (lambda (x) #'m))
                   (Ni2 (lambda (x) #'n2))
                   (Mi2 (lambda (x) #'m2)))
                  (match x code ...))))
                   
    ...))

(define *doc* (make-fluid #f))
(define (get-doc)
  (or (fluid-ref *doc*) ""))
(define set-doc
  (case-lambda
    (()  (fluid-set! *doc* #f))
    ((x)
     (unless (fluid-ref *doc*)
       (fluid-set! *doc* x)))))

(define (u-it m)
  (if (>= (length m) 1)
      (reverse (cdr (reverse m)))
      '()))

(define (tr-comp op x y)
  (match op
    ((or "<" ">" "<=" ">=")
     (list (string->symbol op) x y))
    ("!="    (list (G 'not) (list (Fa 'fast-equal-mac?) x y)))
    ("=="    (list (Fa 'fast-equal-mac?) x y))
    ("is"    (list (G 'eq?) x y))
    ("isnot" (list (G 'not) (list (G 'eq?)    x y)))
    ("in"    (list (Fa 'fastlist-in) x y))
    ("notin" (list (G 'not) (list (Fa 'fastlist-in) x y)))
    ("<>"    (list (G 'not) (list (Fa 'fast-equal-mac?) x y)))))

(define (find-symbols mod syms)
  (define (get-name x)
    (define (f a)
      (if (symbol? a)
          a
          (exp '() a)))
    
    (match x
      ((a . #f)
       (cons (f a) (f a)))
      ((a . b)
       (cons (f a) (f b)))
      (x (let ((x (f x))) (cons x x)))))

  (set! syms (syntax->datum syms))  

  (let lp ((l (map get-name syms)) (rr '()))
    (if (pair? l)
	(let* ((x      (car l))
	       (l      (cdr l)))
	  (lp l (cons x rr)))
	(reverse rr))))

(define (-import- x)
  (let lp ((ll (cdr x)) (m (__import__ x)))
    (if (pair? ll)
	(lp (cdr ll) (ref m (car ll)))
	m)))

(define-syntax @@values
  (lambda (x)
    (syntax-case x ()
      ((_ stx a m mm)
       (let ((l (map car (get-macro-names #f (syntax->datum #'mm)))))
	 (if (pair? l)
	     #'(eval-when (expand)	     
		(@@@values #t stx a m mm))

	     (with-syntax ((aa (let lp ((ll #'a))
				 (if (pair? ll)
				     (if (memq (car (syntax->datum (car ll))) l)
					 (lp (cdr ll))
					 (cons (car ll) (lp (cdr ll))))
				     '()))))
	      #'(eval-when (eval load)
		  (@@@values #f stx aa m mm)))))))))

(define-syntax-rule (@@values-mac stx a m mm)
  (@@@values #t stx a m mm))


(define-syntax @@@values
  (lambda (x)
    (syntax-case x ()
      ((_ do-macro? stx (a ...) m mm)
       (let* ((ll (find-symbols
		   (syntax->datum #'mm)
		   (syntax->datum #'(a ...)))))
         
         (define (f xx)
           (let lp ((xx xx))
             (syntax-case xx ()
               ((x . y)
                (cons (lp #'x) (lp #'y)))
               (() #'())
               (s
                (datum->syntax #'stx
                               (if (symbol? #'s)
                                   #'s
                                   (syntax->datum #'s)))))))

         (with-syntax ((mod (module->py-module
                             (syntax->datum #'mm))))
		      
	   (add-lib-map (syntax->datum #'mod) #t)
		      
	   (if (pair? ll)	       
               (with-syntax ((((bb . cc) ...) (f ll)))
		 (if #'do-macro?
		     (catch #t
		       (lambda ()
			 (let ((M (__import__ (syntax->datum #'mod))))
			   (let lp ((bs (syntax->datum #'(bb ...)))
				    (cs (syntax->datum #'(cc ...))))
			     (match (cons bs cs)
			       (((b . bs) . (c . cs))
				(let ((v (ref M b)))
				  (when (macro? v)
				    (module-set! (current-module) c v))
				  (lp bs cs)))
			       (() #'(values))))))
		       (lambda x #'(values)))

		     #'(catch #t
			 (lambda ()
			   (let ((M (__import__ mod)))
			     (let ((v (ref M 'bb)))
			       (when (not (macro? v))
				 (set! cc v)))
			     ...))
                         
			 (lambda x
			   (raise
			    ImportError x '(m #:select ((bb . cc) ...)))))))
               #'(values))))))))


(gen-table x vs
 (#:power  
  ((_ _ (x) () . #f)
   (exp vs x))
  
  ((_ _ x () . #f)
   (exp vs x))

  ((_ _ (#:identifier "sum") ((#:arglist ((#:comp x (and e (#:cfor . _))
						  . _) . _) . _) . _) . _)
   (let ((s (gensym "s")))
     (gen-sel vs e
	      `((@ (guile) +) ,s ,(exp vs x))
	      s
	      `((,s 0))
	      `(#:final ,s))))
   
  ((_ #f vf trailer . **)
   (let* ((vf      (exp vs vf))
          (fast?   (not (eq? vf 'super))))
     (define (pw x)
       (if **
           `(,(N 'expt) ,x ,(exp vs **))
           x))
     (pw
      (let ((trailer (get-addings vs trailer fast?)))
        `(,(C 'ref-x) ,vf ,@trailer))))))
 
 (#:identifier
  ((#:identifier x . _)
   (string->symbol x)))

 (#:decorated
  ((_ (l ...))
   (with-fluids ((in-dec #t))
     (fluid-set! decorations (map (g vs exp) l)))
   `(,cvalues)))
 
 (#:string
  ((_ l)
   (mk-string vs l)))

 (#:bytes
  ((_ l)
   (let* ((b (make-bytevector (length l))))
     (let lp ((l l) (i 0))
       (if (pair? l)
           (begin
             (bytevector-u8-set! b i (car l))
             (lp (cdr l) (+ i 1)))
           `(,(B 'bytes) ,b))))))
           
     
 (#:+
  (x
   (lr+ vs x)))
 
 (#:-
  (x
   (lr+ vs x)))
 
 (#:*
  (x
   (lr* vs x)))

 (#:@
  (x
   (lr* vs x)))
 
 (#:/
  (x
   (lr* vs x)))
 
 (#:%
  (x
   (lr* vs x)))
 
 (#://
  (x
   (lr* vs x)))

 (#:<<
  ((_ . l)
   (cons (N 'fast<<) (map (g vs exp) l))))
 
 (#:>> 
  ((_ . l)
   (cons (N 'fast>>) (map (g vs exp) l))))

 (#:u~
  ((_ x)
   (list (N 'fast~) (exp vs x))))

 (#:u-
  ((_ x)

   (list '- (exp vs x))))

 (#:u+
  ((_ x)
   (list '+ (exp vs x))))

 (#:band
  (x (lr-and vs x)))
 
 (#:bxor
  (x (lr-xor vs x)))
 
 (#:bor
  (x (lr-or vs x)))
    
 (#:not
  ((_ x)
   (list (G 'not) (list (C 'boolit) #f (exp vs x)))))
 
 (#:or
  ((_ . x)
   (cons* (C 'orx)  '+ (map (lambda (x) (exp vs x)) x))))
    
 (#:and
  ((_ . x)
   (cons* (C 'andx) '+ (map (lambda (x) (exp vs x)) x))))
    
 (#:test
  ((_ e1 #f)
   (exp vs e1))

  ((_ e1 (e2 #f))
   (list (G 'if) (list (C 'boolit) #f (exp vs e2)) (exp vs e1) (C 'None)))

  ((_ e1 (e2 e3))
   (list (G 'if) (list (C 'boolit) #f (exp vs e2)) (exp vs e1) (exp vs e3))))

 (#:del
  ;;We don't delete variables
  ((_  . l)
   `(,(G 'begin)
      ,@(let lp ((l l))
          (match l
            (((#:power #f base () . #f) . l)
             (cons `(set! ,(exp vs base) #f)
                   (lp l)))
   
  
            (((#:power #f base (l ... fin) . #f) . ll)
             (let* ((f     (exp vs base))
                    (fast? (not (eq? f 'super)))
                    (add   (get-addings vs l fast?))
                    (fin   (get-addings vs (list fin) fast?)))
               (cons
                `(,(C 'del-x) (,(C 'ref-x) ,f ,@add) ,@fin)
                (lp ll))))
            (() '()))))))

 (#:with
  ((_ (l ...) code)
   (let* ((l (map (lambda (x)
                    (match x
                      ((a b) (list (exp vs b) (gensym "as") (exp vs a)))
    
                      ((b)   (list (exp vs b)))))
                  l))
          (vs (union vs (let lp ((l l))
                          (match l
                            (((x) . l) (lp l))
                            (((a b c) . l) (cons a (lp l)))
                            (() '()))))))

     (define (f x)
       (match x
         ((a b c) (list 'set! a b))
         ((a) (list (G 'values)))))
     
     (define (g x)
       (match x
         ((a b c) (list b c))
         ((a)     (list a))))
     
     `(,(W 'with) ,(map g l)
       (,(G 'begin)
        ,@(map f l)
        ,(exp vs code))))))

 (#:wal
  ((_ id fkn)
   (let ((v (exp vs id))
         (w (gensym "temp")))
     `(,(G 'let) ((,w ,(exp (cons v vs) fkn)))
       (,(G 'set!) ,v ,w)
       ,w))))
 
 (#:if
  ((_ test a ((tests . as) ...) . else)
   (let* ((tests (cons test tests))
          (as    (cons a    as)))
     `(,(G 'cond)
       ,@(map (lambda (p a)
                (match p
                  ((#:wal it test)
                   (let* ((it2 (exp vs it))
                          (vs2 (cons it2 vs)))
                     (list (list (C 'boolit) #f (exp vs test)) '=>
                           (list (G 'lambda) (list it2) (exp vs2 a)))))
                  (test
                   (list (list (C 'boolit) #f (exp vs test))
                         (exp vs a)))))
              tests as)
       ,@(if else `((#t ,(exp vs else))) '())))))
    
 (#:suite
  ((_ #:stmt  . l) (cons (G 'begin)  (map (g vs exp) l)))
  ((_         . l) (cons (G 'begin)  (map (g vs exp) l))))

 (#:match
  ((_ . l)
   (with-fluids ((ann-fluid '()))
     (compile-match exp l vs))))
 
 (#:classdef
  ((_ class parents code)
   (let* ((name   (exp vs class))
          (class  (get-class-name name)))
     (with-fluids ((is-class? class)
                   (class-nm  (symbol->string class)))
      (let ()
        (define (clean l)
          (match l
            (((#:apply   . l). u) (append (clean l) (clean u)))
            (((`= x v ) . l) (cons* (symbol->keyword x) v (clean l)))
            ((x         . l) (cons x (clean l)))
            (() '())))
                
        (let* ((decor   (let ((r (fluid-ref decorations)))
                          (fluid-set! decorations '())
                          r))               
               (vo  vs)
               (vs  (union (list class) vs))
               (ns  (scope code '()))
               (ls  ns #;(diff ns vs))

               (parents (match parents
                          (() #f)
                          (#f #f)
                          ((#:arglist . _)
                           (get-addings vs (list parents) #f))))
               (cd.ann.doc  (with-fluids ((*doc*     #f)
                                          (ann-fluid (list name)))
                              (let ((cd (wth (exp vs code))))
                                (cons* cd
                                       (fluid-ref ann-fluid)
                                       (get-doc)))))
               (cd      (car  cd.ann.doc))
               (ann     (cdr (reverse (cadr cd.ann.doc))))
               (doc     (cddr cd.ann.doc)))
          `(,(C 'NR--) (,Ni ,Mi) (,Ni2 ,Mi2)
            (set! ,name
             (,(C 'class-decor) ,decor
              (,(C 'with-class) ,class
               (,(C 'mk-py-class2)
                ,class                
                ,(if parents
                    (arglist->pkw (clean parents))
                     `(,(G 'cons) (,(G 'quote) ()) (,(G 'quote) ())))
                ,doc
                ,(cons `(define __annotations__
                          (,(C 'mk-ann) ,@ann))
                       (map (lambda (x)
                              `(define ,x #f))
                            ls))
                ,cd)))))))))))
 (#:verb
  ((_ x) x))
 
 (#:scm
  ((_ (#:string _ s)) (with-input-from-string s read)))

 (#:comma
  ((_
    (and x
         (#:expr-stmt
          ((#:test
            (#:power #f (#:string l) () . #f)
            #f))
          (#:assign))))
   (set-doc (mk-string vs l))
   (exp vs x))
  
  ((_ a)
   (exp vs a))

  ((_ (and a
           (#:expr-stmt
            ((#:test
              (#:power #f (#:string ll) () . #f)
              #f))
            (#:assign))) . l)
   (set-doc (mk-string vs ll))
   `(,(G 'begin) ,(exp vs a) ,(exp vs (cons #:comma l))))
  
  ((_ a . l)
   `(,(G 'begin) ,(exp vs a) ,(exp vs (cons #:comma l)))))
  
 (#:import
  ((_ (#:from (() () . nm) . #f))
   (let* ((xl (map (lambda (nm) (exp vs nm)) nm))
          (l  `(language python module ,@xl))
          (as  (map (lambda (x) (cons x x))
                    (get-exported-symbols l))))
     `(,(C '@@values) *stx* ,as ,l ,xl)))
  
  ((_ (#:from (("." . nn) . nm) . #f))
   (let* ((u   (get-current-module))
          (u   (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
          (xl  (append u (map (lambda (nm) (exp vs (cdr nm))) nm)))
          (l  `(language python module ,@xl))
          (as  (map (lambda (x) (cons x x))
                    (get-exported-symbols l))))

     `(,(C '@@values) *stx* ,as ,l ,xl)))

  ((_ (#:from (".") ll))
   (let* ((u   (get-current-module))
          (u   (reverse (list-cdr-ref (reverse (u-it u)) 0)))
          (xl  (append u (map (lambda (nm) (exp vs (cdr nm))) '())))
          (l  `(language python module ,@xl))
          (ll  (map (lambda (a) (append l (list a)))
                    (map (lambda (x) (exp vs  (car x))) ll)))
          (as  (map (lambda (x) (cons x x))
                    (get-exported-symbols l))))

     `(,(C '@@values) *stx* ,as ,l ,xl)))
       
  ((_ (#:from ("." . nn) . #f))
   (let* ((nm  '())
          (u   (get-current-module))
          (u   (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
          (xl  (append u (map (lambda (nm) (exp vs nm)) (cdr nm))))
          (l  `(language python module ,@xl))
          (as  (map (lambda (x) (cons x x))
                    (get-exported-symbols l))))
     
     `(,(C '@@values) *stx* ,as ,l ,xl)))
  
  ((_ (#:from (() () . nm)  l))
   ;; Make sure to load the module in
   (let* ((xl  (map (lambda (nm) (exp vs nm)) nm))
          (ll `(language python module ,@xl)))
     `(,(C '@@values) *stx* ,(unexp exp l) ,ll ,xl)))

  ((_ (#:from (("." . nn) . nm)  l))
   ;; Make sure to load the module in
   (let* ((u   (get-current-module))
          (u   (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
          (xl  (append u (map (lambda (nm) (exp vs nm)) (cdr nm))))
          (ll  `(language python module ,@xl)))
     `(,(C '@@values) *stx* ,(unexp exp l) ,ll ,xl)))

  ((_ (#:from ("." . nn) l))
     ;; Make sure to load the module in
     (let* ((nm  '())
            (u   (get-current-module))
            (u   (reverse (list-cdr-ref (reverse (u-it u)) (length nn))))
            (xl  (append u (map (lambda (nm) (exp vs nm)) (cdr nm))))
            (ll `(language python module ,@xl)))
       `(,(C '@@values) *stx* ,(unexp exp l) ,ll ,xl)))

  
   ((_ (#:name ((dots ids ...) . as) ...) ...)
    `(,(G 'begin)
      ,@(map
         (lambda (dots ids as)
           `(,(G 'begin)
             ,@(map (lambda (dots ids as)
                      (let* ((u   (get-current-module))
                             
                             (u   (if (null? dots)
                                      '()
                                      (reverse (list-cdr-ref
                                                (reverse (u-it u))
                                                (- (length dots) 1)))))

                              (path (append (if (null? dots) '() u)
                                            (map (g vs exp) ids)))
                              
                              (ml  (get-macro-names
                                    (unexp2 as)
                                    #t
                                    (string-join
                                     (map symbol->string path) ".")))
                              
                              (l   (append '(language python module)
                                           path)))

                        `(,(G 'begin)
			  ,@
			  (if (pair? ml)                                   
			      `((,(C '@@values-mac) *stx* ,ml ,path ,l))
			      '())
                           
			  ,@
			  (if as
			      `((,(G 'begin)
				(,(Ty 'add-lib-map-mac)
				 ,path
				 ,(exp vs as))
				 
				(,(C 'qset!) ,(exp vs as)
				 (,(C '-import-) (,(G 'quote) ,path)))))
			                                     
                              `((,(G 'begin)
				(,(Ty 'add-lib-map-mac2) ,path)
				,(exp
				  vs
				  `(#:expr-stmt
				    ((#:test (#:power #f ,(car ids) ())))
				    (#:assign
				     ((#:verb
				       (,(G 'with-fluids)
					((,(Mo 'inmod) ,(exp '() (car ids))))
					((@ (language python module) __import__)
					 (,(G 'quote) ,path))))))))))))))
		    dots ids as)))
	 dots ids as))))
	       
 (#:for
  ((_ es in code . else)
   (let lp ((es es))
     (match es       
       (((#:power #f (#:tuple . l) . _))
        (lp l))

       (_            
        (let* ((es2   (map (g vs exp) es))
	       (ls2   (let ((l '()))
			(let lp ((es2 es2))
			  (match es2
			    ((a . b)     
			     (begin
			       (lp a)
			       (lp b)))
			    (() (values))
			    (a  (set! l (cons a l)))))
			(reverse l)))
	       
	       (ls3   (let lp ((l ls2))
			(if (pair? l)
			    (cons
			     (let ((x (car l)))
			       (if (eq? x '_)
				   x
				   (gensym "x")))
			     (lp (cdr l)))
			    '())))
	       
	       (ls4   (map cons ls2 ls3))
	       (es3   (let lp ((es2 es2))
			(match es2
			       ((a . b)
				(cons (lp a) (lp b)))

			       (() '())

			       (a (cdr (assoc a ls4))))))
	       (mi3   (if else
			  (map (lambda (x y) `(set! ,x ,y)) ls2 ls3)
			  '((@ (guile) values))))
			   
               (vs2   (union es2 vs))
               (code2 (exp vs2 code))
               (p     (is-ec #t code2 #t (list (C 'continue))))
               (else2 (if else (exp vs2 else) #f))
               (in2   (match in
                        ((in)     (list (exp vs in)))
                        ((in ...) (list `(,(G 'list)
                                          ,@ (map (g vs exp) in))))))
	       (min  (list
		      ((lambda (x y)
			 (if (pair? x)
			     (append x (cons ': y))
			     (cons* x ': y)))
		       (if else es3 es2) in2))))
	  (if p
	      (let ((code3 `(,(G 'begin)
			     (,(C 'with-sp)
			      ((continue (lambda (x)
					   (,(G 'syntax)
					    (-continue))))
			       
			       (break    (lambda (x)
					   (,(G 'syntax)
					    (,(F 'break))))))
			      ,@mi3
			      ,code2)
			     (,(G 'values)))))

		(if else2
		    `(,(F 'for-in-py) -continue ,min ()
		      ,code3
		      #:else
		      (lambda () ,else2))
		      
		    `(,(F 'for-in-py) -continue ,min ()
		      ,code3)))

		
	      (let ((code3 `(,(G 'begin)
			     (,(C 'with-sp)
			      ((break    (lambda (x)
					   (,(G 'syntax)
					    (,(F 'break))))))
			      ,@mi3
			      ,code2)
			     (,(G 'values)))))

		(if else2
		    `(,(F 'for-in-py) ,min ()		      
		      ,code3
		      #:else
		      (lambda () ,else2))
		    
		    `(,(F 'for-in-py) ,min ()
		      ,code3))))))))))
	      
          

 (#:sub
  ((_ l)
   (map (g vs exp) l)))

 (#:while
  ((_ test code . #f)
   (let* ((q     #f)
          (lp    (gensym "lp")))
     (define (wal f)
       (match test
         ((#:wal it test)
          (let* ((it  (exp vs it))
                 (vs2 (cons it vs)))
            `(,(G 'let) ((,it (,(C 'boolit) #t ,(exp vs test))))
              (,(G 'if) (,(C 'boolit) #f ,it) ,(f vs2)))))
         (_
          `(,(G 'if) (,(C 'boolit) #f ,(exp vs test))
            ,(f vs)))))
     
     (let* ((w (wal
                (lambda (vs)
                  (let* ((code2 (exp vs code))
                         (p     (is-ec #t code2 #t (list (C 'continue))))
                         (qq    (is-ec #t code2 #t (list (C 'break)))))
                    
                    (when qq
                      (set! q qq))
                    
                    (if p
                        `(,(G 'begin)
                          (,(C 'let/ecx) continue-ret
                           (,(C 'with-sp) ((continue (continue-ret)))           
                            ,code2))
                          (,lp))
                        `(,(G 'begin)
                          ,code2
                          (,lp))))))))
       
       (if q
           `(,(C 'let/ecx) break-ret
             (,(C 'with-sp) ((break (break-ret)))
              (,(G 'let) ,lp ()
               ,w)))
           `(,(G 'let) ,lp ()
             ,w)))))
           
     
  ((_ test code . else)
   (let* ((lp    (gensym "lp"))
          (q     #f)
          (else  (exp vs else)))
     
     (define (wal f)
       (match test
         ((#:wal it test)
          (let* ((it  (exp vs it))
                 (vs2 (cons it vs)))
            `(,(G 'let) ((,it (,(C 'boolit) #t ,(exp vs test))))
              (,(G 'if) (,(C 'boolit) #f ,it) ,(f vs2)))))
         (_
          `(,(G 'if) (,(C 'boolit) #t ,(exp vs test))
            ,(f vs)
            ,else))))

     (let* ((lp    (gensym "lp"))
            (w     (wal
                    (lambda (vs)
                      (let* ((code2 (exp vs code))
                             (p     (is-ec #t code2 #t (list (C 'continue))))
                             (qq    (is-ec #t code2 #t (list (C 'continue)))))
                        (when qq
                          (set! q qq))
              
                        (if p
                            `(,(G 'begin)
                              (,(C 'let/ecx) ,(C 'continue-ret)
                               (,(C 'with-sp) ((continue (continue-ret)))
                                ,code2))
                                  
                              (,lp))
                            `(,(G 'begin)
                               ,code2
                               (,lp))))))))
       (if q
           `(,(C 'let/ecx) break-ret
             (,(C 'with-sp) ((break (break-ret)))
              (,(G 'let) ,lp ()
               ,w)))
           `(,(G 'let) ,lp ()
             ,w))))))
 (#:try
  ((_ x (or #f ()) #f . fin)
   (if fin
       `(,(T 'try) (,(G 'lambda) () ,(exp vs x))
         #:finally (,(G 'lambda) () ,(exp vs fin)))
       `(,(T 'try) (,(G 'lambda) () ,(exp vs x)))))
    
  ((_ x exc else . fin)
   `(,(T 'try) (lambda () ,(exp vs x))     
     ,@(let lp ((exc exc) (r '()))
         (define (f x)
           (match x
             ((#:list . l)
              `((@ (guile) or) ,@(map (lambda (x) (exp vs x)) l)))
              (x (exp vs x))))
           
         (match exc
           ((((#f . #f) code) . exc)
            (lp exc (cons `(#:except #t ,(exp vs code)) r)))

           ((((test . #f) code) . exc)
            (lp exc (cons `(#:except ,(f test) ,(exp vs code)) r)))
           
           ((((test . as) code) . exc)
            (let ((l (gensym "l"))
                  (t (gensym "tag")))
              (lp exc
                  (cons
                   `(#:except ,(f test) => (,(G 'lambda)
                                            (,t ,l)
                                            (,(G 'let)
                                             ((,(exp vs as)
                                               (,(G 'cadr) ,l)))
                                             ,(exp vs code))))
                   r))))
           (()
            (reverse r))))
     
     ,@(if else `((#:else ,(exp vs else))) '())
     ,@(if fin `(#:finally (,(G 'lambda) () ,(exp vs fin))) '()))))
 
 (#:subexpr
  ((_ . l)
   (exp vs l)))
   
 (#:raise
  ((or (_ #f) (_ . #f))   
   `(,(T 'raise-stx)))
  
  ((_ code #f . l)
   (let ((c (gensym "c")))
     `(,(T 'raise)
       (,(G 'let) ((,c ,(exp vs code)))
        (,(G 'if) (,(O 'pyclass?) ,c)
         (,c ,@(map (lambda (x) (exp vs x)) l))
         ,c)))))
   
  ((_ code from . l)
   (let ()
     (if from
         (let ((o (gensym "o"))
               (c (gensym "c")))              
           `(,(T 'raise)
             (,(G 'let) ((,c ,(exp vs code)))
              (,(G 'let) ((,o (,(G 'if) (,(O 'pyclass?) ,c)
                               (,c ,@(map (lambda (x) (exp vs x)) l))
                               ,c)))
               (,(O 'set) ,o (,(G 'quote) __cause__) ,(exp vs from))
               ,o))))))))
             
      
 (#:yield
  ((_ (#:from x))
   (let ((y (gensym "y"))
         (f (gensym "f")))
     `(,(C 'yield-from-mac) ,(Y 'yield) ,(exp vs x))))
  
  ((_ args)
   `(,(C 'with-ni) ,Ni ,Mi
     (,(Y 'yield) ,@(if args
			(gen-yargs vs args)
			'()))))
  ((_ f args)
   `(,(G 'begin)
     (set! ,(C 'inhibit-finally) #t)
     (,(Y 'yield) ,@(if args (gen-yargs vs args) '())))))
 
 (#:def
  ((_ f
      (#:types-args-list . args)
      _
      code)
   (let* ((decor   (let ((r (fluid-ref decorations)))
                          (fluid-set! decorations '())
                          r))
          (arg_ (get-args_ vs args))
	  (arg= (get-args= vs args))
	  (dd=  (map cadr arg=))
          (c?   (fluid-ref is-class?))
          (f    (exp vs f))
          (y?   (is-yield f #f code))
          (r    (gensym "return"))
          (*f   (get-args* vs args))
	  (dd*  (map cadr *f))
          (**f  (get-args** vs args))
	  (dd** (map cadr **f))
          (aa  `(,@arg_ ,@*f ,@arg= ,@**f))
          (aay `(,@arg_ ,@*f ,@arg= ,@**f))
          (ab  (gensym "ab"))
          (as  (union dd** (union dd* (union dd= arg_))))
          (gl  (glob  code '()))
          (ns  (scope code '()))
          (df  (defs code  '()))
          (vvs (union as (union vs ns)))
          (ex  (gensym "ex"))
          (y   'scm.yield)
          (y.f (gen-yield f))
          (ls  (diff (diff ns as) gl))
          (vars (map (lambda (x) (list x #f)) ls))
          (cd.doc (with-fluids ((is-class? #f)
                                (*doc*     #f)
                                (return    r)
                                (ann-fluid '()))
                    
                    (let ((cd (wth (exp vvs code))))
                      (cons cd (get-doc)))))
          (cd      (car cd.doc))
          (doc     (cdr cd.doc))
          (docv    (gensym "fv"))
          (docvv   (gensym "fvv"))
          (docer   (lambda (x) `(,(G 'let*) ((,docv  ,x)
                                             (,docvv
                                              (,(C 'ref) ,docv
                                               (,(G 'quote)  __get__)
                                               ,(C 'fslask))))
                                 (,(C 'set) ,docvv (,(G 'quote) __doc__)
                                  ,doc)
                                 (,(C 'set) ,docv (,(G 'quote) __doc__)
                                  ,doc)
                                 ,docv))))
     (define (mk y? code)
       (if y?
           `(let* ((,y    (,(G 'lambda) xxx
                           (,(G 'apply) ,(G 'abort-to-prompt) ,ab xxx)))
                   (,y.f ,y))
              ,code)
           code))
     `(,(C 'NR--) (,Ni ,Mi) (,Ni2 ,Mi2)
       (,(G 'begin)
	,(if y?
           `(set! ,f
              ,(docer
                `(,(C 'def-decor) ,decor
                  (,(O 'object-method)
                   (,(Y 'make-generator)
                    (,(C 'NR-) (,(G 'quote) ,f) (,Ni ,Mi) (,Ni2 ,Mi2)
                     (,(D 'lam) #t ((,Ni ,Mi) (,Ni2 ,Mi2)) ,f ,aay
                      (,(C 'with-return-yield) ,r
                       (,(G 'let) ,vars
                        (,(C 'with-self) ,c? ,aa
                         (,(G 'begin) ,cd (,r)))))))
		    #f)))))
             
           `(set! ,f
              ,(docer
                `(,(C 'def-decor) ,decor
                  (,(O 'object-method)
                   (,(C 'NR-) (,(G 'quote) ,f) (,Ni ,Mi) (,Ni2 ,Mi2)
                    (,(D 'lam) #t ((,Ni ,Mi) (,Ni2 ,Mi2)) ,f ,aa
                     (,(C 'with-return) ,r
                      (,(G 'let) ,(map (lambda (x) (list x #f)) ls)
                       (,(C 'with-self) ,c? ,aa
                        ,cd)))))))))))))))
     
 (#:global
  ((_ . _)
   `(,cvalues)))

 (#:nonlocal
  ((_ . _)
   `(,cvalues)))

 (#:starexpr
  ((_ _ _ id . _)
   `(#:star ,(exp vs id))))
 
 (#:list
  ((_ x (and e (#:cfor . _)))
   (let ((l (gensym "l"))
	 (f (gensym "f")))
     `(,(G 'let) ((,f  (,(L 'pylist-ackumulator))))
       ,(gen-sel vs e `(,f ,(exp vs x)) `((@ (guile) values)) '() '())
       (,f))))
  
  ((_ . l)
   (list (L 'to-pylist) (let lp ((l l))
                          (match l                             
                            ((or () #f) `(,(G 'quote) ()))
                            (((#:starexpr  #:power #f (#:list . l) . _) . _)
                             (lp l))
                            (((#:starexpr  #:power #f (#:tuple . l) . _) . _)
                             (lp l))
                            (((#:starexpr . l) . _)
                             `(,(L 'to-list) ,(exp vs l)))
                            ((x . l)
                            `(,(G 'cons) ,(exp vs x) ,(lp l))))))))
 (#:tuple  
  ((_ x (and e (#:cfor . _)))
   (exp vs (list #:comp x e)))
  
  ((_ . l)
   `(,(G 'list->vector)
     ,(let lp ((l l))
        (match l
          (() `(,(G 'quote) ()))
          (((#:starexpr  #:power #f (#:list . l) . _) . _)
           (lp l))
          (((#:starexpr  #:power #f (#:tuple . l) . _) . _)
           (lp l))
          (((#:starexpr . l) . _)
           `(,(L 'to-list) ,(exp vs l)))
          ((x . l)
           `(,(G 'cons) ,(exp vs x) ,(lp l))))))))
 
 (#:lambdef
  ((_ (#:var-args-list . v) e)
   (let ((as (get-args_  vs v))
         (a= (get-args=  vs v))
         (a* (get-args*  vs v))
         (** (get-args** vs v)))         
     (list (C 'object-method)
           (list (G `lambda) `(,@as ,@a* ,@a= ,@**) (exp vs e)))))
  
  ((_ () e)
   (list (C 'object-method) (list (C `lam) `() (exp vs e)))))
 
 (#:stmt
  ((_ l)
   (match l
     ((#:comma (#:import . _) . _)
      (exp vs l))
     (_
      (stmt-wrap (exp vs l) Ni Mi)))))

 (#:type-exp
  ((_ ((a)) b c)
   (exp vs a)))
 
 (#:expr-stmt
  ((_ (l ...) (#:assign))
   (let ((l (map (g vs exp) l)))
     (if (= (length l) 1)
	 (car l)         
	 `(,(G 'values) ,@l))))

  ((_ a (#:assign-typed u))
   `(,(G 'if) #f #f))
   
  ((_ a (#:assign-typed u v w))  
   (fluid-set! ann-fluid
               (cons (list (symbol->string (exp vs a))
                           (exp vs u))
                     (fluid-ref ann-fluid)))
   (exp vs (list #:expr-stmt a (list #:assign v))))
  
  ((_ a (#:assign b c . u))
   (let ((z (gensym "x")))
     `(,(G 'let) ((,z ,(exp vs `(#:expr-stmt1 ,b (#:assign ,c . ,u)))))
       ,(exp vs `(#:expr-stmt ,a (#:assign ((#:verb ,z))))))))
 
  ((_ l type)
   (=> fail)
   (let ((l (explode l)))
     (call-with-values
         (lambda () (match type
                      ((#:assign u)
                       (values #f u))
                      ((#:augassign op u)
                       (values op u))
                      (_ (fail))))
      
       (lambda (op u)
         (cond
          ((= (length l) (length u))
           (if (= (length l) 1)
               (make-set vs op (car l) `((,(C 'setwrap) ,(exp vs (car u)))))
               (let ((l (unwrap-match vs op l))
                     (u `(,(G 'list) ,@(map (g vs exp) u))))
                 `(,(C 'multi-set) ,l ,u))))
        
          ((= (length u) 1)
           (let ((l (unwrap-match vs op l)))
             `(,(C 'multi-set) ,l ,(exp vs (car u)))))

             
          ((and (= (length l) 1))
           (make-set vs op (car l)
                     `((,(G 'list) ,@(map (g vs exp) u))))))))))
  
  ((_
    ((#:test (#:power #f (#:identifier v . _) () . #f) #f))
    (#:assign (l)))
   (let ((s (string->symbol v)))
     `(,s/d ,s ,(exp vs l)))))

 (#:assert
  ((_ x f n m)
   `(,(G 'if)
     (,(G 'not) (,(C 'boolit) #f ,(exp vs (car x))))
     (,(C 'raise) ,(C 'AssertionError)
      ,@(if (> (length x) 1) (list (exp vs (cadr x))) '())
      (,(G 'quote) ,f) ,m ,n))))


  
 (#:expr-stmt1
  ((_ a (#:assign b c . u))
   (let ((z (gensym "x")))
     `(,(G 'let) ((,z ,(exp vs `(#:expr-stmt1 ,b
                                              (#:assign ,c . ,u)))))
       ,(exp vs `(#:expr-stmt1 ,a (#:assign ((#:verb ,z))))))))
  
  ((_ l type)
   (=> fail)
   (let ((l    (explode l)))
     (call-with-values
         (lambda () (match type
                      ((#:assign u)
                       (values #f u))
                      ((#:augassign op u)
                       (values op u))
                      (_ (fail))))
      
       (lambda (op u)
	 (pk 'assign
         (cond
          ((= (length l) (length u))
           (if (= (length l) 1)
               `(,(G 'begin)
                 ,(make-set vs op (car l) `((,(C 'setwrap)
                                           ,(exp vs (car u)))))
                 ,(exp vs (car l)))
               
               (let ((l (unwrap-match vs op l))
                     (u (map (g vs exp) u)))
                 `(,(G 'begin)
                   (,(C 'multi-set) ,l (,(G 'list) ,@u))
                   (,cvalues ,@(map (g exp vs) l))))))
          
          ((and (= (length u) 1))
           (let ((l (unwrap-match vs op l)))
             `(,(G 'begin)
               (,(C 'multi-set) ,l ,(exp vs (car u)))
               (,cvalues ,@(map (g exp vs) l)))))
        
          ((and (= (length l) 1))
           `(,(G 'begin)
             ,(make-set vs op (car l) `((,(G 'list) ,@(map (g vs exp) u))))
             (,cvalues ,(exp vs (car l))))))))))))
  
 (#:return
  ((_  x)
   (if x
       `(,(C 'retmack) ,(fluid-ref return) ,@(map (g vs exp) x))
       `(,(C 'retmack) ,(fluid-ref return)))))
       

 (#:dict
  ((_ . #f)
   `(,(Di 'dict)))

  ((_  (#:e k . v) (and e (#:cfor . _)))
   (let ((ack (gensym "dict"))
	 (set (gensym "set" )))	     
     `(,(G 'let) ((,ack (,(Di 'dict))))
       (,(Di 'with-stis-hash) ((,ack : _ ,set))
          ,(gen-sel vs e `(,set ,(exp vs k) ,(exp vs v))
		    `((@ (guile) values)) '() '())
	  ,ack))))
  
  ((_  (#:e k . v) ...)
   (let ((dict (gensym "dict")))     
     `(,(G 'let) ((,dict (,(Di 'dict))))
        ,@(map (lambda (k v)
                 `(,(Fa 'fastlist-set!) ,dict ,(exp vs k) ,(exp vs v)))
               k v)
        ,dict)))

  ((_  k (and e (#:cfor . _)))
   (let ((ack (gensym "ack")))
     `(,(G 'let) ((,ack (,(Se 'set-ackumulator))))
       ,(gen-sel vs e `(,ack ,(exp vs k))
		 '((@ (guile) values)) '() '())
       (,ack))))
  
  ((_  k ...)
   (let ((set (gensym "dict")))
     `(,(G 'let) ((,set (,(Se 'set))))
       ,@(map (lambda (k)
		`((,(O 'ref) ,set (,(G 'quote) add)) ,(exp vs k)))
	      k)
       ,set))))
                  
                           
 (#:comp
  ((_ x (and e (#:cfor . _)) . _)
   (let ((yield (gensym "yield")))
     `((,(Y 'make-generator)
	(lambda (,yield)
	  ,(gen-sel vs e `(,yield ,(exp vs x))
		    `((@ (guile) values))
		    '() '()))))))
  
  ((_ x #f)
   (exp vs x))

  ((_ x (op . y))
   (tr-comp op (exp vs x) (exp vs y)))

  ((_ x (op . y) . l)
   (let ((m (gensym "op")))
     `(,(G 'let) ((,m ,(exp vs y)))
       (,(G 'and) ,(tr-comp op (exp vs x) m)
        ,(exp vs `(#:comp (#:verb ,m) . ,l))))))))

(define exp (lambda x x))
(set! exp
  (lambda (vs x)        
    (match (pr 'exp (fluid-ref class-nm) x)
      ((e)
       (exp vs e))
    
      ((tag . l)
       (let ((ll (get-pos x)))
         (apply (hash-ref tagis tag
                          (lambda y
			    (match x
			      ((#f #f) (lambda xx #f))
			      (_
			       (let ((msg
				      (format #f "not tag in tagis ~a" tag)))
				 (pr 'WARN msg)
				 (warn msg x))))))
              
                 
                x vs ll)))
      
      (#:True  #t)
      (#:...   'Ellipsis)
      (#:None  (E 'None))
      (#:null  `(,(G 'quote) ()))
      (#:False #f)
      (#:pass  `(,cvalues))
      (#:break
       (C 'break))    
      (#:continue
       (C 'continue))
      (x x))))

(define (get-current-module)
  (define (abspath x)
    (let* ((xx (dirname x))
	   (xx (string-split xx #\/)))
      (if (equal? (car xx) "")
	  x
	  (string-append (getcwd) "/" x))))

  (define (tolist x y)
    (append
     (map string->symbol
	  (string-split x #\/))
     (list
      (string->symbol
       (car
	(string-split (basename y) #\.))))))
  
  (define (test-prefix x)
    (if (and (>= (length x) 3)
	     (eq? (list-ref x 0) 'language)
	     (eq? (list-ref x 1) 'python)
	     (eq? (list-ref x 2) 'module))
	(cdddr x)
	#f))

  (aif it (fluid-ref (@@ (system base compile) %in-file))
       (cond
	((let lp ((l (tolist (dirname it) it)))
	   (if (pair? l)
	       (aif it (test-prefix l)
		    it
		    (lp (cdr l)))
	       #f))
	 => (lambda (it) it))
	((let ((l (tolist (abspath it) it)))
	   (let lp ((ll %load-path))
	     (if (pair? ll)
		 (let lp2 ((p (car ll)) (x l))
		   (if (pair? p)
		       (if (eq? (car p) (car x))
			   (lp2 (cdr p) (cdr x))
			   (test-prefix x))
		       (lp (cdr ll))))
		 #f)))
	 => (lambda (it) it))

	(else
	 #f))
       #f))

(define elsel '())
(cond-expand
  (guile-3.0                (set! elsel '(else =>)))
  ((or guile-2.0 guile-2.2) (set! elsel '())))

(define (o_a x) (object-address x))

(define (simp f)
  (if f
      (let lp ((l %load-path))
        (if (pair? l)
            (let ((prefix (car l)))
              (if (= (string-length prefix) 0)
                  (lp (cdr l))
                  (if (string-prefix? prefix f)
                      (let* ((n (string-length prefix))
                             (N (string-length f)))
                        (let lp ((n n))
                          (if (eqv? (string-ref f n) #\/)
                              (lp (+ n 1))
                              (let* ((M (- N n))
                                     (ret (make-string M)))
                                (let lp ((i 0))
                                  (if (< i M)
                                      (begin
                                        (string-set!
                                         ret i (string-ref f (+ i n)))
                                        (lp (+ i 1)))
                                      ret))))))
                      (lp (cdr l)))))
            f))
      f))
                

(define (python-comp in x)
  (define (strit x)
    (if in
        x
        (with-output-to-string
          (lambda ()
            (let lp ((x x))
              (if (pair? x)
                  (begin
                    (format #t "~s~%" (car x))
                    (lp (cdr x)))))))))

  (define moddef #f)
  (define args
    (match x
      (((#:stmt
         (#:comma
          (#:expr-stmt
           ((#:test
             (#:power #f
                      (#:identifier "module" . _)
                      ((#:arglist arglist))
                      . #f) #f))
           (#:assign)))) . rest)
       (set! moddef #t)
       (cons
	(map (lambda (x)
	       (exp '() x))
	     arglist)
	rest))

      (_
       (aif it (get-current-module)
	    (cons it x)
	    '(())))))

  (define name
    (if (and (module-defined? (current-module) '*main*)
             (fluid-ref (@@ (system base compile) %in-compile)))
        "__main__"
        (if args
            (string-join (map symbol->string (car args)) ".")
            "__main__")))


  (define start
    (match (if (fluid-ref (@@ (system base compile) %in-compile))
               args
               (car args))
      ((args . rest)
       (let ()
	 
         `((define-syntax-rule (define-module- name args ...)
	     (cond-expand
	      (guile-3
	       (define-module name
		 #:declarative? #f
		 args ...))
	      (guile	     
	       (define-module- name args ...))))

	   (define-module- ,(if (equal? name "__main__")
                              '(__main__)
                              `(language python module ,@args))
            #:pure
            #:use-module ((guile) #:select
                          (@ @@ pk let* lambda call-with-values case-lambda
                                   set! = * + - < <= > >= / pair? fluid-set!
                                   use-modules define-syntax-rule
                                   fluid-ref ,@elsel define-values current-module
                                   syntax-rules let-syntax abort-to-prompt))
            #:use-module (language python module python)
            #:use-module ((language python compile)
                          #:select (pks o_a Filei- Modulei- do_not_warn))
            #:use-module (language python exceptions)
            #:use-module ((oop goops) #:select (<complex> <real> <fraction> <integer> <number>))
            #:export ())

           (,(G 'eval-when) (expand eval load)
            (,(G 'define) __path__   "")
            (,(G 'define) __doc__    #f)
            (,(G 'define) __name__   ,name)
            (,(G 'define) __file__   #f))
           
           (,(G 'define) __module__
            (,(G 'quote)
             ,(if (equal? name "__main__")
                  '(__main__)
                  `(language python module ,@args))))

           ,@(if (equal? name "__main__")
                 `((__import__ "__main__" #:skip-error? #t))
                 '()))))

      (x `()))) 
  
  (fluid-set! ignore '())
 
  (strit
   (pr
    (if (fluid-ref (@@ (system base compile) %in-compile))
        (begin
          (if (fluid-ref (@@ (system base compile) %in-compile))
              (set! s/d (C 'qset!))
              (set! s/d (C 'qset!)))
          
          (if moddef
              (set! x (cdr x)))
          
          (let* ((globs (get-globals x))
                 (must  (glob x '()))
                 (e.ann.doc
                  (if (list? x)
                      (with-fluids ((*doc* #f)
                                    (ann-fluid '()))
                        (let ((r (map (g globs exp) x)))
                          (cons* r (fluid-ref ann-fluid)
                                 (get-doc))))
                      (begin
                        (warn "Empty or malfuncitoning module")
                        (cons* '() "" #f))))
                 (e     (car  e.ann.doc))
                 (ann   (cadr e.ann.doc))
                 (doc   (cddr e.ann.doc)))
            
            `(((@ (guile) begin)

              ,@start
              
              (,(G 'define) ,fnm (,(G 'make-hash-table)))
             #; ,(if #f #;(pk *main*)
                   `(,(G 'set!) __name__   "__main__")
                   (if (pair? start)
                       `(,(G 'if) (pk 'main *main*)
                         (,(G 'set!) __name__
                          "__main__"))                       
              `(,(G 'values))))

              ((@ (guile) eval-when) (expand eval load)
               ((@ (guile) begin)
                ((@ (guile) define) __must-- ((@ (guile) quote) ,must))))

	      ((@ (guile) eval-when) (expand eval load)
	       ,@(map (lambda (s)
			(if (pair? s) (set! s (car s)))
			(if (member s (fluid-ref ignore))
			    `(,cvalues)
			    (if (member s (fluid-ref ignore))
				`(,cvalues)                           
				`(,(C 'var) ,s __must--))))
                       
		      (cons (list '__doc__) globs)))

              (,(G 'set!) __doc__ ,doc)
              
              (,(G 'set!) __file__
               ,(simp (fluid-ref
                       (@@ (system base compile) %in-file))))
              
              (,(G 'set!) __path__
               ,(simp
                 (dirname
                  (fluid-ref
                   (@@ (system base compile) %in-file)))))

              ,@e
              
              ((@ (guile) values))
              (,(C 'export-all))))))

        (begin
          (if (fluid-ref (@@ (system base compile) %in-compile))
              (set! s/d 'set!)
              (set! s/d (C 'define-)))
          (if (pair? start)
              (set! x (if (pair? x)
                          (cdr x)
                          '())))
          (let* ((globs (get-globals x))
                 (must  (glob x '()))
                 (sc    (scope x '()))
                 (res   (gensym "res"))
                 (e     (map (g globs exp) x)))
            (for-each dont-warn* sc)
            `((@ (guile) begin)              
              ,@start
              
              ((@ (guile) eval-when) (expand eval load)
               ((@ (guile) begin)
                ((@ (guile) define) __must-- ((@ (guile) quote) ,must))
                ,@(map (lambda (s)
                         (if (member s (fluid-ref ignore))
                             `(,cvalues)
                             `(,(C 'var) ,s __must--))) globs)))
                             
              (,(C 'with-exit)
               ((@ (guile) values)) ,@e))))))))


(define-syntax-parameter break
  (lambda (x) #'(values)))

(define-syntax-parameter continue
  (lambda (x) (error "continue must be bound")))

(define (is-yield f p x)
  (match x
    ((#:def nm args _ code)
     (is-yield f #t code))
    ((#:yield x _)
     (eq? f (exp '() x))) 
    ((#:yield _)
     (not p))
    ((a . l)
     (or
      (is-yield f p a)
      (is-yield f p l)))
    (_
     #f)))



(define-syntax with-sp
  (lambda (x)
    (syntax-case x ()
      ((_ ((x v)) code ...)
       (or (eq? (syntax->datum #'x) 'break)
           (eq? (syntax->datum #'x) 'continue))
       (if (eq? (syntax->datum #'x) 'break)
           #'(syntax-parameterize ((break    (lambda (y) #'v))) code ...)
           #'(syntax-parameterize ((continue (lambda (y) #'v))) code ...)))
      
      ((_ ((x1 v1) (x2 v2)) code ...)
       (and (equal? (syntax->datum #'x1) 'break)
            (equal? (syntax->datum #'x2) 'continue))
       #'(syntax-parameterize ((break    (lambda (y) #'v1))
                               (continue (lambda (y) #'v2)))
            code ...))
      
      ((_ ((x2 v2) (x1 v1)) code ...)
       (and (equal? (syntax->datum #'x1) 'break)
            (equal? (syntax->datum #'x2) 'continue))
       #'(syntax-parameterize ((break    (lambda (y) #'v1))
                               (continue (lambda (y) #'v2)))
            code ...)))))

      
(define (is-ec ret x tail tags)
  (match x
    ((('@ ('guile) 'cond) (p a ... b) ...)
     (or
      (or-map (lambda (x) (or-map (lambda (x) (is-ec ret x #f tags)) x))
              a)
      (or-map (lambda (x) (is-ec ret x tail tags))
              b)))
        
    (((_ _ 'with-self) u v a ... b)
     (or
      (or-map (lambda (x) (is-ec ret x #f tags)) a)
      (is-ec ret b tail tags)))

    (('let-syntax v a ... b)
     (or
      (or-map (lambda (x) (is-ec ret x #f tags)) a)
      (is-ec ret b tail tags)))
        
    ((('@ ('guile) 'begin) a ... b)
     (or
      (or-map (lambda (x) (is-ec ret x #f tags)) a)
      (is-ec ret b tail tags)))
        
    ((('@ ('guile) 'let) lp ((y x) ...) a ... b) (=> next)
     (if (symbol? lp)
         (or
          (or-map (lambda (x) (is-ec ret x #f tags)) x)
          (or-map (lambda (x) (is-ec ret x #f tags)) a)
          (is-ec ret b tail tags))
         (next)))
        
    ((('@ ('guile) 'let) ((y x) ...) a ... b)
     (or
      (or-map (lambda (x) (is-ec ret x #f tags)) x)
      (or-map (lambda (x) (is-ec ret x #f tags)) a)
      (is-ec ret b tail tags)))

    (('let* ((y x) ...) a ... b)
     (or
      (or-map (lambda (x) (is-ec ret x #f tags)) x)
      (or-map (lambda (x) (is-ec ret x #f tags)) a)
      (is-ec ret b tail tags)))

    ((('@ ('guile) 'define) . _)
     #f)

    ((('@ ('guile) 'if) p a b)
     (or
      (is-ec ret p #f   tags)
      (is-ec ret a tail tags)
      (is-ec ret b tail tags)))
        
    ((('@ ('guile) 'if) p a)
     (or
      (is-ec ret #'p #f   tags)
      (is-ec ret #'a tail tags)))

    (('@@ _ _)
     (if (member x tags)
         #t
         #f))

        
    ((a ...)
     (or-map (lambda (x) (is-ec ret x #f tags)) a))

    (x #f)))

(define (returner yield)
  (lambda x
    (yield arg-empty x)
    (let lp ()
      (yield arg-empty)
      (lp))))
  
(define-syntax-rule (with-return-yield r code)
  (let ()
    (define r (returner yield))	
    code))

(define-syntax with-return
  (lambda (x)
    (define (analyze ret x)
      (syntax-case x (let-syntax let* @ @@)
        ((cond- (p a ... b) ...)
         (equal? (syntax->datum #'cond-)
                 '(@ (guile) cond))
         (with-syntax (((bb ...) (map (lambda (x) (analyze ret x)) #'(b ...))))
           #'(cond (p a ... bb) ...)))
        
        (((_ _ with-self-) u v a ... b)         
         (equal? (syntax->datum #'with-self-)
                 'with-self)
         #`(with-self u v a ... #,(analyze ret #'b)))
        
        ((let-syntax v a ... b)
         #`(let-syntax v a ... #,(analyze ret #'b)))
        
        (((@ (guile) begin-) a ... b)
         (equal? (syntax->datum #'begin-)
                 'begin)
         #`(begin a ... #,(analyze ret #'b)))
        
        (((@ (guile) let-) lp v a ... b)
         (and
          (equal? (syntax->datum #'let-)
                  'let)
          (symbol? (syntax->datum #'lp)))
         #`(let lp v a ... #,(analyze ret #'b)))
        
        (((@ (guile) let-) v a ... b)
         (equal? (syntax->datum #'let-)
                 'let)
         #`(let v a ... #,(analyze ret #'b)))

        ((let* v a ... b)
         #t
         #`(let* v a ... #,(analyze ret #'b)))
        
        (((@ (guile) if-) p a b)
         (equal? (syntax->datum #'if-)
                 'if)
         #`(if p #,(analyze ret #'a) #,(analyze ret #'b)))
        
        (((@ (guile) if-) p a)
         (equal? (syntax->datum #'if-)
                 'if)
         #`(if p #,(analyze ret #'a)))
        
        ((_ return a b ...)
         (equal? (syntax->datum #'return) (syntax->datum ret))
         (if (eq? #'(b ...) '())
             #'a
             #`(values a b ...)))
        
        ((_ return)
         (equal? (syntax->datum #'return) (syntax->datum ret))
         #`(values))
        
        (x #'x)))
  
    (define (is-ec ret x tail)
      (syntax-case x (let-syntax let* @@ @)
        (((@ (guile) cond) (p a ... b) ...)
         (equal? (syntax->datum #'cond)
                 'cond)
         (or
          (or-map (lambda (x) (is-ec ret x #f))
                  #'(a ... ...))
          (or-map (lambda (x) (is-ec ret x tail))
                  #'(b ...))))
        
        (((@@ _ with-self) u v a ... b)
         (equal? (syntax->datum #'with-self)
                 'with-self)
         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))

        ((let-syntax v a ... b)
         #t
         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))
        
        (((@ (guile) begin) a ... b)
         (equal? (syntax->datum #'begin)
                 'begin)
         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))
        
        (((@ (guile) let) lp ((y x) ...) a ... b)
         (and
          (equal? (syntax->datum #'let)
                  'let)
          (symbol? (syntax->datum #'lp)))

         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(x ...))
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))
        
        (((@ (guile) let) ((y x) ...) a ... b)
         (equal? (syntax->datum #'let)
                 'let)
         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(x ...))
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))

        ((let* ((y x) ...) a ... b)
         #t
         (or
          (or-map (lambda (x) (is-ec ret x #f)) #'(x ...))
          (or-map (lambda (x) (is-ec ret x #f)) #'(a ...))
          (is-ec ret #'b tail)))

        (((@ (guile) define) . _)         
         (equal? (syntax->datum #'define)
                 'define)
         #f)

        (((@ (guile) if) p a b)
         (equal? (syntax->datum #'if)
                 'if)
         (or
          (is-ec ret #'p #f)
          (is-ec ret #'a tail)
          (is-ec ret #'b tail)))
        
        (((@ (guile) if) p a)
         (equal? (syntax->datum #'if)
                 'if)         
         (or
          (is-ec ret #'p #f)
          (is-ec ret #'a tail)))

        ((_ return b ...)
         (equal? (syntax->datum #'return) (syntax->datum ret))
         (not tail))
        
        ((a ...)
         #t
         (or-map (lambda (x) (is-ec ret x #f)) #'(a ...)))

        (x
         #t
         #f)))

    (syntax-case x ()
      ((_ ret  l)
       (let ((code (analyze #'ret #'l)))
         (if (is-ec #'ret #'l #t)
             #`(let/ecx ret l)
             code))))))

(define void (list 'void))


#;(define-syntax var
  (lambda (x)
    (syntax-case x (cons quote)
      ((_ '())
       #'(values))
      ((_ (cons x v))
       #'(begin (var x) (var v)))
      ((_ (v))
       (begin
         (dont-warn (syntax->datum #'v))
         #'(define! 'v void)))
      ((_ v)
       (begin
         (dont-warn (syntax->datum #'v))
         #'(if (module-defined? (current-module) 'v)
               (values)
               (define! 'v void)))))))

(define-syntax var
  (lambda (x)
    (syntax-case x (cons quote)
      ((_ must)
       #'(values))
      
      ((_ (cons x v) must)
       #'(begin (var x must) (var v must)))
      
      ((_ (x . l) must)
       #'(begin (var x must) (var l must)))

      ((_ () must)
       #'(values))
      
      ((_ v must)
       (let ((sym (syntax->datum #'v)))         
	 #'(begin
	     (var-define v void)))))))

(define-syntax-parameter var-define
  (syntax-rules ()
    ((_ v x)
     (define v
       (if (module-defined? (current-module) 'v)
           (let ((u (module-ref (current-module) 'v)))
             (if (macro? u)
                 x
                 u))
           x)))))

(define-inlinable (non? x) (eq? x #:nil))

(define (gentemp stx) (datum->syntax stx (gensym "x")))

(define-syntax mmatch
  (syntax-rules ()
    ((_ (a . aa) (b . bb) . code)
     (match a (b (mmatch aa bb . code))))
    ((_ () () . code)
     (begin . code))))

(define (mutewarn x y) (list x y))

(define-syntax clambda
  (lambda (x)
    (syntax-case x ()
      ((_ (x ...) code ...)
       (with-syntax ((n (length #'(x ...)))
                     ((y ...) (generate-temporaries #'(x ...))))
         #'(let ((f (lambda (y ... . u)
                      (mmatch (y ...) (x ...) code ...))))
             (if (> n 1)                
                 (case-lambda
                   ((c)
                    (if (pair? c)
                        (let ((cc (cdr c)))
                          (if (pair? cc)
                              (apply f c)			      
                              (apply f (mutewarn c cc))))
                        (py-apply f (* c))))
                   (q (apply f q)))
                 f)))))))

(define (gen-temp x)
  (syntax-case x ()
    ((x ...) (map gen-temp #'(x ...)))
    (x       (car (generate-temporaries (list #'x))))))

(define (replace_ stx l)
  (let lp ((l l))
    (syntax-case l ()
      ((a . l) (cons (lp #'a) (lp #'l)))
      (x
       (if (equal? (syntax->datum #'x) '_)
	   (datum->syntax stx (gensym "_"))
	   #'x)))))

(define-syntax with-syntax*
  (syntax-rules ()
    ((_ () code)     code)
    ((_ () . code) (begin . code))
    ((_ (x . l) . code)
     (with-syntax (x) (with-syntax* l . code)))))

(define-syntax cfor
  (lambda (xx)
    (syntax-case xx ()
      ((_ (x ...) in code next p)
       (or-map pair? #'(x ...))
       #'(for-adv  (x ...) in code next p))
     
      ((_ (x) (a) code #f #f)
       (with-syntax ((x (replace_ xx #'x)))
         #'(if (pair? a)
	       (let/ecx break-ret
		 (let lp ((l a))
		   (if (pair? l)
		       (begin
			 (set! x (car l))
			 (with-sp ((continue (values))
				   (break    (break-ret)))
                                code)
			 (lp (cdr l))))))
	       (for/adv1 (x) (a) code #f #f))))

      ((_ (x) (a) code #f #t)
       (with-syntax ((x (replace_ xx #'x)))
          #'(if (pair? a)
		(let/ecx break-ret
                   (let lp ((l a))
		     (if (pair? l)
			 (begin
			   (let/ecx continue-ret
			     (set! x (car l))
			     (with-sp ((continue (continue-ret))
				       (break    (break-ret)))                     
                                  code))
			   (lp (cdr l))))))
		(for/adv1 (x) (a) code #f #t))))

      ((_ (x) (a) code next #f)
       (with-syntax ((x (replace_ xx #'x)))
          #'(if (pair? a)
		(let/ecx break-ret
		    (let lp ((l a))
		      (if (pair? l)
			  (begin
			    (set! x (car l))
			    (with-sp ((continue (values))
				      (break    (break-ret)))
				code)
                            (lp (cdr l)))))
		    next)
		(for/adv1 (x) (a) code next #f))))

      ((_ (x) (a) code next #t)
       (with-syntax ((x (replace_ xx #'x)))
          #'(if (pair? a)
		(let/ecx break-ret
		   (let lp ((l a))
		     (if (pair? l)
                         (begin
                           (let/ecx continue-ret
                             (set! x (car l))
                             (with-sp ((continue (continue-ret))
                                       (break    (break-ret)))
                                      code))
                           (lp (cdr l)))))
		   next)
		(for/adv1 (x) (a) code next #f))))
    
      ((_ x a code next p)
       #'(for/adv1 x a code next p)))))

(define-syntax for/adv1
  (lambda (zz)
    (syntax-case zz ()
      ((_ (xy ...) (in) code #f #f)
       (with-syntax* ((inv      (gentemp #'in))
		      ((yy ...) (replace_ zz #'(xy ...)))
		      ((xx ...) (gen-temp #'(yy ...))))
	#'(let ((inv (wrap-in in)))
	    (clet (yy ...)
             (catch 'python
               (lambda ()
                 (let lp ()
                   (call-with-values (lambda () (next inv))
                     (clambda (xx ...)
                       (cset! yy xx) ... 
                       (with-sp ((break    (values))
                                 (continue (values)))
                                code
                                (lp))))))
               (lambda (tag c . l)
                 (if (eq? c StopIteration)
                     (values)
                     (apply throw tag c l))))))))

      ((_ (xy ...) (in ...) code #f #f)
       (with-syntax* (((inv ...) (generate-temporaries #'(in ...)))
		      ((yy  ...) (replace_ zz #'(xy ...)))
		      ((xx  ...) (gen-temp #'(yy ...))))
         #'(let ((inv (wrap-in in)) ...)
	     (clet (yy ...)
             (catch 'python
               (lambda ()
                 (let lp ()
                   (call-with-values (lambda () (values (next inv) ...))
                     (clambda (xx ...)
                       (cset! yy xx) ...
                       (with-sp ((break    (values))
                                 (continue (values)))
                                code
                                (lp))))))
               (lambda (tag c . l)
                 (if (eq? c StopIteration)                     
                     (values)
                     (apply throw tag c l))))))))

      ((_ (xy ...) (in) code #f #t)
       (with-syntax* ((inv       (gentemp #'in))
		      ((yy  ...) (replace_ zz #'(xy ...)))
		      ((xx  ...) (gen-temp #'(yy ...))))
	#'(let ((inv (wrap-in in)))
	    (clet (yy ...)
              (let lp ()
                (let/ecx break-ret
                  (catch 'python
                    (lambda ()
                      (call-with-values (lambda () (next inv))
                        (clambda (xx ...)
                          (cset! yy xx) ...
                          (let/ecx continue-ret
                            (with-sp ((break     (break-ret))
                                      (continue  (continue-ret)))
                                     code))
                          (lp))))
                    (lambda (tag c . l)
                      (if (eq? c StopIteration)
                          (values)
                          (apply throw tag c l))))))))))

      ((_ (xy ...) (in ...) code #f #t)
       (with-syntax* (((inv ...) (generate-temporaries #'(in ...)))
		      ((yy  ...) (replace_ zz #'(xy ...)))
		      ((xx  ...) (gen-temp #'(yy ...))))
	 #'(let ((inv (wrap-in in)) ...)
	     (clet (yy ...)
              (let lp ()
                (let/ecx break-ret
                  (catch 'python
                    (lambda ()
                      (call-with-values (lambda () (values (next inv) ...))
                        (clambda (xx ...)
                          (cset! yy xx) ...
                          (let/ecx continue-ret
                            (with-sp ((break     (break-ret))
                                      (continue  (continue-ret)))
                                     code))
                          (lp))))
                    (lambda (tag c . l)
                      (if (eq? c StopIteration)
                          (values)
                          (apply throw tag c l))))))))))

      ((_  (x ...) in code else #f)
       #'(for-adv (x ...) in code else #f))

      ((_ (x ...) in code else #t)
       #'(for-adv (x ...) in code else #t)))))


(define-syntax for-adv
  (lambda (zz)
    (define (gen x y)
      (if (= (length (syntax->datum x)) (= (length (syntax->datum y))))
          (syntax-case x ()
            ((x ...) #'(values (next x) ...)))
          (syntax-case x ()
            ((x)  #'(next x)))))
    
    (syntax-case zz ()
      ((_ (xy ...) (in) code else p)
       (with-syntax* ((inv (gentemp #'in))
		      ((yy ...) (replace_ zz #'(xy ...)))
		      ((xx ...) (gen-temp #'(yy ...))))
				  
           (if (syntax->datum #'p)
               #'(let ((inv (wrap-in in)))
                   (clet (yy ...)
                     (let/ecx break-ret
                       (catch 'python
                         (lambda ()
                           (let lp ()
                             (call-with-values (lambda () (next inv))
                               (clambda (xx ...)
                                 (cset! yy xx) ...
                                 (let/ecx continue-ret
                                   (with-sp ((break     (break-ret))
                                             (continue  (continue-ret)))
                                            code))
                                 (lp)))))
                         (lambda (tag c . l)
                           (if (eq? c StopIteration)
                               else
                               (apply throw tag c l)))))))
             
               #'(let ((inv (wrap-in in)))
                   (clet (yy ...)
                         (let/ecx break-ret
                           (catch 'python
                             (lambda ()
                               (let lp ()
                                 (call-with-values (lambda () (next inv))
                                   (clambda (xx ...)
                                      (cset! yy xx) ...
                                      (with-sp ((break     (break-ret))
                                                (continue  (values)))
                                               code)
                                      (lp)))))
                             (lambda (tag c . l)
                               (if (eq? c StopIteration)
                                   else
                                   (apply throw tag c l))))))))))
      
      ((_ (xy ...) (in ...) code else p)
       (with-syntax* (((inv ...) (generate-temporaries #'(in ...)))
		      ((yy  ...) (replace_ zz #'(xy ...)))
		      (get       (gen #'(inv ...) #'(yy ...)))
		      ((xx ...)  (gen-temp #'(yy ...))))
         (if (syntax->datum #'p)
             #'(clet (yy ...)
                 (let ((inv (wrap-in in)) ...)               
                   (let/ecz break-ret
                     (catch 'python
                       (lambda ()
                         (let lp ()
                           (call-with-values (lambda () get)
                             (clambda (xx ...)
                               (cset! yy xx) ...
                               (let/ecx continue-ret
                                 (with-sp ((break     (break-ret))
                                           (continue  (continue-ret)))
                                          code))
                               (lp)))))
                       (lambda (tag c . l)
                         (if (eq? c StopIteration)
                             else
                             (apply throw tag c l)))))))
             
                 #'(clet (yy ...)
                     (let ((inv (wrap-in in)) ...)
                       (let/ecx break-ret
                         (catch 'python
                           (lambda ()
                             (let lp ()
                               (call-with-values (lambda () get)
                                 (clambda (xx ...)
                                   (cset! yy xx) ...
                                   (with-sp ((break     (break-ret))
                                             (continue  (values)))
                                            code)
                                   (lp)))))
                           (lambda (tag c . l)
                             (if (eq? c StopIteration)
                                 else
                                 (applt throw tag c l)))))))))))))

(define-syntax cset!
  (syntax-rules ()
    ((_ (a . aa) (b . bb))
     (begin
       (cset! a  b)
       (cset! aa bb)))
    ((_ () ())
     (values))
    ((_ a b)
     (set! a b))))

(define-syntax clet
  (syntax-rules ()
    ((_ ((a . l) . u) . code)
     (clet (a l . u) . code))
    ((_ (() . u) . code)
     (clet u . code))    
    ((_ (a . u) . code)
     (let ((a #f))
       (clet u . code)))
    ((_ () . code)
     (begin . code))))
    
(define miss (list 'miss))
(define-inlinable (wr o k x)
  (if (eq? x miss)
      (raise (AttributeError (+ "attribute '" (scm-str k)
                                "' is missing in object "
                                (scm-str o))))
      x))

(define-syntax tolo
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (if (one-expr #'x)
	   #'x
	   #'(call-with-values (lambda () x)
	       (case-lambda
		((z) z)
		(() None)
		(z z))))))))

(define (f-ref-x v l)
  (let lp ((v v) (l l))
    (define-syntax-rule (ret x)
      (let ((l (cdr l)))
        (if (null? l)
            x            
            (lp (tolo x) l))))
    
    (if (pair? l)
        (let ((x (car l)))
          (cond
           ((symbol? x)
            (ret (wr v x (pyref v x miss))))
           ((null? x)
            (ret (v)))
           ((pair? x)
            (ret (apply v x)))
           ((vector? x)
            (ret (fastlist-ref v (vector-ref x 0))))
           (else
            (ret (x v)))))
        v)))

(define (ppu x) #;(pk (syntax->datum x)) x)
(define-syntax ref-x
  (lambda (x)
    ;(pk (syntax->datum x))
    (define (simple-apply? x)
      (syntax-case x ()
        (((op a) . l)
         (if (memq (syntax->datum #'op) '(* **))
             #f
             (simple-apply? #'l)))
        (((op a b) . l)
         (if (eq? (syntax->datum #'op) '=)
             #f
             (simple-apply? #'l)))
        ((_ . l)
         (simple-apply? #'l))
        (() #t)))
      
    (define (simple? x)
      (syntax-case x ()
        (((#:identifier (_ x)) . l)
         (and (symbol? (syntax->datum #'x))
              (not (member (syntax->datum #'x) '(__dict__ __name__ warn))))
         (simple? #'l))
        (((#:apply . v) . l)
         (and (simple-apply? #'v)              
              (simple? #'l)))
        (((#:call . v) . l)
         (simple? #'l))
        (((#:fastid . v) . l)
         (simple? #'l))
        (((#:call-obj . v) . l)
         (simple? #'l))
        (((#:vecref . _) . l)
         (simple? #'l))
        (((#:array-ref . _) . l)
         (simple? #'l))
        (() #t)
        (_  #f)))

    (define (ssimple? x)
      (syntax-case x ()
        (((#:identifier (_ x)) . l)
         (and (symbol? (syntax->datum #'x))
              (not (member (syntax->datum #'x) '(__dict__ __name__ warn))))
         (ssimple? #'l))
        (() #t)
        (_  #f)))

    (define (simple-it x)
      (syntax-case x ()
        (((#:identifier (_ x)) . l)
         (and (symbol? (syntax->datum #'x))
              (not (member (syntax->datum #'x) '(__dict__ __name__ warn))))
         (cons #''x (simple-it #'l)))
      
        (((#:apply . x) . l)
         (cons #'(list . x) (simple-it #'l)))

        (((#:call . x) . l)
         (cons #'(list . x) (simple-it #'l)))

        (((#:fast-id f _) . l)
         (cons #'f (simple-it #'l)))

        (((#:call-obj f) . l)
         (cons #'(lambda (x) (lambda y (apply f x y))) (simple-it #'l)))

        (((#:vecref x) . l)
         (cons #'(vector x) (simple-it #'l)))

        (((#:array-ref x) . l)
         (cons #'(vector x) (simple-it #'l)))

        (() '())))

    (define (ssimple-it x)
      (syntax-case x ()
        (((#:identifier (_ x)) . l)
         (and (symbol? (syntax->datum #'x))
              (not (member (syntax->datum #'x) '(__dict__ __name__ warn))))
         (cons #'x (ssimple-it #'l)))
      
        (() '())))
        

    (define (all-id? v stx)
      (let ((v (syntax->datum v)))
        (if (symbol? v)
            (let lp ((r '()) (x stx))
              (syntax-case x ()
                (((#:identifier ((@ (guile) quote) sym)) . l)
                 (lp (cons (syntax->datum #'sym) r) #'l))
                (((#:identifier sym) . l)
                 (lp (cons (syntax->datum #'sym) r) #'l))
                (l
                 (if (>= (length r) 1)
                     (values (string->symbol
                              (string-join
                               (cons
                                (symbol->string v)
                                (map symbol->string
                                     (reverse r)))
                               "."))
                             #'l)
                     #f))))
            #f)))

    (ppu 
     (syntax-case x (@)
      ((_)
       #'None)
      
      ((_ v)
       #'v)
	    
      ((_ v (#:call-obj f) (#:apply . x))
       #'(f v . x))

      ((_ v . l)
       (cond
        ((all-id? #'v #'l)
         =>
         (lambda (xx)
           (module-defined? (current-module) xx)))
        (else #f))
       (call-with-values (lambda () (all-id? #'v #'l))
         (lambda (xx stx-l)
           (with-syntax ((sym (datum->syntax x xx))
                         ((l ...) stx-l))
             #'(let ((z sym)) (ref-x z l ...))))))
            
      ((_ v (#:call-obj f) . l)
       #'(ref-x (lambda xx (apply f v xx)) . l))
      
      ((_ v (#:call x ...))
       #'(v x ...))

      ((_ v (#:call x ...) . l)
       #'(ref-x (tolo (v x ...)) . l))
      
      ((_ v (#:apply x ...) . l)
       #'(ref-x (tolo (py-apply v x ...)) . l))

      ((_ v (#:apply x ...))
       #'(py-apply v x ...))
            
      ((_ v (#:vecref x) . l)
       #'(ref-x (fastlist-ref v x) . l))

      ((_ v (#:array-ref x) . l)
       #'(ref-x (fastlist-ref v x) . l))
      
      ((_ v (#:vecsub . x) . l)
       #'(ref-x (fastlist-slice v . x) . l))

      ((_ v (#:fast-id f _) . l)
       #'(ref-x (f v) . l))

      ((_ v . l)       
       (ssimple? #'l)
       #`(f-ref-x v '#,(ssimple-it #'l)))

      ((_ v . l)       
       (simple? #'l)
       #`(f-ref-x v (list #,@(simple-it #'l))))

      ((_ v (#:fastfkn-ref f tag) . l)
       #'(let ((vv v))
           (if (is-a? vv <p>)
               (ref-x v (#:identifier tag) . l)
               (ref-x (lambda x
                        (if (pyclass? vv)
                            (apply f x)
                            (apply f vv x))) . l))))
      
      ((_ v (#:identifier ((@ x q) dict)) . l)
       (equal? (syntax->datum #'dict) '__dict__)
       #'(ref-x (py-dict v) . l))

      ((_ v (#:identifier ((@ x q) dict)) . l)
       (equal? (syntax->datum #'dict) '__name__)
       #'(ref-x (pyname v) . l))
      
      ((_ v (#:identifier ((@ _ _) warn-)) . l)
       (equal? 'warn (syntax->datum #'warn-))
       #'(with-fluids ((Ni-f      Ni)
                       (Filei-f   Filei-)
                       (Modulei-f Modulei-))
           (ref-x (wr v 'warn (pyref v 'warn miss)) . l)))

      ((_ v (#:identifier x) . l)
       #'(ref-x (wr v x (pyref v x miss)) . l))

      (_ #'None)))))
      

(define-syntax del-x
  (lambda (x)
    (syntax-case x ()
      ((_ v (#:identifier x))
       #'(ref-x (wr v x (pyref v x #f))))
      ((_ v (#:call-obj x))
       #'(values))
      ((_ v (#:call x ...))
       #'(values))
      ((_ v (#:apply x ...))
       #'(values))
      ((_ v (#:vecref x))
       #'(pylist-delete! v x))
      ((_ v (#:vecsub x ...))
       #'(fastlist-subset! v x ... pylist-null)))))

(define-syntax set-x
  (lambda (x)
    (syntax-case x ()
      ((_ v (a ... b) val)
       #'(set-x-2 (ref-x v a ...) b val))
      ((_ v #f p pa a val)
       #'(set-x p pa (fset-x v a val)))
      ((_ v pre p pa a val)
       #'(set-c v pre a val))
      ((_ v (a ... b) val)
       #'(set-x-2 (ref-x v a ...) b val)))))

(define-syntax set-c
  (syntax-rules ()
    ((_ v (a) (b) val)
     (pyset v a val))
    ((_ v () as val)
     (tr v (fset-x v as val)))
    ((_ v ((#:identifier a) . as) (b . bs) val)
     (set-c (pyref v a #f) as bs val))))
    
(define-syntax fset-x
  (syntax-rules ()
    ((_ v ((#:identifier x) ...) val)
     ((@ (oop pf-objects) fset-x) v (list x ...) val))))

(define-syntax set-x-2
  (syntax-rules (ref-x)
    ((_ (ref-x v) . l)
     (set-x-2 v . l))
    ((_ v (#:fastfkn-ref f id) val)
     (pyset v id val))
    ((_ v (#:fast-id f id) val)
     (pyset v id val))
    ((_ v (#:identifier x) val)
     (pyset v x val)) 
    ((_ v (#:vecref n) val)
     (fastlist-set! v n val))
    ((_ v (#:array-ref n) val)
     (fastlist-set! v n val))
    ((_ v (#:vecsub x ...) val)
     (fastlist-subset! v x ... val))))


(define-syntax class-decor
  (syntax-rules ()
    ((_ () x) x)
    ((_ (f ... r) y)
     (class-decor (f ...) (r y)))))

(define-syntax def-decor
  (syntax-rules ()
    ((_ () x)     x)
    ((_ (f ... r) y)
     (def-decor (f ...) (r y)))))
        
(define-syntax with-self
  (syntax-rules ()
    ((_ #f _       c)
     c)
    ((_ _  (s . b) c)
     (syntax-parameterize ((*self* (lambda (x) #'s))) c))
    ((_ _  () c)
     c)))

(define-syntax with-class
  (syntax-rules ()
    ((_ s c)
     (syntax-parameterize ((*class* (lambda (x) #'s))) c))))

(define (bool- x)
  (cond
   ((boolean? x) x)
   ((symbol?  x) (if (eq? x 'None) #f #t))
   ((number?  x) (not (eq? x 0)))
   ((vector?  x) (not (= (vector-length x) 0)))
   ((null?    x) #f)
   ((pair?    x) #t)
   (else (bool x))))

(define-syntax-rule (-equal? x y)
  (let ((xx x) (yy y))
    (if (and (number? xx) (number? yy))
        (= xx yy)
        ((@@ (oop pf-objects) equal?) xx yy))))

(define (export-all)
  (define mod (current-module))
  (if (module-defined? mod '__all__)
      (begin
        (module-export! mod
          (for ((x : (module-ref mod '__all__))) ((l '()))
               (let ((x (string->symbol (scm-str x))))
                 (if (module-locally-bound? mod x)
                     (cons x l)
                     l))
               #:final l))
        (module-re-export! mod
          (for ((x : (module-ref mod '__all__))) ((l '()))
               (let ((x (string->symbol (scm-str x))))
                 (if (module-locally-bound? mod x)
                     l
                     (cons x l)))
               #:final l)))))

(define (pkkk x)
  (pk (syntax->datum x))
  x)

(define (get-q-n x)
  (syntax-case x ()
    ((cons a b)
     (+ 1 (get-q-n #'b)))
    ((q ())
     0)))

(define (take-n n v)
  (let lp ((i 0) (v (reverse v)) (r '()))
    (if (< i n)
        (if (pair? v)
            (lp (+ i 1) (cdr v) (cons (car v) r))
            (raise (ValueError "wrone number of values in values")))
        (cons
         (reverse v)
         r))))

(define-syntax qset!
  (lambda (x)
    (dutte
    (syntax-case x (@@ @)
      ((_ (cons (#:star x) y) v)
       (let ((n (get-q-n #'y)))
         #`(let* ((h.r (take-n #,n v))
                  (h   (car h.r))
                  (r   (cdr h.r)))
             (qset! x h)
             (qset! y r))))
      
      ((_ (cons x y) v)
       (equal? (syntax->datum #'cons) '(@ (guile) cons))
       #'(let* ((w (to-list v))
                (a (car w))
                (b (cdr w)))
           (qset!  x a)
           (qset!  y b)))
      
      ((_ ((@ (guile) q) ()) v)
       (equal? (syntax->datum #'q) 'quote)
       #'(if (not (null? v))
             (raise (ValueError "too many values to unpack"))
             (values)))

      ((_ ((@@ u li) x) v)
       (equal? (syntax->datum #'li) 'to-pylist)
       #'(let ((w (to-list v)))
	   (qset! x w)))
      
      ((_ (ref v a ...) w)
       #'(set-x v (a ...) w))
            
      ((_  x  v)
       (begin
	 (add-type-num #'x (get-type-num #'v))
	 (add-type-one #'x (get-type-one #'v))
	 (add-type-atm #'x (get-type-atm #'v))
	 #'(begin '(qset x) (set! x v))))))))

(define-syntax qset0!
  (lambda (x)
    (syntax-case x (@@ @)
      ((_ (cons (#:star x) y) v)
       (let ((n (get-q-n #'y)))
         #`(let* ((h.r (take-n v #,n))
                  (h   (car h.r))
                  (r   (cdr h.r)))
             (qset! x h)
             (qset0! y r))))

      ((_ (cons x y) v)
       (equal? (syntax->datum #'cons) '(@ (guile) cons))
       #'(let ((w v))
           (qset!  x (car w))
           (qset0! y (cdr w))))
      
      ((_ ((@ (guile) q) ()) v)
       (equal? (syntax->datum #'q) 'quote)
       #'(if (not (null? v))
             (raise (ValueError "too many values to unpack"))
             (values)))

      ((_ ((@@ u li) x) v)
       (equal? (syntax->datum #'li) 'to-pylist)
       #'(let ((w (to-list v)))
	   (qset! x w)))
      
      ((_ (ref v a ...) w)
       #'(set-x v (a ...) w))
            
      ((_  x  v)
       #'(set! x v)))))

(define-syntax define-
  (lambda (x)
    (syntax-case x (cons quote)
      ((_ (cons x y) v)       
       #'(let ((w v))
	   (define- x (car w))
	   (define- y (cdr w))))
      
      ((_ '() v)
       #'(values))
      
      ((_  x  v)
       #t
       (begin
	 (add-type-num #'x (get-type-num #'v))
	 (add-type-one #'x (get-type-one #'v))
	 (add-type-atm #'x (get-type-atm #'v))
	 #'(set! x v))))))

(define-syntax pks
  (lambda (x)
    (pk (syntax->datum x))
    #f))

(set! (@@ (language python module _ast) mk-string) mk-string)

(define-syntax retmack
  (syntax-rules ()
    ((_ ret)
     (ret None))
    
    ((_ ret arg)
     (call-with-values
         (lambda () arg)
       (lambda x (apply ret x))))
    
    ((_ ret arg ...)
     (ret (call-with-values
              (lambda () arg)
            (case-lambda (() None) ((x) x) (x x)))
          ...))))

(define-syntax assign=
  (lambda (x)
    (syntax-case x ()
      ((_ op val u)
       (begin
	 (add-type-num #'val (get-type-num #'u))
	 (add-type-one #'val (get-type-one #'u))
	 (add-type-atm #'val (get-type-atm #'u))
	 #'(op val u))))))

(define-syntax-rule (oper op v (add ...) val)
  (op (ref-x v add ...) val (lambda (u) (set-x v (add ...) u))))

(define-syntax-rule (oper2 op v pre p pa (add ...) val)
  (op
   (ref-x v add ...)
   val
   (lambda (val-) (set-x v pre p pa (add ...) val-))))

(define (pretty x) (pretty-print (syntax->datum x)) x)
(define-syntax multi-set
  (lambda (x)
    (syntax-case x ()
      ((_ vars val)
       (with-syntax ((vars (let lp ((vars #'vars))
                             (syntax-case vars ()
                               ((#:set (x ...))
                                #'(lambda (z) (x ... z)))
                               
                               (((#:* x) . l)
                                (list #'cons
                                      (list #'list #:* (lp #'x))
                                      (lp #'l)))

                               ((#:identifier x)
                                (error "problem with identifier"))

                               ((x . l)                                
                                (list #'cons (lp #'x) (lp #'l)))
                               
                               (() #''())
                               
                               (x #'(lambda (z) (set! x z)))))))
         #'(call-with-values (lambda () val)
             (multi-set-f vars)))))))
                 

(define (multi-set-f x)
  (letrec ((g (lambda (x y)
                (match (cons x y)
                  ((((#:* x1) x2 . x3) . (y1 . y2))
                   (g (reverse (cons* (list #:r x1) x2 x3)) (reverse y)))
                  
                  ((((#:* x1)) . y)
                   (x1 (to-pylist y)))
                  
                  ((((#:r x1)) . y)
                   (x1 (to-pylist (reverse y))))
                  
                  (((x1 . x2) . (y1 . y2))
                   (f x1 y1)
                   (g x2 y2))
                  
                  (((x1 . x2) . ())
                   (raise (ValueError
                           "Not enough arguments to unpack")))
                  
                  ((() . (y1 . y2))
                   (raise (ValueError (format #f
                                              "Too many values to unpack ~a ~a"
                                              x y))))

                  ((() . ())
                   (values))

                  ((x . y)
                   (x y)))))

           (f  (lambda (x z)
                 (if (pair? x)
                     (if (pair? z)
                         (g x z)                      
                         (let ((r (catch #t
                                    (lambda () (to-list z))
                                    (lambda x  (raise
                                                (TypeError
                                                 "non iterable object"))))))
                           (g x r)))
                     (g x z)))))
    (case-lambda
      ((z) (f x z))
      (()  (f x None))
      (z   (f x z)))))

(define-syntax do_not_warn
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)
       (begin
         (map dont-warn (map syntax->datum #'(x ...)))
         #'(values))))))

(define ann-it? #f)
(define (ann-it)
  (if ann-it?
      #t
      (let ((p (let ((m (resolve-module '(language python module typing))))
                 (module-defined? m 'Any))))
        (when p
          (process-use-modules '(((language python module typing))))
          (set! ann-it? #t))
        p)))

(define-syntax mk-ann
  (lambda (x)
    (syntax-case x ()
      ((_)
       #'(dict))
      
      ((_ (nm tp) ...)
       (ann-it)
       #'(let ((d (dict)))
           (when (ann-it)
             (fastlist-set! d nm tp)
             ...
             (values))
           d)))))



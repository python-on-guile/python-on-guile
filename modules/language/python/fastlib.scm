;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python fastlib)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (language python type)
  #:use-module (language python exceptions)
  #:use-module (language python dict)
  #:use-module (language python set)
  #:use-module (language python hash)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python string)
  #:use-module (language python bytes)
  #:use-module (language python list)
  #:use-module (language python yield)
  #:use-module (language python persist)
  #:use-module (language python bool)
  #:replace (fastlist-ref fastlist-set! fast-clear! fast-equal? fast-hash
                          fastlist-len)
  #:export (fastlist-get fastlist-slice
                     fastlist-subset! fastlist-append! fastlist-pop!
                     fastlist-in
                     fast-equal-mac?
                     ;; STRING
                     fast-startswith fast-endswith))

;;; Sequence fast methods (to be fast to basic datastructures)

(define-syntax m
  (syntax-rules ()
    ((_ (a ...) (b ...))
     (b ... a ...))
    ((_ a       (b ...))
     (apply b ... a))))

;;; --------------------- MKFAST-STR    ----------------------------------------
(define-syntax-rule (mkfast-str fastlist-ref pylist-ref
                                string-ref py-bytes-ref py-bytea-ref l)
  (define (fastlist-ref x . l)
    (let ((xx x))
      (cond
       ((string? xx)
        (m l (string-ref xx)))

       ((struct? xx)
        (let ((cls (class-of xx)))
          (cond               
           ((eq? cls <py-bytes>)
            (m l (py-bytes-ref xx)))

           ((eq? cls <py-bytearray>)
            (m l (py-bytea-ref xx)))
           
           (else
            (m l (pylist-ref xx))))))
                       
       (else
        (m l (pylist-ref xx)))))))



(define py-bytes-startswith
  (resolve-method-g py-startswith `(,<py-bytes>     . _)))
(define py-bytea-startswith
  (resolve-method-g py-startswith `(,<py-bytearray> . _)))
(define py-string-startswith
  (resolve-method-g py-startswith `(,<string>       . _)))

(mkfast-str fast-startswith py-startswith py-string-startswith
        py-bytes-startswith py-bytea-startswith l)




(define py-bytes-endswith
  (resolve-method-g py-endswith `(,<py-bytes>     . _)))
(define py-bytea-endswith
  (resolve-method-g py-endswith `(,<py-bytearray> . _)))
(define py-string-endswith
  (resolve-method-g py-endswith `(,<string>       . _)))

(mkfast-str fast-endswith py-endswith py-string-endswith
        py-bytes-endswith py-bytea-endswith l)


;;;-------------------------- MKFAST ----------------------------

(define-syntax-rule (mkfast fastlist-ref fastlist-ref-f
			    py-list-ref pylist-ref
			    vector-ref list-ref string-ref
			    py-dict-ref py-bytes-ref py-bytea-ref
			    py-set-ref
				
			    (xx x ...))

  (begin
    (define fastlist-ref-f
      (case-lambda
       ((xx x ...)
	(cond
	 ((vector? xx)
	  (vector-ref xx x ...))
	 
	 ((struct? xx)
	  (let ((cls (class-of xx)))
	    (cond
	     ((eq? cls <py-list>)
	      (py-list-ref xx x ...))
	   
	     ((eq? cls <py-bytes>)
	      (py-bytes-ref xx x ...))

	     ((eq? cls <py-bytearray>)
	      (py-bytea-ref xx x ...))
	   
	     ((eq? cls <HH>)
	      (py-dict-ref xx x ...))

	     ((eq? cls <set>)
	      (py-dict-ref xx x ...))

	     (else
	      (pylist-ref xx x ...)))))

	 ((string? xx)
	  (string-ref xx x ...))
         
	 ((pair? xx)
	  (list-ref xx x ...))
         
	 (else
	  (pylist-ref xx x ...))))

       (z (apply pylist-ref z))))

    (define-syntax fastlist-ref
      (lambda (z)
	(syntax-case z ()
	  ((_ xx x ...)
	   (case (get-type-atm #'xx)
	     ((v)
	      #'(vector-ref xx x ...))
	     
	     ((l)
	      #'(py-list-ref xx x ...))
	   
	     ((b)
	      #'(py-bytes-ref xx x ...))

	     ((ba)
	      #'(py-bytea-ref xx x ...))

	     ((d)
	      #'(py-dict-ref xx x ...))

	     ((m)
	      #'(py-set-ref xx x ...))
	     
	     ((s)
	      #'(string-ref xx x ...))
	     
	     (else
	      #'(fastlist-ref-f xx x ...))))
	  
	  (_
	   #'fastlist-ref-f))))))

	  

(define-syntax mk-ref 
  (lambda (x)
    (define (f pre stx)
      (datum->syntax stx (string->symbol (string-append
					  (symbol->string pre)
					  (symbol->string
					   (syntax->datum stx))))))
    (syntax-case x ()
      ((_ fastlist- f-fastlist- nm pylist-ref (x ...))
       (with-syntax ((py-dict-ref    (f 'py-dict-    #'nm))
		     (py-list-ref    (f 'py-list-    #'nm))
		     (py-bytes-ref   (f 'py-bytes-   #'nm))
		     (py-bytea-ref   (f 'py-bytea-   #'nm))
		     (py-vector-ref  (f 'py-vector-  #'nm))
		     (py-string-ref  (f 'py-string-  #'nm))
		     (py-pair-ref    (f 'py-pair-    #'nm))
		     (py-set-ref     (f 'py-set-     #'nm))
		     (fastlist-ref   (f 'fastlist-   #'nm))
		     (fastlist-ref-f (f 'f-fastlist- #'nm)))
#'(begin 
   (define py-dict-ref   (resolve-method-g pylist-ref `(,<HH>      x ...)))
   (define py-list-ref   (resolve-method-g pylist-ref `(,<py-list>      x ...)))
   (define py-bytes-ref  (resolve-method-g pylist-ref `(,<py-bytes>     x ...)))
   (define py-bytea-ref  (resolve-method-g pylist-ref `(,<py-bytearray> x ...)))
   (define py-vector-ref (resolve-method-g pylist-ref `(,<vector>       x ...)))
   (define py-string-ref (resolve-method-g pylist-ref `(,<string>       x ...)))
   (define py-pair-ref   (resolve-method-g pylist-ref `(,<pair>         x ...)))
   (define py-set-ref    (resolve-method-g pylist-ref `(,<set>          x ...)))
   
   (mkfast fastlist-ref fastlist-ref-f py-list-ref pylist-ref
	   py-vector-ref py-pair-ref py-string-ref py-dict-ref
	   py-bytes-ref py-bytea-ref py-set-ref (o x ...))))))))

(mk-ref fastlist- f-fastlist- ref  pylist-ref (x))

(mk-ref fastlist- f-fastlist- get2 py-get (x))
(mk-ref fastlist- f-fastlist- get3 py-get (x y))
(define-syntax fastlist-get
  (lambda (x)    
    (syntax-case x ()
      ((_ a b  ) #'(fastlist-get2 a b  ))
      ((_ a b c) #'(fastlist-get3 a b c))
      ((_ . u)   #'(pylist-get . u))
      (_
       #'(case-lambda
	  ((a b  ) (fastlist-get2 a b  ))
	  ((a b c) (fastlist-get3 a b c))
	  (x       (apply pylist-get x)))))))

(mk-ref fastlist- f-fastlist- set!    pylist-set!    (k v     ))
(mk-ref fastlist- f-fastlist- len     len            (        ))
(mk-ref fastlist- f-fastlist- in      in             (k       ))
(mk-ref fastlist- f-fastlist- slice   pylist-slice   (a b c   ))
(mk-ref fastlist- f-fastlist- subset! pylist-subset! (a b c oo))
(mk-ref fastlist- f-fastlist- pop!    pylist-pop!    (        ))
(mk-ref fastlist- f-fastlist- append! pylist-append! (x       ))
(mk-ref fastlist- f-fastlist- clear   py-clear       (        ))

;;; EQUAL =================================================
(define fast-dict-equal?  (resolve-method-g py-equal?  `(,<HH>      _)))
(define fast-set-equal?   (resolve-method-g py-equal?  `(,<set>       _)))
(define fast-list-equal?  (resolve-method-g py-equal?  `(,<py-list>      _)))
(define fast-bytes-equal? (resolve-method-g
			   py-equal?
			   `(,<py-bytes>     ,<py-bytes>)))
(define fast-bytea-equal? (resolve-method-g
			   py-equal?
			   `(,<py-bytearray> ,<py-bytearray>)))


(define (f-fast-equal? x y)
  (cond
   ((or (number? x) (string? x))
    (equal? x y))

   ((struct? x)
    (let ((cls1 (class-of x))
	  (cls2 (class-of y)))
      (if (eq? cls1 cls2)
	  (cond
	   ((eq? cls1 <py-list>)
	    (fast-list-equal?  x y))
	   ((eq? cls1 <py-bytes>)
	    (fast-bytes-equal? x y))
	   ((eq? cls1 <HH>)
	    (fast-dict-equal? x y))
	   ((eq? cls1 <set>)
	    (fast-set-equal? x y))
	   ((eq? cls1 <py-bytearray>)
	    (fast-bytea-equal? x y))
	   (else
	    (py-equal? x y)))
	  (py-equal? x y))))
   (else
    (py-equal? x y))))

(define-syntax fast-equal?
  (lambda (x)
    (syntax-case x ()
      ((_ x y z w ...)
       #'(and (fast-equal-mac? x y)
	      (fast-equal-mac? y w ...)))
      
      ((_ x y)
       (let ((tx (get-type-atm #'x))
	     (ty (get-type-atm #'y)))
	 (if (and (= tx ty) tx (not (eq? tx 'o)))
	     (case (tx)
	       ((n)
		#'(= x y))

	       ((s v)
		#'(equal? x y))

	       ((l)
		#'(fast-list-equal? x y))

	       ((d)
		#'(fast-dict-equal x y))

	       ((b)
		#'(fast-bytes-equal x y))
	       
	       ((ba)
		#'(fast-bytearray-equal x y))

	       ((m)
		#'(fast-set-equal x y))

	       (else
		#'(fast-equal?- x y)))
	     #'(fast-equal?- x y)))))))

(define fast-dict-hash    (resolve-method-g py-hash `(,<HH>      )))
(define fast-set-hash     (resolve-method-g py-hash `(,<set>          )))
(define fast-list-hash    (resolve-method-g py-hash `(,<py-list>      )))
(define fast-bytes-hash   (resolve-method-g py-hash `(,<py-bytes>     )))
(define fast-bytea-hash   (resolve-method-g py-hash `(,<py-bytearray> )))
(define fast-vector-hash  (resolve-method-g py-hash `(,<vector>       )))
(define fast-pair-hash    (resolve-method-g py-hash `(,<pair>         )))

;;;; HASH
(define (f-fast-hash x)
  (cond
   ((number? x)
    (hash x pyhash-N))
   
   ((string? x)
    (hash x pyhash-N))

   ((vector? x)
    (fast-vector-hash x))

   ((struct? x)
    (let ((cls (class-of x)))
      (cond
       ((eq? cls <py-bytes>)
        (fast-bytes-hash x))
       ((eq? cls <py-list>)
        (fast-list-hash x))
       ((eq? cls <HH>)
        (fast-dict-hash x))
       ((eq? cls <set>)
        (fast-set-hash x))
       ((eq? cls <py-bytearray>)
        (fast-bytea-hash x))
       (else
        (py-hash x)))))

   ((pair? x)
    (fast-pair-hash x))

   ((or (boolean? x) (symbol? x) (null? x))
    (hash x pyhash-N))
   
   (else
    (py-hash x))))

(define-syntax fast-hash
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (case (get-type-atm #'x)
	 ((l)
	  #'(fast-list-hash x))

	 ((n s p)
	  #'(hash x pyhash-N))
	 
	 ((v)
	  #'(fast-vector-hash x))

	 ((d)
	  #'(fast-dict-hash x))

	 ((b)
	  #'(fast-bytes-hash x))

	 ((ba)
	  #'(fast-bytea-hash x))

	 ((m)
	  #'(fast-set-hash x))

	 (else
	  #'(f-fast-hash x)))))))
	 
(set! (@ (oop      pf-objects     ) fast-equal?  ) f-fast-equal?   )
(set! (@ (oop      pf-objects     ) fast-clear!  ) f-fastlist-clear)
(set! (@ (oop      pf-objects     ) fastlist-ref ) f-fastlist-ref  )
(set! (@ (oop      pf-objects     ) fastlist-set!) f-fastlist-set! )
(set! (@ (language python     hash) fast-hash    ) f-fast-hash     )
(set! (@ (language python     list) fastlist-len ) f-fastlist-len  )

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module ((oop dict) #:select (m? isinstance- issubclass-))
  #:use-module (ice-9 match)
  #:use-module (system syntax)
  #:use-module (language python exceptions)
  #:use-module (language python yield)
  #:use-module (language python try)
  #:use-module (language python dir)
  #:use-module (language python list)
  #:use-module (language python bool)
  #:use-module (language python dict)
  #:use-module (language python string)
  #:use-module (language python def)
  #:export (Module ModuleT private public import __import__ modules))

(define-syntax-rule (aif it p . x) (let ((it p)) (if it . x)))

(define-syntax-rule (in-scheme x)
  (let ((lan (current-language)))
    (dynamic-wind
	(lambda () (current-language 'scheme))
	(lambda () x)
	(lambda () (current-language lan)))))

(define (private mod)
  ((rawref mod '__setprivate__) mod #t))

(define (public mod)
  ((rawref mod '__setprivate__) mod #f))

(define e (list 'e))

(define _k
  (lambda (k)
    (if (string? k)
	(string->symbol k)
	k)))

(define (tr-val x)
  (cond
   ((variable? x)
    (variable-ref x))
   
   (else
    x)))

(define _m
  (lambda (self)
    (if (rawref self '_private)
	(rawref self '_module)
	(rawref self '_export))))

(define (globals self)  
  (aif it (rawref self '_export)
       it
       (rawref self '_module)))

(define (get-derefs self)
  (let* ((m (rawref self '_module)))
    (let lp ((l (module-uses m)) (r '()))
      (if (pair? l)
          (let ((i (car l)))
            (match (module-name i)
              (('language 'python 'module a . u)
               (if (not (member a '(python)))
                   (let ((ll '()))
                     (catch #t
                       (lambda ()
                         (module-for-each
                          (lambda (x v) (set! ll (cons (cons x v) ll))) i))
                       (lambda x (values)))
                     (lp (cdr l) (append ll r)))
                   (lp (cdr l) r)))
              (_ (lp (cdr l) r))))
          r))))

(define (fix x)
  x)

(define inmod (make-fluid #f))

(define (ignore m k v)
  (cond
   ((if (variable? v) (variable-bound? v) #t)
    
    (when (variable? v) (set! v (variable-ref v)))
    (when (symbol?   k) (set! k (symbol->string k)))
  
    (or
     (in "-" k)
     (in "?" k)
     (in "!" k)
     (in "." k)
     (if (module-defined? m '--export-macros)                      
	 (let ((ignore (if (module-defined? m '--ignore--)
			   (module-ref m '--ignore--)
			   '())))
	   (if (memq (string->symbol k) ignore)
	       #t
	       #f))
	 (macro? v))))
   (else
    #t)))

(define-python-class Module ()
  (define _modules (make-hash-table))
  (define __setprivate__
    (lambda (self p)
      (rawset self '_private p)))
  
  (define __contains__
    (lambda (self k)
      (let ((r (py-get self k e)))
        (not (eq? r e)))))
  
  (define _cont
    (lambda (self id pre l nm skip-error?)
      (if id
	  (aif it (rawref self id)
	       (begin
                 (__init__ it pre l nm))
                 
	       (begin                 
		 (rawset self id (Module pre l nm))
                 (_make self #f pre nm skip-error?)))
          
          (_make self #t pre nm skip-error?))))

  (define _contupdate
    (lambda (self id pre l nm)
      (if id
	  (aif it (rawref self id)
	       ((ref it '__update__) pre l nm)
	       (rawset self id (Module pre l nm)))
	  #f)))
  
  (define __init__
    (let ((skipper? #f))
      (lam ((* l) (= skip-error #f))
           (set! skipper? skip-error)
           (apply
            (case-lambda
	     ((self pre l nm)
	      (match l
		 ((name)
                  (rawset self '_path (reverse (cons name pre)))	      
                  (_cont self #f (cons name pre) #f (cons name nm) skipper?))
                 
                 ((name . (and l (name2 . _)))
                  (rawset self '_path (reverse (cons name pre)))
                  (_cont self name2 (cons name pre) l  (cons name nm) #t))))
       
        
	     ((self l nm)
	      (_cont self #f l #f nm #f))
        
	     ((self l)
               (cond
                ((pair? l)
		 (if (and
                      (> (length l) 3)
                      (equal? (list (list-ref l 0)
                                    (list-ref l 1)
                                    (list-ref l 2))
                              '(language python module)))
		     (let* ((nm (reverse (cdr (reverse (cdddr l)))))
			    (l  (reverse l))
			    (n  (car l))
			    (l  (cdr l)))                   
		       (__init__ self l (list n) nm))
                 
		     (let* ((l (reverse l))
			    (n (car l))
			    (l  (cdr l)))                   
		       (__init__ self l (list n) '()))))
          
                ((vector? l)
                 (set! l (vector->list l))
                 (if (and
                      (> (length l) 3)
                      (equal? (list (list-ref l 0)
                                    (list-ref l 1)
                                    (list-ref l 2))
                              '(language python module)))
                     (__init__ self (reverse '(language python module)) (cdddr l) '())
                     (__init__ self l)))
          
                (else           
                 (__init__ self
                           (list->vector
                            (let ((x (map string->symbol
                                          (string-split l #\.))))
                              (match x
			        (('language 'python . _)
                                 x)
                                (('oop . _)
                                 x)
                                (else
                                 (append
                                  '(language python module)
                                  x))))))))))
            l))))

  (define __update__
    (case-lambda
     ((self pre l nm)
      (match l
       ((name)
	(_contupdate self #f   (cons name pre) #f (cons name nm)))
       
       ((name . (and l (name2 . _)))
	(_contupdate self name2 (cons name pre) l  (cons name nm)))))
       

     ((self l nm)
      (_contupdate self #f l #f nm))

     ((self l)
      (if (pair? l)
	  (if (and (> (length l) 3)
		   (equal? (list (list-ref l 0)
				 (list-ref l 1)
				 (list-ref l 2))
			   '(language python module)))
	      (__update__ self (reverse '(language python module))
                          (cdddr l) '()))	      
	  (__update__ self
		      (map string->symbol
			   (string-split l #\.)))))))
  
  (define _make
    (lambda (self p? l nm skip-error?)
      (rawset self '_private #t)
      (if (or p? (not (rawref self '_module)))
	  (begin
	    (rawset self '__name__
                    (string-join
                     (map symbol->string (reverse nm)) "."))
	    (let* ((_module
                    (let lp ((first #t))
                      (if (and first (eq? (car l) '__main__))
                          (let ((me (current-module)))
                            (if (equal? (module-name me) '(__main__))
                                me
                                (lp #f)))
                          (if (rawref self '_skip_error)
                              (let ((m (make-module)))
                                (set-module-name! m (reverse l))
                                m)
                              (if p?
                                  (in-scheme (resolve-module (reverse l)))
                                  (let ((m (make-module)))
                                    (set-module-name! m (reverse l))
                                    m))))))
                   
                   (public-i (and _module
                                  (module-public-interface _module))))
              
              (if (and (not skip-error?) (not public-i)
                       (not (ref self '_skip_error)))
                  (raise (ImportError
                          (format #f "No module named '~a'"
                                  (rawref self '__name__)))))

	      (rawset self '_export   (module-public-interface _module))
	      (rawset self '_module   _module)             
	      (hash-set! _modules l self))))))
      
  (define __getattribute__
    (lambda (self k)
      (define (fail)
	(raise (AttributeError "module '~a' has no attribute '~a'" self k)))
      (define (tr x)
        (cond
         ((macro? x)
          x
          #;
          (__getattribute__ self
                            (string-append
                             (if (symbol? k)
                                 (symbol->string k)
                                 k)
                             "-f")))
         ((variable? x)
          (variable-ref x))
   
         (else
          x)))

      (fix
       (let ((k (_k k)))
         (cond
          ((aif it (rawref self k)
                (if (and (struct? it) (eq? (ref it '__name__) 'Module))
                    it
                    #f)
                it) =>
                (lambda (v) v))          
          ((memq k '(__iter__ __repr__ __dir__ items))
           (lambda x (apply (rawref self k) self x)))
          (else
           (let ((x (aif it (rawref self '_export)
                         (tr (module-ref it k e))
                         e)))
             (if (eq? e x)
                 (let ((x (aif it (_m self)
                               (let ((x (tr (module-ref it k e))))
                                 (if (eq? x e)
                                     e
                                     x))                                   
                               e)))
                   (if (eq? e x)
                       (let ((x (tr (rawref self k e))))
                         (if (eq? e x)
                             (fail)
                             x))
                       x))
                 x))))))))
  
  (define __setattr__
    (lambda (self k v)
      (let ((k     (_k k))
	    (fail
             (lambda () (raise (KeyError "setattr in Module ~a" k)))))
	(if (rawref self k)
	    (fail)
	    (aif m (rawref self '_module)
		 (catch #t
                   (lambda ()
                     (if (module-defined? m k)
                         (module-set! m k v)
                         (module-define! m k v)))			
                   (lambda x (fail)))
		 (fail))))))

  (define __global_setitem__
    (lambda (self k v)
      (let ((k     (_k k))
	    (fail  (lambda () (raise (KeyError "global setattr in Module ~a" k)))))
        (aif m (rawref self '_module)
             (catch #t
               (lambda ()
                 (if (module-defined? m k)
                     (module-set! m k v)
                     (begin
                       (module-define! m k v)
                       (module-export! m (list k)))))
               (lambda x (fail)))
             (fail)))))

  (define __global_getitem__
    (lambda (self k)
      (let ((k     (_k k))
	    (fail  (lambda ()
                     (raise (KeyError "global getitem in Module ~a" k)))))
        (aif m (rawref self '_export)
             (catch #t
               (lambda ()
                 (if (module-defined? m k)
                     (tr-val (module-ref m k))
                     (fail)))
               (lambda x (fail)))
             (fail)))))

  (define __global_get__
    (lambda (self k . es)
      (let ((k     (_k k))
	    (fail  (lambda () (raise (KeyError "global get in Module ~a" k)))))
        (aif m (rawref self '_export)
             (catch #t
               (lambda ()
                 (if (module-defined? m k)
                     (tr-val (module-ref m k))
                     (if (pair? es) (car es) #f)))
               (lambda x (fail)))
             (fail)))))

  ;(define __getitem__ __global_getitem__)
  (define __setitem__ __global_setitem__)
 
  (define __delattr__
    (lambda (self k)
      (define (fail) (raise (KeyError "delattr in Module ~a" k)))
      (aif m (rawref self '_module)
	  (let ((k (_k k)))
	    (if (module-defined? m k)
		(module-remove!  m k)
		(raise (KeyError "delattr of missing key in Module ~a" k))))
	  (fail))))

  (define __dir__
    (lambda (self)
      (let* ((h (slot-ref self 'h))
	     (l '("items"))
             (m (_m self))
             (f (lambda (g) (lambda (x) (g (car x) (cdr x)))))
	     (add (lambda (k v)
                    (let ((kk (symbol->string k)))
                      (if (not (ignore m k v))
                          (set! l (cons (symbol->string k) l)))))))
	(hash-for-each add h)
        (if m (module-for-each add m))
        (if (rawref self '_private) (for-each (f add) (get-derefs self)))
	(aif it (rawref self '_export) (module-for-each add it))
        (hash-for-each add (slot-ref self 'h))
	(py-list l))))

  (define items
    (lambda (self)
      (let* ((h (slot-ref self 'h))
             (l '())
             (m (_m self))
             (f (lambda (g) (lambda (x) (g (car x) (cdr x)))))
             (add (lambda (k v)
                    (let ((kk (symbol->string k)))
                      (if (not (ignore m k v))                               
                          (set! l (cons (list kk (tr-val v))
                                        l)))))))
        (module-for-each add m)
        (if (rawref self '_private) (for-each (f add) (get-derefs self)))
	(module-for-each add (rawref self '_export))
	l)))

  (define __iter__
    (lambda (self)
      (let* ((h (slot-ref self 'h))
	     (l (make-hash-table))
             (m (_m self))
             (f (lambda (g) (lambda (x) (g (car x) (cdr x)))))
	     (add (lambda (k v)
                    (let ((kk (symbol->string k)))
                      (if (not (ignore m k v))
			  (hash-set! l kk #t))))))
        (module-for-each add m)

        (if (rawref self '_private)
            (for-each (f add) (get-derefs self)))
        
	(module-for-each add (rawref self '_export))
        
	(hash-fold (lambda (k v s) (cons k s)) '() l))))

  (define __global_iter__
    (lambda (self)
      (let* ((m (globals self))
             (l '())
	     (add (lambda (k v)
                    (let ((k (symbol->string k)))
                      (if (and (not (ignore m k v)) (variable-bound? v))
                          (set! l (cons (list k (variable-ref v))
                                        l)))))))
        (module-for-each add m)
	l)))

	
  
  (define __repr__
    (lambda (self) (format #f "Module(~a)" (rawref self '__name__))))

  (define __getitem__
    (lambda (self k)
      (define k (if (string? k) (string->symbol k) k))
      (__getattribute__ self k)))) 


(define-syntax import
  (lambda (x)
    (syntax-case x ()
      ((_ (a ...) var)
       #`(import-f #,(case (syntax-local-binding #'var)
		       ((lexical)
			#'var)
		       ((global)
			#'(if (module-defined? (current-module)
					       (syntax->datum #'var))
			      var
			      #f))
		       (else
			#f)) a ...)))))

(define-python-class MetaModuleT (type)
  (define __instancecheck__
    (lambda (self n)
      (isinstance- n Module)))

  (define __subclasscheck__
    (lambda (self x)
      (issubclass- x Module))))
  
                     
(define-python-class ModuleT (Module #:metaclass MetaModuleT)
  (define __init__
    (lambda (self . x)
      (rawset self '_skip_error #t)
      (apply (ref Module '__init__) self x))))

(define (import-f x f . l)
  (if x
      (if (m? x)
	  (begin (apply (rawref x '__update__) x l) x)
	  (apply f l))
      (apply f l)))

(define miss (list 'miss))
(define ms dict)
#;
(define-python-class ms (dict)
  (define __getitem__
    (lambda (self k)
      (let ((ret (get self k miss)))
        (when (eq? ret miss)
          (raise (KeyError "No key in ms[key]")))
        ret)))
  
  (define get
    (lambda* (self k #:optional (e miss))
      (if (string? k)
          (aif it (py-get (ref self '__hashmap__) k #f)
               it
               (if (eq? e miss)
                   None
                   e))))))
          
(define (def? l)
  (py-get modules (string-join (map symbol->string l) ".") #f))

(define modules (ms))
(define (__import__ x . ll)
  (if (pair? x) (set! x (string-join (map symbol->string x) ".")))
  (when (not (bool x))
    (raise (ValueError "Empty module name")))
  
  (if (equal? x "__main__")
      (let ((x (py-get modules "__main__" #f)))
        (if x
            x
            (let ((e (Module (vector '__main__))))
              (pylist-set! modules "__main__" e)
              (pylist-set! modules "__main__" e)
              e)))
      (let ((y (py-get modules x #f)))
        (if y
            y
            (let lp ((e0 #f) (e #f) (x (to-list (py-split x "."))) (r '()))
              (define (find m xs)
                (if (pair? xs)
                    (find (rawref m (string->symbol (car xs))) (cdr xs))
                    m))

              (if (pair? x)
                  (let* ((xx (reverse (cons (car x) r)))
                         (path (py-join "." xx)))
                    (aif it (py-get modules path #f)
                         (if e
                             (lp e0 it (cdr x) (cons (car x) r))
                             (lp it it (cdr x) (cons (car x) r)))
                         (let ((m (find (Module path) (cdr xx))))
                           (if e
                               (begin
                                 (rawset e (string->symbol (car x)) m)
                                 (pylist-set! modules path e0)
                                 (lp e0 m (cdr x) (cons (car x) r)))
                               (begin
                                 (pylist-set! modules path m)
                                 (lp m m (cdr x) (cons (car x) r)))))))
                  e0))))))

(set! (@@ (oop dict) Module) Module)

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python parser-tool)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log parsing scanner)
  #:use-module ((logic guile-log parser)
		#:select (setup-parser
			  f-nl f-nl!			  
			  *current-file-parsing*
			  make-file-reader file-next-line file-skip))
  #:use-module (logic guile-log)
  #:re-export (f-nl f-nl!)
  #:export (f-seq f-seq! f-or f-or! f-not f-not! f-true f-false f-cons f-cons*
		  f-list INDENT <p-lambda> f* ff* ff? f? ff+ f+
		  f-reg f-reg! f-tag f-tag! f-eof f-out f-and f-and!
		  mk-token p-freeze parse f-append
		  .. xx <p-cc>
		  f-pk))

;; Preliminary
(define do-print #f)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))


(begin
  (define-guile-log-parser-tool (<p-lambda> (X XL N M INDENT)) <p-define> .. 
    xx <p-cc>)


  (make-guile-log-scanner-tools <p-lambda> <fail> <p-cc> <succeds> .. 
				(X XL N M INDENT)
				(c) (d)
				s-false s-true s-mk-seq s-mk-and s-mk-or)

  ;; Sets up a standar parser functionals with INDENT field added
  (setup-parser
   <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx
   X XL ((N 0) (M 0) (INDENT (list 0)))
   s-false s-true s-mk-seq s-mk-and s-mk-or))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))

(define-module- (language python expression)
  #:use-module (ice-9 match)
  #:use-module (language python librarys)
  #:export (mk-mk-atom mk-mk-expr))

(eval-when (compile eval load)
  (define (pp . x)  #;(apply pk x) (car (reverse! x))))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define bin-there (make-hash-table))

(define-syntax-rule (cache x n on-false code)
  (let ()
    (define (f it)
      (let ((ret code))
	(cond
	 ((or ret on-false)
	  (hashq-set! bin-there x (cons (cons n ret) it)))
	 (else
	  (values)))
	ret))
    
    (aif it (hashq-ref bin-there x)
	 (aif it2 (assq n it)
	      (cdr it2)
	      (f it))
	 (f '()))))


(define (listify x)
  (syntax-case x ()
    ((y ...) #'(y ...))
    (y       #'y)))

(define-syntax-rule (mk-mk-expr mk-expr one-atom doer doer-lib on-false? void
				signature)
  (define-syntax-rule (mk-expr n p integer-expr integer-atom
			       libber
			       f-proj-i
			       f-tri-ops-i-i
			       f-bin-ops-i-i
			       f-un-op-i)
    (define (integer-expr x)
      (define (f op x)       
	(set! x (listify x))
		  
	(cond
	 ((memq (syntax->datum op) f-proj-i)
	  p)
       
	 ((case (length x)
	    ((3)
	     (and
	      (memq (syntax->datum op) f-tri-ops-i-i)
	      (and-map integer-atom x)
	      p))
	    
	    ((2)
	     (and
	      (memq (syntax->datum op) f-bin-ops-i-i)
	      (and-map integer-atom x)
	      p))
	     
	    ((1)
	     (and
	      (memq (syntax->datum op) f-un-op-i)
	      (and-map integer-atom x)
	      p))

	    (else
	     #f))
	  p)
      
	 ((or
	   (signature #t (syntax->datum op) x)
	   (signature #f (syntax->datum op) x)
	   (libber? libber #f (syntax->datum op))
	   (doer-lib  #t (syntax->datum op) x)	   
	   (doer         (syntax->datum op) x))
	  p)

	 (else
	  #f)))

      (pp 'integer-expr (syntax->datum x)
	  (cache x n on-false?
	    (if (eq? p void)
		void
	     (syntax-case x (@@ @)
	      ((app . x)
	       (eq? (syntax->datum #'app) 'py-apply)
	       (integer-expr #'x))

	      (((@@ _ ref-x) . u)
	       (eq? (syntax->datum #'ref-x) 'ref-x)
	       (syntax-case #'u ()	   	
		 ((lib (#:identifier ((@ _ q) op)) (#:apply . x))
		  (or (signature (syntax->datum #'lib)
				 (syntax->datum #'op)  #'x)
		      (doer-lib (syntax->datum #'lib)
				(syntax->datum #'op) #'x)
		      (libber? libber
			       (syntax->datum #'lib)
			       (syntax->datum #'op))))

		 ((op (#:apply . x))
		  (integer-expr #'(op . x)))
	   
		 ((o (#:call-obj op) (#:apply . x))
		  (integer-expr #'(op . x)))

		 (_
		  void)))
	    
	      (((@@ _ setwrap) x)
	       (eq? (syntax->datum #'setwrap) 'setwrap)
	       (integer-expr #'x))

	      (((@@ _ op) . x)
	       (f #'op #'x))
	      
	      (((@ _ op) . x)
	       (f #'op #'x))

	      ((op . x)
	       (f #'op #'x))

	      (a
	       (integer-atom #'a)))))))))

(define-syntax-rule (mk-mk-atom mk-atom myeq? get-stx-type-atm all void)
  (define-syntax-rule (mk-atom string-atom n string-expr string?)
    (define (string-atom x)
      (syntax-case x (@@)
	(((@@ _ setwrap) x)
	 (eq? (syntax->datum #'setwrap) 'setwrap)
	 (string-atom #'x))
        
	((_ . _)
	 (string-expr x))
	
	(a
	 (let ((t  (get-stx-type-atm #'a))
	       (aa (syntax->datum  #'a)))
	   (or (if (eq? n void)
		   n
		   (if (eq? n all)
		       (if (eq? t void)
			   #f
			   n)
		       (myeq? t n)))
	       (string? aa))))))))

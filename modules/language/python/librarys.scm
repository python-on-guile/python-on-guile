;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python librarys)
  #:export (add-lib-map
	    add-lib-map-mac
	    add-lib-map-mac2
	    add-lib-map-mac3
	    libber?))
(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define lib-mapper (make-hash-table))
(define (add-lib-map name nm)
  (when (string? name) (set! name (string->symbol name)))
  (when (string? nm  ) (set! nm   (string->symbol nm  )))

  (aif it (hashq-ref lib-mapper (current-module))
       (hashq-set! it name nm)
       (let ((it (make-hash-table)))
	 (hashq-set! it name nm)
	 (hashq-set! lib-mapper (current-module) it))))

(define-syntax add-lib-map-mac
  (lambda (x)
    (syntax-case x ()
      ((_ (d) nm)
       (begin
	 (add-lib-map (syntax->datum #'d) (syntax->datum #'nm))
	 #'#t))
      (_
       #'#t))))

(define-syntax add-lib-map-mac2
  (lambda (x)
    (syntax-case x ()
      ((_ (d))
       (begin
	 (add-lib-map (syntax->datum #'d) (syntax->datum #'d))
	 #'#t))
      (_
       #'#t))))

(define-syntax add-lib-map-mac3
  (lambda (x)
    (syntax-case x ()
      ((_ d)
       (begin
	 (add-lib-map (syntax->datum #'d) #t)
	 #'#t))
      (_
       #'#t))))
  
(define (libber? libber mod x)
  (or-map (lambda (xx)
	    (let ((lib (car xx)))
	      (if mod
		  (and (aif it (hashq-ref lib-mapper (current-module))
			    (aif it (hashq-ref it lib)
				 (equal? it mod)
				 #f)
			    #f)
		       (if (member x (cadr xx)) #t #f))
		  (if (member x (cadr xx)) #t #f))))
	  libber))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python list)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (ice-9 format)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module ((oop dict) #:select (int py-mod-))
  #:use-module (language python type)
  #:use-module (language python hash)
  #:use-module (language python tuple)
  #:use-module (language python exceptions)
  #:use-module (language python yield)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python bool)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:re-export (pylist-ref pylist-set! pylist-delete!)
  #:export (to-list to-pylist <py-list> py-list
            pylist-append! pylist-remove!
            pylist-slice pylist-subset! pylist-reverse!
            pylist-pop! pylist-count pylist-extend! len 
            pylist-insert! pylist-sort!
            pylist-index pylist-null py-clear
            pylist pylist-listing Slice
            py-all py-any py-reversed print-pair fastlist-len
	    pylist-ackumulator pylist-map
            += *= py+= py*= mk-list-slice mk-list-subset! index-ref
	    pylist-mac pylist-filter pylist-map))

(define vec-i 8)
(define n-i   9)

(define (pylist? x)
  (eq? (class-of x))
  (ref py-list '__goops__))
  
  
       

(define-python-class Slice    ())
(define-syntax-rule (pset o n val . rest)
  (cond
   ((number? n)
    (let () . rest))
   
   ((pair? n)
    (if (> (length n) 1)
        (fastlist-set! (fastlist-ref o (car n)) (cdr n) val)
        (fastlist-set! o (car n) val)))
   
   ((vector? n)
    (pylist-subset! o (vector-ref n 0) (vector-ref n 1) (vector-ref n 2) val))
   
   (else
    (let () . rest))))

(define-syntax-rule (pref o n . rest)
  (cond
   ((number? n)
    (let () . rest))
   
   ((pair? n)
    (if (> (length n) 1)
        (pylist-ref (pylist-ref o (car n)) (cdr n))
        (pylist-ref o (car n))))
   
   ((vector? n)
    (pylist-slice o (vector-ref n 0) (vector-ref n 1) (vector-ref n 2)))
   
   (else
    (let () . rest))))

(define-method (+ (x <null>) (y <pair>))
  (let lp ((l y))
    (if (pair? l)
	(cons (car l) (lp (cdr l)))
	'())))

(define-method (+ (x <pair>) (y <null>))
  (let lp ((l x))
    (if (pair? l)
	(cons (car l) (lp (cdr l)))
	'())))

(define-method (in x (y <null>)) #f)
(name-object in)

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-class <py-list>  () vec nn)
(name-object <py-list>)

(cpit <py-list> (o (lambda (o n l)
		      (slot-set! o 'nn n)
		      (slot-set! o 'vec (list->vector l)))
		   ((@ (guile) list)
		    (slot-ref o 'nn)
		    (vector->list (slot-ref o 'vec)))))

(define (index-ref x e)
  (define (er)
    (if e
        (TypeError "Index not an integer or has method __index__")
        x))

  (or (and (number? x) (integer? x) x)
      (and (struct? x)
           (aif it (ref x '__index__)
                (it)
                (er)))
      (er)))

(define (index->nat x n)
  (define (check x)
    (when (or (< x 0) (>= x n))
	  (raise (IndexError "Index out of range")))
    x)

  (check
   (if (< x 0)
       (+ n x)	
       x)))

(define-method (pylist-delete! (o <py-list>) k)
  (let* ((n (struct-ref o n-i))
         (k (index-ref k #t))
         (k (index->nat k n)))
    
    (pylist-subset! o k (+ k 1) None pylist-null)))

(define-method (pylist-delete! (o <py>) k)
  (aif it (ref-in-class o '__delitem__)
       (it o k)
       (begin
         (next-method))))

(name-object pylist-delete!)

(define py-list-delete! (resolve-method-g pylist-delete! `(,<py-list> _)))

(define-method (py-hash (o <py-list>))
  (let ((n (min complexity (struct-ref o n-i)))
        (v (struct-ref o vec-i)))
    (let lp ((i 0) (s 0))
      (if (< i n)
          (lp (+ i 1)
              (xy (fast-hash (vector-ref v i)) s))
          s))))

(define-method (to-list x)
  (catch #t
    (lambda ()
      (if (or (pair? x) (null? x))
          x
          (for ((i : x)) ((r '()))
               (cons i r)
               #:final (reverse r))))
    (lambda xx
      (raise (TypeError (+ "cannot unpack non-iterable "
                           (pyname x)
                           " object"))))))

(define-method (pyname (u <py-list>))
  "list")

(define-method (to-list (x <py>))
  (aif it (ref-in-class x '__tolist__)
       (it x)
       (next-method)))
(name-object to-list)

(defpair (to-list x) x)

(define-method (to-list (x <yield>))
  (define l '())
  (catch 'python
    (lambda ()
      (let lp ()
        (set! l (cons (next x) l))
        (lp)))
    (lambda (tag c v t)
      (if (eq? c StopIteration)
          (reverse l)
          (throw 'python c v t)))))

(define-method (to-list (x <py-list>))
  (let ((vec (struct-ref x vec-i))
        (n   (struct-ref x n-i)))
    (let lp ((i 0))
      (if (< i n)
          (cons (vector-ref vec i) (lp (+ i 1)))
          '()))))

(define-method (to-pylist (l <py-list>))
  l)

(name-object to-pylist)

(defpair (to-pylist l)
  (let* ((n   (length l))
         (o   (pylist (* 2 n)))
	 (vec (struct-ref o vec-i)))
    
    (let lp ((l l) (i 0))
      (if (pair? l)
          (begin
            (vector-set! vec i (car l))
            (lp (cdr l) (+ i 1)))))
    
    (struct-set! o n-i n)
    o))

(define-method (to-pylist (l <vector>))
  (pylist l))

(define-method (to-pylist (o <string>))
  (to-pylist (string->list o)))

(define-method (bool (o <py-list>))
  (if (= (fastlist-len o) 0)
      #f
      o))

(define-method (bool (o <vector>))
  (if (= (fastlist-len o) 0)
      #f
      o))

(define-method (bool (o <string>))
  (if (= (fastlist-len o) 0)
      #f
      o))

(define-method (to-pylist l)
  (if (null? l)
      (pylist 0)
      (raise (ValueError "not able to make a pylist"))))

;;; REF
(define-method (pylist-ref (o <py-list>) nin)
  (let ((n0 (index-ref nin #f)))
    (pref o n0
	  (let* ((N (struct-ref o n-i))
		 (n (index->nat n0 N)))
	    (if (and (>= n 0) (< n N))
		(vector-ref (struct-ref o vec-i) n)
		(raise (IndexError "Index out of range for list")))))))

(defpair (pylist-ref o n)
  (let ((n (index-ref n #f)))
    (pref o n
          (let* ((n (index->nat n (length o)))
                 (N (length o)))
            (if (and (>= n 0) (< n N))          
                (list-ref o n)
                (raise (IndexError
                        (format #f "scm list wrong index ~a" n))))))))
  
(define-method (pylist-ref (o <vector>) n)
  (let ((n (index-ref n #f)))
    (pref o n
          (vector-ref
           o (let* ((N (vector-length o))
                    (n (index->nat n  N)))
               (if (and (>= n 0) (< n N))
                   n
                   (raise (IndexError "index out of range in tuple ref"))))))))

(define py-list-ref (resolve-method-g pylist-ref `(,<py-list>      _)))

;;; SET
(define-method (pylist-set! (o <py-list>) nin val)
  (let ((n0 (index-ref nin #f)))
    (pset o n0 val
	  (let* ((N (struct-ref o n-i))
		 (n (index->nat n0 N )))
	    (if (and (>= n 0) (< n N))
		(vector-set! (struct-ref o vec-i) n val)
		(raise (IndexError "Index out of range in list set")))))))
	      
(name-object pylist-set!)

(defpair (pylist-set! o n val)
  (let ((n (index-ref n #f)))
    (pset o n val
          (list-set!
           o
           (let* ((N (length o))
                  (n (index->nat n N)))
             (if (and (< n N) (>= n 0))
                 n
                 (raise (IndexError "Index out of range in tuple set"))))
           val))))



(define-method (pylist-set! (o <vector>) n val)
  (let ((n (index-ref n #f)))
    (pset o n val
          (vector-set! o (let* ((N (vector-length o))                          
                                (n (index->nat  n N)))
                           (if (and (>= n 0) (< n N))
                               n
                               (raise
                                (IndexError
                                 "Index out of range in tuple set"))))
                       val))))


;;SLICE
(define-method (pylist-slice (o <py>) . l)
  (aif it (ref-in-class o '__getitem__)
       (it o (apply vector l))
       (next-method)))
(name-object pylist-slice)
              
(define-syntax-rule (mk-list-slice o M n1 n2 n3 <py-list> get-n code)
  (define-method (pylist-slice (o <py-list>) n1 n2 n3)
    (define N get-n)
    (define (f n)
      (let ((x (if (< n 0) (+ N n) n)))
        (if (< x 0)
            0
            (if (> x N)
                N
                x))))

    (define-syntax-rule (check n)
      (or (and (number? n) (integer? n))
          (eq? n None)
          (and (struct? n)
               (aif it (ref n '__index__)
                    (begin
                      (set! n (it))
                      #t)
                    #f))))

    (when (not (and (check n1) (check n2) (check n3)))
      (raise (TypeError "slice indices must be integers or None or have an __index__ method")))

    (let* ((n1   (f (if (eq? n1 None) 0  (index-ref n1 #t))))
           (n2   (f (if (eq? n2 None) N  (index-ref n2 #t))))
           (n3
	    (let ((n3 (if (eq? n3 None) 1  n3)))
	      (cond
	       ((> n3 0)
		(when (> n1 n2)
		  (raise (IndexError "second index is lower then first"))))
	       ((< n3 0)
		(when (> n2 n1)
		  (raise (IndexError "second index is higher then first"))))
	       (else
		(raise (IndexError "can't have index step 0"))))
	      n3))
		     
           (M    (cond
		  ((= n3 1)		   
		   (- n2 n1))
		  
		  ((> n3 0)
		   (let lp ((i n1) (s 0))
		     (if (< i n2)
			 (lp (+ i n3) (+ s 1))
			 s)))

		  ((< n3 0)
		   (let lp ((i n1) (s 0))
		     (if (> i n2)
			 (lp (+ i n3) (+ s 1))
			 s))))))
                           
                           
      code)))

(mk-list-slice o M n1 n2 n3 <py-list>  (struct-ref o n-i)
          (let* ((vec    (struct-ref o vec-i))
		 (ret    (pylist M))
		 (newvec (struct-ref ret vec-i)))
            (cond
	     ((= n3 1)
	      (begin
		(vector-move-left! vec n1 n2 newvec 0)
		(struct-set! ret n-i M)
		ret))
	     
	     ((> n3 0)	      
	      (let lp ((i n1) (j 0))
		(if (< i n2)
		    (begin
		      (vector-set! newvec j (vector-ref vec i))
		      (lp (+ i n3) (+ j 1)))
		    (begin
		      (struct-set! ret n-i  M)
		      ret))))
	     
	     ((< n3 0)	      
	      (let lp ((i n1) (j 0))
		(if (> i n2)
		    (begin
		      (vector-set! newvec j (vector-ref vec i))
		      (lp (+ i n3) (+ j 1)))
		    (begin
		      (struct-set! ret n-i  M)
		      ret)))))))

(mk-list-slice o M n1 n2 n3 <vector>  (vector-length o)
               (let ((newvec (make-vector M)))
                 (cond
		  ((= n3 1)
		   (begin
		     (vector-move-left! o n1 n2 newvec 0)
		     newvec))
		  
		  ((> n3 0)
		   (let lp ((i n1) (j 0))
		     (if (< i n2)
			 (begin
			   (vector-set! newvec j (vector-ref o i))
			   (lp (+ i n3) (+ j 1)))
			 newvec)))

		  ((< n3 0)
		   (let lp ((i n1) (j 0))
		     (if (> i n2)
			 (begin
			   (vector-set! newvec j (vector-ref o i))
			   (lp (+ i n3) (+ j 1)))
			 newvec))))))

(mk-list-slice o M n1 n2 n3 <string>  (string-length o)
               (let ((newvec (make-string M)))
		 (cond
		  ((= n3 1)
		   (begin
		     (string-copy! newvec 0 o n1 n2)
		     newvec))
		  
		  ((> n3 0)
		   (let lp ((i n1) (j 0))
		     (if (< i n2)
			 (begin
			   (string-set! newvec j (string-ref o i))
			   (lp (+ i n3) (+ j 1)))
			 newvec)))

		  ((< n3 0)
		   (let lp ((i n1) (j 0))
		     (if (> i n2)
			 (begin
			   (string-set! newvec j (string-ref o i))
			   (lp (+ i n3) (+ j 1)))
			 newvec))))))

(mk-list-slice o M n1 n2 n3 <pair>  (length o)
               (reverse!
		(cond
		 ((> n3 0)
		  (let lp ((o (list-cdr-ref o n1)) (i n1))
		    (if (< i n2)
			(cons (car o)
			      (lp (list-cdr-ref o n3) (+ i n3)))
			'())))
		 ((< n3 0)
		  (let lp ((o (list-cdr-ref o n1)) (i n1))
		    (if (> i n2)
			(cons (car o)
			      (lp (list-cdr-ref o n3) (+ i n3)))
			'()))))))
          
(define-method (pylist-slice o n1 n2 n3)
  (if (null? o)
      (to-list (pylist-slice (to-pylist o) n1 n2 n3))
      (raise (TypeError "cannot slice this type"))))
  
;;SUBSET
(define-method (pylist-subset! (o <py>) n1 n2 n3 val)
  (aif it (ref-in-class o '__setitem__)
       (it o (vector n1 n2 n3) val)
       (next-method)))

(name-object pylist-subset!)

(define clearvec (make-vector 1000 #f))
(define (clear-vec vec n m)
  (let lp ((nn n) (mm (min 1000 (- m n))))
    (if (< nn m)
        (let ((nnn (+ nn mm)))
          (vector-move-left! clearvec 0 mm vec nn)
          (lp nnn (min 1000 (- n nnn)))))))

(define (has-vector? x)
  (cond
   ((vector? x) (values x (vector-length x)))
   ((struct? x) 
    (cond
     ((eq? (class-of x) <g-list>)
      (values (slot-ref x 'vec) (slot-ref x 'nn)))
     ((is-a? x <g-list>)
      (values (slot-ref x 'vec) (slot-ref x 'nn)))
     (else
      (values #f #f))))
   (else
    (values #f #f))))

(define-syntax-rule (mk-list-subset! <py-list> get-n maker checker)
  (define-method (pylist-subset! (o <py-list>) n1 n2 n3 val)
    (define N (get-n o))

    (define-syntax-rule (check n)
      (or (and (number? n) (integer? n))
          (eq? n None)
          (and (struct? n)
              (aif it (ref n '__index__)
                   (begin
                     (set! n (it))
                     #t)
                   #f))))

    (define-syntax-rule (get1 n default)
      (if (eq? n None)
	  default
	  (index->nat n N)))

    (define-syntax-rule (get2 n default)
      (if (eq? n None)
	  default
	  n))
    
    (when (not (and (check n1) (check n2) (check n3)))
      (raise (TypeError "slice indices must be integers or None or have an __index__ method")))

    (let* ((n1   (get1 n1 0))
	   (n2   (get1 n2 N))
	   (n3   (get2 n3 1))
	   (N2   (fastlist-len val)))
        
        (define (mkq set ref1 ref2 chk fini move1 move2 clear)
          (if (= n3 1)
              (let* ((M0 (- n2 n1))
                     (M1 (min M0 N2))
                     (k1 (+ n1 M1))
                     (d  (- N n2))
                     (k2 (+ k1 d)))
                (move1 0 M1 n1 k1)
                (when (> M0 N2)                 
                  (chk)
                  (move2 n2 N k1 k2)
                  (clear k2 N))
                (fini k2))
              (let lp ((i 0) (j n1))
                (if (< j n2)
                    (if (< i N2)
                        (begin
                          (set j (ref1 i))
                          (lp (+ i 1) (+ j n3)))
                        (let lp ((j2 j))
                          (if (< j2 n2)
                              (lp (+ j2 n3))
                              (let lp ((k1 j) (k2 j2))
                                (chk)
                                (if (< k2 N)                                  
                                    (begin
                                      (set k1 (ref2 k2))
                                      (lp (+ k1 1) (+ k2 1)))
                                    (let lp ((i k1))
                                      (if (< i N)
                                          (begin
                                            (set i #f)
                                            (lp (+ i 1)))
                                          (fini k1))))))))))))

        (let ((none (lambda x 1)))
          (mkq none none none checker none none none none)
          
          (call-with-values (lambda () (maker o val))
            (lambda (set ref1 ref2 fini move1 move2 clear)
              (mkq set ref1 ref2 none fini move1 move2 clear))))
        
        (values))))


(mk-list-subset! <py-list>
                 (lambda (o) (struct-ref o n-i))
                 (lambda (o val)
                   (let ((vec (struct-ref o vec-i))
                         (ref (get-ref-for-sequence val)))
                     (values
                      (lambda (k v) (vector-set! vec k v))
                      ref
                      (lambda (l)   (vector-ref vec l))
                      (lambda (n)   (slot-set! o 'nn n))
                      (aif it (has-vector? val)
                           (lambda (n1 m1 n2 m2)
                             (vector-move-left! it n1 m1 vec n2))
                           (lambda (n1 m1 n2 m2)
                             (let lp ((i n1) (j n2))
                               (if (< i n1)
                                   (begin
                                     (vector-set! vec i (ref j))
                                     (lp (+ i 1) (+ j 1)))))))
                      (lambda (n1 m1 n2 m2)
                        (vector-move-left! vec n1 m1 vec n2))
                      (lambda (n m)
                        (clear-vec vec n m)))))
                 values)
                 


(mk-list-subset! <vector>
                 (lambda (vec) (vector-length vec))
                 (lambda (vec val)
                   (let ((ref (get-ref-for-sequence val)))
                     (values
                      (lambda (k v) (vector-set! vec k v))
                      ref
                      (lambda (l) (vector-ref vec l))
                      values
                      (aif it (has-vector? val)
                           (lambda (n1 m1 n2 m2)
                             (vector-move-left! it n1 m1 vec n2))
                           (lambda (n1 m1 n2 m2)
                             (let lp ((i n1) (j n2))
                               (if (< i n1)
                                   (begin
                                     (vector-set! vec i (ref j))
                                     (lp (+ i 1) (+ j 1)))))))
                      values
                      values)))
                 (lambda ()
                   (raise (ValueError "slice set can't resize a vector"))))



(mk-list-subset! <pair>
                 (lambda (o) (length o))
                 (lambda (o val)
                   (let* ((ref (get-ref-for-sequence val))
                          (l   o)
                          (kk  0)
                          (set (lambda (k v)
                                 (let ((ll (list-cdr-ref l (- k kk))))
                                   (set! l ll)
                                   (set! kk k)
                                   (set-car! ll v)))))
                     (values
                      (lambda (k v) (set k v))
                      ref
                      (let ((ll   #f)
                            (kkk  0))
                        (lambda (k)
                          (if ll
                              (let ((lll (list-cdr-ref ll (- k kkk))))
                                (set! ll lll)
                                (set! kkk k)
                                (car lll))
                              (let ((lll (list-cdr-ref l (- k kk))))
                                (set! ll lll)
                                (set! kkk k)
                                (car lll)))))
                      values
                      (lambda (n1 m1 n2 m2)
                        (let lp ((l (list-cdr-ref l n1)) (i n1) (j n2))
                          (if (< i m1)
                              (begin
                                (set-car! l (ref j))
                                (lp (cdr l) (+ i 1) (+ j 1))))))
                      values)))
                              
                 (lambda ()
                   (raise (ValueError "slice set can't resize a vector"))))

(mk-list-subset! <string>
                 (lambda (s) (string-length s))
                 (lambda (s val)                   
                   (let ((ref (if (string? val)
                                  (lambda (k) (string-ref val k))
                                  (lambda (k)
                                    (let ((ret (pylist-ref val k)))
                                      (when (not (string? ret))
                                        (raise ValueError "String is needed"))
                                      (string-ref ret 0))))))
                     (values
                      (lambda (k v) (string-set! s k v))
                      ref
                      (lambda (k) (string-ref s k))
                      values
                      (lambda (n1 m1 n2 m2)
                        (let lp ((i n1) (j n2))
                          (if (< i m1)
                              (begin
                                (string-set! s i (ref j))
                                (lp (+ i 1) (+ j 1))))))
                      values)))
                          
                 (lambda ()
                   (raise (ValueError "slice set can't resize a vector"))))


;;APPEND
(define-method (pylist-append! (o <py-list>) val)
  (let* ((vec (struct-ref o vec-i))
	 (n   (struct-ref o n-i))
	 (N   (vector-length vec)))
    (if (< n N)
	(vector-set! vec n val)
	(let* ((N2    (max 1 (* 2 N)))
	       (vec2 (make-vector N2)))
	  (vector-move-left! vec 0 N vec2 0)
	  (vector-set! vec2 n val)
	  (struct-set! o vec-i vec2)))
    (struct-set! o n-i (+ n 1)))
  (values))

(name-object pylist-append!)

(define-method (pylist-append! o n)  
  (raise (AttributeError ((@ (guile) format) #f "appendint of ~s is missing in class ~s" n o))))

(define-method (pylist-append! (o <py>) n . l)
  (aif it (ref-in-class o 'append)
       (apply it o n l)
       (next-method)))

(define py-list-append!
  (resolve-method-g pylist-append! `(,<py-list>      _)))

(define-method (write (o <py-list>) . l)
  (define port (if (null? l) #t (car l)))
    (let* ((l (map (lambda (x) (print-scm x write)) (to-list o))))
      (if (null? l)
          (format port "[]")
          (format port "[~a~{, ~a~}]" (car l) (cdr l)))))
  
(define-method (display (o <py-list>) . l)
  (define port (if (null? l) #t (car l)))
  (let* ((l (map (lambda (x) (print-scm x display)) (to-list o))))
    (if (null? l)
        (format port "[]")
        (format port "[~a~{, ~a~}]" (car l) (cdr l)))))


(define-method (+ (o1 <py-list>) (o2 <py-list>))
  (let* ((vec1 (struct-ref o1 vec-i))
         (vec2 (struct-ref o2 vec-i))        
         (n1   (struct-ref o1 n-i))
         (n2   (struct-ref o2 n-i))
         (n    (+ n1 n2))
	 (o    (pylist n))
         (vec  (struct-ref o vec-i)))
    
    (vector-move-left! vec1 0  n1 vec 0)
    (vector-move-left! vec  0  n2 vec n1)
    
    (struct-set! o n-i n)
    o))

(define-method (+ (o1 <null>) (o2 <vector>))
  
  (let* ((n   (fastlist-len o2))
         (ret (make-vector n)))
    (vector-move-left! o2 0 n ret 0)
    ret))

(define-method (+ (o2 <vector>) (o1 <null>))  
  (let* ((n   (fastlist-len o2))
         (ret (make-vector n)))
    (vector-move-left! o2 0 n ret 0)
    ret))

(define-method (+ (o1 <pair>) (o2 <vector>))
  (let* ((n   (fastlist-len o2))
         (ret (make-vector (+ (fastlist-len o1) n))))
    (let lp ((i 0) (l o1))
      (if (pair? l)
          (begin
            (vector-set! ret i (car l))
            (lp (+ i 1) (cdr l)))
          (vector-move-left! o2 0 n ret i)))
    
    ret))

(define-method (+ (o1 <vector>) (o2 <vector>))
  (let* ((n1   (fastlist-len o1))
         (n2   (fastlist-len o2))
         (ret (make-vector (+ n1 n2))))
    (vector-move-left! o1 0 n1 ret 0)
    (vector-move-left! o2 0 n2 ret n1)
    ret))

(define-method (+ (o2 <vector>) (o1 <pair>))
  (let* ((n   (fastlist-len o2))
         (ret (make-vector (+ (fastlist-len o1) n))))

    (vector-move-left! o2 0 n ret 0)
    
    (let lp ((i n) (l o1))
      (if (pair? l)
          (begin
            (vector-set! ret i (car l))
            (lp (+ i 1) (cdr l)))))
    ret))

(define-method (+ (o1 <pair>) (o2 <pair>))
  (append o1 o2))

(define-method (+ (o1 <py-tuple>) o2)
  (+ (slot-ref o1 'l) o2))

(define-method (+ o2 (o1 <py-tuple>))
  (+ o2 (slot-ref o1 'l)))

(define-method (+ (o1 <string>) (o2 <string>))
  (string-append o1 o2))

(define-method (+ (o1 <symbol>) (o2 <symbol>))
  (string->symbol
   (string-append
    (symbol->string o1)
    (symbol->string o2))))

(define (*-move vec n val)
  (let lp ((i 2) (j 0) (k n))
    (when (<= i val)
      (begin
        (vector-move-right! vec j k vec k)
        (let* ((ii (min (* 2 i) val)))
          (when (< i ii)
            (lp ii (* n (- (* 2 i) ii)) (+ k (- k j)))))))))

(define (*-move-str vec n val)
  (let lp ((i 2) (j 0) (k n))
    (when (<= i val)
      (begin
        (string-copy! vec k vec j k)
        (let* ((ii (min (* 2 i) val)))
          (when (< i ii)
            (lp ii (* n (- (* 2 i) ii)) (+ k (- k j)))))))))

(define-method (* (x <integer>) (o1 <py-list>)) (* o1 x))
(define-method (* (o1 <py-list>) (x <integer>))
  (let* ((vec  (struct-ref o1 vec-i))
         (n    (struct-ref o1 n-i))
         (n2   (* n x))
	 (o    (pylist n2))
	 (vec2 (struct-ref o vec-i)))
    
    (when (> x 0)      
      (vector-move-left! vec 0 n vec2 0)
      (when (> x 1)
        (*-move vec2 n x)))
    
    (struct-set! o n-i n2)
    o))

(define-method (* (x <integer>) (vec <string>)) (* vec x))
(define-method (* (vec <string>) (x <integer>))
  (let* ((n    (string-length vec))
         (n2   (* n x))
         (vec2 (make-string n2)))

    (when (> x 0)      
      (string-copy! vec2 0 vec 0 n)
      (when (> x 1)
        (*-move-str vec2 n x)))

    vec2))

(define-method (* (vec <vector>) (x <integer>))
  (let* ((n    (vector-length vec))
         (n2   (* n x))
         (vec2 (make-vector n2)))

    (when (> x 0)      
      (vector-move-left! vec 0 n vec2 0)
      (when (> x 1)
        (*-move vec2 n x)))
    
    vec2))

(define-method (* (x <integer> ) (l <pair>)) (* l x))
(define-method (* (x <py-tuple>) l) (* (slot-ref x 'l) l))
(define-method (* l (x <py-tuple>)) (* l (slot-ref x 'l)))
(define-method (* (l <pair>) (x <integer>))
  (let lp1 ((i 0))
    (if (< i x)
        (let lp2 ((k l))
            (if (pair? k)
                (cons (car k) (lp2 (cdr k)))                      
                (lp1 (+ i 1))))
        '())))


;;REVERSE
(define-method (pylist-reverse! (o <py-list>))
  (let* ((N   (struct-ref o n-i))
         (M   (- N 1))
         (n   (floor-quotient N 2))
         (vec (struct-ref o vec-i)))
    (let lp ((i 0))
      (if (< i n)
          (let ((swap (vector-ref vec i))
                (k (- M i)))
            (vector-set! vec i (vector-ref vec k))
            (vector-set! vec k swap)
            (lp (+ i 1)))))))
(name-object pylist-reverse!)

(define-method (pylist-reverse! (o <py>) . l)
  (aif it (ref-in-class o 'reverse) 
       (apply o it l)
       (next-method)))

;;POP!
(define-method (pylist-pop! (o <py-list>) . l)
  (let* ((n   (struct-ref o n-i))
	 (m   (- n 1))
	 (vec (struct-ref o vec-i)))
    (if (> n 0)
	(let ((ret (vector-ref vec m)))
	  (struct-set! o n-i m)
	  (vector-set! vec m #f)
	  ret)
	(raise IndexError "pop from empty list"))))

(name-object pylist-pop!)

(define-method (pylist-pop! (o <py>) . l)
  (aif it (ref-in-class o 'pop) 
       (apply it o l)
       (next-method)))

;;COUNT
(define-method (pylist-count (o <py-list>) q)
  (let* ((n   (struct-ref o n-i))
         (vec (struct-ref o vec-i)))
    (let lp ((i 0) (sum 0))
      (if (< i n)
          (if (fast-equal? (vector-ref vec i) q)
              (lp (+ i 1) (+ sum 1))
              (lp (+ i 1) sum      ))
          sum))))
(name-object pylist-count)

(define-method (pylist-count (s <string>) q)
  (let* ((n   (string-length s))
         (q   (if (and (string? q) (>= (string-length q) 1))
                  (string-ref q 0)
		  (raise (ValueError "not a valid string in str count")))))
    (let lp ((i 0) (sum 0))
      (if (< i n)
          (if (eq? (string-ref s i) q)
              (lp (+ i 1) (+ sum 1))
              (lp (+ i 1) sum      ))
          sum))))

(define-method (pylist-count (s <vector>) q)
  (let* ((n (vector-length s)))
    (let lp ((i 0) (sum 0))
      (if (< i n)
          (if (fast-equal? (vector-ref s i) q)
              (lp (+ i 1) (+ sum 1))
              (lp (+ i 1) sum      ))
          sum))))

(defpair (pylist-count l q)
    (let lp ((l l) (sum 0))
      (if (pair? l)
          (if (eq? (car l) q)
              (lp (cdr l) (+ sum 1))
              (lp (cdr l) sum      ))
          sum)))

(define-method (pylist-count (o <py>) . l)
  (aif it (ref-in-class o 'count) 
       (apply it o l)
       (next-method)))

;; extend!
(define-method (pylist-extend! (o <py-list>) iter)
  (for ((x : iter)) ()
       (py-list-append! o x)))
(name-object pylist-extend!)

(define-method (pylist-extend! (o <py>) . l)
  (aif it (ref-in-class o 'extend) 
       (apply it o l)
       (next-method)))

;; equal?
(define-method (py-equal? o1             (o2 <py-list>))
  (equal o2 o1))

(define-method (py-equal? (o1 <py-list>) o2)
  (equal o1 o2))

(define (equal o1 o2)
  (define (f)
    (let ((n1   (struct-ref o1 n-i))
	  (n2   (struct-ref o2 n-i))
	  (vec1 (struct-ref o1 vec-i))
	  (vec2 (struct-ref o2 vec-i)))
      (or (eq? vec1 vec2)
	  (and
	   (= n1 n2)
	   (let lp ((i 0))
	     (if (< i n1)
		 (and (fast-equal? (vector-ref vec1 i) (vector-ref vec2 i))
		      (lp (+ i 1)))
		 #t))))))


  (cond
   ((and (struct? o2)
         (or (eq? (class-of o2) <g-list>)    
             (is-a? o2          <py-list>)))
    (f))
   (else
     #f)))
    
      

(define-method (py-equal? (x <null>) (y <vector>))
  (= (fastlist-len y) 0))

(define-method (py-equal? (y <vector>) (x <null>))
  (= (fastlist-len y) 0))

(define-method (py-equal? (x <pair>) y)
  (cond
   ((vector? y)
    (and
     (= (fastlist-len x) (fastlist-len y))
     (let lp ((x x) (i 0))
       (if (pair? x)
           (and (fast-equal? (car x) (vector-ref y i))
                (lp (cdr x) (+ i 1)))
           #t))))
   ((pair? y)
    (and
     (= (fastlist-len x) (fastlist-len y))
     (let lp ((x x) (y y))
       (if (pair? x)
           (and (fast-equal? (car x) (car y))
                (lp (cdr x) (cdr y)))
           #t))))
   (else
    (next-method))))

(define-method (py-equal? (x <vector>) (y <vector>))
  (and
     (= (fastlist-len x) (fastlist-len y))
     (let ((n (fastlist-len x)))
       (let lp ((i 0))
         (if (< i n)
             (and
              (fast-equal? (vector-ref x i) (vector-ref y i))
              (lp (+ i 1)))
             #t)))))

(define-method (py-equal? y (x <pair>))
  (cond
   ((vector? y)
    (and
     (= (fastlist-len x) (fastlist-len y))
     (let lp ((x x) (i 0))
       (if (pair? x)
           (and (fast-equal? (car x) (vector-ref y i))
                (lp (cdr x) (+ i 1)))
           #t))))
   ((pair? y)
    (and
     (= (fastlist-len x) (fastlist-len y))
     (let lp ((x x) (y y))
       (if (pair? x)
           (and (fast-equal? (car x) (car y))
                (lp (cdr x) (cdr y)))
           #t))))
   (else
    (next-method))))
                     

(define-class <py-seq-iter>  () o i n d)
(define-class <py-list-iter> (<py-list>) i d)

(name-object <py-seq-iter>)
(name-object <py-list-iter>)

(cpit <py-list-iter> (o (lambda (o i d)
			  (slot-set! o 'i i)
			  (slot-set! o 'd d))
			(list
			 (slot-ref o 'i)
			 (slot-ref o 'd))))

(cpit <py-seq-iter> (o (lambda (o oo i n d)
			 (slot-set! o 'o oo)
			 (slot-set! o 'i i)
			 (slot-set! o 'nn i)
			 (slot-set! o 'd d))
		       (list
			(slot-ref o 'o)
			(slot-ref o 'i)
			(slot-ref o 'nn)
			(slot-ref o 'd))))



(define-method (write (o <py-list-iter>) . l)
  (define port (if (null? l) #t (car l)))
  (for ((x : o)) ((l '()))
       (cons x l)
       #:final
       (let ((l (reverse l)))
         (if (null? l)
             (format port "iter[]")
             (format port "iter[~a~{, ~a~}]" (car l) (cdr l))))))

(define-method (write (o <py-seq-iter>) . l)
  (define port (if (null? l) #t (car l)))
  (for ((x : o)) ((l '()))
       (cons x l)
       #:final
       (let ((l (reverse l)))
         (if (null? l)
             (format port "iter[]")
             (format port "iter[~a~{, ~a~}]" (car l) (cdr l))))))


;;WRAP-IN
(define-method (wrap-in (o <py-list>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (slot-ref o 'nn  ))
    (slot-set! out 'vec (slot-ref o 'vec))
    (slot-set! out 'i   0)
    (slot-set! out 'd   1)
    out))

(define (make-vec v n i d)
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   n)
    (slot-set! out 'vec  v)
    (slot-set! out 'i    i)
    (slot-set! out 'd    v)
    out))

(set! (@@ (language python for) make-vec) make-vec)

(define-method (wrap-in (o <vector>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (vector-length o))
    (slot-set! out 'vec o)
    (slot-set! out 'i   0)
    (slot-set! out 'd   1)
    out))

(define-method (py-reversed (o <py-list>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'i   (- (slot-ref o 'nn) 1))
    (slot-set! out 'vec (slot-ref o 'vec))
    (slot-set! out 'nn   (slot-ref o 'nn))
    (slot-set! out 'd   -1)
    out))
(name-object py-reversed)

(define-method (py-reversed (o <py>))
  (aif it (ref-in-class o '__reversed__)
       (it o)
       (let* ((a (ref-in-class o '__getitem__))
              (a (if a (lambda (x) (a o x)) a))
              (n (ref-in-class o '__len__))
              (n (if n (lambda () (n o)) n)))
         (if (and a n)
             (let ((ret (make <py-seq-iter>)))
               (slot-set! ret 'o a)
               (slot-set! ret 'i (n))
               (slot-set! ret 'n -1)
               (slot-set! ret 'd -1)
               ret)
             (next-method)))))

(define-syntax-rule (e x) (catch #t (lambda () x) (lambda xx #f)))

(define-method (wrap-in (o <py>))
  (aif it (ref-in-class o '__iter__)
       (let ((x (it o)))
         (cond
          ((pair? x)
           (wrap-in x))
          (else
           x)))
       (let* ((a (ref-in-class o '__getitem__))
              (a (if a (lambda (x)
                         (catch #t
                           (lambda () (a o x))
                           (lambda x (handle-arg arg-empty))))
                     a)))
         (if a
             (let ((ret (make <py-seq-iter>)))
               (slot-set! ret 'o a)
               (slot-set! ret 'i 0)
               (slot-set! ret 'n -1)
               (slot-set! ret 'd 1)
               ret)
             (next-method)))))


(define-method (wrap-in (o <py-list-iter>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (slot-ref o 'nn  ))
    (slot-set! out 'vec  (slot-ref o 'vec ))
    (slot-set! out 'i    (slot-ref o 'i   ))
    (slot-set! out 'd    (slot-ref o 'd   ))
    out))
  

(define-method (wrap-in (o <py-seq-iter>))
  (let ((ret (make <py-seq-iter>)))
    (slot-set! ret 'o  (slot-ref o 'o))
    (slot-set! ret 'i  (slot-ref o 'i))
    (slot-set! ret 'n  (slot-ref o 'n))
    (slot-set! ret 'd  (slot-ref o 'd))
    ret))

;;NEXT
(define-next ((o <py-seq-iter>) n)
  (let ((i (slot-ref o 'i))
        (d (slot-ref o 'd))
        (a (slot-ref o 'o)))    
    (let ((r (a i)))
      (if (eq? r arg-empty)
          r
          (let ((ii (+ i d)))
            (begin
              (slot-set! o 'i ii)
              r))))))
        
(define-next ((o <py-list-iter>) n)
  (let ((i   (struct-ref o 2 ))
        (d   (struct-ref o 3 ))
        (n   (struct-ref o 1 ))
        (vec (struct-ref o 0 )))
    (if (> d 0)
        (if (< i n)
            (let ((ret (vector-ref vec i)))
              (struct-set! o 2 (+ i 1))
              ret)
            arg-empty)
        (if (>= i 0)
            (let ((ret (vector-ref vec i)))
              (struct-set! o 2 (- i 1))
              ret)
            arg-empty))))
  
;;INSERT
(define-method (pylist-insert! (o <py-list>) i val)
  (let* ((vec (struct-ref o vec-i))
         (n   (struct-ref o n-i))
         (N   (vector-length vec))
         (i   (index-ref i #t))
         (i   (index->nat i n)))
    (if (and (>= i 0) (<= i n))
        (begin
          (when (= N n)
            (let* ((NN (* 2 N))
                   (vec2 (make-vector NN)))
              (vector-move-left! vec 0 N vec2 0)
              (struct-set! o vec-i vec2)
              (set! vec vec2)))
          (vector-move-right! vec i N vec (+ i 1))
          (vector-set! vec i val)
	  (struct-set! o n-i (+ n 1)))
	
        (raise IndexError "Wrong index in insert"))))
(name-object pylist-insert!)

(define-method (pylist-insert! (o <py>) . l)
  (aif it (ref-in-class o 'insert) 
       (apply it o l)
       (next-method)))

;;CLEAR
(define-method (py-clear (o <p>))
  (aif it (ref-in-class o 'clear) 
       (apply it o)
       (next-method)))

(define-method (py-clear (o <py-list>))
  (clear-vec (slot-ref o 'vec) 0 (slot-ref o 'nn))
  (slot-set! o 'nn  0))

;;REMOV
(define py-list-subset!
  (resolve-method-g pylist-subset! `(,<py-list>      _ _ _ _)))

(define-method (pylist-remove! (o <py-list>) val)
  (let ((n   (struct-ref o n-i  ))
        (vec (struct-ref o vec-i)))
    (let lp ((i 0))
      (if (< i n)
          (let ((r (vector-ref vec i)))
            (if (fast-equal? r val)
                (py-list-subset! o i (+ i 1) 1 '())
                (lp (+ i 1))))
          (raise ValueError "list removal has no element to remove")))))
(name-object pylist-remove!)

(define-method (pylist-remove! (o <py>) . l)
  (aif it (ref-in-class o 'remove) 
       (apply it o l)
       (next-method)))

;; SORT!
(define-syntax-rule (wrap-key xx)
  (call-with-values (lambda () xx)
    (case-lambda
      ((x) x)
      (x   x))))
    
(define (id x) x)
(define (sort- it key reverse)
  (catch #t
    (lambda ()      
      (for ((x : it)) ((l '()) (i 0))
           (values (cons ((@ (guile) list) (wrap-key (key x)) i x) l)
                   (+ i 1))
           
           #:final
           (begin
             (let lp ((l (sort (reverse! l) (if reverse > <)))
                      (i 0))
               (if (pair? l)
                   (let ((x (car l)))
                     (fastlist-set! it i (caddr x))
                     (lp (cdr l) (+ i 1))))))))
    (lambda x
      (pk x)
      (raise (TypeError "problem in sorting layout")))))

(define-method (pylist-sort! (o <py-list>) . l)
  (apply
   (lambda* (#:key (key id) (reverse #f))
     (sort- o key reverse))
   l))
(name-object pylist-sort!)

(define-method (pylist-sort! (o <py>) . l)
  (aif it (ref-in-class o 'sort) 
       (apply it o l)
       (next-method)))

;; INDEX
(define-method (pylist-index (o <py-list>) val . l)
  (let* ((n   (struct-ref o n-i))
	 (f   (lambda (m) (index->nat (index-ref m #t) n)))
         (vec (struct-ref o vec-i)))
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x)  n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i n1))
              (if (< i n2)
                  (let ((r (vector-ref vec i)))
                    (if (fast-equal? r val)
                        i
                        (lp (+ i 1))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))

(name-object pylist-index)

(define-method (pylist-index (o <string>) val . l)
  (let* ((n   (string-length o))
         (f   (lambda (m) (index->nat (index-ref m #t) n)))
         (val (cond
               ((and (string? val) (> (string-length val) 0))
                (string-ref val 0))
               ((char? val)
                val)
               (else
                (raise (TypeError
                        "value to string index fkn is not a character"))))))
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (aif it (string-index o val n1 n2)
                 it
                 (raise ValueError "could not find value in index fkn"))
            (raise IndexError "index out of scop in index fkn"))))))

(define-method (pylist-index (o <vector>) val . l)
  (let* ((n   (vector-length o))
	 (f   (lambda (m) (index->nat (index-ref m #t) n))))
    
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i n1))
              (if (< i n2)
                  (let ((r (vector-ref o i)))
                    (if (fast-equal? r val)
                        i
                        (lp (+ i 1))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))

(defpair (pylist-index o val . l)
  (let* ((n   (length o))
	 (f   (lambda (m) (index->nat (index-ref m #t) n))))

    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i o))
              (if (pair? i)
                  (let ((r (car i)))
                    (if (fast-equal? r val)
                        i
                        (lp (cdr i))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))
                 
(define-method (pylist-index (o <py>) . l)
  (aif it (ref-in-class o 'index) 
       (apply it o l)
       (next-method)))


;; len

(define (tp x)
  (cond
   ((string? x)
    "<string>")
   ((procedure? x)
    "<procedure>")
   ((boolean? x)
    "<boolean>")
   ((vector? x)
    "<vector>")
   ((hash-table? x)
    "<hashtable>")
   ((pair? x)
    "<scm-list>")
   ((number? x)
    "<number>")
   ((symbol? x)
    (symbol->string x))
   (else
    (print-scm x write))))

(define (tp2 x)
  (cond
   ((string? x)
    #t)
   ((boolean? x)
    #t)
   ((vector? x)
    #t)
   ((pair? x)
    #t)
   ((procedure? x)
    #t)
   ((number? x)
    #t)
   ((symbol? x)
    #t)
   ((hash-table? x)
    #t)
   (else
    #f)))
(define-method (py-equal? x y)
  (define (error)
    (raise (TypeError (+ "can't compare "
                         (tp x)
                         " and "
                         (tp y)))))

  (catch #t
    (lambda ()
      ((@ (guile) equal?) x y))
    (lambda xx (error))))

(defpair (len l)  (length l))
(define-method (len x)
  (if (null? x)
      0
      (raise  (TypeError (+ "not a suitable length of " (print-scm x write))))))
(define-method (len (v <vector>))  (vector-length v))
(define-method (len (s <string>))  (string-length s))
(define-method (len (o <py-list>)) (struct-ref o n-i))
(define-method (len (o <py>))
  (aif it (ref-in-class o '__len__)
       (it o)
       (next-method)))
(name-object len)

(define fastlist-len len)

(define (bo x) (if x #t #f))
(define-method (in x (l <py-tuple>)) (bo (member x (slot-ref l 'l))))
(define-method (in x (l <pair>))     (bo  (member x l)))
(define-method (in x (l <vector>))
  (define n (vector-length l))
  (let lp ((i 0))
    (if (< i n)
        (if (fast-equal? x (vector-ref l i))
            #t
            (lp (+ i 1)))
        #f)))

(define-method (in (x <string>) (s <string>))
  (if (string-contains s x) #t #f))

(define-method (in (x <char>) (s <string>))
  (let/ec ret
    (string-for-each
     (lambda (ch)
       (if (eq? ch x)
	   (ret #t)))
     s))
  #f)

(define-method (in x (o <py-list>))
  (define l (struct-ref o vec-i))
  (define n (struct-ref o n-i))
  (let lp ((i 0))
    (if (< i n)
        (if (fast-equal? x (vector-ref l i))
            #t
            (lp (+ i 1)))
        #f)))

(define-method (in x (o <py>))
  (aif it (ref-in-class o '__contains__)
       (it o x)
       (next-method)))

(define-syntax-rule (defgen (op r s o1 o2) code ...)
  (begin
    (define-method (op (o1 <py-list>) (o2 <py-list>)) code ...)
    (define-method (op (o1 <pair>)    (o2 <pair>  )) code ...)
    (define-method (op (o1 <pair>)    (o2 <vector>)) code ...)
    (define-method (op (o1 <vector>)  (o2 <pair>)) code ...)
    (define-method (op (o1 <null>)    (o2 <pair>  )) code ...)
    (define-method (op (o1 <pair>)    (o2 <null>  )) code ...)
    (define-method (op (o1 <null>)    (o2 <vector>  )) code ...)
    (define-method (op (o1 <vector>)  (o2 <null>  )) code ...)
    (define-method (op (o1 <null>)    (o2 <null>  )) code ...)
    (define-method (op (o1 <py-tuple>) o2)
      (op (slot-ref o1 'l) o2))
    (define-method (op o2 (o1 <py-tuple>))
      (op o2 (slot-ref o1 'l)))
    (define-method (op (o1 <vector>) (o2 <vector>)) code ...)
    (define-method (op (o1 <py>)      o2)
      (aif it (ref-in-class o1 'r)
           (it o1 o2)
           (next-method)))
    (define-method (op o1 (o2 <py>))
      (aif it (ref-in-class o2 's)
           (it o2 o1)
           (next-method)))))

(defgen (< __le__ __gt__ o1 o2)
  (let ((n1 (fastlist-len o1))
        (n2 (fastlist-len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (< x1 x2)
          (break #t))
      (if (> x1 x2)
	  (break #f))
      #:final
      (< n1 n2))))

(define-method (< (o1 <py>) o2)
  (aif it (ref-in-class o1 '__lt__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__ge__)
            (not (it o1 o2))
            (aif it (ref-in-class o1 '__gt__)
                 (not (or (fast-equal? o1 o2) (it o1 o2)))                 
                 (next-method)))))

(define-method (< o2 (o1 <py>))
  (aif it (ref-in-class o1 '__ge__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__le__)
            (or (fast-equal? o1 o2) (not (it o1 o2)))
            (aif it (ref-in-class o1 '__lt__)
                 (not (it o1 o2))
                 (next-method)))))

(defgen (<= __lt__ __ge__ o1 o2)
  (let ((n1 (fastlist-len o1))
        (n2 (fastlist-len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (< x1 x2)
          (break #t))
      (if (> x1 x2)
	  (break #f))

      #:final
      (<= n1 n2))))

(define-method (<= (o1 <py>) o2)
  (aif it (ref-in-class o1 '__le__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__ge__)
            (or (fast-equal? o1 o2) (not (it o1 o2)))
            (aif it (ref-in-class o1 '__gt__)
                 (not (it o1 o2))
                 (next-method)))))

(define-method (<= o2 (o1 <py>))
  (aif it (ref-in-class o1 '__ge__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__le__)
            (or (fast-equal? o1 o2) (not (it o1 o2)))
            (aif it (ref-in-class o1 '__lt__)
                 (not (it o1 o2))
                 (next-method)))))

(defgen (> __ge__ __lt__ o1 o2)
  (let ((n1 (fastlist-len o1))
        (n2 (fastlist-len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (> x1 x2)
          (break #t))
      (if (< x1 x2)
	  (break #f))

      #:final
      (> n1 n2))))

(define-method (> (o1 <py>) o2)
  (aif it (ref-in-class o1 '__gt__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__le__)
            (not (it o1 o2))
            (aif it (ref-in-class o1 '__lt__)
                 (not (or (fast-equal? o1 o2) (it o1 o2)))                 
                 (next-method)))))

(define-method (> o2 (o1 <py>))
  (aif it (ref-in-class o1 '__le__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__ge__)
            (or (fast-equal? o1 o2) (not (it o1 o2)))
            (aif it (ref-in-class o1 '__gt__)
                 (not (it o1 o2))
                 (next-method)))))

(defgen (>= __gt__ __le__ o1 o2)
  (let ((n1 (fastlist-len o1))
        (n2 (fastlist-len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
         (if (> x1 x2)
             (break #t))
	 (if (< x1 x2)
	  (break #f))

         #:final
         (>= n1 n2))))

(define-method (>= (o1 <py>) o2)
  (aif it (ref-in-class o1 '__ge__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__le__)
            (or (fast-equal? o1 o2) (not (it o1 o2)))
            (aif it (ref-in-class o1 '__lt__)
                 (not (it o1 o2))
                 (next-method)))))

(define-method (< o2 (o1 <py>))
  (aif it (ref-in-class o1 '__lt__)
       (it o1 o2)
       (aif it (ref-in-class o1 '__ge__)
            (not (it o1 o2))
            (aif it (ref-in-class o1 '__gt__)
                 (not (or (fast-equal? o1 o2) (it o1 o2)))                 
                 (next-method)))))

(define-python-class list (object <py-list>)
  (define __repr__
    (lambda (self)
      (write self #f)))
    
  (define  __init__
    (letrec ((mk
	      (lambda (self n)
		(slot-set! self 'vec (make-vector n))
		(slot-set! self 'nn   0)))
		   
	     (__init__
              (case-lambda
	       ((self)
		(mk self 30)
                 (slot-set! self 'vec (make-vector 30))
                 (slot-set! self 'nn   0))
	       ((self val)
		(if (number? val)
		    (mk self val)
		    (call-with-values (lambda () (has-vector? val))
		      (lambda (it n)
			(if it
			    (let ((v (make-vector n)))
			      (vector-move-left! it 0 n v 0)
			      
			      (struct-set! self vec-i v)
			      (struct-set! self n-i   n))

			    (let* ((l (__init__ self))
				   (v (struct-ref self vec-i))
				   (n (vector-length v)))

			      (for-in ((x : val)) ((i 0))
			        (when (>= i n)
				  (struct-set! self n-i n)
				  (pylist-append! self x)
				  (set! v (struct-ref self vec-i))
				  (set! n (vector-length v)))
				   
				(vector-set! v i x)
				(+ i 1)
			      
				#:final
				(struct-set! self n-i i)))))))))))
      __init__)))

(name-object list)

(define pylist list)

(define-method (py-class (o <py-list>) list))

(define (pylist-listing)
  (let ((l
         (to-pylist
          (map symbol->string
               '(append
                 count
                 clear
                 extend
                 index
                 pop
                 insert
                 remove
                 reverse
                 sort
                 get
                 __matmul__
                 __rmatmul__                 
                 __init__
                 __le__
                 __lt__
                 __gt__
                 __ge__
                 __ne__
                 __eq__
                 __len__
                 __init__
                 __add__
                 __sub__
                 __rsub__
                 __neg__
                 __mul__
                 __rmul__
                 __radd__
                 __repr__
                 __contains__
                 __getattr__
                 __setattr__
                 __delattr__
                 __delitem__
                 __setitem__
                 __iter__
                 )))))
    
    (pylist-sort! l)
    l))

(define (py-all x)
  (for-in ((i : x)) ()
    (unless (bool i) (break #f))
    #:final
    #t))

(define (py-any x)
  (for-in ((i : x)) ()
    (when (bool i) (break #t))
    #:final
    #f))

(define py-list list)

(set! (@@ (oop dict) to-list) to-list)

(define-method (scheme? (o <py-list>)) py-list)
(define-method (+ (o1 <py-list>) (o2 <pair>))
  (+ o1 (to-pylist o2)))
(define-method (+ (o2 <pair>) (o1 <py-list>))
  (+ (to-pylist o2) o1))
(define-method (+ (o1 <py-list>) (o2 <null>))
  (+ o1 (to-pylist o2)))
(define-method (+ (o2 <null>) (o1 <py-list>))
  (+ (to-pylist o2) o1))


(define-method (py+= var val set) (set (+ var val)))
(define-method (py+= (var <p>) val set)
  (aif it (ref var '__iadd__)
       (it val)
       (next-method)))

(define-method (py+= (var <py-list>) val set)
  (let* ((vec1 (slot-ref var 'vec))
         (n1   (slot-ref var 'nn))
         (N    (vector-length vec1))
         (n2   (fastlist-len val))
         (n3   (+ n1 n2)))
    
    (when (> n3 N)
      (let* ((N2   (max n3 (* N 2)))
             (vec3 (make-vector N2)))
        (vector-move-left! vec1 0 n1 vec3 0)
        (slot-set! var 'vec vec3)
        (set! vec1 vec3)))
    
    (slot-set! var 'nn n3)
    
    (call-with-values (lambda () (has-vector? val))
      (lambda (vec2 n2)
        (if vec2
            (vector-move-left! vec2 0 n2 vec1 n1)
            
            (for ((x : (invector val))) ((i n1))
		 (vector-set! vec1 i x)
		 (+ i 1)))))

    (values)))

(define-method (py*= var val set) (set (* var val)))
(define-method (py*= (var <p>) val set)
  (aif it (ref var '__imull__)
       (it val)
       (next-method)))
  
(define-method (py*= (var <py-list>) (val <integer>) set)
  (cond
   ((< val 0)
    (error "wrong number in *= for python lists"))
   ((= val 0)
    (py-clear var))
   ((= val 1)
    (values))
   (else
    (let* ((vec (slot-ref var 'vec))
           (n   (slot-ref var 'nn))
           (N   (vector-length vec))
           (n2  (* val n)))

      (when (> n2 N)
        (let* ((N2   (max n2 (* 2 N)))
               (vec2 (make-vector N2)))
          (vector-move-left! vec 0 n vec2 0)
          (slot-set! var 'vec vec2)
          (set! vec vec2)))
      
      (*-move vec n val)

      (slot-set! var 'nn n2)
      (values)))))

(define-syntax *=
  (syntax-rules ()
    ((_ var val)     (py*= var val (lambda (v) (set! var v))))
    ((_ var val act) (py*= var val act))))

(define-syntax +=
  (syntax-rules ()
    ((_ var val)     (py+= var val (lambda (v) (set! var v))))
    ((_ var val act) (py+= var val act))))

(define-method (py-class o)
  (pk (ref o '__class__))
  (raise   
   (AttributeError
    (format #f "py-class! no attribute '__class__ on object ~a"
            (class-of o)))))

(define <g-list> (ref pylist '__goops__))

(define (get-ref-for-sequence val)
  (cond
   ((vector? val)
    (lambda (k) (vector-ref val k)))

   ((struct? val)
    (let ((cls (class-of val)))
      (cond
       ((eq? cls <g-list>)
        (let ((vec (slot-ref val 'vec)))
          (lambda (k) (vector-ref vec k))))
       (else
        (lambda (k)
          (pylist-ref val k))))))
           
   ((string? val)
    (lambda (k)
      (let ((ret (make-string 1)))
        (string-set! ret 0 (string-ref val k))
        ret)))

   ((pair? val)
    (let ((l val) (n 0))
      (lambda (k)
        (let ((ll (list-cdr-ref l (- k n))))
          (set! l ll)
          (set!  n k)
          (car ll)))))

   (else
    (lambda (k) (pylist-ref val k)))))

(define pylist-null (pylist 0))

(define* (pylist-ackumulator #:optional (n 15))
  (let* ((l (py-list n))
	 (i 0)
	 (v (struct-ref l vec-i))
	 (n (vector-length v)))
    (case-lambda
     ((x)
      (when (>= i n)
	(struct-set! l n-i i)
	(pylist-append! l x)
	(set! v (struct-ref l vec-i))
	(set! n (vector-length v)))
      
      (vector-set! v i x)
      (set! i (+ i 1)))

     (()
      (struct-set! l n-i i)
      l))))
	  

(define (pylist-filter f l)
  (if (procedure? f)
      ((make-generator
        (lambda (yield)
          (for-in ((x : l)) ()
               (if (f x)
                   (yield x))))))
      ((make-generator
        (lambda (yield)
          (for-in ((x : l)) ()
	    (if (not (equal? f x))
		(yield x))))))))


(define gmap (@ (guile) map))
(define (pylist-map f . l)
  ((make-generator
    (lambda (yield)
      (if (null? l)
	  (values)
	  (let ((iters (gmap wrap-in l)))
	    (catch #t
	      (lambda ()
		(let lp ()
		  (let ((vals (gmap next iters)))
		    (yield (apply f vals))
		    (lp))))
	      (lambda xx
		None))))))))

(define (map-list f . l)
  (if (null? l)
      (pylist)
      (if (= (len l) 1)
	  (let ((ret (pylist-ackumulator (len (car l)))))
	    (for-in ((x : (car l))) ()
	       (ret (f x))
		 
	       #:final (ret)))
		 
	  (let ((iters (gmap wrap-in l))
		(ret   (pylist-ackumulator)))
	    (try
	     (lambda ()
	       (let lp ()
		 (let ((vals (gmap next iters)))
		   (ret (apply f vals))
		   (lp))))
	     (#:except StopIteration))
	    (ret)))))

(define (filter-list f l)
  (let ((ret (pylist-ackumulator)))
    (for-in ((x : l)) ()
       (when (let ((r (f x))) (if (boolean? r) r (bool r)))
	 (ret x))
       #:final (ret))))

  
(define-syntax pylist-mac
  (lambda (x)
    (syntax-case x (@@)
      ((_ (map . l))
       (eq? (syntax->datum #'map) 'map)
       #'(map-list . l))

      ((_ ((@@ _ _) map (#:apply . l)))
       (eq? (syntax->datum #'map) 'map)
       #'(map-list . l))

      ((_ (filter . l))
       (eq? (syntax->datum #'filter) 'filter)
       #'(filter-list . l))

      ((_ ((@@ _ _) filter (#:apply . l)))
       (eq? (syntax->datum #'filter) 'filter)
       #'(filter-list . l))

      ((_ . a)
       #'(pylist . a))
      
      (_ #'pylist))))


(set! (@@ (language python for)  licls) <py-list>)

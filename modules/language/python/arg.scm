;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python arg)
  #:use-module (language python exceptions)
  #:use-module (oop goops)
  #:export (arg-empty empty-arg? produce-arg <arg> handle-arg))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-class <arg>  () x y l n)

(define arg-empty (list 'empty))

(define arg-cache (make-thread-local-fluid #f))

(define-syntax-rule (get-arg)
  (aif it (fluid-ref arg-cache)
       it
       (let ((it (make <arg>)))
	 (fluid-set! arg-cache it)
	 it)))

(define-syntax make-arg
  (lambda (x)
    (syntax-case x ()
      ((_)
       #'None)
    
      ((_ x)
       #'x)

      ((_ x y)
       #'(let ((ret (get-arg)))
	   (struct-set! ret 0 x)
	   (struct-set! ret 1 y)
	   (struct-set! ret 3 2)
	   ret))

      ((_ x y z ...)
       (with-syntax ((n (length #'(z ...))))
	 #'(let ((ret (get-arg)))
	     (struct-set! ret 0 x)
	     (struct-set! ret 1 y)
	     (struct-set! ret 2 (list z ...))
	     (struct-set! ret 3 (+ 2 n))
	     ret)))

      (_
       #'(case-lambda
	  (()
	   (produce-arg))

	  ((x)
	   (if (eq? x arg-empty)
	       (produce-arg arg-empty)
	       x))

	  ((x y)
	   (if (eq? x arg-empty)
	       (produce-arg arg-empty y)
	       (produce-arg x         y)))

	  ((x y . l)    
	   (let ((ret (get-arg)))
	     (struct-set! ret 0 x)
	     (struct-set! ret 1 y)
	     (struct-set! ret 2 l)
	     (struct-set! ret 3 (+ 2 (length l)))
	     ret)))))))

(define-syntax produce-arg
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (eq? (syntax->datum #'x) 'arg-empty)
       #'(make-arg arg-empty None))

      ((_ x y)
       (eq? (syntax->datum #'x) 'arg-empty)
       #'(make-arg arg-empty y))

      ((_ x ...)
       #'(make-arg x ...))
   
      (_
       #'(lambda x (apply make-arg x))))))

(define (handle-arg-2 x r n)
  (case n
    ((2)
     (values x (struct-ref r 1)))
    
    ((1)
     (list x (struct-ref r 1)))
    
    ((-1)
     (values x (struct-ref r 1)))
    
    (else
     (let ()
       (define s (string-append "not enough values to unpack "
				(format #f "expected ~a, " n)
				"got 2"))
       (throw 'python ValueError (ValueError s) None)))))

(define (handle-arg-n x r n)
  (define m (struct-ref r 3))
  (cond
   ((= n m)
    (apply values x (struct-ref r 1) (struct-ref r 2)))
    
   ((= n 1)
    (cons* x (struct-ref r 1) (struct-ref r 2)))
    
   ((= n -1)
    (apply values x (struct-ref r 1) (struct-ref r 2)))
    
   ((> n m)
    (let ()
      (define s (string-append "not enough values to unpack "
			       (format #f "(expected ~a, " n)
			       (format #f "got ~a)" m)))
      (throw 'python ValueError (ValueError s) None)))

   (else
    (let ()
      (define s (+ "too many values to unpack "
		   (format #f "(got ~a, " m)
		   (format #f "expected ~a)" n)))
      (throw 'python ValueError (ValueError s) None)))))

(define (empty-arg? x)
  (and
   (struct? x)
   (eq? (struct-ref x 0) arg-empty)))

(define (handle-struct-arg r n at-empty)
  (cond
   ((eq? (class-of r) <arg>)
    (if (eq? 2 (struct-ref r 3))
	(let ((x (struct-ref r 0)))
	  (if (eq? x arg-empty)
	      (at-empty (struct-ref r 1))
	      (handle-arg-2 x r n)))
	(handle-arg-n (struct-ref r 0) r n)))
	    
   (else
    r)))

(define-syntax handle-arg
  (syntax-rules ()
    ((_ ret)
     (handle-arg ret -1 (lambda (x) arg-empty)))
    
    ((_ ret n)
     (handle-arg ret n  (lambda (x) arg-empty)))
    
    ((_ ret n at-empty)
     (let ((r ret))
       (if (struct? r)
	   (handle-struct-arg r n at-empty)
	   r)))))


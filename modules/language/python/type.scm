;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python type)
  #:use-module (language python signatures)
  #:use-module (language python librarys)
  #:use-module (language python toolbox)
  #:use-module (language python expression)
  #:use-module (rnrs bytevectors)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:export (add-type-num
	    add-type-one
	    add-type-atm

	    get-sym-type-num
	    get-sym-type-one
	    get-sym-type-atm

	    integer-expr
	    rational-expr
	    numeric-expr
	    one-expr

	    get-signature
	    
	    get-type-atm
	    get-type-one
	    get-type-num))
	    
	    	  
(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-syntax-rule (mk-tracker symbol-types add-type get-sym-type)
  (begin
    (define symbol-types (make-hash-table))
    (define (add-type x type)
      (let ((s (syntax->datum #'x)))
	(when (symbol? s)
	  (hash-set! symbol-types s type))))

    (define (get-sym-type stx)
      (let ((s (syntax->datum stx)))
	(if (symbol? s)
	    (hash-ref symbol-types s 2)
	    2)))))

(mk-tracker symbol-types     add-type-num get-sym-type-num)
(mk-tracker symbol-types-one add-type-one get-sym-type-one)
(mk-tracker symbol-types-atm add-type-atm get-sym-type-atm)
    

(define (false?     x) #f)
(define (true?      x) #t)
(define (-integer?  x) (and (number? x) (integer? x)))
(define (-rational? x) (and (number? x) (rational? x)))
(define (one?       x)
  (or (number?     x)
      (string?     x)
      (boolean?    x)
      (vector?     x)
      (bytevector? x)
      (pair?       x)
      (null?       x)
      (keyword?    x)
      (symbol?     x)))

;; Machinary setup
;; ================================= ATOM TOOLBOX
(mk-mk-atom mk-atom <= get-sym-type-num '_ '*)

;; NUMERIC MACHINERY
(mk-atom integer-atom   0 integer-expr  -integer? )
(mk-atom rational-atom  0 rational-expr -rational?)
(mk-atom numeric-atom   1 numeric-expr   number? )

;; ONE RETURN VALUE MACHINERY
(mk-mk-atom mk-atom-one eq? get-sym-type-one '_ '*)
(mk-atom-one one-atom '_ one-expr one?)

(mk-mk-atom mk-atom-atm eq? get-sym-type-atm '_ '*)

;; ATOMIC TYPE DEDUCTIONS
(define i-number    'n  )
(define i-string    's  )
(define i-bytes     'b  )
(define i-bytearray 'ba )
(define i-list      'l  )
(define i-vector    'v  )
(define i-predicate 'p  )
(define i-dict      'd  )
(define i-set       's  )
(define i-all       '_  )
(define i-void      '*  )

(mk-atom-atm number-atom    i-number     number-expr    number?    )
(mk-atom-atm string-atom    i-string     string-expr    string?    )
(mk-atom-atm bytes-atom     i-bytes      bytes-expr     false?     )
(mk-atom-atm bytearray-atom i-bytearray  bytearray-expr false?     )
(mk-atom-atm list-atom      i-list       list-expr      false?     )
(mk-atom-atm vector-atom    i-vector     vector-expr    vector?    )
(mk-atom-atm predicate-atom i-predicate  predicate-expr boolean?   )
(mk-atom-atm dict-atom      i-dict       dict-expr      false?     )
(mk-atom-atm set-atom       i-set        set-expr       false?     )
(mk-atom-atm all-atom       i-all        all-expr       one?       )
(mk-atom-atm void-atom      i-void       void-expr      true?      )

;; ============================ EXPRESION TOOLBOX ============
(define (non-signature . l) #f)
(mk-mk-expr mk-expr     one-atom doer doer-lib #t '* non-signature)
(mk-mk-expr mk-expr-atm all-atom doer doer-lib #f '* get-signature )


;; NUMERIC EXPR
(mk-expr 0 0 integer-expr integer-atom f-lib-i f-proj-i
	 f-tri-ops-i-i-i
	 f-bin-ops-i-i
	 f-un-ops-i)

(mk-expr 1 1 rational-expr rational-atom f-lib-r f-proj-r
	 f-tri-ops-r-r-r
	 f-bin-ops-r-r
	 f-un-ops-r)
    
(mk-expr 2 2 numeric-expr numeric-atom f-lib-n f-proj-n
	 f-tri-ops-n-n-n
	 f-bin-ops-n-n
	 f-un-ops-n)

;; ONE EXPRE
(mk-expr 'one #t
	 one-expr one-atom
	 f-lib-one
	 f-proj-one
	 f-tri-ops-one
	 f-bin-ops-one
	 f-un-ops-one)


;; ATOMIC EXPR
(mk-expr-atm 'p 'n
	     number-expr number-atom
	     '()
	     '(int float complex)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 's
	     string-expr string-atom
	     '()
	     '(str)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'b
	     bytes-expr bytes-atom
	     '()
	     '(bytes)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'ba
	     bytearray-expr bytearray-atom
	     '()
	     '(bytearray)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'l
	     list-expr list-atom
	     '()
	     '(list to-pylist)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'v
	     vector-expr vector-atom
	     '()
	     '(tuple)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'p
	     predicate-expr predicate-atom
	     '()
	     '(bool)
	     '()
	     '()
	     '())


(mk-expr-atm 'p 'd
	     dict-expr dict-atom
	     '()
	     '(dict)
	     '()
	     '()
	     '())

(mk-expr-atm 'p 'm
	     set-expr set-atom
	     '()
	     '(set)
	     '()
	     '()
	     '())

(mk-expr-atm 'p '_
	     all-expr all-atom
	     f-lib-one
	     f-proj-one
	     f-tri-ops-one
	     f-bin-ops-one
	     f-un-ops-one)

(mk-expr-atm 'p '*
	     void-expr void-atom
	     '()
	     '()
	     '()
	     '()
	     '())

;; ======================== WRAP UP =======================
(define (ok-library-funktion? mod x)
  (or
   (not mod)
   (libber? f-lib-all mod x)))


(define (handle-signature sign arg)
  (case sign
    ((*)  (void-expr      arg))
    ((n)  (number-expr    arg))
    ((_)  (all-expr       arg))
    ((s)  (string-expr    arg))
    ((b)  (bytes-expr     arg))
    ((ba) (bytearray-expr arg))
    ((l)  (list-expr      arg))
    ((v)  (vector-expr    arg))
    ((p)  (predicate-expr arg))
    ((d)  (dict-expr      arg))
    ((m)  (set-expr       arg))))

(define (type-check mod op args)  
  (define (E n) (lambda (x) (= (length (syntax->datum args)) n)))
  (if (and (not (member op '(@ @@))) (ok-library-funktion? mod op))
      (let lp ((l (generate-all-signatures op (length (syntax->datum args)))))
	(match l
	 (((and x (a _ c) (? (E 1))) . l)
	  (if (handle-signature a (car args))
	      x
	      (lp l)))

	 (((and x (a b _ c) (? (E 2))) . l)
	  (if (and (handle-signature a (car  args))
		   (handle-signature b (cadr args)))
	      x
	      (lp l)))
	 
	 (()
	  #f)

	 ((x . l)
	  (lp l))))
      #f))

    
(define (doer op x)
  (aif it (type-check #f (syntax->datum op) x)
       (car (reverse it))
       #f))

(define (doer-lib mod op x)
  (aif it (type-check mod (syntax->datum op) x)
       (car (reverse it))
       #f))

;; ======================= PUBLIC INTERFACE ==================
(define (get-signature mod op x)
  (type-check mod (syntax->datum op) x))


(define (get-type-atm x)
  (or-map (lambda (t)
	    (if (handle-signature t x)
		t
		#f))
	  '(n s l b d m v p ba _ *)))
   
(define (get-type-one x) (one-expr x))
(define (get-type-num x)
  (if (integer-expr x)
      0
      (if (numeric-expr x)
	  1
	  2)))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python bool)
  #:use-module (oop goops)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:use-module (oop pf-objects)
  #:export (bool boolean))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-method (bool x)
  (cond
   ((null? x)
    #f)
   ((equal? x '(EOF))
    #f)
   ((eq? x None)    
    #f)
   (else x)))
(name-object bool)

(define-method (bool (x <integer>)) (if (= x 0) #f x))

(define-method (bool (x <p>))
  (aif it (ref-in-class x '__bool__ #f)
       (it x)
       (next-method)))

(define-method (bool (x <py>))
  (aif it (get-bool (get-class x))
       (it x)
       (next-method)))
		     

(define-method (+ (a <boolean>) b)
  (+ (if a 1 0) b))
(define-method (+ b (a <boolean>))
  (+ (if a 1 0) b))
(define-method (* (a <boolean>) b)
  (* (if a 1 0) b))
(define-method (* b (a <boolean>))
  (* (if a 1 0) b))
(define-method (- (a <boolean>) b)
  (- (if a 1 0) b))
(define-method (- b (a <boolean>))
  (- b (if a 1 0)))

(define-python-class boolean ()
  (define __new__
    (lambda (cls x . l)
      (if (pair? l)
          (let ((self ((rawref object '__new__) cls)))
            (set self '__bool__ (bool x))
            self)
          (bool x)))))

(set boolean '__name__ 'bool)

(define-method (scheme? (o <boolean>)) boolean)


(define-method (write (o <boolean>) . l)
  (apply write (if o "True" "False") l))


(define-method (display (o <boolean>) . l)
  (apply display (if o "True" "False") l))

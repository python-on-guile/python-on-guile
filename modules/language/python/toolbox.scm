;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python toolbox)
  #:export (f-tri-ops-i-i-i
	    f-bin-ops-i-i
	    f-un-ops-i
	    f-proj-i
	    f-lib-i
	    
	    f-tri-ops-r-r-r
	    f-bin-ops-r-r
	    f-un-ops-r
	    f-proj-r
	    f-lib-r
	    
	    f-tri-ops-n-n-n
	    f-bin-ops-n-n
	    f-un-ops-n
	    f-proj-n
	    f-lib-n

	    f-tri-ops-one
	    f-bin-ops-one
	    f-un-ops-one
	    f-proj-one
	    f-lib-one

	    f-lib-all))

(define f-tri-ops-i-i-i '())
(define f-bin-ops-i-i '(fast+ fast- fast* + - *
			      fast& fast-or fast^ fast// fast<<
			      fast>> fast%))
(define f-un-ops-i '(- + fast~))

(define f-proj-i '(len int fast-equal-mac? < > <= >= eq?))

(define f-lib-i '())



(define f-tri-ops-r-r-r '())

(define f-bin-ops-r-r '(fast+ fast- fast/ fast* + - * /
			      fast& fast-or fast^ fast// fast<<
			      fast>> fast%
			      fast-equal-mac? < > <= >= eq?))
(define f-un-ops-r '(- +))

(define f-proj-r f-proj-i)

(define f-lib-r '())




(define f-tri-ops-n-n-n '())

(define f-bin-ops-n-n '(fast+ fast- fast/ fast* + - * /
			      fast& fast-or fast^ fast// fast<<
			      fast>> fast%
			      fast-equal-mac? < > <= >= eq?))
(define f-un-ops-n '(+ - fast~))


(define f-proj-n (append '(complex float) f-proj-i))

(define f-lib-n
  '((math
     (sin cos tan sinh cosh tanh log exp log10 log2 acos asin atan sqrt pow
	  atan2 erf erfc gamma lgamma log1p expm1))
    
    (statistics
     (pstdev pvariance stdev variance
	     median median_low median_high median_grouped
	     mean fmean mode harmonic_mean))))




;; One argument traces
(define f-tri-ops-one '(py-center py-endswith py-startswith py-expandtabs
				  py-find py-rfind  py-ljust py-rjust py-rljust
				  py-split py-rsplit py-splitlines
				  ))

(define f-bin-ops-one (append '(py-center py-endswith py-startswith
					  py-expandtabs py-find py-rfind
					  py-join  py-ljust py-rjust py-rljust
					  py-partition py-rpartition
					  py-split py-rsplit py-splitlines
					  and or
					  )
			      f-bin-ops-n-n))


(define f-un-ops-one  (append '(len fastlist-len bool py-isascii
				    py-hex py-fromhex py-imag py-real
				    py-isinteger py-bin py-indexr
				    py-bit-length py-to-bytes
				    py-capitalize py-strip py-title
				    py-isalnum py-isalpha py-isdigit py-islower
				    py-isspace py-isupper py-istitle
				    py-lower py-upper py-lstrip py-rstrip
				    py-split py-rsplit py-splitlines
				    py-swapcase and or not
				    )
			      f-un-ops-n))

(define f-proj-one (append
		    '(list dict set byte string bytes bytearray tupple bool
			   to-pylist)
		    f-proj-n))

(define f-lib-one  f-lib-n )
(define f-lib-all  f-lib-n )

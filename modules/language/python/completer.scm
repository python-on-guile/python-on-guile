;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python completer)
  #:use-module (language python list)
  #:use-module (language python dir)
  #:use-module (system base language)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (oop pf-objects)
  #:export (complete-python))

(define old
  (@@ (ice-9 readline) *readline-completion-function*))

(define dotted-words-regexp "(\\w+(\\.\\w+)*)\\.(\\w*)$")

(define (complete-python proc)
  "Return a readline completion procedure for completing dotted Python
names."
  (define older #f)
  (define completions '())
  (define regexp #f)
  (define prefix "")
  (letrec ((complete
            (lambda (text continue?)
              (cond
               (continue?
		(if older
		    (old text continue?)
		    (match completions
		      (() #f)
		      ((candidate . rest)
		       (set! completions rest)
		       (if (string-match regexp candidate)
			   (string-append prefix "." candidate)
			   (complete text #t))))))

	                      ;; Initialize completions
               ((and (equal? (language-name (current-language)) 'python)
                     (in "." text))
		(set! older #f)
                (and=> (string-match dotted-words-regexp text)
                       (lambda (m)
                         (let ((head (match:substring m 1))
                               (tail (match:substring m (1- (match:count m)))))
                           (set! prefix head)
                           (set! completions (to-list (dir (proc head))))
                           (set! regexp (string-append
                                         "^" (if (string-null? tail)
                                                 "[^_]"
                                                 (regexp-quote tail))))
                           (complete text #t)))))
               (else
		(set! older #t)
		(old text continue?))))))
    complete))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python def)
  #:use-module (ice-9 pretty-print)
  #:use-module (system syntax internal)
  #:use-module (oop pf-objects)
  #:use-module (system syntax internal)
  #:use-module ((oop dict) #:select (hset! mkw to-list slask-it 
                                           set-procedure-property!-
                                           def-dict))
  #:use-module (language python exceptions)
  #:use-module (language python adress)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-11)
  #:export (def def0 lam lam0 py-apply))

(define e (list 'error))
(define-syntax-rule (kif it p x y) (let ((it p)) (if (not (eq? it e)) x y)))
(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define (NRR name n m f x)
  (set-procedure-property!- x '__file__  f)
  (set-procedure-property!- x 'line      (car  n))
  (set-procedure-property!- x 'column    (cadr n))
  (set-procedure-property!- x 'line2     (car  m))
  (set-procedure-property!- x 'column2   (cadr m))
  (if name (set-procedure-property! x 'name name))
  
  (set-procedure-property!-- x 'filename f)
  (set-procedure-property!-- x 'line      (car  n))
  (set-procedure-property!-- x 'column    (cadr n))
  (set-procedure-property!-- x 'line2     (car  m))
  (set-procedure-property!-- x 'column2   (cadr m))
  (if name (set-procedure-property!-- x 'name name))
  x)

(cond-expand
 (guile-2.2
  (define-syntax NR
    (lambda (x)
      (syntax-case x ()
        ((_ name (n ...) (m ...) y)
         (let* ((xx (if (syntax? x) (syntax-expression x) #f))
                (yy (if xx (source-properties xx) #f)))
           #`(NRR name (list n ...) (list m ...)
                  #,(if yy (aif it
                                (assoc 'filename yy) (cdr it)
                                "python-shell")
                        "python-shell")
                  y)))))))
  
 (guile-2.0
  (define-syntax NR
    (lambda (x)
      (syntax-case x ()
        ((_ name line col stx)
         #t
         #'stx))))))

(define (keyword-it x) (symbol->keyword (syntax->datum x)))

(define (filt k x n)
  (define (to-hash x)
    (let ((h '()))
      (let lp ((x x))
        (if (pair? x)
            (if (pair? (cdr x))
                (let ((k (car x))
                      (v (cadr x)))
                  (if (keyword? k)
                      (begin
                        (set! h (cons (cons k v) h))
                        (lp (cddr x)))
                      (throw 'python
                             ArgumentError
                             (ArgumentError "not a keyword in keyw section")
                             #f)))
                (throw 'python
                       ArgumentError
                       (ArgumentError
                        "Not a keyword pair in keyword secrtion") #f))
            h))))

  (define (from-hash h)
    (let lp ((h h))
      (if (pair? h)
          (cons* (caar h) (cdar h) (lp (cdr h)))
          '())))
  
  (if (or-map (lambda (x) (keyword? x)) x)
      (let lp ((x x) (i 0) (k k) (l '()))
        (if (and (pair? x) (< i n))
            (let ((xx (car x)))
              (if (keyword? xx)
                  (let ((kws (to-hash x)))
                    (let lp ((i i) (k k) (l l) (kws kws))
                      (if (and (pair? k) (< i n))
                          (let ((kk (car k)))
                            (kif it (assoc kk kws)
                                 (lp (+ i 1)
                                     (cdr k)
                                     (cons (cdr it) l)
                                     (assoc-remove! kws kk))
                                 (throw
                                  'python
                                  ArgumentError
                                  (ArgumentError
                                   ((@ (guile) format)
                                    #f
                                    "missing keword arg for ~a"
                                    kk)) #f)))
                          (append (reverse l) (from-hash kws)))))
                  (lp (cdr x) (+ i 1) (cdr k) (cons xx l))))             
            (append (reverse l) x)))
      x))
                                 
(define (fold lam s l)
  (if (pair? l)
      (lam (car l) (fold lam s (cdr l)))
      s))

(define-syntax-rule (take-1 pww ww* kw s v)
  (if (not pww)
      (values ww*
              (let ((ss (strit s)))
                (if (in ss kw)
                    (let ((it (py-get kw ss #f)))
                      (pylist-delete! kw ss)
                      it)
                    v)))
      (if (pair? ww*)
          (begin
            (pylist-delete! kw s)
            (values (cdr ww*) (car ww*)))
          (values ww*
                  (let ((ss (strit s)))
                    (if (in ss kw)
                        (let ((it (py-get kw ss #f)))
                          (pylist-delete! kw ss)
                          it)
                        v))))))
                       
(define (strit x) (symbol->string (keyword->symbol x)))

(define get-akw
  (case-lambda
    ((-dict l)
     (let lp ((l l) (args '()) (kw (if def-dict
                                       (-dict)
                                       (make-hash-table))))
       (match l
         (((? keyword? k) v . l)
          (pylist-set! kw (strit k) v)
          (lp l args kw))
         ((x . l)
          (lp l (cons x args) kw))
         (()
          (values (reverse args) kw)))))
    ((l) (get-akw def-dict l))))

(define (pytonize kw) kw)

(define-syntax-rule (mk lam -dict)
(define-syntax lam
  (lambda (x)
    (define-syntax-rule (mk get-as (k v s) x y z w)
      (define get-as
        (lambda (a s)
          (syntax-case a (= * **)
            ((=- k v) (eq? (syntax->datum #'=- ) '= ) x)
            ((**-  k) (eq? (syntax->datum #'**-) '**) y)
            ((*-   k) (eq? (syntax->datum #'*- ) '* ) z)          
            (k                                        w)))))

    (mk get-as (k v s)
        s                       s            s            (cons #'k s))
    (mk get-kw (k v s)
        s                       (cons #'k s) s            s           )
    (mk get-ww (k v s)
        s                       s            (cons #'k s) s           )
    (mk get-kv (k v s)
        (cons (cons #'k #'v) s) s            s            s           )

    (define (->kw x) (symbol->keyword (syntax->datum x)))

    (define (add as code)
      #`(let ((co (slask-it #,code)))
          (set-procedure-property!- co 'arglist '#,as)
          co))

    (syntax-case x (*)
      ((_ #t ((N (... ...)) (M (... ...))) name (arg (... ...)) code (... ...))
       (let* ((as  (fold get-as '() #'(arg (... ...))))
              (kw  (fold get-kw '() #'(arg (... ...))))
              (ww- (fold get-ww '() #'(arg (... ...))))
              (kv  (fold get-kv '() #'(arg (... ...)))))
         (add #'(arg (... ...))
         (if (and-map null? (list kw ww- kv))
             #`(let ((f (NR 'name (N (... ...)) (M (... ...))
                            (lambda (#,@as . u12345678)
                              (if (and (pair? u12345678)
                                       (not (keyword? (car u12345678))))
                                  (throw
                                   'python
                                   ArgumentError
                                   (ArgumentError
                                    "too many arguments to function") #f))
                              (let () code (... ...))))))
                 (case-lambda
                   ((#,@as)
                    (if (or #,@(map (lambda (a) #`(keyword? #,a)) as))
                        (throw
                         'python
                         ArgumentError
                         (ArgumentError
                          "too many arguments to lambda") #f)
                        (f #,@as)))
                   (xxx                    
                    (apply f (filt '#,(map keyword-it as) xxx (length xxx))))))
             (with-syntax ((kw      (if (null? kw)
                                        (datum->syntax x (gensym "kw"))
                                        (car kw)))
                           (ww      (if (null? ww-)
                                        (datum->syntax x (gensym "ww"))
                                        (car ww-)))
                           ((k (... ...)) (map car kv))
                           ((s (... ...)) (map ->kw (map car kv)))
                           ((v (... ...)) (map cdr kv))
                           (nn      (length as)))
               #`(lambda xxx
                   (apply
                    (NR 'name (N (... ...)) (M (... ...))
                        (lambda (#,@as . l)
                          (call-with-values (lambda () (get-akw -dict l))
                            (lambda (ww* kw)
                              (let*-values (((ww* k) (take-1 #,(null? ww-) ww*
                                                             kw s v))
                                            (... ...))
                                (let ((ww ww*)
                                      (kw (pytonize kw)))
                                  (let () code (... ...))))))))
                    (filt '#,(map keyword-it as) xxx nn))))))))
    
      ((_ (arg (... ...)) code (... ...))
       #'(lam #t ((0 0) (0 0)) void (arg (... ...)) code (... ...)))))))

(mk lam  def-dict)
(mk lam0 def-dict)

(define-syntax-rule (def (f . args) code ...)
  (define f (lam args code ...)))

(define-syntax-rule (def0 (f . args) code ...)
  (define f (lam0 args code ...)))

(define (no x)
  (and-map
   (lambda (x)
     (syntax-case x (* ** =)
       ((*  _)  #f)
       ((** _)  #f)
       ((= a b) #f)
       (_ #t)))
   x))

(define (mk-k x)
  (if (keyword? x)
      x
      (symbol->keyword
       (if (string? x)
	   (string->symbol x)
	   x))))
  
(define-syntax m*
  (syntax-rules (* ** =)
    ((_ (=  a b))
     (list (symbol->keyword 'a) b))
    ((_ (*  a)) a)
    ((_ (** kw))
     (mkw kw))
    ((_ a) (list a))))

(define-syntax py-apply  
  (lambda (x)
    (syntax-case x ()
      ((_ f a ... (op x))
       (and (syntax-case #'op (*)
              (* #t)
              (_ #f))
            (and-map (lambda (q)
                       (syntax-case q (* ** =)
                         ((=-  _ _) (eq? (syntax->datum #'=- ) '= ) #f)
                         ((*-  _  ) (eq? (syntax->datum #'*- ) '* ) #f)
                         ((**- _  ) (eq? (syntax->datum #'**-) '**) #f)
                         (_     #t))) #'(a ...)))
       #'(if (or (null? x) (pair? x))
             (apply f a ... x)
             (apply f a ... (to-list x))))

      ((_ f a ...)
       (if (no #'(a ...))
	   #'(f a ...)
	   #'(apply f
                    (let lp ((l (list (m* a) ...)))
                      (if (pair? l)
                          (append (to-list (car l)) (lp (cdr l)))
                          '()))))))))
	       

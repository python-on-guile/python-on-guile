;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python signatures)
  #:use-module (ice-9 match)
  #:export (generate-all-signatures))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define signs (make-hash-table))

(define (generate-all-signatures op n)
  (hash-ref signs (cons n (syntax->datum op)) '()))

(define (make-signatures l)
  (define (all-l a l)
    (let lp ((l l))
      (match l
	((x . l)
	 (hash-set! signs (cons (length l) a) x)
	 (lp l))
	(_
	 (values)))))
  
  (define (all-a a l)
    (let lp ((a a))
      (match a
       ((a . aa)
	(all-l a l)
	(lp aa))
       (()
	(values)))))
  
  (let lp ((l l))
    (match l
      ((((a ...) (l ...)) . ll)
       (all-a a l)
       (lp ll))

      (((a (l ...)) . ll)
       (all-a (list a) l)
       (lp ll))

      (_
       (values)))))

(define (fold f s l)
  (if (pair? l)
      (fold f (f (car l) s) (cdr l))
      (reverse s)))
		 
(define (comp alts l)
  (define nums (let lp ((l l) (old '()))
		 (match l
		   ((x . l)
		    (if (number? x)
			(if (member x old)
			    (lp l old)
			    (lp l (cons x old)))
			(lp l old)))
		   (()
		    old))))
  (define n (length alts))
  (define m (length nums))
  (define N (expt n m))
  (define a (let lp ((i 0) (l '()))
	      (if (< i N)
		  (let lp2 ((j 0) (x i) (r '()))
		    (if (< j m)
			(let* ((mod (modulo x n))
			       (x   (- x mod)))
			  (lp2 (+ j 1)
			       (if (= x 0)
				   x
				   (/ x n))
			       (cons (list-ref alts mod) r)))
			(lp (+ i 1) (cons (reverse r) l))))
		  (reverse l))))
  (define b (map (lambda (x) (map cons nums x)) a))

  (fold
   (lambda (x s)
     (if (eq? x ':)
	 s
	 (cons x s)))
   '()
   (map (lambda (bb) (map (lambda (x)
			    (if (number? x)
				(cdr (assoc x bb))
				x))
			  l))
	b)))


(define (all-comp . l) (comp '(s b v l n d m) l))
(define (seq-comp . l) (comp '(s b v l      ) l))
(define (vec-comp . l) (comp '(    v l      ) l))
(define (str-comp . l) (comp '(s b          ) l))

#|
n  = number
l  = python list
v  = python typle or guile vector
d  = python dictionary
m  = python set
b  = bytes
ba = bytearray
s  = sheme string or python string
p  = boolean
_  = any internal object that does nothing special
o  = (values)
*  = any scheme/python object 


|#
(let ((: ':))
  (make-signatures
   `(((* fast*)
      (
       ((n n n)
	,@(seq-comp 1 'n : 1))
       ))
       
     ((+ fast+)
      (
       ,(all-comp 1 : 1)
	 
       (
	(m m m)
	,@(str-comp  1 2 : 's)
	,@(vec-comp  1 2 : 'l)
	,@(all-comp  1 1 : '1)
	)))

     ((/ fast/)
      (
       ((n n n))
       ))
       
     ((- fast- fast~)
      (
       ((n n))
       ((n n n)
	(m m m))
       ))

     ((< > <= >=)
      (
       ,(all-comp 1 2 : 'p)
       ))

    
     ((fast& fast^ fast-or fast<< fast>> fast**
	     py-/ py-logand py-logior py-logxor
	     py-lshift py-rshift py-mod py-floordiv py-round
	     py-lognot)
	       
      (
       ((n n n))
       ))

     ((fast~ py~ py-abs py-bit-length)
      (
       ((n n))
       ))

     ((not)
      (
       ((* p))
       ))
       
     ((hex py-hex bin py-bin)
      (
       ((s n))
       ))
       
     ((fast%)
      (()
       ((n n n) (b b b) ,@(str-comp 1 '_ : 's))))

     ((sin cos tan sinh cosh tanh log exp log10 log2 acos asin atan sqrt erf
	   lgamma erfc expm1 log1p py-imag py-real py-trunc py-floor py-round)
      (
       ((n n))
       ))
     
     ((pow gamma atan2)
      (
       ((n n n))
       ))

     ((py+= fast+=)
      (
       ((n  n  o)
	(l  l  o)
	(l  v  o)
	(v  l  o)
	(v  v  o)
	(s  b  o)
	(s  ba o)
	(s  s  o)
	(b  b  o)
	(b  ba o)
	(m  m  o)
	(m  l  o)
	(m  v  o)
	(ba b  o)
	(ba ba o))))

     ((py-= fast-=)
      (
       ((n n o)
	(m m o))
       ))

     ((py*=    py/=    py//=    py%=    py&=    py^=    py-or=    py<<=  
       fast*=  fast/=  fast//=  fast%=  fast&=  fast^=  fast-or=  fast<<=
       py>>=   **=
       fast>>= fast**=)

      ((n n o)))

       
     (len
      (
       ,(all-comp 1 : 'n)
       ()))
     
     ((pylist-ref fastlist-ref)
      (
       ((b  n n)
	(ba n n)
	(s  n s)
	(l  n *)
	(v  n *)
	(d  * *))
       
       ((b  n n n)
	(b  n b n)
	(ba n n n)
	(ba n b n)
	(s  n s s)
	(s  n b s)
	(s  n n s)
	(l  n * *)
	(v  n * *)
	(d  * * *))	  
       ))

     ((pylist-append! fastlist-append!)
      (
       ((l  * o)
	(ba n o))
       ))
     
     ((pylist-set! fastlist-set!)
      (
       ((l  n * o)
	(v  n * o)
	(s  n s o)
	(s  n b o)
	(s  n n o)
	(b  n n o)
	(b  n b o)
	(ba n n o)
	(ba n b o)
	(d  * * o))
       ))

     ((in fastlist-in)
      (
       ((* l  p)
	(* v  p)
	(s s  p)
	(b s  p)
	(n s  p)
	(b b  p)
	(n b  p)
	(b ba p)
	(n ba p))
       ))

     ((pylist-pop!)
      (
       ((l *))
       ))

     ((py-get fast-get)
      (
       ((l  n *)
	(v  n *)
	(s  n s)
	(b  n n)
	(ba n n)
	(d  * *))
       
       ((l  n *  *)
	(v  n *  *)
	(s  n s  s)
	(s  n b  s)
	(s  n ba s)
	(s  n n  s)
	(b  n n  n)
	(b  n b  n)
	(b  n ba n)
	(ba n n  n)
	(ba n b  n)
	(ba n ba n))	  
       ))

     ((fastlist-slice pylist-slice)
      (
       ((l  n n n l )
	(v  n n n v )
	(s  n n n s )
	(b  n n n b )
	(ba n n n ba))
       ))

     
     ((fastlist-subset! pylist-subset!)
      (
       ((l  n n n l )
	(ba n n n ba))
       ))

     ((fastlist-append! pylist-append!)
      (
       ((l  *  o)
	(ba n  o)
	(ba b  o)
	(ba ba o))
       ))
       
     ((fast-equal-mac? equal? py-equal? eq?)
      (
       ,(all-comp 1 1 : 'p)
       ))

     ((and or)
      (
       ((_ _ : _))
       ))
       
     ((pstdev pvariance stdev variance
	      median median_low median_high median_grouped
	      mean fmean mode harmonic_mean)

      (
       ,(seq-comp 1 : 'n)
       )))))

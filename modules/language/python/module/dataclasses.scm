;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module dataclasses)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop pf-objects)
  #:use-module ((system base compile) #:select ((compile . scm-compile0)))
  #:use-module (language python def)
  #:use-module (language python try)
  #:use-module (language python for)
  #:use-module (language python dict)
  #:use-module (language python module copy)
  #:use-module (language python tuple)
  #:use-module (language python exceptions)
  #:use-module (language python list)
  #:use-module (language python set)
  #:use-module (language python number)
  #:use-module ((guile) #:select ((list . scm-list)))
  #:use-module ((language python module keyword) #:select
                (iskeyword))
  #:use-module ((language python module types) #:select
                (new_class MappingProxyType))
  #:use-module ((language python module re) #:select
                ((compile . re-compile)))
  #:use-module ((language python module sys) #:select
                ((modules . sys-modules)))
  #:use-module ((language python module python) #:select (isinstance str bool repr delattr))
  #:use-module (language python module)
  #:export (dataclass field fields as_dict as_tuple make_dataclass replace InitVar))

(define __name__   "dataclasses")
(define __file__   "dataclasses.scm")
(define __module__ "dataclasses")
(define is_dataclasses_compile #t)
(define (scm-compile code)
  (if is_dataclasses_compile
      (scm-compile0 code)
      ((@ (guile) eval) code (current-module))))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define (pp x)
  (pretty-print x)
  x)

(define _PARAMS "__dataclass_params__")
(define _FIELDS "__dataclass_fields__")
(define _POST_INIT_NAME "__post_init__")

(define __P  (string->symbol _PARAMS))
(define __F  (string->symbol _FIELDS))
(define __PO (string->symbol _POST_INIT_NAME))

(define _MODULE_IDENTIFIER_RE (re-compile "^(?:\\s*(\\w+)\\s*\\.)?\\s*(\\w+)"))

(define-python-class FrozenInstanceError (AttributeError))

(define-python-class _HAS_DEFAULT_FACTORY_CLASS ()
  (define __repr__
    (lambda (self)
      "<factory>")))

(define _HAS_DEFAULT_FACTORY (_HAS_DEFAULT_FACTORY_CLASS))

(define-python-class _MISSING_TYPE ())
(define MISSING (_MISSING_TYPE))

(define _EMPTY_METADATA (MappingProxyType (dict)))


(define-python-class _FIELD_BASE ()
  (define  __init__
    (lambda (self name)
      (set self 'name name)))
  
  (define __repr__
    (lambda (self)
      (ref self 'name)))
  
  (define __str__ __repr__))

(define _FIELD          (_FIELD_BASE "_FIELD"))
(define _FIELD_CLASSVAR (_FIELD_BASE "_FIELD_CLASSVAR"))
(define _FIELD_INITVAR  (_FIELD_BASE "_FIELD_INITVAR"))

(define-python-class InitVar ()
  (define __init__
    (lambda (self type)
      (set self 'type type)))

  (define __repr__
    (lambda (self)
      (define type-name
        (if (isinstance (ref self 'type) type)
            (ref (ref self 'type) '__name__)
            (repr (ref self 'type))))
        (py-mod "dataclasses.InitVar[%s]" type-name))))

(define-python-class _DataclassParams ()
  (define __init__
    (lambda (self init repr eq order unsafe_hash frozen)
      (set self 'init  init)
      (set self 'repr  repr)
      (set self 'eq    eq)
      (set self 'order order)
      (set self 'unsafe_hash unsafe_hash)
      (set self 'frozen frozen)))

  (define __repr__
    (lambda (self)
      (py-mod "DataclassParams(init=%s,repr=%s,eq=%s,order=%s,unsafe_hash=%s,frozen=%s"
            (scm-list (ref self 'init)
                      (ref self 'repr)
                      (ref self 'eq)
                      (ref self 'order)
                      (ref self 'unsafe_hash)
                      (ref self 'frozen)))))

  (define __set_name__
    (lambda (self owner name)
      (let ((func (ref (type (ref self 'default)) "__set_name__" #f)))
        (when func
          (func (ref self 'default) owner name))))))

(define-python-class Field ()
  (define __init__
    (lambda (self default default_factory init repr hash compare metadata)
      (set self 'name None)
      (set self 'type None)
      (set self 'default default)
      (set self 'default_factory default_factory)
      (set self 'init init)
      (set self 'repr repr)
      (set self 'hash hash)
      (set self 'compare compare)
      (set self 'metadata
           (if (eq? metadata None)
               _EMPTY_METADATA
               (MappingProxyType metadata)))
      (set self '_field_type None)))

  (define __repr__
    (lambda (self)
      (py-mod
       "Field(name=%s,type=%s,default=%s,default_factory=%s,init=%s,repr=%s,hash=%s,compare=%s,metadata=%s,_field_type=%s"
       (scm-list (ref self 'name) (ref self 'type) (ref self 'default)
                 (ref self 'default_factory) (ref self 'init) (ref self 'repr)
                 (ref self 'hash) (ref self 'compare) (ref self 'metadata)
                 (ref self '_field_type))))))

(def (field (= default MISSING) (= default_factory MISSING) (= init #t) (= repr #t)
            (= hash None) (= compare #t) (= metadata None))
    """Return an object to identify dataclass fields.
    default is the default value of the field.  default_factory is a
    0-argument function called to initialize a field's value.  If init
    is True, the field will be a parameter to the class's __init__()
    function.  If repr is True, the field will be included in the
    object's repr().  If hash is True, the field will be included in
    the object's hash().  If compare is True, the field will be used
    in comparison functions.  metadata, if specified, must be a
    mapping which is stored but not otherwise examined by dataclass.
    It is an error to specify both default and default_factory.
    """
    (when (and (not (eq? default MISSING)) (not (eq? default_factory MISSING)))
      (raise (ValueError "cannot specify both default and default_factory")))

    (Field default default_factory init repr hash compare metadata))

(define (_is_classvar a_type typing)
    (or (eq? a_type (ref typing 'ClassVar))
        (and (eq? (type a_type) (ref typing '_GenericAlias))
             (eq? (ref a_type '__origin__) (ref typing 'ClassVar)))))


(define (_is_initvar a_type dataclasses)
  (or (eq? a_type (ref dataclasses 'InitVar))
      (eq? (type a_type) (ref dataclasses 'InitVar))))

(define (_is_type annotation cls a_module a_type is_type_predicate)
  (define match ((ref _MODULE_IDENTIFIER_RE 'match) annotation))
  (if match
      (let ()
        (define ns          #f)
        (define module-name ((ref match 'group) 1))
        
        (if (not (bool module-name))
            (set! ns
              (py-dict
               (py-get sys-modules (ref cls '__module__))))
                 
            (let ((module (py-get sys-modules (ref cls '__module__) #f)))
              (when (and module
                         (aif it (ref module (string->symbol
                                              module-name) #f)
                              (eq? it a_module)
                              #f))
                
                (set! ns (py-dict module)))))
    
        (if (and ns
                 (is_type_predicate
                  (py-get ns ((ref match 'group) 2))
                  a_module))
            #t
            #f))
      #f))
        
(define  (_get_field cls a_name a_type)
  (define default (ref cls (string->symbol a_name) MISSING))

  (define f
    (if (isinstance default Field)
        default
        (begin
          #;(when (isinstance default MemberDescriptorType)
            (set! default MISSING))
          (py-apply field (= default default)))))

  (set f 'name a_name)
  (set f 'type a_type)

  (set f '_field_type _FIELD)

  (let ((typing (py-get sys-modules "typing" #f)))
    (when typing
      (when (or (_is_classvar a_type typing)
                (and (isinstance (ref f 'type) str)
                     (_is_type (ref f 'type)
                               cls
                               typing
                               (ref typing 'ClassVar)
                               _is_classvar)))
        (set f '_field_type _FIELD_CLASSVAR))))

  (when (eq? (ref f '_field_type)  _FIELD)
    (let ((dataclasses (py-get sys-modules "dataclasses" #f)))
      (when (or (_is_initvar a_type dataclasses)
                (and (isinstance (ref f 'type) str)
                     (_is_type (ref f 'type) cls dataclasses (ref dataclasses 'InitVar)
                               _is_initvar)))
        (set f '_field_type _FIELD_INITVAR))))

  (when (in (ref f '_field_type) (scm-list _FIELD_CLASSVAR _FIELD_INITVAR))
    (when (not (eq? (ref f 'default_factory) MISSING))
      (raise (TypeError (py-mod "field %s cannot have a default factory"
                              (ref f 'name)))))
    (when (and (eq? (ref f '_field_type) _FIELD)
               (isinstance (ref f 'default) (scm-list dict py-list py-set)))
      (raise
       (ValueError
        (py-mod "mutable default %s for field %s is not allowed: use default_factory"
              (scm-list (ref f 'default) (ref f 'name)))))))
  f)
  
(define (make-init fields frozen has-post-init self-name globals)
  (define s    string->symbol)
  (define name (s self-name))
  (define (default f)
    (cond
     ((and (eq? (ref f  'default) MISSING) (eq? (ref f 'default_factory) MISSING))
      #f)
     
     ((not (eq? (ref f 'default) MISSING))
      (string->symbol (py-mod "_dflt_%s" (ref f 'name))))
     
     ((not (eq? (ref f 'default_factory) MISSING))
      '(@@ (language python module dataclasses) _HAS_DEFAULT_FACTORY))))
  
  (define (def-arg f)
    (let ((def (default f)))          
      (if def
          `(= ,(s (ref f 'name)) ,def) 
          (string->symbol (ref f 'name)))))
      
  (define (args)
    (let lp ((l fields) (r (scm-list name)))
      (if (pair? l)
          (lp (cdr l)
              (aif it (def-arg (car l)) 
                   (cons it r)
                   r))
          (reverse r))))

  (define (init-1 f)
    (define default_name (s (py-mod "_dflt_%s" (ref f 'name))))
    (define value #f)
    (if (not (eq? (ref f 'default_factory) MISSING))
        (if (ref f 'init)
            (begin
              (set! value
                `((@ (guile) if)
                  (eq?
                   ,(ref f 'name)
                   (@ (language python module dataclasses) _HAS_DEFAULT_FACTORY)
                   (%s)
                   ,(ref f 'name)))))

            (begin
              (set! value `default_name)))

        (if (ref f 'init)
            (if (eq? (ref f 'default) MISSING)
                (set! value (s (ref f 'name)))
                (begin
                  (module-define! globals  default_name
                                  (ref f 'default))
                  (set! value (s (ref f 'name)))))))
    

    (if (and value (not (eq? (ref f '_field_type) _FIELD_INITVAR)))
        `((@ (oop pf-objects) set) ,name
          ((@ (guile) quote) ,(s (ref f 'name)))  ,value)
        #f))
  
  (define (post)
    (if has-post-init
        (let ((args (string-join (map (lambda (x) (ref x 'name)) fields) " ")))
          (scm-list `(((@ (oop pf-objects) ref) ,name
                       ((@ (guile) quote)
                        ,__PO))
                      ,args)))
        '()))
  
  (define (init)
    (let lp ((l fields) (r '()))
      (if (pair? l)
          (let* ((f (car l))
                 (x (init-1 f)))
            (if x
                (lp (cdr l) (cons x r))
                (lp (cdr l) r)))
          (reverse (append (post) r)))))

  (define (defaults)
    (let lp ((l fields))
      (if (pair? l)
          (let ((f (car l)))
            (aif it (default f)
                 (if (symbol? it)
                     (cons (list it (ref f 'default)) (lp (cdr l)))
                     (lp (cdr l)))
                 (lp (cdr l))))
          '())))
            
  (let ((seen-default #f))
    (let lp ((l fields))
      (if (pair? l)
          (let ((f (car l)))
            (if (ref f 'init)
                (if (and (not (eq? (ref f 'default) MISSING))
                         (eq? (ref f 'default_factory) MISSING))
                    (set! seen-default #t)
                    (when seen-default
                      (raise (TypeError
                              (py-mod "Non-default argumen %s follows default argument"
                                    (ref f 'name)))))))
            (lp (cdr l))))))

  (let ((defaults (defaults)))
    (apply
     (scm-compile
      `((@ (guile) lambda) ,(map car defaults)
        ((@ (language python def) lam) ,(args)
         ,@(init)
         ((@ (guile) values)))))
     (map cadr defaults))))

(define (_hash_set_none cls fields globals) None)
(define (_hash_add cls fields globals)
  (define flds (let lp ((l fields))
                 (if (pair? l)
                     (let ((f (car l)))
                       (if (if (eq? (ref f 'hash) None)
                               (ref f 'compare) 
                               (ref f 'hash))
                           (cons f (lp (cdr l)))
                           (lp (cdr l))))
                     '())))
  (_hash_fn flds globals))

(define (_hash_exception cls fields globals)
  (raise (TypeError (+ "Cannot overwrite attribute __hash__ in class "
                       (ref cls '__name__)))))

(define _hash_action
  (dict
   `(((#f #f #f #f) #f)
     ((#f #f #f #t) #f)
     ((#f #f #t #f) #f)
     ((#f #f #t #t) #f)
     ((#f #t #f #f) ,_hash_set_none)
     ((#f #t #f #t) #f)
     ((#f #t #t #f) ,_hash_add)
     ((#f #t #t #t) #f)
     ((#t #f #f #f) ,_hash_add)
     ((#t #f #f #t) ,_hash_exception)
     ((#t #f #t #f) ,_hash_add)
     ((#t #f #t #t) ,_hash_exception)
     ((#t #t #f #f) ,_hash_add)
     ((#t #t #f #t) ,_hash_exception)
     ((#t #t #t #f) ,_hash_add)
     ((#t #t #t #t) ,_hash_exception))))

(define (_hash_fn fields globals)
  (define (line f)
    `((@ (language python hash) py-hash) ((@ (oop pf-objects) ref)
                                          self ((@ (guile) quote)
                                                ,(string->symbol
                                                  (ref f 'name))))))
  (scm-compile `((@ (guile) lambda) (self)
                 ((@ (guile) modulo)
                  (+ 0 ,@(map line fields))
                  (@ (language python hash) pyhash-N)))))


(define (make-repr fields global)
  (scm-compile
   `((@ (guile) lambda) (self)
     (+ ((@ (oop pf-objects) ref)
         ((@ (oop pf-objects) ref)
          self ((@ (guile) quote) __class__))
         ((@ (guile) quote) __qualname__))
        
        "("
        
        ((@ (guile) string-join)
         ((@ (guile) let) lp ((l ((@ (guile) quote) ,fields)) (first #t))
          ((@ (guile) if) ((@ (guile) pair?) l)
           ((@ (guile) cons)
            ((@ (guile) let) ((name
                               ((@ (guile) car) l)))
             
             (+
              ((@ (guile) if) first
               ""
               ",")
              name
              "="
              ((@ (language python module python) repr)
               ((@ (oop pf-objects) ref) self
                ((@ (guile) string->symbol) name)))))
            (lp ((@ (guile) cdr) l) #f))
           ((@ (guile) quote) ())))
         "")
        ")"))))
                        
(define (make-cmp fields op global)
  (define (mk-tuple name)
    `((@ (guile) list)
      ,@(map (lambda (f)
               `((@ (oop pf-objects) ref)
                 ,name ((@ (guile) quote)
                        ,(string->symbol
                          (ref f 'name)))))
             fields)))
  
  (define self-tuple  (mk-tuple 'self))
  (define other-tuple (mk-tuple 'other))
  (define (C x) `(@ (oop pf-objects) ,x))
  (define (G x) `(@ (guile) ,x))
  (define (T x) `(@ (language python try) ,x))
  (define (E x) `(@ (language python exceptions) ,x))
  
  (scm-compile `((@ (guile) lambda) (self other)
                 ((@ (guile) and)
                  ,(if (list? op)
                       `((@ (guile) eq?)
                         (,(C 'ref) self  ((@ (guile) quote) __class__))
                         (,(C 'ref) other ((@ (guile) quote) __class__)))
                       `(,(G 'when)
                         (not ((@ (guile) eq?)
                               (,(C 'ref) self  ((@ (guile) quote) __class__))
                               (,(C 'ref) other ((@ (guile) quote) __class__))))
                         (,(T 'raise)
                          (,(E 'TypeError)
                           "Can't compare differnet dataclasses"))))
                  (,op ,self-tuple ,other-tuple)))))

(define (_process_class cls init repr eq order unsafe_hash frozen)
  (define fields              (dict))
  (define order-dict          (dict))
  (define order-list          '())
  (define i                   0)
  (define any-frozen-base     #f)
  (define has-dataclass-bases #f)
  (define cls-annotations     (aif it (find-in-class cls '__annotations__ #f)
                                   it
                                   (dict)))
    
  (define cls-fields          (for ((k v : cls-annotations)) ((l '()))
                                   (begin
                                     (cons (_get_field cls k v) l))
                                   #:final (reverse l)))

  (define field-list '())
  (define (field-list-f)
    (set! field-list
      (let lp ((l order-list))
        (if (pair? l)
            (let* ((name (car l))
                   (f    (pylist-ref fields name)))
              (if (eq? (ref f '_field_type) _FIELD)
                  (cons f (lp (cdr l)))
                  (lp (cdr l))))
            '()))))

  
  (define (x-list x)
    (lambda ()
      (let lp ((l field-list))
        (if (pair? l)
            (let* ((f (car l)))
              (if (ref f x)
                  (cons f (lp (cdr l)))
                  (lp (cdr l))))
            '()))))
  
  (define repr-list
    (lambda ()
      (map (lambda (x) (ref x 'name)) ((x-list 'repr)))))
  
  (define compare-list (x-list 'compare))
  
  (define class-hash (find-in-class cls '__hash__ MISSING))
  (define has-explicit-hash
    (not (or (eq? class-hash MISSING)
             (and (eq? class-hash None)
                  (find-in-class cls '__eq__ #f)))))

  (define (hash-action)
    (pylist-ref _hash_action
                (scm-list #t #;(bool unsafe_hash)
                          (bool eq)
                          #t #;(bool frozen)
                          has-explicit-hash)))

    
  (define (add-f f)
    (let ((name (ref f 'name)))
      (if (not (py-get fields name #f))
          (set! order-list (cons name order-list)))
      (pylist-set! fields name f)
      (set! i (+ i 1))
      (values)))

  (set cls _PARAMS (_DataclassParams init repr eq order unsafe_hash frozen))

  (let lp ((l (reverse (cdr (ref cls '__mro__)))))
    (if (pair? l)
        (let ((p (car l)))
          (let ((base-fields (find-in-class p  __F #f)))
            (when base-fields
              (set! has-dataclass-bases #t)
              (for ((f : base-fields)) ()
                   (add-f f))
              (when (frozen? p)
                (set! any-frozen-base #t)))))))

  (let lp ((l cls-fields))
    (if (pair? l)
        (let* ((f     (car l))
               (name  (ref f 'name))
               (sname (string->symbol name)))
          (add-f (car l))
          (when (isinstance (find-in-class cls sname None)
                            Field)
            (let ((default (ref f 'default)))
              (if (eq? default MISSING)
                  (delattr cls sname)
                  (set cls sname default))))
          (lp (cdr l)))))

  (for ((name value : (py-dict cls))) ()
       (when (and (isinstance value Field)
                  (not (in name cls-annotations)))
         (raise (TypeError (py-mod "%s is a field but has no type annotations" name)))))

  (when has-dataclass-bases
    (when (and any-frozen-base (not frozen))
      (raise (TypeError "cannot inherrent a frozen dataclass from a non frozen one")))

    (when (and (not any-frozen-base) frozen)
      (raise (TypeError "cannot inherrent a non frozen dataclass from a frozen one"))))

  (set cls __F fields)
  (set! order-list (reverse order-list))
  (set cls '__match_args__ order-list)

  (when (and order (not eq))
    (raise (ValueError "eq must be true if order is true")))

  (when init
    (let ()
      (define has-post-init (ref cls __PO))
      (define flds (for ((f v : fields)) ((flds '()))
                        (if (in (ref v '_field_type)
                                (scm-list _FIELD _FIELD_INITVAR))
                            (cons v flds)
                            flds)
                        #:final (reverse flds)))
    
      (set cls '__init__
           (object-method
            (make-init flds
                       frozen
                       has-post-init
                       (if (in "self" fields)
                           "__dataclass_self"
                           "self")
                       (current-module))))))

  (field-list-f)

  (when repr
    (set cls '__repr__
         (object-method
          (make-repr (repr-list) (current-module)))))

  (when eq
    (set cls '__eq__
         (object-method
          (make-cmp (compare-list)
                    '(@ (oop pf-objects) py-equal?)
                    (current-module)))))

  (when order
    (let ((flds (compare-list)))
      (let lp ((l (scm-list (scm-list '__lt__ '<)
                            (scm-list '__le__ '<=)
                            (scm-list '__gt__ '>)
                            (scm-list '__ge__ '>=))))
        (if (pair? l)
            (let ((a (caar l))
                  (b (cdar l)))
              (set cls a
                   (object-method
                    (make-cmp
                     flds b (current-module)))))
            (cdr l)))))

  (let ((action (hash-action)))
    (when action
      (set cls '__hash__
           (object-method
            (action cls field-list (current-module))))))

  (py-recalc-object cls)

  (when (not (ref cls '__doc__))
    (set cls '__doc__
         (string-join
          (cons
           "dataclass with fields: "
           (let lp ((l order-list) (first #t))
             (if (pair? l)
                 (if first
                     (cons*     (car l) (lp (cdr l) #f))
                     (cons* "," (car l) (lp (cdr l) #f)))
                 '()))))))

  (when frozen (set cls '__freeze_object__ #t))
  (freeze cls)

  cls)

(def (dataclass (* l)
                (= init #t) (= repr #t) (= eq #t) (= order #f)
                (= unsafe_hash #f) (= frozen #f))

     (let ((l (to-list l)))
       (if (null? l)
           (lambda (cls) (_process_class cls init repr eq order unsafe_hash frozen))
           (begin
             (when (> (length l) 1)
               (raise (ArgumentError "Can only have 0 or 1 positional parameter")))

             (_process_class (car l) init repr eq order unsafe_hash frozen)))))
                             
               
(define (fields o)
  (define fields (aif it (ref o __F)
                      it
                      (raise (TypeError
                              "must be called with a dataclass type"))))

  (for ((k f : fields)) ((l '()))
       (if (eq? (ref f '_fields_type) _FIELD)
           (cons f l)
           l)
       #:final l))

(define (_is_dataclass_instance obj)
  "Returns True if obj is an instance of a dataclass."
  (ref (type obj) __F))

(define (is_dataclass obj)
  (define cls (if (isinstance obj type)
                  obj
                  (type obj)))
  (if (ref cls __F)
      #t
      #f))

(define (_asdict_inner obj fact)
  (cond
   ((_is_dataclass_instance obj)
    (fact
     (let lp ((l (fields obj)))
       (if (pair? l)
           (let ((f (car l)))
             (cons (scm-list
                    (ref f 'name)
                    (_asdict_inner (ref obj (string->symbol
                                             (ref f 'name)))
                                   fact))
                   (lp (cdr l)))
             '())))))

   ((and (isinstance obj tuple) (ref obj __F))
    (py-apply (type obj) (* (map (lambda (x)
                                   (_asdict_inner x fact))
                                 (to-list obj)))))


   ((isinstance obj (py-list tuple))
    ((type obj) (map (lambda (x)
                       (_asdict_inner x fact))
                     (to-list obj))))

   ((isinstance obj dict)
    ((type obj) (for ((k v : obj)) ((l '()))
                     (cons (scm-list (_asdict_inner k fact)
                                     (_asdict_inner v fact))
                           l)
                     #:final l)))

   (else
    (deepcopy obj))))
                                   

    
(def (asdict obj (= dict_factory dict))
     (when (not (_is_dataclass_instance obj))
       (raise (TypeError
               "asdict() shoulkd be called on dataclass instances")))
     (_asdict_inner obj dict_factory))

(define (_astuple_inner obj fact)
  (cond
   ((_is_dataclass_instance obj)
    (fact
     (let lp ((l (fields obj)))
       (if (pair? l)
           (let ((f (car l)))
             (cons (scm-list
                    (ref f 'name)
                    (_astuple_inner (ref obj (string->symbol
                                             (ref f 'name)))
                                   fact))
                   (lp (cdr l)))
             '())))))

   ((and (isinstance obj tuple) (ref obj __F))
    (py-apply (type obj) (* (map (lambda (x)
                                   (_astuple_inner x fact))
                                 (to-list obj)))))


   ((isinstance obj (py-list tuple))
    ((type obj) (map (lambda (x)
                       (_astuple_inner x fact))
                     (to-list obj))))

   ((isinstance obj dict)
    ((type obj) (for ((k v : obj)) ((l '()))
                     (cons (scm-list (_astuple_inner k fact)
                                     (_astuple_inner v fact))
                           l)
                     #:final l)))
   (else
    (deepcopy obj))))


(def (astuple obj (= tuple_factory tuple))
     (when (not (_is_dataclass_instance  obj))
       (raise
        (TypeError
         "astuple() should be called on dataclass oinstances")))

     (_astuple_inner obj tuple_factory))

(define-syntax make_dataclass
  (lambda (x)
    (define (mod-)
      (datum->syntax x (module->py-module (module-name (current-module)))))

    (syntax-case x ()
      ((_ . l)
       (with-syntax ((mod (mod-)))
         #'(make_dataclass- 'mod . l)))
      (_
       (with-syntax ((mod (mod-)))
         #'(lambda x
             (apply make_dataclass- 'mod x)))))))

(def (make_dataclass- mod cls_name fields (= bases '())
                     (= namespace None) (= init #t)
                     (= repr #t) (= eq #t) (= order #f)
                     (= unsafe_hash #f) (= frozen #f))
"Return a new dynamically created dataclass.
The dataclass name will be 'cls_name'.  'fields' is an iterable
of either (name), (name, type) or (name, type, Field) objects. If type is
omitted, use the string 'typing.Any'.  Field objects are created by
the equivalent of calling 'field(name, type [, Field-info])'.
  
  C = make_dataclass('C', ['x', ('y', int), ('z', int, field(init=False))], bases=(Base,))
  
is equivalent to:

  @dataclass
  class C(Base):
    x: 'typing.Any'
    y: int
    z: int = field(init=False)

For the bases and namespace parameters, see the builtin type() function.    The parameters init, repr, eq, order, unsafe_hash, and frozen are passed to
dataclass().
"

  (define seen (py-set))
  (define anns (dict))
  (define name "")
  (define tp   None)

  (set! namespace
    (if (eq? namespace None)
        (dict)
        ((ref namespace 'copy))))

  (let lp ((l (to-list fields)))
    (if (pair? l)      
        (let ((item (car l)))
          (cond
           ((isinstance item str)
            (set! name item)
            (set! tp "typing.Any"))

           ((= (len item) 2)
            (let ((l (to-list item)))
              (set! name (car l))
              (set! tp   (cadr l))))

           ((= (len item) 3)
            (let ((l (to-list item)))
              (set! name (car l))
              (set! tp   (cadr l))
              (pylist-set! namespace name (caddr l))))

           (else
            (raise (TypeError (+ "Invalid field: " (repr item))))))

          (when (not (isinstance name str))
            (raise (TypeError (+ "Field names must be valid identifiers "
                                 (repr name)))))

          (when (iskeyword name)
            (raise (TypeError (+ "Field names must not be keywords "
                                 (repr name)))))

          (when (in name seen)
            (raise (TypeError (+ "Field name duplicated: " (repr name)))))

          ((ref seen 'add) name)
          (pylist-set! anns name tp)

          (lp (cdr l)))))

  (pylist-set! namespace "__annotations__" anns)

  (let ((d (dict)))
    (pylist-set! d "__module__" (string-join (map symbol->string mod) "."))
    (let ((cls (new_class cls_name bases d
                          (lambda (ns) (py-update ns namespace)))))
      (py-apply dataclass cls (= init init) (= repr repr) (= eq eq)
                (= order order) (= unsafe_hash unsafe_hash) (= fozen frozen)))))
              

(def (replace obj (** changes))
"Return a new object replacing specified fields with new values.
This is especially useful for frozen classes.  Example usage:
  @dataclass(frozen=True)
  class C:
     x: int
     y: int
  c = C(1, 2)
  c1 = replace(c, x=3)
  assert c1.x == 3 and c1.y == 2
"
  (when (not (_is_dataclass_instance obj))
    (raise (TypeError "replace() should be called with a dataclass instance")))
  

  (let lp ((l (to-list (ref obj __F))))
    (if (pair? l)
        (let* ((f (car l))
               (nm (ref f 'name)))
          (when (eq? (ref f '_field_type) _FIELD_CLASSVAR)
            (lp (cdr l)))

          (when (not (ref f 'init))
            (when (in nm changes)
              (raise
               (ValueError
                (py-mod
                 (+
                  (py-mod "field %s is declared with it=False, "
                        nm)
                  "it cannot be specified with replace()")))))

            (lp (cdr l)))

          (when (in nm changes)
            (when (eq? (ref f '_field_type) _FIELD_INITVAR)
              (raise
               (ValueError
                (+ (py-mod "InitVar %s " nm)
                   "must be specified with replace()"))))

            (pylist-set! changes nm (ref obj (string->symbol nm))))

          (lp (cdr l)))))
            
  (py-apply (ref obj '__class__) (** changes)))

(define --export-macros #t)

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module cmath)
  #:export (polar rect exp log log10 sqrt pi e tau inf nan infj nanj isclose
		  isnan isinf isfinite sin cos tan asin acos atan
		  sinh cosh tanh asinh acosh atanh))

(define (polar x)
  (list (magnitude x) (angle x)))

(define (rect r phi)
  (make-polar r phi))

;; pow log

(define exp   (@ (guile) exp))
(define log   (@ (guile) log))
(define log10 (@ (guile) log10))
(define sqrt  (@ (guile) sqrt))

;; trig
(define sin   (@ (guile) sin))
(define cos   (@ (guile) cos))
(define tan   (@ (guile) tan))
(define asin  (@ (guile) asin))
(define acos  (@ (guile) acos))
(define atan  (@ (guile) atan))

;; hyp
(define sinh   (@ (guile) sinh))
(define cosh   (@ (guile) cosh))
(define tanh   (@ (guile) tanh))
(define asinh  (@ (guile) asinh))
(define acosh  (@ (guile) acosh))
(define atanh  (@ (guile) atanh))

;; classification
(define isfinite1 (@ (language python module math) isfinite))

(define (isfinite x)
  (and (isfinite1 (real-part x))
       (isfinite1 (imag-part x))))

(define (isinf x)
  (or (inf? (real-part x))
      (inf? (imag-part x))))

(define (isnan x)
  (or (nan? (real-part x))
      (nan? (imag-part x))))

(define* (isclose a b #:key (rel_tol 1e-9) (abs_tol 0.0))
  (define e-abs (magnitude (- a b)))
  (if (< e-abs (max (* rel_tol (max (magnitude a) (magnitude b))) abs_tol))
      #t
      #f))
 
(define pi  (@ (language python module math) pi))
(define e   (@ (language python module math) e))
(define tau (@ (language python module math) tau))
(define inf (@ (language python module math) inf))
(define nan (@ (language python module math) nan))
(define infj (make-rectangular 0 inf))
(define nanj (make-rectangular 0 nan))


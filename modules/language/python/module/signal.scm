;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module signal)
  #:use-module (language python exceptions)
  #:use-module (language python try)
  #:use-module (ice-9 threads)
  #:replace (SIGHUP SIGINT
                   SIGABRT    SIGHUP     SIGPOLL    SIGSTKSZ   SIGURG
                   SIGALRM    SIG_IGN    SIGPROF    SIGSTOP    SIGUSR1
                   SIGBUS     SIGILL     SIGPWR     SIGSYS     SIGUSR2
                   SIGCHLD    SIGINT     SIGQUIT    SIGTERM    SIGVTALRM
                   SIGCLD     SIGIO      SIGRTMAX   SIGTRAP    SIGWINCH
                   SIGCONT    SIGIOT     SIGRTMIN   SIGTSTP    SIGXCPU
                   SIG_DFL    SIGKILL    SIGSEGV    SIGTTIN    SIGXFSZ
                   SIGFPE     SIGPIPE    SIGSTKFLT  SIGTTOU)
            
  #:export (default_int_handler
             getsignal signal alarm pause raise_signal))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-syntax-rule (m a ...)
  (begin
    (define a (@ (guile) a))
    ...))

(m
 SIGABRT    SIGPOLL    SIGSTKSZ   SIGURG     SIGHUP
 SIGALRM    SIG_IGN    SIGPROF    SIGSTOP    SIGUSR1
 SIGBUS     SIGILL     SIGPWR     SIGSYS     SIGUSR2
 SIGCHLD    SIGINT     SIGQUIT    SIGVTALRM  
 SIGCLD     SIGIO      SIGRTMAX   SIGTRAP    SIGWINCH
 SIGCONT    SIGIOT     SIGRTMIN   SIGTSTP    SIGXCPU
 SIG_DFL    SIGKILL    SIGSEGV    SIGTTIN    SIGXFSZ
 SIGFPE     SIGPIPE    SIGSTKFLT  SIGTTOU    SIGTERM)

(define default_int_handler
  (lambda (x f)
    (raise (KeyboardInterrupt))))

(define *lock*   (make-mutex 'recursive))
(define (lock)   (lock-mutex *lock*))
(define (unlock) (unlock-mutex *lock*))
(define (with f) (lambda x (dynamic-wind
                             (lambda () #f)
                             (lambda () (lock) (apply f x))
                             (lambda () (unlock)))))
(define py2scm (make-weak-key-hash-table))
(define scm2py (make-weak-key-hash-table))

(define (to-py c)
  (aif it (hash-ref scm2py c)
       it
       (let ((f (lambda (fr x) (c x))))
         (hash-set! scm2py c f)
         (hash-set! py2scm f c)
         f)))

(define (to-scm c)
  (aif it (hash-ref py2scm c)
       it
       (let ((f (lambda (x) (c #f x))))
         (hash-set! scm2py f c)
         (hash-set! py2scm c f)
         f)))
  
(define (getsignal i)
  (let* ((f (sigaction i))
         (c (car f)))
    (cond
     ((integer? c)
      c)
     ((procedure? c)
      (to-py c))
     (else
      SIG_DFL))))

(define signal
  (case-lambda
    ((i h)
     (let ((p (getsignal i)))
       (sigaction i
         (cond
          ((integer? h)
           h)
          ((procedure? h)
           (to-scm h))))
       (to-py p)))))

(define alarm (@ (guile) alarm))
(define pause (@ (guile) pause))
(define raise_signal (with (@ (guile) raise)))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _thread)
  #:use-module (ice-9 threads)
  #:export (get_ident))

(define get_ident current-thread)

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module imp)
  #:use-module (language python module)
  #:use-module (language python exceptions)
  #:use-module (language python module os path)
  #:use-module (ice-9 match)
  #:export (get_magic PY_COMPILED PY_SOURCE load_module))

(define (get_magic) #vu8(1 212 0 233 12 222 3 6 123))

(define PY_COMPILED 0)

(define PY_SOURCE   1)

(define (source->package p) (car  (string-split p #\.)))
(define (load_module a f path b)
  (set! path (abspath path))
  (Module 
   (let lp ((l (string-split (source->package path) "/")))
     (match l
       ((("language" "python" "module" . u) . _)
        u)
       ((_ . l)
        (lp l))
       (() None)))))
  

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module math)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python list)
  #:use-module (language python exceptions)
  #:use-module (language python number)
  #:use-module (system foreign)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  
  #:export (ceil copysign fabs factorial factorial2 floor fmod frexp fsum
		 gcd isclose
		 isfinite isinf isnan modf trunc exp expm1 log log1p log2 log10
		 pow sqrt acos asin atan cos sin tan atan2 hypot pi prod
		 degrees radians acosh asinh atanh cosh sinh tanh
		 erf erfc gamma lgamma e tau inf nan dist isqrt
		 jacobi legendre kronecker primorial binomial lucas lukas2))

(use-modules (system foreign-library))
(use-modules (persist persistance))

(define lib (load-foreign-library "libguile-persist"))
(define fac-
  (pointer->procedure
   '*
   (dynamic-func "scm_mp_fac" lib)
   (list unsigned-long)))

(define (fac x) (car (pointer->scm (fac- x))))

(define fac2-
  (pointer->procedure
   '*
   (dynamic-func "scm_mp_2fac" lib)
   (list unsigned-long)))

(define (fac2 x) (car (pointer->scm (fac2- x))))

(define fac2-
  (pointer->procedure
   '*
   (dynamic-func "scm_mp_2fac" lib)
   (list unsigned-long)))

(define (fac2 x) (car (pointer->scm (fac2- x))))

(define primorial-
  (pointer->procedure
   '*
   (dynamic-func "scm_mp_primorial" lib)
   (list unsigned-long)))

(define (primorial x) (car (pointer->scm (primorial- x))))

(define lukas1-
  (pointer->procedure
   '*
   (dynamic-func "scm_mp_lucas" lib)
   (list unsigned-long)))

(define (lukas x) (car (pointer->scm (lukas1- x))))

(define lukas2-
  (pointer->procedure
   '*
   (dynamic-func "scm_lucas2_ui" lib)
   (list unsigned-long)))

(define (lukas2 x) (apply values (pointer->scm  (lukas2- x))))

(define binomial-
  (pointer->procedure
   '*
   (dynamic-func "scm_bin_ui" lib)
   (list '* '*)))

(define (binomial x y) (car (pointer->scm (binomial- x y))))

(define jacobi-
  (pointer->procedure
   unsigned-long
   (dynamic-func "scm_jacobi" lib)
   (list '* '*)))

(define (jacobi x y) (pointer->scm (jacobi- x y)))

(define legendre-
  (pointer->procedure
   unsigned-long
   (dynamic-func "scm_legendre" lib)
   (list '* '*)))

(define (legendre x y) (pointer->scm (legendre- x y)))

(define kronecker-
  (pointer->procedure
   unsigned-long
   (dynamic-func "scm_kronecker" lib)
   (list '* '*)))

(define (kronecker x y) (pointer->scm (kronecker- x y)))


(define (real! s x)
  (if (complex? x)
      (if (= (imag-part x) 0)
          (real-part x)
          (raise ValueError "real math fkn result in complex number" s))
      x))

(define ceil
  (lambda (x)
    ((@ (guile) inexact->exact)
     ((@ (guile) ceiling)
      x))))
  
(define (copysign x y)
  (let ((x ((@ (guile) abs) x)))
    (if (< y 0) (- x) x)))

(define (fabs x) ((@ (guile) abs) x))


(define (factorial N)    
  (when (not (and (number? N) (integer? N)))
    (raise ValueError "Not an integer"))
  
  (when (< N 0)
    (raise ValueError "Negative integer"))

  (fac N))

(define (factorial2 N)    
  (when (not (and (number? N) (integer? N)))
    (raise ValueError "Not an integer"))
  
  (when (< N 0)
    (raise ValueError "Negative integer"))

  (fac2 N))
	     
(define (isqrt x)
  (inexact->exact (floor (sqrt x))))

(define (floor x)
  ((@ (guile) inexact->exact)
   ((@ (guile) floor)
    x)))

(define (fmod x y) ((@ (guile) truncate-remainder) x y))

(define frexp
  (let ((f (pointer->procedure double
                               (dynamic-func "frexp" (dynamic-link))
                               (list double '*))))
    (lambda (x)
      (let* ((v  (make-bytevector 4))
	     (vp (bytevector->pointer v)))
	(list (f x) (bytevector-s32-ref v 0 (native-endianness)))))))

(define (fsum it)
  (for ((x : it)) ((s 0))
       (+ s x)
       #:final s))

(define* (prod it #:key (start 1))
  (for ((x : it)) ((s start))
       (* s x)
       #:final s))
       

(define gcd (@ (guile) gcd))

(define* (isclose a b #:key (rel_tol 1e-9) (abs_tol 0.0))
  (define e-abs (abs (- a b)))
  (if (< e-abs (max (* rel_tol (max (abs a) (abs b))) abs_tol))
      #t
      #f))

(define (isfinite x)
  (if (or (inf? x) (nan? x))
      #f
      #t))

(define isinf inf?)
(define isnan nan?)

(define (ldexp x i) (* x ((@ (guile) expt) 2 i)))

(define (modf x)
  (let* ((x1 (floor x))
	 (x2 (- x x1)))
    (values x2 x1)))

(define trunc py-trunc)

;; Power and logarithms
(define (exp x) (real! 'exp ((@ (guile) exp) x)))

(define expm1
  (let ((f (pointer->procedure double
                               (dynamic-func "expm1" (dynamic-link))
                               (list double))))
    (lambda (x) (f x))))

(define* (log x #:optional (base #f))
  (real! 'log
         (if (not base)
             ((@ (guile) log) x)
             (/ ((@ (guile) log) x) ((@ (guile) log) base)))))

(define log1p
  (let ((f (pointer->procedure double
                               (dynamic-func "log1p" (dynamic-link))
                               (list double))))

    (lambda (x) (f x))))

(define log2
  (let ((f (pointer->procedure double
                               (dynamic-func "log2" (dynamic-link))
                               (list double))))
    (lambda (x) (f x))))

(define (log10 x) (real! 'log10 ((@ (guile) log10) x)))
  
(define (pow x y) (real! 'pow ((@ (guile) expt) x y)))

(define (sqrt x) (real! 'sqrt ((@ (guile) sqrt) x)))

;; Trigs
(define (acos x) (real! 'acos ((@ (guile) acos) x)))
(define (asin x) (real! 'asin ((@ (guile) asin) x)))
(define (atan x) (real! 'atan ((@ (guile) atan) x)))
(define (cos  x) (real! 'cos ( (@ (guile) cos) x)))
(define (sin  x) (real! 'sin ( (@ (guile) sin) x)))
(define (tan  x) (real! 'tan ( (@ (guile) tan) x)))

(define atan2
  (let ((f (pointer->procedure double
                               (dynamic-func "atan2" (dynamic-link))
                               (list double double))))
    (lambda (x y) (f x y))))

(define (hypot x)
  (for ((i : x)) ((s 0))
       (+ (* i i) s)
       #:final
       (sqrt s)))

;; angular conversion
(define pi (* 4 (atan 1)))

(define degrees
  (let ((f (/ 360 (* 2 pi))))
    (lambda (x) (* f x))))

(define radians
  (let ((f (/ (* 2 pi) 360)))
    (lambda (x) (* f x))))

;; Hyperbolic funcitons
(define (acosh x) (real! 'acosh ((@ (guile) acosh) x)))
(define (asinh x) (real! 'asinh ((@ (guile) asinh) x)))
(define (atanh x) (real! 'atanh ((@ (guile) atanh) x)))
(define (cosh  x) (real! 'cosh ( (@ (guile) cosh) x)))
(define (sinh  x) (real! 'sinh ( (@ (guile) sinh) x)))
(define (tanh  x) (real! 'tanh ( (@ (guile) tanh) x)))


;; Special functions
(define erf
  (let ((f (pointer->procedure double
                               (dynamic-func "erf" (dynamic-link))
                               (list double))))
    (lambda (x) (f x))))

(define erfc
  (let ((f (pointer->procedure double
                               (dynamic-func "erfc" (dynamic-link))
                               (list double))))
    (lambda (x) (f x))))

(define gamma
  (let ((f (pointer->procedure double
                               (dynamic-func "tgamma" (dynamic-link))
                               (list double))))

    (lambda (x)
      (if (integer? x)
	  (factorial (- x 1)) 
	  (f x)))))

(define lgamma
  (let ((f (pointer->procedure double
                               (dynamic-func "lgamma" (dynamic-link))
                               (list double))))
    (lambda (x)
      (f x))))

;; constants
(define e   (exp 1))
(define tau (* 2 pi))
(define inf ((@ (guile) inf)))
(define nan ((@ (guile) nan)))

(define (dist x y)
  (let ((n1 (len x))
        (n2 (len y)))
    (if (= n1 n2)
        (for ((i : x) (j : y)) ((s 0))
             (let ((d (- i j)))
               (+ (* d d) s))
             #:final
             (sqrt s))  
        (raise (ValueError "in math.dist x and y must have the same length")))))

        

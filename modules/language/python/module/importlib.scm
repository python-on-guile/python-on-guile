;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (langauge python module importlob)
  #:use-module (language python def)
  #:use-module (language python string)
  #:use-module (language python bool)
  #:use-module (language python try)
  #:use-module (language python list)
  #:use-module (language python exceptions)
  #:use-module (oop pf-objects)
  #:use-module (language python module)
  #:export (import_module reload))


(define import_module
  (lam (name (= package None))
    (if (symbol? name) (set! name (symbol->string name)))
    (set! name (scm-str name))

    (when (eq? (string-ref name 0) #\.)
      (when (not (bool package))
        (raise TypeError
               (format #f "the 'package' argument is required to perform a relative\nimport for ~a" name))))
    (let* ((n  (let lp ((i 0) (n 0))
                 (if (eq? (string-ref name i) #\.)
                     (lp (+ i 1) (+ n 1))
                     n)))
           (nm (string-split (pylist-slice name n None None) #\.))
           (rr (if (bool package)
                   (string-split (ref package '__name__) #\_)
                   '()))
           (l  (let lp ((i n) (l (reverse rr)))
                 (if (> i 0)
                     (lp (- i 1) (cdr l))
                     (append '(languge python module)
                             (reverse l) nm)))))
      
      (Module l))))

(define (reload module)
  (reload-module (rawref module '_module)))
      

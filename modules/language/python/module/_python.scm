;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _python)
  #:use-module (oop goops)
  #:use-module ((oop dict) #:select (slask-it))
  #:use-module (ice-9 match)
  #:use-module (ice-9 readline)
  #:use-module ((oop pf-objects) #:select
                (<p> <property> class-method static-method ref (set . pf-set)
                     py-super-mac type object pylist-ref define-python-class
		     object-method py-dict rawdel rawref kind ref-in-class
                     pyclass? pyobject? pytype? print-scm))
  #:use-module (rnrs bytevectors)
  #:use-module (language python exceptions       )
  #:use-module ((language python module string   ) #:select ())
  #:use-module ((language python module io       ) #:select (open))
  #:use-module (language python def              )
  #:use-module (language python inspect          ) 
  #:use-module (language python for              )
  #:use-module (language python for2             )
  #:use-module (language python try              )
  #:use-module (language python yield            )
  #:use-module (language python module           )
  #:use-module (language python list             )
  #:use-module (language python dict             )
  #:use-module (language python set              )
  #:use-module (language python string           )
  #:use-module (language python bytes            )
  #:use-module (language python set              )
  #:use-module (language python number           )
  #:use-module (language python dir              )
  #:use-module (language python hash             )
  #:use-module (language python property         )
  #:use-module (language python range            )
  #:use-module (language python tuple            )
  #:use-module (language python eval             )
  #:use-module (language python bool             )
  #:use-module (language python matrix           )

  #:replace (list abs min max hash round format map filter)
  
  #:re-export (StopIteration GeneratorExit RuntimeError
                             Exception ValueError TypeError
                             IndexError KeyError AttributeError
                             sendException next KeyboardInterrupt
                             GeneratorExit sendClose RuntimeError
                             SyntaxError compile OSError
                             len dir next dict None property range
                             tuple bytes bytearray locals globals
                             exec type object __import__ frozenset
			     Warning BytesWarning DeprecationWarning
                             SyntaxWarning backtrace SCMError
                             py-list Ellipsis Slice eval_scm
                             isinstance issubclass
                             )
  
  #:export (print repr complex float int str scmcall
                  set all any bin callable reversed
                  chr classmethod staticmethod objectmethod
                  divmod enumerate delattr bool 
                  getattr hasattr setattr delattr hex
                  iter sum id input oct ord pow super
                  sorted zip vars slice map-))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define (re-export! mod l)
  (cond-expand
    (guile-3.0
     (module-re-export! mod l #:replace? #t))
    ((or guile-2.0  guile-2.2)
     (module-re-export! mod l))))
  
(re-export! (current-module) '(send open eval raise))

(define er ((@ (guile) list) 'error))
(define (vars x)
  (let ((l1
         (for ((k : (dir x))) ((l '()))
              (cons k l)
              #:final
              (py-set l)))
         (l2
          (for ((k : (dir (ref x '__class__)))) ((l '()))
               (cons k l)
               #:final
               (py-set l))))

    (for ((k : (- l1 l2))) ((l '()))
         (cons (cons k (getattr x k None)) l)
         #:final
         (dict l))))
         

(define (remove-int x)
  (let ((x (if (pair? x)
               (string-join ((@ (guile) map) symbol->string (cdddr x)) ".")
               x)))
    (if (or (py-startswith x "oop.pf-objects")
            (py-startswith x "language.python"))
        #f
        x)))

(set! (@@ (oop dict) remove-int) remove-int)

(define (repr x . l)
  (if (pair? l)
      (print-scm (car l) write)
      (print-scm x write)))

(set! (@ (oop dict) repr-) repr)

(define abs     py-abs)
(define str     py-str)
(define complex py-complex)
(define float   py-float)
(define int     py-int)
(define round   py-round)
(define set     py-set)
(define all     py-all)
(define any     py-any)
(define bin     py-bin)
(define divmod  py-divmod)
(define format  py-format)
(define hash    py-hash)
(define hex     py-hex)


(define-method (callable  x                     ) #f)
(define-method (callable (x <procedure>        )) #t)
(define-method (callable (x <procedure-class>  )) #t)
(define-method (callable (x <applicable>       )) #t)
(define-method (callable (x <primitive-generic>)) #t)
(define-method (callable (x <p>))
  (ref x '__call__))

(define (chr x) (list->string ((@ (guile) list) (integer->char x))))

(define objectmethod object-method)
(define classmethod  class-method)
(define staticmethod static-method)

(def (enumerate l (= start 0))
  ((make-generator
    (lambda (yield)
      (for ((x : l)) ((i start))
           (yield i x)
           (+ i 1))))))

(define filter pylist-filter)
(define map    pylist-map)

(define miss (slask-it ((@ (guile) list) 'miss)))

(define* (getattr a b #:optional (k miss))
  (define (nm x)
    (car (reverse (string-split x #\.))))
  
  (define (f)
    (let ((r (ref a (if (string? b) (string->symbol b) b) miss)))
      (if (eq? r miss)
          (if (eq? k miss)
              (if (equal? b "__module__")
                  "<builtin>"
                  (if (isinstance a Module)
                      (raise (AttributeError
                              "module '~a' has no attribute '~a'"
                              (ref a '__name__ 'main) b))
                      (raise (AttributeError
                              "type object '~a' has no attribute '~a'"
                              (nm
                               (ref (if (pyobject? a)
                                        (type a)
                                        a)
                                    '__name__
                                    "anonymous-object"))
                              b))))
              k)
          r)))
  (if (port? a)
      (cond
       ((equal? b "encoding")
        (port-encoding a))
       (else
        (f)))
      (f)))
        
  
(define (setattr a k v)
  (pf-set a (if (string? k) (string->symbol k) k) v))

(define (hasattr a b)
  (let ((r (ref a (if (string? b) (string->symbol b) b) miss)))
    (not (eq? r miss))))

(define iter
  (case-lambda
    ((o) (aif it (wrap-in o)
              it
              (aif get (ref o '__getitem__)
                   ((make-generator
                     (lambda (yield)
                       (for () ((i 0))
                            (yield (get i))
                            (+ i 1)))))
                   (raise TypeError "not iterable" o))))
    ((f sent)
     ((make-generator 
       (lambda (yield)
         (for () ()
              (let ((r (f)))
                (if (equal? r sent)
                    (break)
                    (yield r))))))))))
                               
                                      

(define map-
  (case-lambda
    ((f G)
     (for ((x : G)) ((l '())) (cons (f x) l) #:final l))
    ((f G1 G2)
     (for ((x1 : G1) (x2 : G2)) ((l '())) (cons (f x1 x2) l) #:final l))
    ((f G1 G2 G3)
     (for ((x1 : G1) (x2 : G2) (x3 : G3)) ((l '()))
          (cons (f x1 x2 x3) l)
          #:final l))
    ((f G1 G2 G3 G4)
     (for ((x1 : G1) (x2 : G2) (x3 : G3) (x4 : G4)) ((l '()))
          (cons (f x1 x2 x3 x4) l)
          #:final l))))
                    
(define* (sum i #:optional (start 0))
  (for-in ((x : i)) ((s start))
	  (+ s x)
	  #:final
	  s))


(define (id x) (object-address x))

(define (input str)
  ((@ (guile) format) #t str)
  (readline))

(define (idx x) x)


(define  (min . l)
  (if (null? l)
      (inf)
      (if (and-map number? l)
          (let lp ((l l))
            (if (pair? l)
                (py-min- (car l) (lp (cdr l)))
                (inf)))
          (apply py-min- l))))


(def (py-min- (* l) (= key idx) (= default miss))
     (let lp ((l l))
       (match l
         ((it)
          (for ((x : it)) ((s miss) (b miss))
               (if (eq? s miss)
                   (values (key x) x)
                   (let ((k (key x)))
                     (if (< k s)
                         (values k x)
                         (values s b))))
               #:final
               (if (eq? b miss)
		   (if (eq? default miss)
		       (raise ValueError
			      "min does not work for zero length list")
		       default)
                   b)))
         (_ (lp ((@ (guile) list) l))))))

(define  (max . l)
  (if (null? l)
      (- (inf))          
      (if (and-map number? l)
          (let lp ((l l))
            (if (pair? l)
                (py-max- (car l) (lp (cdr l)))
                (- (inf))))
          (apply py-max- l))))

(def (py-max- (* l) (= key idx) (= default miss))
     (let lp ((l l))
       (match l
         ((it)
          (for ((x : it)) ((s miss) (b miss))
               (if (eq? s miss)
                   (values (key x) x)
                   (let ((k (key x)))
                     (if (> k s)
                         (values k x)
                         (values s b))))
               #:final
               (if (eq? b miss)
		   (if (eq? default miss)
		       (raise ValueError
			      "min does not work for zero length list")
		       default)
                   b)))
         (_ (lp ((@ (guile) list) l))))))

(define (oct x) (+ "0o" (number->string (py-index x) 8)))
(define (ord x)
  (if (byte? x)
      (bytevector-u8-ref (bv-scm x) 0)
      (char->integer (string-ref (pylist-ref x 0) 0))))

(define pow
  (case-lambda
    ((x y)
     (expt x y))
    ((x y z)
     (if (and (number? y) (integer? y) (number? x) (integer? x) (number? z) (integer? z))
	 (modulo-expt x y z)
	 (modulo (expt x y) z)))))

(define-syntax-rule (super . l) (py-super-mac . l))       

(define-syntax list
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       #'(pylist-mac . l))
      (_ #'pylist))))

(define reversed py-reversed)
(define (key-id x) x)
(def (sorted it (= key key-id) (= reverse #f))     
     (define l (to-pylist '()))
     (for ((x : it)) () (pylist-append! l x))
     (pylist-sort! l #:key key #:reverse reverse)
     l)

(define (zip . l)
  (let ((l ((@ (guile) map) wrap-in l)))
    ((make-generator
      (lambda (yield)
        (let lp ()
          (let lp2 ((l l) (r '()))
            (if (pair? l)
                (call-with-values (lambda () (next (car l)))
                  (lambda z
                    (lp2 (cdr l) (append (reverse z) r))))
                (begin
                  (yield (reverse r))
                  (lp))))))))))
  
(define print
  (lam ((= file #f) (* l) (= end "\n"))
       (if file
	   (if (port? file)
	       #t
	       (set! file (ref file '_port)))
	   (set! file
             (ref (eval_scm "(@ (language python module sys) stdout)") '_port)))
       (with-output-to-port file
	 (lambda ()
           (let lp ((l l))
             (apply
              (case-lambda
                (()  ((@ (guile) display) end))
                ((x) ((@ (guile) display) x   ) (lp '()))
                (l   ((@ (guile) display) l   ) (lp '())))
              l))))))


(define-syntax-rule (mk cl cls ? tp)
  (begin
    (set! (@@ (oop pf-objects) cl) cls)
    (set! (@@ (oop pf-objects) ? ) (lambda (x) (isinstance x tp)))))

(mk int-cls    int   int?   int)
(mk tuple-cls  tuple tuple? tuple)
(mk string-cls str   str?   str)
(mk bytes-cls  bytes bytes? bytes)
(mk list-cls   list  list?  list)
(mk float-cls  float float? float)

(define-python-class slice ()
  (define __init__
    (lam (self x (= y None) (= z None))
         (pf-set self 'x x)
         (pf-set self 'y y)
         (pf-set self 'z z))))


(set! (@@ (oop dict) m?)
  (lambda (x) (isinstance x (@@ (language python module) Module))))

(define (delattr o key) (rawdel o key))
(define bool boolean)

(define (scmcall f . l)
  (apply (eval_scm f) l))


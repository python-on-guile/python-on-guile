;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module pwd)
  #:use-module (oop pf-objects)
  #:use-module (language python module collections abc)
  #:use-module (language python exceptions)
  #:use-module (language python yield)
  #:use-module (language python try)
  #:replace (getpwuid getpwnam)
  #:export (getpwall))

(define-python-class PWD (Sequence)
  (define __init__
    (lambda (self a0 a1 a2 a3 a4 a5 a6)
      (set self 'pw_name   a0)
      (set self 'pw_passwd a1)
      (set self 'pw_uid    a2)
      (set self 'pw_gid    a3)
      (set self 'pw_gecos  a4)
      (set self 'pw_dir    a5)
      (set self 'pw_shell  a6)))

  (define __len__
    (lambda (self) 7))
  
  (define __getitem__
    (lambda (self n)
      (case n
        ((0) (ref self 'pw_name))
        ((1) (ref self 'pw_passwd))
        ((2) (ref self 'pw_uid))
        ((3) (ref self 'pw_gid))
        ((4) (ref self 'pw_gecos))
        ((5) (ref self 'pw_dir))
        ((6) (ref self 'pw_shell))
        (else
         (raise KeyError "only ref 0-6"))))))

(define-syntax-rule (ca code)
  (catch #t
    (lambda () code)
    (lambda x (raise OSError x))))

(define (getpwuid uid)
  (ca (apply PWD (vector->list ((@ (guile) getpwuid) uid)))))
                  
(define (getpwnam nm)
  (ca (apply PWD (vector->list ((@ (guile) getpwnam) nm)))))

(define (getpwall)
  (define l
    (let lp ((l (vector->list ((@ (guile) getgroups)))) (r '()))
      (if (pair? l)
          (let* ((v (getgrgid (car l)))
                 (u (vector-ref v 0))
                 (w (vector-ref v 3)))
            (let lp2 ((w w) (r (if (member u r) r (cons u r))))
              (if (pair? w)
                  (if (member (car w) r)
                      (lp2 (cdr w) r)
                      (lp2 (cdr w) (cons (car w) r)))
                  (lp (cdr l) r))))
          r)))
  ((make-generator
   (lambda (yield)
     (let lp ((l l))
       (if (pair? l)
           (begin
             (catch #t
               (lambda () (yield (getpwnam (car l))))
               (lambda x #f))               
             (lp (cdr l)))))))))
           
           
                     
         

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module re flags)
  #:export (set-flags get-flags *flags*
		      A ASCII DEBUG I IGNORECASE L LOCALE M MULTILINE
                      X VERBOSE S DOTALL U UNICODE F FAST))

(define *flags* (make-fluid 0))
(define (set-flags x) (fluid-set! *flags* x))
(define (get-flags)   (fluid-ref *flags*))

(define A          1)
(define ASCII      A)

(define DEBUG      2)

(define I          4)
(define IGNORECASE I)

(define L          8)
(define LOCALE     L)

(define M          16)
(define MULTILINE  M)

(define X          32)
(define VERBOSE    X)

(define S          64)
(define DOTALL     X)

(define U          128)
(define UNICODE    U)

(define F          256)
(define FAST       F)

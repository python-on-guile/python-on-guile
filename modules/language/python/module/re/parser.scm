;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module re parser)
  #:use-module (language python guilemod)
  #:use-module (language python module re flag-parser)
  #:use-module (ice-9 unicode)
  #:export(parse-reg e-matcher pretty))

(define byte-special
  (let ()
    (define fx (f-reg! "[0-9a-fA-F]"))
    (define fo  (f-reg! "[0-7]"))
    (define he (mk-token (f-seq fx fx)    (lambda (x) (string->number x 16))))
    (define oc (mk-token (f-seq fo fo fo) (lambda (x) (string->number x 8))))
    (f-or!
     (f-seq (f-tag "a") (f-out 7))
     (f-seq (f-tag "b") (f-out 8))
     (f-seq (f-tag "f") (f-out 12))
     (f-seq (f-tag "r") (f-out 13))
     (f-seq (f-tag "t") (f-out 9))
     (f-seq (f-tag "v") (f-out 11))
     (f-seq (f-tag "x") he)     
     oc)))

(define f-esc
  (mk-token
   (let ()
     (define fi   (f-reg! "[0-9a-fA-F]"))
     (define he4  (mk-token (f-seq fi fi fi fi)
                            (lambda (x) (string->number x 16))))
     (define he8  (mk-token (f-seq fi fi fi fi fi fi fi fi)
                            (lambda (x) (string->number x 16))))
     (define name (f-seq (f-tag "{") (mk-token (f+ (f-not! (f-tag "}"))))
                         (f-tag "}")))
     (define (check n x)
       (if x x (error (format #f "Wrong unicode name N{~a}" n))))
     
     (f-seq (f-tag "\\")
            (f-or!
             (f-let ((x byte-special))
                    (f-out (integer->char x)))
             (f-seq (f-tag "N")
                    (f-let ((n name))
                           (f-out (check n (formal-name->char n)))))
             (f-seq (f-tag "u")
                    (f-let ((x he4))
                           (f-out (integer->char x))))
             (f-seq (f-tag "U")
                    (f-let ((x he8))
                           (f-out (integer->char x))))
             (f-reg! "[][()*+?]"))))))


(define-syntax-rule (mk n tag str) (define n (f-seq tag (f-tag str))))
(mk f-. #:dot  ".")
(mk f-^ #:^    "^")
(mk f-$ #:$    "$")
(mk q*  #:*    "*")
(mk q?  #:?    "?")
(mk q+  #:+    "+")
(mk q*? #:*?   "*?")
(mk q?? #:??   "??")
(mk q+? #:+?   "+?")

(define subexpr (f-list #:sub
                        (f-seq (f-tag "(")  (Ds ee) (f-tag ")"))))

(define f-back
  (f-or! (f-list #:class (mk-token (f-reg! "[AZbBdDsSwntr]")))
         (mk-token (f-seq (f-reg! "[0-7]") (f-reg! "[0-7]") (f-reg! "[0-7]"))
                   (lambda (x)
                     (list->string
                      (list (integer->char (string->number x 8))))))
         (mk-token (f-reg! "."))))

(define anongroup (f-list #:?:  "(?:" (Ds ee) ")"))
(define namegroup (f-list #:?P< "(?P<" (mk-token (f+ (f-not! (f-reg "[> ]")))) ">" (Ds ee) ")"))

(define (ch not)
  (f-list #:ch
          (f-or! f-esc
                 (f-seq (f-char #\\) f-back)                 
                 (mk-token (f-not! not)))))
(define number (mk-token (f+ (f-reg! "[0-9]")) string->number))
(define incant (f-list #:?P= "(?P=" (f-or! number
					   (mk-token (f+ (f-not! (f-reg "[) ]"))))) ")"))
(define coment  (f-and
                 (f-seq "(?#" (f* (f-not (f-tag ")"))) ")")
                 f-true))
(define repn?   (f-list #:rep? "{" number "}" "?"))
(define repnm?  (f-list #:rep? "{" number "," number "}" "?"))
(define repn    (f-list #:rep  "{" number "}"))
(define repnm   (f-list #:rep  "{" number "," number "}"))

(define lookh   (f-list #:?= "(?=" (Ds ee) ")"))
(define lookh!  (f-list #:?! "(?!" (Ds ee) ")"))

(define rev     (f-list #:?<= "(?<=" (Ds ee) ")"))
(define rev!    (f-list #:?<! "(?<!" (Ds ee) ")"))
(define flags   (f-list #:flags "(?"
                        (f-let ((on (mk-token (f* (f-reg! "[aiLmsux]"))))
                                (off
                                 (f-or!
                                  (f-seq "-" (mk-token (f+ (f-reg! "[aimsx]"))))
                                  (f-out ""))))
                          (f-list (f-out on) (f-out off)
                                  ":"
                                  (f-with-flags on off (Ds ee)) ")"))))
(define flags2  (f-list #:flags2 "(?"
                        (mk-token (f* (f-reg! "[aiLmsux]")))
                        ")"))
(define (bch f) (f-let ((chr
                         (f-or! (f-seq (f-or! (f-tag "\\n") f-nl)      
                                       (f-out (list->string (list #\newline))))
                                f-esc
                                (f-seq (f-char #\\) f-back)
                                f)))
                  (f-ascii chr)
                  (f-out chr)))
(define bbody (f-cons (f-or!
		       (f-list #:range (bch (mk-token (f-reg! ".")))
			       "-" (bch (mk-token
                                         (f-and
                                          (f-not (f-tag "]"))
                                          (f-reg! ".")))))
		       (f-list #:ch    (bch (mk-token (f-reg! ".")))))
		      (ff*
		       (f-or!
			(f-list #:range (bch (mk-token (f-not! (f-tag "]"))))
				"-"
				(bch (mk-token (f-not! (f-tag "]")))))
                        (f-seq (f-tag " ") (f-out (list #:ch " ")))
			(f-list #:ch (bch (mk-token (f-not! (f-tag "]")))))))))

(define (f-if a b c) (f-or! (f-seq a b) c))
(define choice
  (f-cons #:bracket
          (f-or!
           (f-seq "[^]" (f-out (list (list #:ch (f-out #f) (f-out "^")))))
           (f-cons*
            (f-tag "[")
            (f-if (f-tag "^") (f-out #t) (f-out #f))
            (f-seq bbody "]")))))


(define f-bar  (f-tag "|"))

(define qqq    (f-let ((chr (ch (f-reg "[[?+|*.$^()\\]"))))
                 (f-ascii (cadr chr))
                 (f-out chr)))

(define qq     (f-cons #:seq (f-list qqq)))
(define atom   (f-or! qq f-. flags2 choice subexpr anongroup namegroup incant coment lookh lookh! rev rev! f-^ f-$ flags))
(define spec   (f-list #:op atom (f-or! q+? q?? q*? q* q? q+ repn? repnm? repn repnm)))
(define aatom  (f-or! spec atom))
(define f-com  (f-seq (f-tag "#")
                      (f-reg "[ \t]")
                      (f* (f-not (f-or! f-eof f-nl)))))
(define ws     (f-or!
                (f-and
                 f-verbose
                 (f* (f-or! f-com f-nl (f-reg "[ \t\r]"))))
                f-true))

(define line   (f-cons* #:seq ws (ff* (f-seq ws aatom ws) )))
(define ee     (f-cons* #:or line (ff* (f-seq f-bar line))))
(define pretty (make-fluid #f))
(define (parse-reg str flags)
  (with-fluids ((*whitespace* ws)
                (*flags*      flags))
    (if (equal? str "")
        #t
        (parse str (f-seq ee f-eof)))))

(define e-matcher ee)

  

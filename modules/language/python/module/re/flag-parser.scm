;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module re flag-parser)
  #:use-module (language python module re flags)
  #:use-module (language python try)
  #:use-module (language python exceptions)
  #:use-module (ice-9 pretty-print)
  #:use-module (parser stis-parser scanner)
  #:use-module (parser stis-parser slask)
  #:use-module (parser stis-parser macros)
  #:use-module ((parser stis-parser)
		#:select (setup-parser
			  f-nl f-nl! Ds	*whitespace*
			  *current-file-parsing*
			  make-file-reader file-next-line file-skip))
  #:use-module (parser stis-parser macros)
  #:re-export (f-nl f-nl! Ds *whitespace*)
  #:export (f-seq f-seq! f-or f-or! f-not f-not! f-true f-false f-cons f-cons*
		  f-list INDENT <p-lambda> f* ff* ff? f? ff+ f+ f-let
		  f-reg f-reg! f-tag f-tag! f-eof f-out f-and f-and!
		  mk-token p-freeze parse f-append XL X N M f-pos
		  .. xx <p-cc> <p-define> f-scope f-char gg? f-checkr f-ftr
		  f-pk *flags* f-verbose f-with-flags f-ascii))

;; Preliminary
(define do-print (slask-it #f))
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

(define *flags* (make-fluid 0))


(begin
  (define-parser-tool (<p-lambda> (X XL N M FLAGS)) <p-define> .. 
    xx <p-cc>)


  (make-scanner-tools <p-lambda> <fail> <p-cc> <succeds> .. 
                      (X XL N M FLAGS)
                      (c) (d)
                      s-false s-true s-mk-seq s-mk-and s-mk-or)

  ;; Sets up a standar parser functionals with INDENT field added
  (setup-parser
   <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx
   X XL ((N 0) (M 0) (FLAGS (fluid-ref *flags*)))
   s-false s-true s-mk-seq s-mk-and s-mk-or))


(define f-verbose
  (<p-lambda> (c)
    (if (= (logand FLAGS VERBOSE) VERBOSE)
        (<p-cc> c)
        <fail>)))

(define (f-ascii ch)
  (<p-lambda> (c)
    (if (string? ch)
        (if (= (logand FLAGS ASCII) ASCII)
            (if (< (char->integer (string-ref ch 0)) 128)
                (<p-cc> c)
                (<code> (raise (ValueError
                                (+ "non acii sign in ascii regex : "
                                   ch)))))
            (<p-cc> c))
        (<p-cc> c))))

(define (change-flags on off cur)
  (let ((on  (string->list on))
        (off (string->list off)))
    
    (define-syntax-rule (mask-on ch val)
      (if (member ch on)
          (set! cur (logior cur val))))
    (define-syntax-rule (mask-off ch val)
      (if (member ch off)
          (set! cur (logand cur (lognot val)))))
    
    (mask-on  #\a ASCII)
    (mask-on  #\i IGNORECASE)
    (mask-on  #\L LOCALE)
    (mask-on  #\m MULTILINE)
    (mask-on  #\x VERBOSE)
    (mask-on  #\s DOTALL)
    (mask-off #\a ASCII)
    (mask-off #\i IGNORECASE)
    (mask-off #\m MULTILINE)
    (mask-off #\s DOTALL)
    (mask-off #\x VERBOSE)
    cur))

(define (f-with-flags on off f)
  (<p-lambda> (c)
    (let ((FLAGS (change-flags on off FLAGS)))
      (.. (f c)))))


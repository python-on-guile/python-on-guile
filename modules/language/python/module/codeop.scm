;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module codeop)
  #:use-module (language python eval)
  #:use-module (language python def)
  #:use-module (oop pf-objects)  
  #:export (compile_command Compile CommandCompiler))

(define compile_command
  (lam (source (= filename "<input>") (= symbol "single"))
       (compile source filename symbol)))

(define-python-class Compile ()
  (define __call__
    (lambda (self . l)
      (apply compile_command l))))

(define CommandCompiler (Compile))

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module gzip)
  #:use-module (language python def)
  #:use-module (language python exceptions)
  #:use-module (language python inspect)
  #:use-module (language python bytes)
  #:use-module (language python list)
  #:use-module (oop goops)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 binary-ports)
  #:export (compress decompress))

(def (compress data (= compresslevel 9) (= mtime None))
     (when (not (or (isinstance data bytes) (isinstance data bytearray)))
       (raise (ValueError "data is not a bytevector or bytearray")))
     (let ((n  (len data))
	   (bv (if (isinstance data bytes)
		   (slot-ref data 'bytes)
		   (slot-ref data 'vec)))
	   (c  compresslevel))
       (when (not (and (number? c) (integer? c) (> c 0) (< c 10)))
	 (raise (ValueError "wrong compresslevel, needs 1,..,9")))
       (call-with-values (lambda () (pipeline (list
					       (list
						"gzip"
						"-f"
						(format #f "-~a" c)))))
	 (lambda (out in pids)
	   (dynamic-wind
	       (lambda () (values))

	       (lambda ()
		 (call-with-new-thread
		  (lambda ()
		    (put-bytevector in bv 0 n)
		    (close in)))
		 (let ((bv (get-bytevector-all out)))
		   (let ((b (bytes)))
		     (slot-set! b 'bytes bv)
		     (close out)
		     b)))
	       
	       (lambda ()
		 (close out)
		 (close in)))))))

(define (decompress data)
     (when (not (or (isinstance data bytes) (isinstance data bytearray)))
       (raise (ValueError "data is not a bytevector or bytearray")))
     (let ((n  (len data))
	   (bv (if (isinstance data bytes)
		   (slot-ref data 'bytes)
		   (slot-ref data 'vec))))
       (call-with-values (lambda () (pipeline (list
					       (list
						"gunzip"))))
	 (lambda (out in pids)
	   (dynamic-wind
	       (lambda () (values))

	       (lambda ()
		 (call-with-new-thread
		  (lambda ()
		    (put-bytevector in bv 0 n)
		    (close-port in)))
       
		 (let ((bv (get-bytevector-all out)))
		   (let ((b (bytes)))
		     (slot-set! b 'bytes bv)
		     (close-port out)
		     b)))
	       
	       (lambda ()
		 (close-port out)
		 (close-port in)))))))

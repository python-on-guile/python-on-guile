;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _sha1)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha1))

(define-python-class sha1 (Summer)
  (define name     "sha1")
  (define digest_size 20)
  
  (define _command "/usr/bin/sha1sum"))

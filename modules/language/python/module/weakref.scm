;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module weakref)
  #:use-module (language python dict)
  #:use-module (language python set)
  #:use-module (language python exceptions)
  #:use-module ((oop pf-objects) #:select
                (set
                 (ref . pyref)
                 define-python-class))
  #:export (WeakKeyDictionary WeakValueDictionary WeakSet ref))

(define WeakKeyDictionary   weak-key-dict)
(define WeakValueDictionary weak-value-dict)
(define WeakSet             weak-set)

(define-python-class ref ()
  (define __init__
    (lambda* (self x #:optional (callback 'None))
      (let ((h (make-weak-value-hash-table 1)))
        (set self '_ref h)
        (set self '__callback__ callback)
        (hash-set! h 0 x))))

  (define __call__
    (lambda (self)
      (let ((r (hash-ref (pyref self '_ref) 0 None)))
        r))))
      

  
      

  

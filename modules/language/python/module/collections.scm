;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module collections)
  #:use-module (ice-9 control)
  #:use-module (ice-9 format)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module (language python module collections abc)
  #:use-module (language python module heapq)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python yield)
  #:use-module (language python def)
  #:use-module (language python list)
  #:use-module (language python string)
  #:use-module (language python dict)
  #:use-module (language python def)
  #:use-module (language python bool)
  #:use-module (language python hash)
  #:use-module (language python set)
  #:use-module (language python range)
  #:use-module (language python module)
  #:use-module (language python exceptions)
  #:use-module (language python module keyword)
  #:use-module ((language python module python)
		#:select ((map . pymap) str isinstance reversed classmethod iter
			  any repr property super sorted
			  enumerate (map . py-map)))

  #:use-module ((language python module operator)
		#:select (itemgetter))
  
  #:re-export (Container Hashable Iterable Iterator Reversable Generator
			 Sized Callable Collection Sequence MutableSequence
			 ByteString Set MutableSet Mapping MutableMapping
			 MappingView ItemsView KeysView ValuesView)

  #:export (OrderedDict ChainMap Counter UserDict UserString UserList
			namedtuple defaultdict deque))

#|
* namedtuple   factory function for creating tuple subclasses with named fields
* deque        list-like container with fast appends and pops on either end
* ChainMap     dict-like class for creating a single view of multiple mappings
* Counter      dict subclass for counting hashable objects
* OrderedDict  dict subclass that remembers the order entries were added
* defaultdict  dict subclass that calls a factory function to supply missing values
* UserDict     wrapper around dictionary objects for easier dict subclassing
* UserList     wrapper around list objects for easier list subclassing
* UserString   wrapper around string objects for easier string subclassing
|#

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define (py-add! o k) ((ref o 'add) k))

(define-python-class _OrderedDictKeysView (KeysView)
  (define __reversed__
    (lambda (self)
      ((make-generator
         (lambda (yield)
	   (for ((k v : (reversed (ref self '_mapping)))) ()
		(yield k))))))))

(define-python-class _OrderedDictValuesView (ValuesView)
  (define __reversed__
    (lambda (self)
      ((make-generator
         (lambda (yield)
	   (for ((k v : (reversed (ref self '_mapping)))) ()
		(yield v))))))))

(define-python-class _OrderedDictItemsView (ItemsView)
  (define __reversed__
    (lambda (self)
      ((make-generator
         (lambda (yield)
	   (for ((k v : (reversed (ref self '_mapping)))) ()
		(yield (list k v)))))))))

(define-inlinable (link) (vector 0 0 0))
(define-inlinable (get-prev  l  ) (vector-ref  l 0))
(define-inlinable (get-last  l  ) (vector-ref  l 0))
(define-inlinable (get-first l  ) (vector-ref  l 1))
(define-inlinable (get-next  l  ) (vector-ref  l 1))
(define-inlinable (get-key   l  ) (vector-ref  l 2))
(define-inlinable (set-prev! l v) (vector-set! l 0 v))
(define-inlinable (set-next! l v) (vector-set! l 1 v))
(define-inlinable (set-key!  l v) (vector-set! l 2 v))

(define OrderedDict dict)
	       
(define (u self)
  (let ((s (py-set)))
    (let lp ((l (ref self 'maps)))
      (if (pair? l)
          (begin
            (set! s ((ref s 'union) ((ref (car l) 'keys))))
            (lp (cdr l)))
          s))))

(define-python-class ChainMap (MutableMapping)
  (define __init__
    (lambda (self . l)
      (set self 'maps
	   (if (null? l)
	       (list (dict))
	       l))))

  (define __getitem__
    (lambda (self key)
      (let/ec ret
	 (let lp ((l (ref self 'maps)))
	   (if (pair? l)
	       (let ((m (car l)))
		 (if (in key m)
		     (pylist-ref m key)
		     (lp (cdr l))))
	       (raise (KeyError key)))))))
  

  (define get
    (lambda* (self key #:key (default None))
      (if (in key self)
	  (pylist-ref self key)
	  default)))

  (define __len__
    (lambda (self)
      (len (u self))))
  

  (define __iter__
    (lambda (self)
      (iter (u self))))

  (define __contains__
    (lambda (self key)
      (for ((m : (ref self 'maps))) ()
	   (if (in key m)
	       (break #t))
	   #:final #f)))
  
  (define __bool__
    (lambda (self)
      (any (ref self 'maps))))

  (define __repr__
    (lambda (self)
      (let ((l (map (lambda (x) (repr x)) (ref self 'maps))))
	(format #f "~a(~a,~{,~a~})"
		(ref (ref self '__class__) '__name__)
		(car l)
		(cdr l)))))

  (define fromkeys
    (class-method
     (lambda (cls iter . args)
       (apply cls (for ((x : iter)) ((l '()))
		       (cons (apply py-fromkeys x args) l)
		       
		       #:final
		       (reverse l))))))

  (define copy
    (lambda (self)
      (let ((maps (ref self 'maps)))
	(apply (ref self '__class__) (copy (car maps)) (cdr maps)))))

  (define __copy__
    (lambda (self)
      ((ref self 'copy))))

  (define new_child
    (lambda* (self #:optional (m None))
      (apply (ref self '__class__)
             (if (bool m) m (dict)) (ref self 'maps))))

  (define parents
    (property
     (lambda (self)
       (apply (ref self '__class__) (cdr (ref self 'maps))))))
  
  (define __setitem__
    (lambda (self key value)
      (pylist-set! (car (ref self 'maps)) key value)))
    

  (define __delitem__
    (lambda (self key)
      (try
       (lambda () (pylist-delete! (car (ref self 'maps))))
       (#:except KeyError =>
	 (lambda x
	   (raise KeyError
		  (format #f "Key not found in the first mapping: ~a" key)))))))

  (define popitem
    (lambda (self)
      (try
       (lambda () (popitem (car (ref self 'maps))))	 
       (#:except KeyError =>
        (lambda x
	  (raise KeyError "No keys found in the first mapping"))))))

  (define pop
    (lambda (self key . args)
      (try
       (lambda () (apply pylist-pop! (car (ref self 'maps)) args))
       (#:except KeyError =>
	(lambda x
	 (raise KeyError
		(format #f "Key not found in the first mapping: ~ a" key)))))))

  (define keys
    (lambda (self)
      (py-list (u self))))

  (define values
    (lambda (self)
      (for ((k : (u self))) ((l '()))
           (cons (pylist-ref self k) l)
           #:final
           (py-list (reverse l)))))
  
  (define items
    (lambda (self)
      (for ((k : (u self))) ((l '()))
           (cons (vector k (pylist-ref self k)) l)
           #:final
           (py-list (reverse l)))))      
           
  (define clear
    (lambda (self)
      (py-clear (car (ref self 'maps))))))

(define-python-class Counter (dict)
  (define __init__
    (lam0 (self (* args) (** kwds))
        (if (> (length args) 1)
            (raise TypeError
		   (format
		    #f
		    "expected at most 1 arguments, got ~a"
		    (length  args))))
        ((ref (super Counter self) '__init__))
        (py-apply py-update self (** kwds))
        (when (pair? args)
          (let ((xx (car args)))
            (if (isinstance xx dict)
                (py-update self xx)
                (for ((x : xx)) ()
                     (cond
                      ((eq? x #t)
                       (pylist-set! self 1 (+ (py-get self 1 0) 1)))
                      ((eq? x #f)
                       (pylist-set! self 0 (+ (py-get self 0 0) 1)))
                      (else
                       (pylist-set! self x (+ (py-get self x 0) 1))))
                     (values)))))))


  (define __missing__
    (lambda (self key) 0))

  (define most_common
    (lambda* (self #:key (n None))
       (if (eq? n None)
            (sorted ((ref self 'items) #:key (itemgetter 1) #:reverse #t))
	    (nlargest n (py-items self) #:key (itemgetter 1)))))

  (define elements
    (lambda (self)
      ((make-generator
        (lambda (yield)
          (for ((k v : self)) ()
               (if (and (number? v) (integer? v) (> v 0))
                   (for ((i : (range v))) ()
                        (yield k)))))))))

  (define fromkeys
    (lambda x
      (raise NotImplementedError)))
    
  
  (define update
    (lam0 (self (* args) (** kwds))
	 (if (> (len args) 1)
	     (raise TypeError
		    (format #f "expected at most 1 arguments, got ~a"
			    (len args))))

	 (if (= (len args) 1)
	     (let ((iterable (iter (car args))))
	       (if (not (eq? iterable None))
		   (if (isinstance iterable <py-hashtable>)
		       (for ((elem count : iterable)) ()
			    (pylist-set! self elem
					 (+ count (py-get self elem 0))))
		       (for ((k : iterable)) ()
			    (pylist-set! self k
					 (+ 1 (py-get self k 0))))))))
        
	 (for ((k count : kwds)) ()
	      (pylist-set! self k
			   (+ count (py-get self k 0))))))
  (define subtracts
    (lam0 (self (* args) (** kwds))
	 (if (> (len args) 1)
	     (raise TypeError
		    (format #f "expected at most 1 arguments, got ~a"
			    (len args))))

	 (let ((iterable (py-get args 0 None)))
	   (if (not (eq? iterable None))
	       (if (isinstance iterable <py-hashtable>)
		   (for ((elem count : iterable)) ()
			(pylist-set! self elem
				     (- (py-get self elem 0) count)))
		   (for ((elem : iterable)) ()
			(pylist-set! self elem
				     (- (py-get self elem 0) 1))))))
	        
	(for ((k v : kwds)) ()
	     (pylist-set! self k
			  (- (py-get self k 0) v)))))

  (define __delitem__
    (lambda (self k)
      (if (in k self)
	  ((ref dict '__delitem__) k))))


  (define __add__
    (lambda (self other)
      (if (not (isinstance other Counter))
	  NotImplemented
	  (let ((result (Counter)))	    
	    (for ((elem count : self)) ()
		 (let ((newcount (+ count (pylist-ref other elem))))
		   (if (> newcount 0)
		       (pylist-set! result elem newcount))))
	    
	    (for ((elem count : other)) ()
		 (if (and (not (in elem self))
			  (> count 0))
		     (pylist-set! result elem count)))
	    
	    result))))

  (define __sub__
    (lambda (self other)
      (if (not (isinstance other Counter))
	  NotImplemented
	  (let ((result (Counter)))	    
	    (for ((elem count : self)) ()
		 (let ((newcount (- count (pylist-ref other elem))))
		   (if (> newcount 0)
		       (pylist-set! result elem newcount))))
	    
	    (for ((elem count : other)) ()
		 (if (and (not (in elem self))
			  (> count 0))
		     (pylist-set! result elem (- count))))
	    
	    result))))

  (define __or__
    (lambda (self other)
      (if (not (isinstance other Counter))
	  NotImplemented
	  (let ((result (Counter)))	    
	    (for ((elem count : self)) ()
		 (let ((newcount (max count (pylist-ref other elem))))
		   (if (> newcount 0)
		       (pylist-set! result elem newcount))))
	    
	    (for ((elem count : other)) ()
		 (if (and (not (in elem self))
			  (> count 0))
		     (pylist-set! result elem count)))
	    
	    result))))

  (define __and__
    (lambda (self other)
      (if (not (isinstance other Counter))
	  NotImplemented
	  (let ((result (Counter)))	    
	    (for ((elem count : self)) ()
		 (let ((newcount (min count (pylist-ref other elem))))
		   (if (> newcount 0)
		       (pylist-set! result elem newcount))))	    
	    result))))

  (define __pos__
    (lambda (self)
      (let ((result (Counter)))       
        (for ((elem count : self)) ()
	     (if (> count 0)
                (pylist-set! result elem count)))
        result)))


  (define __neg__
    (lambda (self)
      (let ((result (Counter)))
        (for ((elem count : self)) ()
	     (if (< count 0)
		 (pylist-set! result elem (- count))))
        result)))

  (define _keep_positive
    (lambda (self)
      (define ks
	(for ((k v : self)) ((l '()))
	     (if (<= v 0)
		 (cons k l)
		 l)))
      (let lp ((ks ks))
	(if (pair? ks)
	    (begin
	      (pylist-remove! self (car ks))
	      (lp (cdr ks)))))
      self))

  (define __iadd__
    (lambda (self other)
      (for ((elem count : other)) ()
	   (pylist-set! self elem
			(+ (py-get self elem 0) count)))
        ((ref self '_keep_positive))))

  (define __isub__
    (lambda (self other)
      (for ((elem count : other)) ()
	   (pylist-set! self elem
			(- (py-get self elem 0) count)))
      ((ref self '_keep_positive))))

  (define __ior__
    (lambda (self other)
      (for ((elem count : other)) ()
	   (pylist-set! self elem
			(max (py-get self elem 0) count)))
      ((ref self '_keep_positive))))

  (define __iand__
    (lambda (self other)
      (for ((elem count : other)) ()
	   (pylist-set! self elem
			(min (py-get self elem 0) count)))
      ((ref self '_keep_positive)))))

(define mod (current-module))

(define-syntax namedtuple
  (lambda (x)
    (define (mod-)
      (datum->syntax x (module->py-module (module-name (current-module)))))
    
    (syntax-case x ()
      ((_ . l)
       (with-syntax ((mod (mod-)))
         #'(namedtuple- mod . l)))
      (_
       (with-syntax ((mod (mod-)))
         #'(lambda x
             (apply namedtuple- mod x)))))))

(def0 (namedtuple- mod2 typename field_names
		 (= verbose #f)
		 (= rename  #f)
		 (= module  None))

     (define-syntax-rule (v x)
       (let ((xx x))
	 (if verbose
	     (begin
	       (set! verbose xx)
	       xx)
	     xx)))

     (let ((seen (py-set)))
       (if (string? field_names)
	   (set! field_names
             (map string-trim-both
                  (string-split field_names #\,))))
       
       (set! field_names
         (for ((fs : field_names)) ((r '()))
              (if (isinstance fs str)
                  (append (reverse (string-split (scm-str fs) #\space))
                          r)
                  (cons fs r))
              #:final
              (reverse r)))

       (set! field_names (py-list (map scm-str field_names)))
       (set! typename (scm-str typename))

       (if rename
	   (for ((index name : (enumerate field_names))) ()
		(if (or (not (py-identifier? name))
			(iskeyword name)
			(py-startswith name "_")
			(in name seen))
		    (pylist-set! field_names index (format #f "_~a"index)))
		(py-add! seen name)))

       (for ((name : (+ (pylist (list typename)) field_names))) ()
	    (if (not (string? name))
		(raise TypeError "Type names and field names must be strings"))
	    (if (not (py-identifier? name))
		(raise ValueError
		       (+ "Type names and field names must be valid "
			  (format #f "identifiers: ~a" name))))
	    (if (iskeyword name)
		(raise ValueError
		       (+ "Type names and field names cannot be a "
			  (format #f "keyword: ~a" name)))))

       (set! seen (py-set))
       
       (for ((name : field_names)) ()
	    (if (and (py-startswith name "_") (not rename))
		(raise ValueError
		       (+ "Field names cannot start with an underscore: "
			  name)))
	    (if (in name seen)
		(raise ValueError
		       (+ "Encountered duplicate field name: "
			  name)))
	    (py-add! seen name))

       (set! field_names (map string->symbol (to-list field_names)))

       (letrec ((class
       (make-p-class (string->symbol typename) '(())
         (lambda (dict)
           (pylist-set! dict '__match_args__
                        (map symbol->string field_names))
           (pylist-set! dict '_fields (map symbol->string field_names))
           
           (pylist-set! dict '__init__
                   (object-method
                    (eval (v `(lam
                               (self
                                ,@(map (lambda (key) `(= ,key ,None))
                                       field_names))
                               ((@ (guile) values))
                               ,@(map (lambda (key) `(set self ',key ,key))
                                      field_names)))
                          mod)))           

           (pylist-set! dict '_replace
             (object-method
              (lam (self (** kw))
                (let ((newclass (class)))
                  (let lp ((l (to-list field_names)))
                    (if (pair? l)
                        (let ((field (car l)))
                          (set newclass
                               field
                               (aif it (pylist-ref kw (symbol->string field))
                                    it
                                    (ref self field))))))))))

           
           (pylist-set! dict '__getitem__
             (object-method
              (lambda (self i)               
                (if (number? i)
                    (let ((ii (if (< i 0)
                                  (+ (len field_names) i)
                                  i)))
                      (if (and (>= ii 0) (< ii (len field_names)))
                          (ref self (pylist-ref field_names ii))
                          (raise (IndexError (format #f "wring index ~a" ii)))))
                    (ref self (scm-sym i))))))

           (pylist-set! dict '__iter__
             (object-method
              (make-generator
               (let ((n (len field_names)))
                 (lambda (yield self)
                   (let lp ((i 0))
                        (if (< i n)
                            (begin
                              (yield (ref self (pylist-ref field_names i)))
                              (lp (+ i 1))))))))))

           (let ((n (len field_names)))
             (pylist-set! dict '__len__
                          (object-method
                           (lambda (self) n))))
           
           (pylist-set! dict '__hash__
                        (object-method
                         (lambda (self)
                           (py-hash (list->vector (to-list self))))))
                      
           (pylist-set! dict '__eq__
             (object-method
              (lambda (self x)
                (if (= (len x) (len self))
                    (for ((a : self) (b : x)) ()
                         (if (py-equal? a b)
                             (values)
                             (break #f))
                         #:final #t)
                    #f))))
                         

           (pylist-set! dict '__lt__
             (object-method
              (lambda (self x)
                (< (to-list self) (to-list x)))))

           (pylist-set! dict '__le__
             (object-method
              (lambda (self x)
                (<= (to-list self) (to-list x)))))

           (pylist-set! dict '__gt__
             (object-method
              (lambda (self x)
                (> (to-list self) (to-list x)))))

           (pylist-set! dict '__ge__
             (object-method
              (lambda (self x)
                (>= (to-list self) (to-list x)))))                              
           
           (pylist-set! dict '__setitem__
              (object-method
                 (lambda (self i val)
                   (if (number? i)
                    (let ((ii (if (< i 0)
                                  (+ (len field_names) i)
                                  i)))
                      (if (and (>= ii 0) (< ii (len field_names)))
                          (set self (pylist-ref field_names ii) val)
                          (raise (IndexError
                                  (format #f "wring index ~a" ii)))))
                    (set self (scm-sym i) val)))))
           
           (pylist-set! dict '__repr__
              (object-method
               (lambda (self . l)
                 (let ((l (map (lambda (x)
                                 (format #f "~a=~a"
                                         x
                                         (ref self x)))
                               (to-list field_names))))
                   (format #f "~a(~a~{,~a~})"
                           typename
                           (car l)
                           (cdr l))))))

           (pylist-set! dict '_make
                        (class-method
                         (lambda (cls li)
                           (apply cls (to-list li)))))

           (when (eq? module None)
             (set! module mod2))

           (pylist-set! dict '__module__ module)
           
           (if verbose (pretty-print verbose))))))
         class)))


(define-python-class UserDict ()
  (define __init__
    (lambda (self . x)
      (set self 'data (apply dict x))))
        
  
  (define __repr__
    (lambda (self)
      ((ref dict '__repr__) (ref self 'data)))))

(define-python-class UserString ()
  (define __init__
    (lambda (self . x)      
      (set self 'data (apply py-str x))))
        
  
  (define __repr__
    (lambda (self)
      (repr (ref self 'data)))))

(define-python-class UserList ()
  (define __init__
    (lambda (self . x)      
      (set self 'data (apply py-list x))))
        
  
  (define __repr__
    (lambda (self)
      (repr (ref self 'data)))))


(define-python-class defaultdict (dict)
  (define __init__
    (lambda (self default_factory . l)
      (apply (ref dict '__init__) self l)
      (set self 'default_factory default_factory)))

  (define __missing__
    (lambda (self key)
      (let ((d (ref self 'default_factory)))
	(if (eq? d None)
	    (raise KeyError (format #f "key ~a is missing" key))
	    (pylist-ref d key))))))

(define-python-class deque ()
  (define __bool__
    (lambda (self)
      (> (ref self '_i) 0)))
  
  (define __init__
    (lambda* (self #:optional (iterable '()) (maxlen None))
      (let ((head (link)))
	(set-prev! head    head)
	(set-next! head    head)
	(set self '_head   head)
	(set self 'maxlen maxlen)
	(for ((x : iterable)) ((i 0))
	     (if (eq? i maxlen)
		 (begin
		   (set self '_i i)		   
		   (break))
		 (begin
		   (append self x)
		   (+ i 1)))
	     #:final
	     (set self '_i i)))))

  (define append
    (lambda (self x)
      (let ((m (ref self 'maxlen))
	    (i (ref self '_i)))
	(if (and (not (eq? m None)) (= m (+ i 1)))
	    (raise ValueError "deque reached its limit"))
	(let* ((head (ref self '_head))
               (link (link))
               (last (get-last head)))
	  (set-key!  link x)
	  (set-prev! link last)
	  (set-next! link head)
	  (set-prev! head link)
          (set-next! last link)
	  (set self '_i (+ i 1))))))

  (define appendleft
    (lambda (self x)
      (let ((m (ref self 'maxlen))
	    (i (ref self '_i)))
	(if (= m (+ i 1))
	    (raise ValueError "deque reached its limit"))
	(let* ((head  (ref self '_head))
               (link  (link))
               (first (get-first head)))
	  (set-key!  link  x)
	  (set-next! link  first)
	  (set-prev! link  head)
	  (set-next! head  link)
          (set-prev! first link)
	  (set self '_i (+ i 1))))))

  (define clear
    (lambda (self)
      (let ((head (ref self '_head)))
	(set-prev! head head)
	(set-next! head head)
	(set self '_i 0))))

  (define copy
    (lambda (self)
      (defaultdict self (ref self 'maxlen))))

  (define count
    (lambda (self x)
      (for ((y : self)) ((i 0))
	 (if (equal? x y)
	     (+ i 1)
	     i)
	 #:final i)))

  (define extend
    (lambda (self iterable)
      (let ((f (ref self 'append)))
	(for ((x : iterable)) ()
	   (f x)))))

  (define extendleft
    (lambda (self iterable)
      (let ((f (ref self 'appendleft)))
	(for ((x : iterable)) ()
	   (f x)))))

  (define index
    (lambda* (self x #:optional (start 0) (stop -1))
      (for ((y : self)) ((i 0))
	   (if (< i start)
	       (+ i 1)
	       (if (= i stop)
		   (raise ValueError "index is not found")
		   (if (equal? x y)
		       (break i)
		       (+ i 1))))
	   #:final
	   (raise ValueError "index is not found"))))

  (define insert
    (lambda (self n x)
      (let ((m (ref self 'maxlen))
	    (j (ref self '_i)))
	
	(if (= m (+ j 1))
	    (raise IndexError "deque reached its limit"))

	(if (or (< n 0) (> n j))
	    (raise IndexError "index in insert out of bound"))
	
	(let lp ((p (ref self '_head)) (i 0))
	  (if (<= i j)
	      (if (= i n)
		  (let ((link (link))
			(pp   (get-next p)))
		    (set-key!  link x)
		    (set-next! p    link)
		    (set-prev! pp   link)
		    (set-prev! link p   )
		    (set-next! link pp)
		    (set self '_i (+ j 1)))
		  (lp (get-next p) (+ i 1))))))))

  (define pop
    (lambda (self)
      (let* ((i (ref self '_i))
	     (h (ref self '_head))
	     (n (get-prev h))
	     (p (get-prev n)))
	
	(if (eq? i 0)
	    (raise IndexError "pop of empty dequeue"))

	(set-prev! h p)
	(set-next! p h)
	(set self '_i (- i 1))
	(get-key n))))

  (define popleft
    (lambda (self)
      (let* ((i (ref self '_i))
	     (h (ref self '_head))
	     (n (get-next h))
	     (p (get-next n)))
	
	(if (eq? i 0)
	    (raise IndexError "pop of empty dequeue"))

	(set-next! h p)
	(set-prev! p h)
	(set self '_i (- i 1))
	(get-key n))))
	
 (define remove
    (lambda (self value)
      (let ((j (ref self '_i)))
	
	(if (= j 0)
	    (raise ValueError "can'r remove deque which is empty"))
	
	(let lp ((p (get-next (ref self '_head))) (i 0))
	  (if (< i j)
	      (if (equal? value (get-key p))
		  (let ((prev (get-prev p))
			(next (get-next p)))
		    (set-next! prev next)
		    (set-prev! next prev)
		    (set self '_i (- j 1)))
		  (lp (get-next p) (+ i 1)))
	      (raise ValueError "remove: element is not in deque"))))))
  
  (define reverse
    (lambda (self)
      (let ((h (ref self '_head))
	    (n (ref self '_i)))
	(let lp ((h h) (i 0))
	  (if (<= i n)
	      (let ((n (get-next h))
		    (l (get-prev h))
		    (r (get-next h)))
		(set-next! h l)
		(set-prev! h r)
		(lp n (+ i 1))))))))
  
  (define rotate
    (lambda (self n)
      (define h (ref self '_head))
      
      (define (rotate+)
	(let* ((n  (get-next h))
	       (nn (get-next h))
	       (p  (get-prev h)))
	  (set-next! p n)
	  (set-prev! n p)
	  (set-next! h nn)
	  (set-prev! h n)
	  (set-prev! nn h)
	  (set-next! n  h)))
      
      (define (rotate-)
	(let* ((n  (get-prev h))
	       (nn (get-prev h))
	       (p  (get-next h)))
	  (set-prev! p n)
	  (set-next! n p)
	  (set-prev! h nn)
	  (set-next! h n)
	  (set-next! nn h)
	  (set-prev! n  h)))
      
      (define rotate (if (> n 0) rotate+ rotate-))
      (define d      (if (> n 0) 1       -1     ))

      (let lp ((i 0))
	(if (not (= i n))
	    (begin
	      (rotate)
	      (lp (+ i d)))))))

      (define __inter__
	(lambda (self)
	  (let ((h (ref self '_head)))
	  ((make-generator
	     (lambda (yield)
	       (let lp ((p (get-next h)))
		 (if (not (eq? p h))
		     (begin
		       (yield (get-key p))
		       (lp (get-next p)))))))))))

      (define __reversed__
	(lambda (self)
	  (let ((h (ref self '_head)))
	  ((make-generator
	     (lambda (yield)
	       (let lp ((p (get-prev h)))
		 (if (not (eq? p h))
		     (begin
		       (yield (get-key p))
		       (lp (get-prev p)))))))))))

      (define __contains__
	(lambda (self x)
	  (try
	   (lambda ()
	     (if ((ref self 'index) x)
		 #t
		 #f))
	   (#:except IndexError =>
	     (lambda x #f)))))


      (define __len__
	(lambda (self)
	  (ref self '_i)))

      (define __getitem__
	(lambda (self i)
	  (let ((n (ref self '_i)))
	    (if (or (>= i n) (< i 0))
		(raise IndexError i))
	    (let lp ((p (get-next (ref self '_head))) (j 0))
	      (if (= i j)
		  (get-key p)
		  (lp (get-next p) (+ j 1)))))))

      (define __setitem__
	(lambda (self i v)
	  (let ((n (ref self '_i)))
	    (if (or (>= i n) (< i 0))
		(raise IndexError i))
	    (let lp ((p (get-next (ref self '_head))) (j 0))
	      (if (= i j)
		  (set-key! p v)
		  (lp (get-next p) (+ j 1)))))))

      (define __delitem__
	(lambda (self i)
	  (let ((n (ref self '_i)))
	    (if (or (>= i n) (< i 0))
		(raise IndexError i))
	    (let lp ((p (get-next (ref self '_head))) (j 0))
	      (if (= i j)
		  (let ((prev (get-prev p))
			(next (get-next p)))
		    (set-next! prev next)
		    (set-prev! next prev)
		    (set self '_i (- n 1)))
		  (lp (get-next p) (+ j 1)))))))

      (define __repr__
	(lambda (self)
	  (let ((l (to-list self)))
	    (if (pair? l)
		(format #f "deque([~a~{, ~a~}])" (car l) (cdr l))
		"deque([])"))))
      
      (define __add__
	(lambda (self iter)
	  (let ((o ((ref self 'copy))))
	    (let ((f (ref o 'append)))
	      (for ((x : iter)) ()
		   (f x)))
	    o)))

      (define __iadd__
	(lambda (self iter)
	  (let ((o self))
	    (let ((f (ref o 'append)))
	      (for ((x : iter)) ()
		   (f x)))
	    o)))

      (define __mul__
	(lambda (self n)
	  (let ((o (deque)))
	    (let ((f (ref o 'append)))
	      (let lp ((i 0))
		(if (< i n)
		    (begin		   
		      (for ((x : self)) ()
			   (f x))
		      (lp (+ i 1)))
		    o))))))

      (define __imul__
	(lambda (self n)
	  (if (= n 0)
	      ((ref self 'clear))
	      (let ((o self))
		(let ((f (ref o 'append)))	      
		  (let lp ((i 1))
		    (if (< i n)
			(begin		   
			  (for ((x : self)) ()
			       (f x))
			  (lp (+ i 1)))
			o))))))))

(define --ignore--      '(aif link))
(define --export-macros #t)

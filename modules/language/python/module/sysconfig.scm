;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module sysconfig)
  #:export (get_config_vars get-config_var))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define vars (make-hash-table))

(define (get_config_vars . l)
  (let ((m (make-hash-table)))
    (let lp ((l l))
      (when (pair? l)
        (let ((k (car l)))
          (aif it (hash-ref vars k)
               (hash-set! m k it)
               (hash-set! m k 'None))
          (lp (cdr l)))))
    m))

(define (get_config_var name)
  (aif it (hash-ref vars name)
       it
       'None))
  

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module pkgutil)
  #:use-module (language python string)
  #:use-module (language python yield)
  #:use-module (language python module)
  #:use-module (language python for)
  #:use-module (language python def)
  #:use-module (language python try)  
  #:use-module (oop pf-objects)
  #:use-module (language python exceptions)
  #:use-module (language python module os path)
  #:use-module (language python module os)
  #:use-module (ice-9 match)
  #:export (iter_modules walk_packages))

(define (source->package p) (car  (string-split p #\.)))

(define (get-ext         p) (cadr (string-split p #\.)))

(define (is-package? path)
  (or (isdir path)
      (and
       (is-source-file? path)
       (isdir (source->package path)))))

(define (get-package-dir p)
  (if (isdir p)
      p
      (source->package p)))

(define source-exts '("py" "scm" "pl"))
(define (is-source-file? x)
  (and
   (isfile x)
   (member (get-ext x) source-exts)))

(define (module-name-from-path p)
  (let lp ((l (string-split (source->package p) "/")))
    (match l
      ((("language" "python" "module" . u) . _)
       (string-join u "."))
      ((_ . l)
       (lp l))
      (() None))))

(define (module-prefix-from-path p)
  (let lp ((l (reverse (string-split (source->package p) "/"))))
    (match l
      ((("module" "python" "language" . u) . _)       
       (string-join (reverse l) "/"))
      ((_ . l)
       (lp l))
      (() None))))

(define (module->path m x)
  (string-append (module-prefix-from-path x)
                 (string-join
                  (string-split m #\.)
                  "/")))
  

(define (iter_modules path)
  (set! path (abspath (scm-str path)))
  (make-generator
   (lambda (yield)
     (cond
      ((is-package? path)
       (let lp ((l (listdir (get-package-dir path))))
         (if (pair? l)
             (let ((p (car l)))
               (if (is-source-file? p)
                   (let ((pp (module-name-from-path p)))
                   (if (is-package? pp)
                       (yield (importer p pp) pp #t)
                       (yield (importer p pp) pp #f))))
             (lp (cdr l)))))
      ((is-source-file? path)
       (yield #f (module-name-from-path path) #f)))))))
                   
(define walk_packages
  (lam (dirs (= path ""))
    (make-generator
     (lambda (yield)
       (for ((x : dirs)) ()
            (for ((a b c : (iter_modules x))) ()
              (yield a b c)
              (if c
                  (for ((a b c : (walk_packages (list (module->path x b)))))
                    ()
                    (yield a b c)))))))))


(define-python-class loader ()
  (define __init__
    (lambda (self importer module)
      (set self 'importer importer)
      (set self 'module   module)))
  
  (define get_filename
    (lambda (self modname)
      (let* ((imp (ref self 'importer))
             (x   (ref imp 'x))
             (d   (module->path modname x)))
        (let lp ((ext source-exts))
          (if (pair? ext)
              (let ((f (string-append d "." ext)))
                (if (isfile f)
                    f
                    (lp (cdr ext))))
              (raise ValueError (format
                                 #f "Not a correct path found at ~a"
                                 d)))))))

  (define load_module
    (lambda (self modname)
      (Module (string-split modname ".")))))
        
(define-python-class importer ()
  (define __init__
    (lambda (self x name)
      (set self 'x    x)
      (set self 'name name)))

  (define find_module
    (lambda (self module)
      (loader self module))))


module(xml,parsers)

"""Python interfaces to XML parsers.

This package contains one module:

expat -- Python wrapper for James Clark's Expat parser, with namespace
         support.

"""

import xml.parsers.expat as expat

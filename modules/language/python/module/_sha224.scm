;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _sha224)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha224))

(define-python-class sha224 (Summer)
  (define name     "sha224")
  (define digest_size 28)
  
  (define _command "/usr/bin/sha224sum"))

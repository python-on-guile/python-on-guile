;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _sha3_512)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha3_512))

(define-python-class sha3_512 (Summer)
  (define name     "sha3_512")
  (define digest_size 64)
  
  (define _command "/usr/bin/sha3_512sum"))

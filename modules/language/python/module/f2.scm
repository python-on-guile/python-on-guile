;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module f2)
  #:use-module (language python exceptions)
  #:use-module (language python for)
  #:use-module (language python yield)
  #:use-module (language python try)
  #:re-export (next send sendException Exception)
  #:export (gen))

(define gen
  (make-generator
   (lambda (yield n)
     (try
      (lambda ()
        (let lp ((i 0) (s 0))
          (if (< i n)
              (let ((x (+ s i)))
                (call-with-values (lambda () (yield x))
                  (lambda x (pk 'send x)))
                (lp (+ i 1) x)))))
      
      (#:except Exception =>
                (lambda (tag l)
                  (apply pk tag l)))
      
      #:finally
      (lambda ()
        (pk 'yeeeeeees))))))

(for ((i : (gen 10))) ()
     (pk  i))

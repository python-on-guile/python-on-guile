;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module atexit)
  #:export (register unregister _atexit))

(define __callbacks '())
(define _atexit
  (lambda ()
    (let lp ((l __callbacks))
      (if (pair? l)
          (begin
            (catch #t
              (lambda ()
                (apply (caar l) (cdar l)))
              (lambda x
                (pk "error in atexit cleanup" x)))
            (lp (cdr l)))))))

(define (register f . l)
  (set! __callbacks (cons (cons f l) __callbacks))
  f)

(define (uregister f)
  (set! __callbacks
    (let lp ((l __callbacks))
      (if (pair? l)
          (if (eq? (caar l) f)
              (lp (cdr l))
              (cons (car l) (lp (cdr l))))
          '()))))

  

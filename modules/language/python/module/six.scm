;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module six)
  #:use-module (language python module python)
  #:export (PY2 PY3 string_types))

(define PY2 #f
(define PY3 #t)
(define string_types str)

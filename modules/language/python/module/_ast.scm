;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _ast)
  #:use-module (oop pf-objects)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (language python exceptions)
  #:use-module (language python property)
  #:use-module (language python dir)
  #:use-module (language python for)
  #:use-module (language python list)
  #:export (PyCF_ONLY_AST PyCF_TYPE_COMMENTS PyCF_ALLOW_TOP_LEVEL_AWAIT
                          AST Expression Assign AugAssign Name
                          Add Sub Mult NatMult Div Mod BitAnd BitOr BitXor
                          LShift RShift Pow FloorDiv BinOp Expr Compare
                          Eq GtE LtE NotEq In Is IsNot NotIn Gt Lt
                          Call GeneratorExp Starred keyword comprehension
                          Subscript ExtSlice Slice Index If IfExp Attribute
                          While For With withitem Constant BoolOp
                          And Or Not UAdd USub Invert UnaryOp Try
                          ExceptHandler Dict DictComp List Tuple
                          Set SetComp Assert Yield From FunctionDef
                          Return arguments arg Continue Break Delete
                          Raise Global Module ClassDef Lambda Import
                          alias ImportFrom MatMult AsyncFunctionDef
                          comp compx Pass))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define *n1* (make-fluid 0))
(define *n2* (make-fluid 0))
(define *m1* (make-fluid 0))
(define *m2* (make-fluid 0))

(define (N x)
  (if (string? x)
      x
      (if (pyobject? x)
          (ref x 'id x)
          x)))

(define (M x)
  (if (list? x)
      (py-list x)
      x))

(define PyCF_ONLY_AST              (ash 1 10))
(define PyCF_TYPE_COMMENTS         (ash 1 12))
(define PyCF_ALLOW_TOP_LEVEL_AWAIT (ash 1 13))

(define decorators (make-fluid '()))

(define muted '("col_offset" "end_col_offset" "lineno" "end_lineno" "n" "s"))

(define-python-class AST ()
  (define _fields
    (property
     (lambda (self)
       (for ((x : (dir self))) ((l '()))
         (if (or (equal? (string-ref x 0) #\_)
                 (member x muted))                 
             l
             (cons x l))
         #:final (reverse l)))))
  
  (define __init__
    (case-lambda
      ((self)
       (values))
      
      ((self x)       
       (set self 'lineno         (fluid-ref *n1*))
       (set self 'end_lineno     (fluid-ref *n2*))
       (set self 'col_offset     (fluid-ref *m1*))
       (set self 'end_col_offset (fluid-ref *m2*))))))
  

(define ast (ref AST '__init__))

(define-python-class Compare (AST)
  (define __init__
    (lambda (self left c ops)
      (ast self #t)
      (set self 'left           left)
      (set self 'ops            ops)
      (set self 'comparators    c))))
      

(define-python-class Name (AST)
  (define __init__
    (lambda (self id)
      (ast self #t)
      (set self 'id id))))

(define-python-class Expr (AST)
  (define __init__
    (lambda (self e)
      (ast self #t)
      (set self 'value (M e)))))

(define-python-class BinOp (AST)
  (define __init__
    (lambda (self op x y)
      (ast self #t)
      (set self 'left  x)
      (set self 'right y)
      (set self 'op    op))))



(define-python-class Assign (AST)
  (define __init__
    (lambda (self x l)
      (ast self #t)
      (set self 'type_commment "")
      (set self 'value         x)
      (set self 'target        l))))

(define-python-class AugAssign (AST)
  (define __init__
    (lambda (self x op l)
      (ast self #t)
      (set self 'type_commment "")
      (set self 'op            op)
      (set self 'value         x)
      (set self 'target        l))))

(define-python-class Expression (AST)
  (define __init__
    (lambda (self body)
      (set self 'body (M body)))))
        
(define-python-class Eq (AST))
(define-python-class GtE (AST))
(define-python-class LtE (AST))
(define-python-class NotEq (AST))
(define-python-class In (AST))
(define-python-class Is (AST))
(define-python-class IsNot (AST))
(define-python-class NotIn (AST))
(define-python-class Gt (AST))
(define-python-class Lt (AST))
(define-python-class And (AST))
(define-python-class Or (AST))
(define-python-class Not (AST))
(define-python-class UAdd (AST))
(define-python-class Invert (AST))
(define-python-class USub (AST))

(define-python-class comprehension (AST)
  (define __init__
    (lambda (self keys iter cifs)
      (set self 'target keys)
      (set self 'iter   iter)
      (set self 'ifs    cifs))))

(define-python-class keyword (AST)
  (define __init__
    (lambda (self arg value)
      (set self 'arg   (N arg))
      (set self 'value value))))

(define-python-class Starred (AST)
  (define __init__
    (lambda (self value)
      (ast self #t)
      (set self 'value value))))

(define-python-class GeneratorExp (AST)
  (define __init__
    (lambda (self elt generators)
      (ast self #t)
      (set self 'elt        elt)
      (set self 'generators generators))))

(define-python-class Call (AST)
  (define __init__
    (lambda (self func args keywords)
      (ast self #t)
      (set self 'func     func)
      (set self 'args     args) 
      (set self 'keywords keywords)))) 

(define-python-class Subscript (AST)
  (define __init__
    (lambda (self value slice)
      (ast self #t)
      (set self 'value    value)
      (set self 'slice    slice))))

(define-python-class ExtSlice (AST)
  (define __init__
    (lambda (self dims)
      (set self 'dims (M dims)))))

(define-python-class Slice (AST)
  (define __init__
    (lambda (self lower step upper)
      (set self 'lower lower)
      (set self 'step  step)
      (set self 'upper upper)))) 

(define-python-class Index (AST)
  (define __init__
    (lambda (self value)
      (set self 'value value)))) 

(define-python-class Attribute (AST)
  (define __init__
    (lambda (self value)
      (ast self #t)
      (set self 'value (N value)))) )

(define-python-class If (AST)
  (define __init__
    (lambda (self test body orelse)
      (AST self #t)
      (set self 'test   test)
      (set self 'body   (M body))
      (set self 'orelse (M orelse)))) )

(define-python-class IfExp (AST)
  (define __init__
    (lambda (self test body orelse)
      (ast self #t)
      (set self 'test   test)
      (set self 'body   body)
      (set self 'orelse orelse)))) 

(define-python-class While (AST)
  (define __init__
    (lambda (self test body orelse)
      (ast self #t)
      (set self 'test   test)
      (set self 'body   (M body))
      (set self 'orelse (M orelse)))) )

(define-python-class For (AST)
  (define __init__
    (lambda (self target iter body orelse)
      (ast self #t)
      (set self 'target target)
      (set self 'iter   iter)
      (set self 'body   (M body))
      (set self 'orelse (M orelse)))))

(define-python-class With (AST)
  (define __init__
    (lambda (self items body)
      (ast self #t)
      (set self 'items  items)
      (set self 'body   (M body)))))

(define-python-class withitem (AST)
  (define __init__
    (lambda (self context_expr optional_vars)
      (set self 'context_expr  context_expr)
      (set self 'optional_vars (map N optional_vars)))))

(define-python-class Constant (AST)
  (define __init__
    (lambda (self value)
      (set self 'value  value)))) 

(define-python-class BoolOp (AST)
  (define __init__
    (lambda (self op values)
      (ast self #t)
      (set self 'op      op)
      (set self 'values  values)))) 

(define-python-class UnaryOp (AST)
  (define __init__
    (lambda (self op operand)
      (ast self #t)
      (set self 'op       op)
      (set self 'aoperand operand)))) 

(define-python-class Tuple (AST)
  (define __init__
    (lambda (self elts)
      (ast self #t)
      (set self 'elts elts)))) 

(define-python-class List (AST)
  (define __init__
    (lambda (self elts)
      (ast self #t)
      (set self 'elts elts)))) 

(define-python-class Dict (AST)
  (define __init__
    (lambda (self keys values)
      (ast self #t)
      (set self 'keys   keys)
      (set self 'values values)))) 

(define-python-class DictComp (AST)
  (define __init__
    (lambda (self key value generators)
      (ast self #t)
      (set self 'key        key)
      (set self 'value      value)
      (set self 'generators generators)))) 

(define-python-class Set (AST)
  (define __init__
    (lambda (self elts)
      (ast self #t)
      (set self 'elts   elts)))) 

(define-python-class SetComp (AST)
  (define __init__
    (lambda (self elt generators)
      (ast self #t)
      (set self 'elt        elt)
      (set self 'generators generators)))) 

(define-python-class Try (AST)
  (define __init__
    (lambda (self body handlers orelse finalbody)
      (ast self #t)
      (set self 'handlers  (M handlers))
      (set self 'orelse    (M orelse))
      (set self 'finalbody (M finalbody))
      (set self 'body      (M body)))))

(define-python-class ExceptHandler (AST)
  (define __init__
    (lambda (self type name body)
      (ast self #t)
      (set self 'type  type)
      (set self 'name  (N name))
      (set self 'body  (M body)))))

(define-python-class Assert (AST)
  (define __init__
    (lambda (self test msg)
      (ast self #t)
      (set self 'test  test)
      (set self 'msg   msg)))) 

(define-python-class Yield (AST)
  (define __init__
    (lambda (self value)
      (ast self #t)
      (set self 'value value)))) 

(define-python-class YieldFrom (AST)
  (define __init__
    (lambda (self value)
      (ast self #t)
      (set self 'value value)))) 

(define-python-class Return (AST)
  (define __init__
    (lambda (self value)
      (ast self #t)
      (set self 'value value)))) 

(define-python-class Continue (AST)
  (define __init__
    (lambda (self)
      (ast self #t)))) 

(define-python-class Break (AST)
  (define __init__
    (lambda (self)
      (ast self #t)))) 

(define-python-class FunctionDef (AST)
  (define __init__
    (lambda (self name args body returns decorator_list)
      (ast self #t)
      (set self 'type_comment   "")
      (set self 'name           (N name))
      (set self 'args           args)
      (set self 'body           (M body))
      (set self 'returns        returns)
      (set self 'decorator_list (M decorator_list)))))

(define-python-class AsyncFunctionDef (AST)
  (define __init__
    (lambda (self name args body returns decorator_list)
      (ast self #t)
      (set self 'type_comment   "")
      (set self 'name           (N name))
      (set self 'args           args)
      (set self 'body           (M body))
      (set self 'returns        returns)
      (set self 'decorator_list (M decorator_list)))))

(define-python-class arg (AST)
  (define __init__
    (lambda (self arg)
      (ast self #t)
      (set self 'type_comment "")
      (set self 'arg          (N arg))
      (set self 'annotation   ""))))

(define-python-class arguments (AST)
  (define __init__
    (lambda (self args defaults kwarg kw_defaults kwonlyargs posonlyargs vararg)
      (set self 'args        (map N args))
      (set self 'defaults    defaults)
      (set self 'kwarg       (N kwarg))
      (set self 'kw_defaults kw_defaults)
      (set self 'kwonlyargs  (map N kwonlyargs))
      (set self 'posonlyargs (map N posonlyargs))
      (set self 'vararg      (N vararg)))))

(define-python-class Delete (AST)
  (define __init__
    (lambda (self targets)
      (ast self #t)
      (set self 'targets (map N targets)))))

(define-python-class Raise (AST)
  (define __init__
    (lambda (self exc cause args)
      (ast self #t)
      (set self 'exc exc)
      (set self 'cause cause)
      (set self 'args args))))

(define-python-class Global (AST)
  (define __init__
    (lambda (self names)
      (ast self #t)
      (set self 'names (map N names)))))

(define-python-class Module (AST)
  (define __init__
    (lambda (self body)
      (set self 'type_ignores None)
      (set self 'body         (M body)))))

(define-python-class ClassDef (AST)
  (define __init__
    (lambda (self name bases body keywords decorator_list)
      (ast self #t)
      (set self 'name           (N name))
      (set self 'bases          bases)
      (set self 'keywords       keywords)
      (set self 'body           (M body))
      (set self 'decorator_list (M decorator_list)))))

(define-python-class Lambda (AST)
  (define __init__
    (lambda (self args body)
      (ast self #t)
      (set self 'args args)
      (set self 'body body))))

(define-python-class Import (AST)
  (define __init__
    (lambda (self names)
      (ast self #t)
      (set self 'names names))))

(define-python-class ImportFrom (AST)
  (define __init__
    (lambda (self module names)
      (ast self #t)
      (set self 'names  names)
      (set self 'module module)
      (set self 'level  0))))

(define-python-class alias (AST)
  (define __init__
    (lambda (self name asname)
      (set self 'name    (N name))
      (set self 'asnames (N asname)))))

(define (sieve x y)
  (cond
   ((equal? x #:pass)
    y)
   ((pair? x)
    (append x y))
   ((pair? y)
    (cons x y))))

(define (list-it x)
  (if (pair? x)
      (car x)
      x))

(define (get-pos y)
  (aif it (object-property y 'position)
       it
       #f))

(define-syntax-rule (reg x code)
  (let ((c   (lambda () code))
        (n.m (get-pos x)))
    (if n.m
        (let ((n1 (list-ref n.m 0))
              (m1 (list-ref n.m 1))
              (n2 (list-ref n.m 2))
              (m2 (list-ref n.m 3)))
          (with-fluids ((*n1* n1)
                        (*n2* n2)
                        (*m1* m1)
                        (*m2* m2))
            (c)))
        (c))))

(define (tr-comp op)
    (cond
     ((eq? op '==)
      Eq)
     ((eq? op '>=)
      GtE)
     ((eq? op '<=)
      LtE)
     ((eq? op '<>)
      NotEq)
     ((eq? op '!=)
      NotEq)
     ((eq? op 'in)
      In)
     ((eq? op 'is)
      Is)
     ((eq? op 'isnot)
      IsNot)
     ((eq? op 'notin)
      NotIn)
     ((eq? op '>)
      Gt)
     ((eq? op '<)
      Lt)))

(define (mkop op)
  (cond
   ((equal? op "+=")
    (Add))
   ((equal? op "-=")
    (Sub))
   ((equal? op "*=")
    (Mult))
   ((equal? op "@=")
    (MatMult))
   ((equal? op "/=")
    (Div))
   ((equal? op "%=")
    (Mod))
   ((equal? op "&=")
    (BitAnd))
   ((equal? op "|=")
    (BitOr))
   ((equal? op "^=")
    (BitXor))
   ((equal? op "<<=")
    (LShift))
   ((equal? op ">>=")
    (RShift))
   ((equal? op "**=")
    (Pow))
   ((equal? op "//=")
    (FloorDiv))))

(define-syntax-rule (make-match x m (a (b c ...) ...) ...)
  (begin
    (hash-set! m a (lambda (z) (match z (b c ...) ...)))
    ...))

(define (comp-parents x)
  (let lp ((x x) (b '()) (k '()))
    (match x
      (#f (cons '() '()))
      
      (() (cons (reverse b) (reverse k)))
      
      (((#:arg x) . l)
       (lp l (cons (comp x) b) k))
      
      (((#:= k v) . l)
       (lp l b (cons (keyword (comp k) (comp v)) k))))))

(define (compif x)
  (reg x
    (match x
      ((#:cif test #f)
       (cons (comp test) '()))
      ((#:cif test (and x (#:cif . _)))
       (cons (comp test) (compif x))))))

(define (compfor x)
  (reg x
   (match x
     ((#:cfor keys iter #f)
      (cons (comprehension (comp keys) (comp iter) None) '()))
     
     ((#:cfor keys iter (and x (#:cfor . _)))
      (cons (comprehension (comp keys) (comp iter) None)
            (compfor x)))
     
     ((#:cfor keys iter (and x (#:cif . _)))
      (cons (comprehension (comp keys) (comp iter) (compif x))
            '())))))

(define (comparglist z)
  (match z
    ((#:arglist l)
     (let lp ((l l) (args '()) (keyw '()))
       (match l
         (((#:** test) . l)
          (lp l args (cons (keyword None (comp test)) keyw)))
         
         (((#:*  test) . l)
          (lp l (cons (Starred (comp test)) args) keyw))

         (((#:= test1 test2) . l)
          (lp l args (cons (keyword (comp test1) (comp test2)) keyw)))
         
         (((#:comp test #f) . l)
          (lp l (cons (comp test) args) keyw))

         (((#:comp test for) . l)
          (lp l (cons (GeneratorExp (comp test) (compfor for))
                      args) keyw))
         
         ((test . l)
          (lp l (cons (comp test) args) keyw))

         (()
          (cons (reverse args) (reverse keyw))))))))

(define (trycomp x)
  (reg x
    (match x
      (((test . #f) code)
       (ExceptHandler None (comp test) (comp code)))
      ((#f code)
       (ExceptHandler None None (comp code)))
      (((test . as) code)
       (ExceptHandler (comp as) (comp test) (comp code))))))

(define (compscript l)
  (let lp ((l l) (r '()))
    (match l
      (((t #f #f) . l)
       (lp l (cons (reg t (Index (comp t))) r)))
      (((a b c) . l)
       (lp l (cons (Slice
                    (if a (comp a) None)
                    (if b (comp b) None)
                    (if c (comp c) None))
                   r)))
      (()
       (ExtSlice (reverse r))))))

(define (comparg x trailer)
  (match (reverse trailer)
    ((#f . l)
     (Call (comparg x l) '() '()))

    (((and z (#:arglist . _)) . l)
     (let* ((a.k (comparglist z))
            (a   (car a.k))
            (k   (cdr a.k)))
       (Call (comparg x l) a k)))

    (((and z (#:subscripts . u)) . l)
     (Subscript (comparg x l) (compscript l)))

    ((a . l)
     (Attribute (comparg x l) (comp a)))

    (()
     (comp x))))

(define (compdef x)
  (let lp ((x x) (a '()) (d '()) (kw '()) (kwv '())
           (kwo #f) (poso #f) (var None))
    (match x
      (((#:arg x) . l)
       (lp l (cons (arg (comp x)) a) d kw kwv kwo poso var))
      
      (((#:= k v) . l)
       (cond
        (kwo
         (lp l a d kw (cons (comp v) kwv) (cons (arg (comp k)) kwo) poso var))
        (poso
         (lp l a (cons (comp v) d) kw kwv (cons (arg (comp k)) poso) kwo var))
        (else
         (lp l (cons (arg (comp k)) a) (cons (comp v) d) kw kwv poso kwo var)))
       
       (lp l (cons x a) (cons v d) kw kwv kwo poso var))

      (((#:* x) . l)
       (lp l a d kw kwv kwo poso (comp x)))

      (((#:** x) . l)
       (lp l a d (comp x) kwv kwo poso var))

      (((#:1/) . l)
       (lp l a d kw kwv kwo '() var))

      (((#:1*) . l)
       (lp l a d kw kwv '() poso var))
      (()
       (arguments a d kw kwv kwo poso var)))))

(define (compalias x)
  (match x
    ((x . #f) (alias (N (comp x)) None))
    ((x . y ) (alias (N (comp x)) (N (comp y))))))

(define comp-matches (make-hash-table))

(define (comp x)
  (reg x
    (match x
      ((and c (a . _))
       (aif f (hash-ref comp-matches a #f)
            (f c)            
            (begin
              (warn "missing tag " a)
              #f)))
      (#:True
       (Constant #t))

      (#:False
       (Constant #f))

      (#:None
       (Constant None))

      (#:Ellipsis
       (Constant Ellipsis))

      (#:pass
       (Pass))
    
      (#:break
       (Break))

      (#:Continue
       (Continue))

      (x (Constant x)))))

(define mk-string (lambda x ""))

(define (compx x)
  (reg x
    (match x
      ((c . l)
       (sieve (comp c) (compx l)))
      (() '()))))

(make-match x comp-matches     
 (#:identifier
  ((_ id . _)
   (Name id)))
   
 (#:expr
  ((#:expr x)
   (Expr (comp x))))

 (#:%
  ((_ x y)
   (BinOp (mkop "%=") (comp x) (comp y))))
   
 (#:/
  ((_ x y)
   (BinOp (mkop "/=") (comp x) (comp y))))
   
 (#:@
  ((_ x y)
   (BinOp (mkop "@=") (comp x) (comp y))))
   
 (#://
  ((_ x y)
   (BinOp (mkop "//=") (comp x) (comp y))))
   
 (#:+
  ((_ x y)
   (BinOp (mkop "+=") (comp x) (comp y))))
   
 (#:-
  ((_ x y)
   (BinOp (mkop "-=") (comp x) (comp y))))
   
 (#:<<
  ((_ x y)
   (BinOp (mkop "<<=") (comp x) (comp y))))
   
 (#:>>
  ((_ x y)
   (BinOp (mkop ">>=") (comp x) (comp y))))
   
 (#:band
  ((_ x y)
   (BinOp (mkop "&=") (comp x) (comp y))))
   
 (#:bxor
  ((_ x y)
   (BinOp (mkop "^=") (comp x) (comp y))))
   
 (#:bor
  ((_ x y)
   (BinOp (mkop "|=") (comp x) (comp y))))
   
 (#:band
  ((_ x y)
   (BinOp (mkop "&=") (comp x) (comp y))))

 (#:and
  ((_ . l)
   (BoolOp (And) (map comp l))))

 (#:or
  ((_ . l)
   (BoolOp (Or) (map comp l))))

 (#:not
  ((_  l)
   (BoolOp (Not) (list (comp l)))))
 
 (#:u~
  ((_ x) (UnaryOp (Invert) (comp x))))
 
 (#:u-
  ((_ x) (UnaryOp (USub) (comp x))))
 
 (#:u+
  ((_ x) (UnaryOp (Invert) (comp x))))
  
 (#:global
  ((_ global l)
   (Global (map comp l))))

 (#:import
  ((_ (#:from (() () . nm) . #f))
   (ImportFrom (list (alias nm None))))

  ((_ (#:from (("." . nn) . nm) . #f))
   (ImportFrom (+ (* "." (+ 1 (length nn))) nm) (alias "*"  None)))

  ((_ (#:from ("." . nn) . #f))
   (ImportFrom (* "." (+ 1 (length nn))) (alias "*" None)))

  ((_ (#:from (() () . nm)  l))
   (ImportFrom nm (map compalias l)))

  ((_ (#:from (("." . nn) . nm)  l))
   (ImportFrom  (+ (* "." (+ 1 (length nn))) nm)
                (map compalias l)))

  ((_ (#:from ("." . nn) l))
   (ImportFrom (* "." (+ 1 (length nn)))
               (map compalias l)))
  
  ((_ (#:name ((dots ids ...) . as) ...) ...)
   (Import (map (lambda (dots ids as)
                  (alias
                   (+ (* "." (length dots)) (string-join ids "."))
                   (if as as None)))
                dots ids as))))
 
 (#:power
  ((_ out x1 tr . x2)
   (if x2
       (BinOp (mkop "**=") (comparg x1 tr) (comp x2))
       (comparg x1 tr))))

 (#:tuple
  ((_ x (and e (#:cfor . _)))
   (Tuple (GeneratorExp (comp x) (compfor e))))
    
  ((_ . l)
   (Tuple (map comp l))))
 
 (#:dict
  ((_  (#:e k . v) ...)
   (Dict (map comp k) (map comp v)))

  ((_ (#:e k . v) (and e (#:cfor . _)))
   (DictComp (comp k) (comp v) (compfor e)))
    
  ((_  k (and e (#:cfor . _)))
   (SetComp (comp k) (compfor e)))
    
  ((_ k ...)
   (Set (map comp k))))
   
 (#:list
  ((_ x (and e (#:cfor . _)))
   (List (GeneratorExp (comp x) (compfor e))))
    
  ((_ . l)
   (List (map comp l))))
 
 (#:bytes
  ((_ l)
   (let* ((b (make-bytevector (length l))))
     (let lp ((l l) (i 0))
       (if (pair? l)
           (begin
             (bytevector-u8-set! b i (car l))
             (lp (cdr l) (+ i 1)))
           (Constant b))))))

 (#:string
  ((_ l)
   (Constant (mk-string '() l))))

 (#:test
  ((_ e1 #f)
   (comp e1))

  ((_ e1 (e2 #f))
   (IfExp (comp e1) (comp e2) None))

  ((_ e1 (e2 e3))
   (IfExp (comp e1) (comp e2) (comp e3))))

 (#:yield
  ((_ (#:from x))
   (YieldFrom (comp x)))
    
  ((_ args)
   (Yield  (if args (comp args) None))))

 (#:del
  ((_ x) (Delete (comp x))))

 (#:lambdef
  ((_ (_ . args) code)
   (Lambda (compdef args) (comp code))))

 (#:stmt
  ((_ x)
   (comp x)))

 (#:sub
  ((_ x)
   (comp (list #:tuple x))))

 (#:subexpr
  ((_ x)
   (comp x)))

 (#:starexpr
  ((_ x)
   (Starred (comp x))))

 (#:comma
  ((_ . l)
   (compx l)))
   
 (#:return
  ((_ x)
   (Return (comp x))))

 (#:decorated
  ((_ (l ...))
   (fluid-set! decorators (map comp l))))
   
 (#:def
  ((_ f (#:types-args-list . args) _ code)
   (let ((ret (FunctionDef (comp f) (compdef args) (compx code) None
                           (fluid-ref decorators))))
     (fluid-set! decorators '())
     ret)))

 (#:classdef
  ((_ name parents code)
   (let* ((b.k (comp-parents parents))
          (b   (car b.k))
          (k   (cdr b.k))
          (r   (ClassDef (comp name) b k (compx code)
                         (fluid-ref decorators))))
     (fluid-set! decorators '())
     r)))
   
 (#:comp
  ((_ left . l)
   (let* ((c.op (let lp ((l l) (cc '()) (oop '()))
                  (match l
                    ((op c . l)
                     (lp l (cons (comp c) cc) (cons ((tr-comp op)) oop)))
                    (()
                     (cons (reverse cc) (reverse oop)))))))
     (Compare (comp left) (car c.op) (cdr c.op)))))


 (#:identifier
  ((_ id . _)
   (Name id)))

 (#:suite
  ((_ . l)
   (compx l)))

 (#:if
  ((_ test a l  . else)
   (if (pair? l)
       (If (comp test)
           (compx a)
           (compx (cons* #:if (caar l) (cdar l) (cdr l) else)))
       (if else
           (If (comp test) (compx a) (compx else))
           (If (comp test) (compx a) None)))))
 (#:while
  ((_ test code . else)
   (While (comp test) (compx code) (if else (compx else) None))))
   
 (#:for
  ((_ es in code . final)
   (For (comp es)  (comp in) (compx code) (if final (compx final) None))))

 (#:try
  ((_ x exc else . fin)
   (Try (list-it (compx x))
        (map trycomp exc)
        (if else (compx else) None)
        (if fin  (compx fin ) None))))

 (#:raise
  ((_ #f)
   (Raise None None))
  ((_ code  #f . l)
   (Raise (comp code) None (map comp l)))
  ((_ code  from . l)
   (Raise (comp code) (comp from) (map comp l))))
   
 (#:assert
  ((_ x f . _)
   (Assert (comp x) (comp f))))
   
 (#:with
  ((_ (l ...) code)
   (let ()
     (define (withcomp x)
       (match x
         ((a b)
          (withitem (comp a) (comp b)))
         ((a)
          (withitem (comp a) None))))
     (With (map withcomp l) (compx code)))))
   
 (#:expr-stmt
  ((_ l (#:assign))
   (map (lambda (x) (Expr (comp x))) l))
    
  ((_ x (#:assign . l))
   (Assign (comp x) (map comp l)))
  
  ((_ x (#:augassign op l))
   (AugAssign (comp x) (mkop op) (map  comp l)))))



(define-syntax-rule (mkaug l ...)
  (begin
    (define-python-class l (AST))
    ...))

(mkaug Pass Add Sub Mult NatMult Div Mod BitAnd BitOr BitXor LShift RShift Pow
       FloorDiv MatMult)

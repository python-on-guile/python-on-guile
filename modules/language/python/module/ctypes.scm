;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module ctypes)
  #:use-module (system foreign)
  #:use-module ((oop pf-objects)
                #:select
                (pyobject? pyclass? pytype? get-class object type
                           define-python-class ref set pyref rawref
                           rawset get-goops copy-class))
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (language python exceptions)
  #:use-module (language python bytes)
  #:use-module (language python persist)
  #:use-module (language python module copy)
  #:use-module (language python string)
  #:use-module ((language python module python) #:select
                (isinstance issubclass raise str (int . pyint) bool bytes))
  #:use-module (language python list)
  #:use-module (language python property)
  #:use-module (language python def)
  #:use-module (language python set)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:export(CDLL cdll
                c_bool c_char c_byte c_ubyte c_short c_ushort c_int c_uint
                c_long c_ulong c_longlong c_ulonglong c_size_t c_ssize_t
                c_float c_double c_longdouble c_char_p c_void_p
                create_string_buffer c_buffer sizeof byref pointer
                c_pointer Structure Union POINTER cast CFUNCTYPE))

(define-syntax-rule (aif it a b c) (let ((it a)) (if it b c)))

(define (get-u8  m x) (bytevector-u8-ref m x))
(define (get-s8  m x) (bytevector-s8-ref m x))
(define (get-u16 m x) (bytevector-u16-ref m x (native-endianness)))
(define (get-s16 m x) (bytevector-s16-ref m x (native-endianness)))
(define (get-u32 m x) (bytevector-u32-ref m x (native-endianness)))
(define (get-s32 m x) (bytevector-s32-ref m x (native-endianness)))
(define (get-u64 m x) (bytevector-u64-ref m x (native-endianness)))
(define (get-s64 m x) (bytevector-s64-ref m x (native-endianness)))
(define (get-f32 m x) (bytevector-ieee-single-ref m x (native-endianness)))
(define (get-d64 m x) (bytevector-ieee-double-ref m x (native-endianness)))

(define (set-u8  m x v) (bytevector-u8-set! m x v))
(define (set-s8  m x v) (bytevector-s8-set! m x v))
(define (set-u16 m x v) (bytevector-u16-set! m x v (native-endianness)))
(define (set-s16 m x v) (bytevector-s16-set! m x v (native-endianness)))
(define (set-u32 m x v) (bytevector-u32-set! m x v (native-endianness)))
(define (set-s32 m x v) (bytevector-s32-set! m x v (native-endianness)))
(define (set-u64 m x v) (bytevector-u64-set! m x v (native-endianness)))
(define (set-s64 m x v) (bytevector-s64-set! m x v (native-endianness)))
(define (set-f32 m x v) (bytevector-ieee-single-set! m x v (native-endianness)))
(define (set-d64 m x v) (bytevector-ieee-double-set! m x v (native-endianness)))

(define (mk-u8 x)
  (let ((r (make-bytevector 1)))
    (set-u8 r 0 x)
    r))
(define (mk-s8 x)
  (let ((r (make-bytevector 1)))
    (set-s8 r 0 x)
    r))
(define (mk-u16 x)
  (let ((r (make-bytevector 2)))
    (set-u16 r 0 x)
    r))
(define (mk-s16 x)
  (let ((r (make-bytevector 2)))
    (set-s16 r 0 x)
    r))
(define (mk-u32 x)
  (let ((r (make-bytevector 4)))
    (set-u32 r 0 x)
    r))
(define (mk-s32 x)
  (let ((r (make-bytevector 4)))
    (set-s32 r 0 x)
    r))
(define (mk-u64 x)
  (let ((r (make-bytevector 8)))
    (set-u64 r 0 x)
    r))
(define (mk-s64 x)
  (let ((r (make-bytevector 8)))
    (set-s64 r 0 x)
    r))

(define (mk-d64 x)
  (let ((r (make-bytevector 8)))
    (set-d64 r 0 x)
    r))
(define (mk-f32 x)
  (let ((r (make-bytevector 8)))
    (set-f32 r 0 x)
    r))

(define (type- x)
  (cond   
   ((eq? x None)
    '*)
   ((number? x)
    (cond
     ((integer? x)
      (cond
       ((and (>= x -2147483648)
             (<= x  2147483647))
        int32)
       (else
        int64)))
     ((real? x)
      double)))
   ((isinstance x str)
    '*)     
   ((isinstance x bytes)
    '*)
   ((and (struct? x) (or (isinstance x ctype) (issubclass x ctype)))
    (ref x '_type))
   (else
    (aif it (ref x '_as_parameter_) 
         (type- it)
         (raise ValueError "could not deduce type for" x)))))
       
(define (as_parameter x)
  (cond
   ((eq? x None) 0)
   ((and (number? x) (real? x) (or (integer? x) (inexact? x)))
     x)
   ((and (number? x) (real? x) (exact? x))
    (exact->inexact x))
   ((isinstance x string) (string->pointer (scm-str x)))
   ((isinstance x bytes)  (bytevector->pointer (bv-scm x)))
   ((isinstance x ctype)
    ((ref x '_as_parameter)))
   ((struct? x)
    (aif it (ref x '_as_parameter_)
         (as_parameter it)
         (raise ValueError
                "wrong default value type in foreign fkn application")))))

(define-python-class CDLL ()
  (define __init__
    (case-lambda
      ((self)
       (rawset self 'name  'default-lib)
       (rawset self 'lib   (dynamic-link)))
      
      ((self val)
       (rawset self 'lib  (dynamic-link val))
       (rawset self 'name val))))

  (define __repr__
    (lambda (self)
      (format #f "<CDLL '~a'>" (rawref self 'name))))
  
  (define LoadLibrary
    (case-lambda
      ((str)
       (CDLL str))
      ((self str)
       (CDLL str))))

  (define __getattr__
    (lambda (self k)
      (catch #t
        (lambda ()
          (letrec ((this
                    (let ((f (dynamic-func k (rawref self 'lib))))
                      (lambda x
                        (define get as_parameter)
                        
                        (define (a12 p x)
                          (p (get x))
                          (ref p '_type int32))

                        (define (totype atypes)
                          (let lp ((l atypes))
                            (if (pair? l)
                                (cond
                                 ((isinstance (car l) Structure)
                                  (append (totype (ref (car l) '_type))
                                          (lp (cdr l))))
                                 (else
                                  (cons (type- (car l))
                                        (lp (cdr l)))))
                                '())))
                        
                        (let* ((docache   #f)
                               (wrap      (lambda (x) x))
                               (restype
                                (ref
                                 (aif it (ref this 'restype #f)
                                      (begin
                                        (set! wrap (lambda (f)
                                                     (lambda x
                                                       (ref
                                                        (it (apply f x))
                                                        'value))))
                                        it)
                                      c_int)
                                 '_type)))
                          (aif it (ref this 'cache)
                               (apply it (map get x))
                               (let ((f (aif it (ref this 'argtypes #f)
                                             (begin
                                               (set! docache #t)
                                               (pointer->procedure
                                                restype f
                                                (totype (to-list it))))
                                             (pointer->procedure
                                              restype f
                                              (map type- x)))))
                                 (if docache
                                     (set this 'cache (wrap f)))
                                 (apply (wrap f) (map get x)))))))))
            this))
        
        (lambda x
          (raise (AttributeError "no attribute ~a in object ~a"
                                 k (ref self 'name))))))))

(define cdll (CDLL))
  
(define (r tp val)
  (cond
   ((eq? tp 'c_void_p)
    "")
   (else
    val)))

(define-python-class FieldType ()
  (define __init__
    (lambda (self i off bits sz tp)
      (set self 'pos    i)
      (set self 'offset off)
      (set self 'bits   bits)
      (set self 'size   sz)
      (set self 'type   tp)))

  (define __repr__
    (lambda (self)
      ((@ (guile) format) #f "<Field type=~a offs=~a:~a bits=~a size=~a>"
              (ref  (ref self 'type) '__name__)
              (ref self 'pos)
              (ref self 'offset)
              (ref self 'bits)
              (ref self 'size)))))

(define-python-class CMeta (type)
  (define __getattr__
    (lambda (cls k)
      (aif it (rawref cls '_fields_ #f)
           (let ((sz (rawref cls '_pack_)))
             (let lp ((l (to-list it)) (i 0) (off 0) (tt #f))
               (match l
                 (((s t) . l)
                  (let ((d ((ref t '_size) t)))
                    (if (equal? s k)
                        (FieldType i 0 0 d t)
                        (lp l (+ i (max d sz)) 0 #f))))
                 (((s t b) . l)
                  (let ((d ((ref t '_size) t)))
                    (if (= off 0)
                        (if (equal? s k)
                            (FieldType i off b d t)
                            (let ((off (+ b off)))
                              (if (< off (* d 8))
                                  (lp l (+ i (max d sz)) 0 #f)
                                  (lp l i off t))))
                        (if (eq? t tt)
                            (if (< (+ b off) (* sz 8))
                                (if (equal? s k)
                                    (FieldType i off b d t)
                                    (lp l i (+ b off) t))
                                (lp (cons (list s b t) l)
                                    (+ i (max d sz)) 0 #f))
                            (lp (cons (list s b t) l) (+ i (max d sz)) 0 #f)))))
                 (()
                  (raise (AttributeError "no attribute ~a" k))))))
           (raise (AttributeError "no attribute ~a" k)))))
    
  (define __mul__
    (lambda (cls n)
      (when (not (isinstance n pyint))
        (raise TypeError "ctype can only be multiplied by integer"))
      
      (let ((nm (format #f "~a_Array_~a" (ref cls '__name__) n)))
        (aif it (hash-ref *ctypes* (list nm cls n) #f)
             it
             (let ((cls2 (CMeta nm (list c_array) (make-hash-table))))
               (rawset cls2 '__name__ nm)
               (rawset cls2 '_parent cls)
               (rawset cls2 '_len n)
               (hash-set! *ctypes* (list nm cls n) cls2) 
               cls2)))))
  
    (define __rmul__ __mul__))

(define *ctypes* (make-weak-value-hash-table))

(define (POINTER cls)
  (define nm (format #f "LP_~a" (ref cls '__name__)))
  (aif it (hash-ref *ctypes* (cons nm cls) #f)
       it
       (let ((cls2 (CMeta nm (list c_pointer) (make-hash-table))))
         (set cls2 '__name__ nm)
         (set cls2 '_parent cls)
         (hash-set! *ctypes* (cons nm cls)  cls2)
         cls2)))

(define (CFUNCTYPE res . args)
  (define nm (format #f "FKN~{.~a~}" (map (lambda (cls) (ref cls '__name__))
                                      (cons res args))))
  (aif it (hash-ref *ctypes* (cons* nm res args) #f)
       it
       (let ((cls2 (CMeta nm (list c_fkn) (make-hash-table))))
         (set cls2 '__name__ nm)
         (set cls2 '_rettype res)
         (set cls2 '_argtypes args)
         (hash-set! *ctypes* (cons* nm res args) cls2)
         cls2)))



(define-syntax-rule (define-ctype nm x)
  (define nm
    (let ((cls x))
      (set cls '__name__ (symbol->string nm))
      cls)))

(define protect (make-weak-value-hash-table))

(define-python-class ctype (#:metaclass CMeta)
  (define _sz 0)
  (define _size
    (lambda (self) (ref self '_sz)))
  
  (define _len 0)

  (define __add__
    (lambda (self x) (+ (ref self 'value) x)))

  (define __mul__
    (lambda (self x) (* (ref self 'value) x)))

  (define __radd__
    (lambda (self x) (+ (ref self 'value) x)))

  (define __rmul__
    (lambda (self x) (* (ref self 'value) x)))

  (define __sub__
    (lambda (self x) (- (ref self 'value) x)))

  (define __rsub__
    (lambda (self x) (- x (ref self 'value))))

  (define _serialize-into
    (lambda (self b i)
      ((ref self '_tobv) b i (ref self 'value))
      (+ i ((ref self '_size)))))
  
  (define _serialize-from
    (lambda (self b i)
      (set self '_b   b)
      (set self '_pos i)
      (+ i ((ref self '_size)))))
  
  (define _as_ptr
    (lambda (self)
      (let ((b   (ref self '_b))
            (pos (ref self '_pos)))
         (+
          (pointer-address
           (bytevector->pointer b))
          pos))))
        
  (define _get
    (lambda (self)
      ((ref self '_frombv) (ref self '_b) (ref self '_pos))))

  (define _as_parameter
    (lambda (self)
      ((ref self '_from_bv) (ref self '_b) (ref self '_pos))))

  (define _copy
    (lambda (self b2 n2)
      (let ((b1 (ref self '_b))
            (n1 (ref self '_pos))
            (N  (ref self '_len)))
        (let lp ((i 0) (j1 n1) (j2 n2))
          (if (< i N)
              (begin
                (set-u8 b1 j1 (get-u8 b2 j2))
                (lp (+ i 1) (+ j1 1) (+ j2 1))))))))

  (define value
    (property
     (lambda (self)
       ((ref self '_get)))
     
     (lambda (self val)
       (cond
        ((isinstance val ctype)
         (let ((b2 (ref val  '_b))
               (n2 (ref val  '_pos)))
           (_copy self b2 n2)))
        
        ((isinstance val bytes)
         (_copy self (slot-ref val '_bytes) (len val)))

        ((number? val)
         ((ref self '_tobv)
          (ref self '_b)
          (ref self '_pos)
          ((ref self '_check) val)))))))

    (define __init__
      (case-lambda
        ((self)
         (__init__ self (ref self '_default)))
        
        ((self val)
         (define l ((ref self '_size)))
         (set self '_len l)
         
         (cond
          ((pointer? val)
           (set self '_pos 0)
           (set self '_b (pointer->bytevector val l)))
          ((isinstance val (get-class self))
           (__init__ self)
           (set self 'value val))
          (else
           (set self '_pos 0)
           (set self '_b (make-bytevector l))
           (set self 'value val))))

        ((self b i)
         (__init__ self)
         (_copy self b i))))

  (define __repr__
    (lambda (self)
      (if (ref self '_simple)
      (format #f "~a"
              (r (ref self '__name__)
                 (ref self 'value)))
      (format #f "~a(~a)"
              (ref self '__name__)
              (r (ref self '__name__)
                 (ref self 'value)))))))

(define-python-class c_bool (ctype)
  (define _sz   1)
  (define _type uint8)

  (define _frombv
    (lambda (self bv i)
      (get-u8 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u8 bv i val)))

  (define _default #f)
  
  (define _check
    (lambda (self val)
      (pyint (bool val)))))
  

(define-python-class c_char (ctype)
  (define _sz 1)
  (define _type uint8)
  
  (define _get
    (lambda (self)
      (bytes ((@ (guile) list) ((ref self '_as_parameter))))))
  
  (define _frombv
    (lambda (self bv i)
      (get-u8 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u8 bv i val)))

  (define _default 0)
  
  (define _check
    (lambda (self val)
      (if (eq? val 0)
          0
          (let ((val (bytes val)))
            (when (not (= (len val) 1))
              (raise TypeError "not a char" val))
            (pylist-ref val 0))))))

(define-python-class c_byte (ctype)
  (define _sz   1)
  (define _type int8)

  (define _frombv
    (lambda (self bv i)
      (get-s8 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-s8 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not
             (and
              (number? val)
              (integer? val)
              (<= val 127)
              (>= val -128)))
        (raise TypeError "not signed byte" val))
      val)))
    
(define-python-class c_ubyte (ctype)
  (define _sz   1)
  (define _type uint8)

  (define _frombv
    (lambda (self bv i)
      (get-u8 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u8 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not (and
                  (number? val)
                  (integer? val)
                  (>= val -128)
                  (<= val 255)))
        (raise TypeError "not a unsigned bytes" val))
      (logand #xff val))))

(define-python-class c_short (ctype)
  (define _sz   2)
  (define _type uint16)

  (define _frombv
    (lambda (self bv i)
      (get-s16 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-s16 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not
             (and
              (number? val)
              (integer? val)
              (<= -32768 val)
              (<= val 32767)))
        (raise TypeError "not a short" val))
      val)))

(define-python-class c_ushort (ctype)
  (define _sz   2)
  (define _type int16)

  (define _frombv
    (lambda (self bv i)
      (get-u16 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u16 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not (and
                  (number? val)
                  (integer? val)
                  (>= val -32768)
                  (<= val 65535)))
        (raise TypeError "not a unsigned short" val))
      (logand val #xffff))))

(define-python-class c_int (ctype)
  (define _sz   4)
  (define _type int32)

  (define _frombv
    (lambda (self bv i)
      (get-s32 bv i)))
  
  (define _tobv
    (lambda (self bv i val)
      (set-s32 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not
             (and
              (number? val)
              (integer? val)
              (<= -2147483648 val)
              (<= val 2147483647)))
        (raise TypeError "not a int" val))       
      val)))

(define-python-class c_uint (ctype)
  (define _sz   4)
  (define _type uint32)
  
  (define _frombv
    (lambda (self bv i)
      (get-u32 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u32 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not (and
                  (number? val)
                  (integer? val)
                  (>= val -2147483648)
                  (<= val 4294967295)))
        (raise TypeError "not a unsigned int" val))
      (logand val #xffffffff))))

(define-python-class c_long (ctype)
  (define _sz   8)
  (define _type int64)

  (define _frombv
    (lambda (self bv i)
      (get-s64 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-s64 bv i val)))

  (define _default 0)
    
  (define _check
    (lambda (self val)
      (when (not
             (and
              (number? val)
              (integer? val)    
              (<= -9223372036854775808 val)
              (<= val 9223372036854775807)))
        (raise TypeError "not a long" val))
      val)))
      
(define-python-class c_ulong (ctype)
  (define _sz   8)
  (define _type uint64)

  (define _frombv
    (lambda (self bv i)
      (get-u64 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u64 bv i val)))

  (define _default 0)
  
  (define _check
    (lambda (self val)
      (when (not (and
                  (number? val)
                  (integer? val)
                  (>= val -9223372036854775808)
                  (<= val 18446744073709551615)))
        (raise TypeError "not a unsigned long" val))
      (logand val #xffffffffffffffff))))

(define c_longlong c_long)
(define c_ulonglong c_ulong)
(define c_size_t  c_ulong)
(define c_ssize_t c_long)

(define-python-class c_float (ctype)
  (define _sz   4)
  (define _type float)

  (define _frombv
    (lambda (self bv i)
      (get-f32 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-f32 bv i val)))
  
  (define _default 0.0)
  
  (define _check
    (lambda (self val)
      (when (not (and (number? val) (real? val)))
        (raise TypeError "not a float" val))
      val)))
  
(define-python-class c_double (ctype)
  (define _sz   8)
  (define _type double)
 
  (define _frombv
    (lambda (self bv i)
      (get-d64 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-d64 bv i val)))

  (define _default 0.0)
  
  (define _check
    (lambda (self val)
      (when (not (and (number? val) (real? val)))
        (raise TypeError "not a float" val))
      val)))

(define c_longdouble c_double)

(define (bv-len pt)
  (let lp ((i 0))
    (let* ((pt2 (make-pointer (+ (pointer-address pt) i)))
           (b   (pointer->bytevector pt2 1)))
      (if (= (get-u8 b 0) 0)
          i
          (lp (+ i 1))))))

(define (bv-zero byte)
  (let* ((bv (slot-ref byte 'bytes))
        (n  (len bv)))
    (let lp ((i 0))
      (if (< i n)
          (if (= (get-u8 bv i) 0)
              #t
              (lp (+ i 1)))
          (let ((bb (make-bytevector (+ n 1))))
            (slot-set! byte 'bytes bb)
            (let lp ((i 0))
              (if (< i n)
                  (begin
                    (set-u8 bb i (get-u8 bv i))
                    (lp (+ i 1)))
                  (set-u8 bb n 0))))))))

(define *st* (make-hash-table))

(define same-type
  (case-lambda
    ((x y)
     (if (eq? x y)
         #t
         (same-type x y *st*)))
    ((x y d)
     (cond
      ((eq? x y)
       #t)
      
      ((hash-ref d (cons x y))
       =>
       (lambda (p)
         (if (eq? p #t)
             #t
             #f)))
      
      ((and (isinstance x c_fkn)
            (isinstance y c_fkn))
       (and
        (same-type (ref x '_rettype) (ref y '_rettype) d)
        (and-map
         (lambda (x) (same-type (car x) (cdr x) d))
         (map cons (ref x '_argtypes) (ref y '_argtypes)))))
      
      ((and (or (isinstance x c_pointer) (isinstance x c_array))
            (or (isinstance y c_pointer) (isinstance y c_array)))
       
       (same-type (ref x '_parent) (ref y '_parent)))
      
      ((and (isinstance x c_array)
            (isinstance y c_array))
       (and
        (= (ref x '_len) (ref y '_len))
        (same-type (ref x '_parent) (ref y '_parent))))
    
      ((and (isinstance x Structure)
            (isinstance y Structure))
       (hash-set! d (cons x y) #t)
       (if (let lp ((x (to-list (ref x '_fields_)))
                    (y (to-list (ref y '_fields_))))
             (match (list x y)
               (((x . xx) (y . yy))
                (match (cons x y)
                  (((s1 t1) . (s2 t2))
                   (and
                    (same-type t1 t2)
                    (lp xx yy)))
                  (((s1 t1 b1) . (s2 t2 b2))
                   (and (= b1 b2)
                        (same-type t1 t2 d)
                        (lp xx yy)))
                  (else #f)))
               ((() ())
                #t)
               (_ #f)))
           #t
           (begin
             (hash-set! d (cons x y) d)
             #f)))

      (else
       #f)))))    

(define-python-class c_char_p (ctype)
  (define _type '*)

  (define _sz 8)

  (define _tobv
    (lambda (self bv i val)
      (set-u64 bv i val)))

  (define _frombv
    (lambda (self b i)
      (get-u64 b i)))

  (define _as_parameter
    (lambda (self)
      (make-pointer (get-u64 (ref self '_b) (ref self '_pos)))))
  
  (define _get
    (lambda (self)
      (let ((x (get-u64 (ref self '_b) (ref self '_pos))))
        (if (= x 0)
            None
            (ref self '_guard))))) 

  (define _serialize-from
    (lambda (self b i)
      (set self '_b b)
      (set self '_pos i)
      (let ((val2 (get-u64 b i)))
        (when (> val2 0)
          (let* ((val  (make-pointer val2))
                 (n    (bv-len val)))
            (set self '_len n)
            (let ((b (bytes)))
              (slot-set! b 'bytes (pointer->bytevector val n))
              (set self '_guard b))))
        (+ 8 i))))
      
  (define value
    (property
     _get
     (lambda (self val)
       (let lp ((val val))
         (cond
          ((isinstance val bytes)
           (let ((val (if (isinstance val 'CBytes)
                          val
                          (bytes val))))
             (bv-zero val)
             (set self '_len (bytevector-length (slot-ref val 'bytes)))
             (set self '_guard val)
             (set-u64 (ref self '_b) (ref self '_pos)
                      (pointer-address
                       (bytevector->pointer
                        (slot-ref val 'bytes))))))
          
          ((isinstance val str)
           (lp (bytes val)))

          ((isinstance val c_pointer)
           (let ((p1 (ref self '_pointer))
                 (p2 (ref val '_pointer)))
             (when (not
                    (same-type
                     (if (pyobject? p1) (get-class p1) p1)
                     (if (pyobject? p2) (get-class p2) p2)))
               (raise TypeError "pointers of different type")
               (set-u64 (ref self '_b) (ref self '_pos)
                        (get-u64 (ref val '_b) (ref val '_pos)))
               (set self '_pointer val))))

          (else
           (raise ValueError "wrong type to set ac_char_p")))))))

       
  (define __init__
    (case-lambda
      ((self)
       (set self '_len 0)
       (set self '_b (make-bytevector 8))
       (set self '_pos 0))
      
      ((self val)
       (__init__ self)
       (if (pointer? val)
           (if (equal? val %null-pointer)
               (begin
                 (set-u64 (ref self '_b) (ref self '_pos) 0)
                 (set self '_guard %null-pointer))
               (let ((val2 (pointer-address val)))
                 (set-u64 (ref self '_b) (ref self '_pos) val2)
                 (let ((n (bv-len val)))
                   (set self '_len n)
                   (let ((b (bytes)))
                     (slot-set! b 'bytes (pointer->bytevector val n))
                     (set self '_guard b)))))
           (begin
             (when (not (isinstance val bytes))
               (raise TypeError "not a bytes object" val))
             
             (let ((val (if (isinstance val 'CBytes)
                            val
                            (bytes val))))
               (set self 'vaue val))))))))
        
(define-python-class c_void_p (ctype)
  (define _type void)
  (define __init__
    (lambda (self)
      (set self '_b    (mk-u8 0))
      (set self '_pos 0)
      (values))))

(define *pointers* (make-weak-value-hash-table))
(define *bpointers* (make-weak-value-hash-table))


(define-python-class c_pointer (ctype)
  (define _sz    8)
  (define _type '*)  

  (define _as_parameter
    (lambda (self)
      (make-pointer (get-u64 (ref self '_b) (ref self '_pos)))))

  (define _get
    (lambda (self)
      (get-u64 (ref self '_b) (ref self '_pos))))

  (define _tobv
    (lambda (self bv i val)
      (set-u64 bv i val)))

  (define _frombv
    (lambda (self b i)
      (get-u64 b i)))

  (define __bool__
    (lambda (self)
      (bool (_get self))))
  
  (define __getitem__
    (lambda (self i)
      (let* ((p    (ref self '_parent))
             (c    (if (pyobject? p) (get-class p) p))
             (s    (if (pyobject? p) ((ref p '_size)) ((ref p '_size) p))) 
             (addr (+ (* i s) (get-u64 (ref self '_b) (ref self '_pos))))
             (b    (pointer->bytevector (make-pointer addr) s))
             (p2   (c)))
        
        (when (= (get-u64 (ref self '_b) (ref self '_pos)) 0)
          (raise ValueError "NULL pointer access"))
        
        ((ref p2 '_serialize-from) b 0)
        p2)))

  (define __setitem__
    (lambda (self i x)
      (set (__getitem__ self i) 'value x)))
        
  (define contents
    (property
     (lambda (self)
       (__getitem__ self 0))
     (lambda (self x)
       (__setitem__ self 0 x))))
    
  (define _serialize-from
    (lambda (self b n)
      (set self '_b b)
      (set self '_pos n)
      (let ((p (ref self '_parent)))
        (if (pyobject? p)
            (set self '_parent (get-class p))))
      (+ n (ref self '_sz))))

  (define __repr__
    (lambda (self)
      (format #f "c_pointer(~a)" (get-u64 (ref self '_b) (ref self '_pos)))))

  (define value
    (property
     (lambda (self) self)
     (lambda (self val)
       (cond
        ((or (isinstance val c_pointer) (isinstance val c_array))
         (let* ((p1 (ref self '_parent))
                (p2 (ref val '_parent))
                (c1 (if (pyobject? p1) (get-class p1) p1))
                (c2 (if (pyobject? p2) (get-class p2) p2)))
           (when (not (same-type c1 c2))
             (raise TypeError "Cannot set  pointer of the wrong type"))
           (set self '_guard val)
           (set-u64
            (ref self '_b)
            (ref self '_pos)
            (if (isinstance val c_pointer)
                (get-u64 (ref val '_b) (ref val '_pos))
                ((ref val '_as_ptr)))))) 

        ((isinstance val pyint)
         (set-u64 (ref self '_b) (ref self '_pos) val))))))

           

             
  (define __init__
    (case-lambda
      ((self)
       (set self '_b   (mk-u64 0))
       (set self '_pos 0))
      
      ((self val)
       (cond
        ((ref self '_parent) =>
         (lambda (p)
           (cond
            ((isinstance val pyint)
             (__init__ self)
             (when (> val 0)
               (aif it (hash-ref *pointers* val)
                    (set self '_parent it)
                    (let* ((x (p))
                           (n  ((ref p '_size) p))
                           (b (pointer->bytevector (make-pointer val) n)))
                      ((ref x '_serialize-from) b 0)
                      (set self '_parent x)
                      (hash-set! *pointers* val x))))
             
             (set-u64 (ref self '_b) (ref self '_pos) val))
            
            ((pointer? val)
             (__init__ self (pointer-address val)))
            
            ((isinstance val ctype)
             (let* ((c (if p (if (pyobject? p) (get-class p) p))))
               (when (not (same-type c (get-class val)))
                 (raise TypeError "wrong type in c_pointer constructor"))
               (__init__ self ((ref val '_as_ptr))))))))
           
        ((isinstance val ctype)
         (__init__ self)
         (let ((addr ((ref val '_as_ptr))))
           (hash-set! *pointers* addr val)
           (set self '_parent val)
           (set-u64 (ref self '_b) (ref self '_pos) addr))))))))

(define create_string_buffer
  (case-lambda
    ((n)
     (cond
      ((number? n)
       (CBuffer
        (make-bytevector n)))
      ((or (isinstance n bytes) (isinstance n str))
       (CBuffer
        (let* ((b (bv-scm n))
               (N (len b))
               (v (make-bytevector (+ N 1))))
          (let lp ((i 0))
            (if (< i N)
                (begin
                  (set-u8 v i (get-u8 b i))
                  (lp (+ i 1)))
                (begin
                  (set-u8 v i 0)
                  (bytes v)))))))))
    ((n m)
     (CBuffer
      (cond
       ((or (isinstance n bytes) (isinstance n str))
        (let* ((b (bv-scm n))
               (N (len b))
               (v (make-bytevector m)))
          (let lp ((i 0))
            (if (< i N)
               (begin
                 (bytevector-u8-set! v i (bytevector-u8-ref b i))
                 (lp (+ i 1)))
               (let lp ((i i))
                 (if (< i m)
                     (begin
                       (bytevector-u8-set! v i 0)
                       (lp (+ i 1)))
                     v)))))))))))

(define-python-class CBuffer (bytes)
  (define raw
    (property
     (lambda (self)
       self)))
  
  (define value
    (property
     (lambda (self)
       (let* ((b (slot-ref self 'bytes))
              (n (len b)))
         (let lp ((i 0) (r '()))
           (if (< i n)
               (let ((x (bytevector-u8-ref b i)))
                 (if (= x 0)
                     (bytes (reverse r))
                     (lp (+ i 1) (cons x r))))
               (bytes (reverse r))))))
     
     (lambda (self val)
       (let* ((b  (slot-ref self 'bytes))
              (n  (len b))
              (bb (slot-ref (bytes val) 'bytes))
              (nn (len bb)))
         (let lp ((i 0))
           (if (< i nn)
               (if (< i n)
                   (begin
                     (set-u8 b i (get-u8 bb i))
                     (lp (+ i 1)))
                   (set-u8 b (- i 1) 0))
               (if (< i n)
                   (set-u8 b i 0)
                   (set-u8 b (- i 1) 0)))))))))

  
(define c_buffer create_string_buffer)

(define (sizeof x)
  (cond
   ((isinstance x ctype)
    ((ref x '_size)))
   ((issubclass x ctype)
    ((ref x '_size) x))
   ((isinstance x bytes)
    (len x))
   ((number? x)
    (if (integer? x)
        4
        (if (real? x)
            8)))))

(define (byref x) (c_pointer x))
(define pointer byref)
(define fail (list 'fail))

(define-python-class c_bits (ctype)
  (define _as_parameter
    (lambda (self)
      (raise ValueError "c_bits does not allow parameter of it")))

  (define _get
    (lambda (self)
      (ash 
       (logand (ref self '_mask)
               ((ref (ref self '_parent) '_get)))
       (- (ref self '_offset)))))

  (define value
    (property
     _get
     (lambda (self val)
       (set (ref self '_parent) 'value
            (ash val (ref self '_off))))))
       
       
  (define __init__
    (lambda (self t off len)
      (set self '_parent t)
      (set self '_off    off)
      (set self '_sz     (ref t '_sz))
      (set self '_mask   (ash (- (ash 1 len) 1) off)))))

(define-python-class SMeta (CMeta)
  (define __new__
    (lambda (type name parents . l)
      (define cls (apply CMeta name parents l))
      (match parents
        ((p . _)
         (aif it (ref p '_fields_)
              (aif it2 (ref cls '_fields)
                   (set cls '_fields_ (+ it it2))
                   (set cls '_fields_ it))
              (values)))
        (_ (values)))
      cls)))

(define-python-class Structure (ctype #:metaclass SMeta)
  (define _fromlist
    (lambda (self x)
      ;; Set positional arguments
      (define m (ref self '_m_))
      (let lp ((x (to-list x)) (f (ref self '_fields_)))
        (match x
          ((x . xx)
           (match f
             (((s t) . ff)
              (if (pair? x)
                  ((ref (hash-ref m s) '_fromlist) x)
                  (set (hash-ref m s) 'value x))
              (lp xx ff))

             (((s t b) . ff)
              (set (hash-ref m s) 'value x)
              (lp xx ff))
           
             (()
              (raise TypeError "too many initializers"))))
          (()
           (values))))))

  (define __init__
    (lam (self (* x) (** xx))
         (let* ((m      (make-hash-table))
                (fields (to-list (ref self '_fields_)))
                (l
           
                 ;; Fill in the hashmap with defult values
                 (let lp ((f fields) (off 0) (tt #f) (ttt #f) (l '()))
                   (match f
                     (()
                      (reverse l))
               
                     (((s t) . ff)
                      (let ((tt (t)))
                        (rawset tt '_simple #t)
                        (hash-set! m s tt)
                        (lp ff 0 #f #f (cons tt l))))
                     
                     (((s t b) . ff)
                      (let ()
                        (define (inc tt off ll)
                          (let ((off (+ b off)))
                            (if (> off (* ((ref t '_size) t) 8))
                                (lp (cons (list s t b) ff) 0 #f #f l)
                                (let ((t2 (c_bits tt off b)))
                                  (rawset t2 '_simple #t)
                                  (hash-set! m s t2)
                                  (lp ff off tt  t ll)))))
      
                        (define (init)
                          (let* ((tt (t)))                            
                            (inc tt 0 (cons tt l))))
                  
                        (cond
                         ((= off 0)
                          (init))
                         ((eq? t ttt)
                          (inc tt off l))
                         (else
                          (init)))))))))

           (rawset self '_fields_ fields)
           (rawset self '_m_ m)
           (rawset self '_l_ l)

           (let* ((sz ((ref self '_size)))
                  (b  (make-bytevector sz)))

             (rawset self '_pos 0)
             (rawset self '_b   b)
             (_de-serialize self)
             (let ((addr (pointer-address (bytevector->pointer b))))
               (rawset self '_value (mk-u64 addr))))

           (_fromlist self x)

           ;; Set keyword arguments
           (hash-for-each
            (lambda (s x)
              (aif it (hash-ref m s #f)
                   (if (pair? x)
                       ((ref it '_from_list) x)
                       (set it 'value x))
                   (raise TypeError "Missing key in struct" s)))
            xx))))

  (define _as_ptr
    (lambda (self)
      (pointer-address (bytevector->pointer (ref self '_b)))))

  (define _pack_ 4)
  (define _sz    #f)
  (define _size
    (lambda (self)
      (aif it (ref self '_sz #f)
           it
           (if (ref self '_l_)
               (let ((sz
                      (let* ((d (ref self '_pack_))
                             (l (ref self '_l_)))
                        (let lp ((l l) (n 0))
                          (if (pair? l)
                              (let* ((e ((ref (car l) '_size)))
                                     (f (modulo e d)))
                                (lp (cdr l) (+ n e (if (> f 0) (- d f) 0))))
                              n))))) 
                 (rawset self '_sz sz)
                 sz)
               (let* ((p (self))
                      (s ((ref p '_size))))
                 (set self '_sz s)
                 s)))))
      
  (define _serialize-into
    (lambda (self b n)
      (let* ((d (ref self '_pack_))
             (l (ref self '_l_)))
        (let lp ((l l) (n n))
          (if (pair? l)
              (let* ((n2 ((ref (car l) '_serialize-into) b n))
                     (e  (- n2 n))
                     (f  (modulo e d)))
                (lp (cdr l) (+ n e (if (> f 0) (- d f) 0))))
              n)))))

  (define _serialize-from
    (lambda (self b n)
      (set self '_pos n)
      (set self '_b   b)
      (let* ((d (ref self '_pack_))
             (l (ref self '_l_)))
        (let lp ((l l) (n n))
          (if (pair? l)
              (let* ((n2 ((ref (car l) '_serialize-from) b n))
                     (e  (- n2 n))
                     (f  (modulo e d)))
                (lp (cdr l) (+ n e (if (> f 0) (- d f) 0))))
              n)))))

  (define _serialize
    (lambda (self)
      ((ref self '_serialize-into) (ref self '_b) (ref self '_pos))))

  (define _de-serialize
    (lambda (self)
      ((ref self '_serialize-from) (ref self '_b) (ref self '_pos))))
       
  (define __getattr__
    (lambda (self k)
      (aif it (ref (hash-ref (ref self '_m_) k) 'value)
          it
          (raise (AttributeError "no attribute ~a" k)))))
  
  (define __setattr__
    (lambda (self k v)
      (aif it (hash-ref (ref self '_m_) k)
           (set it 'value v)
           (let ((it (ref self k fail)))
             (if (eq? it fail)
                 (raise (AttributeError
                         "Can only set already present attributes ~a" k))
                 ((rawref object '__setattr__) self k v))))))
    
  (define __repr__
    (lambda (self)
      (let* ((m (ref self '_m_))
             (l (fold
                 (lambda (k s) (cons* 
                                (ref (hash-ref m (car k))  'value)
                                (car k)
                                s))
                 '()
                 (ref self '_fields_))))
        (format #f "~a(~{~a:~a ~}~a:~a)"
                (ref self '__name__)
                (reverse (cddr l))
                (cadr l)
                (car  l)))))

  (define value
    (property
     (lambda (self) self)
     (lambda (self val)
       (when (not (same-type (get-class self) (get-class val)))
         (raise ValueError "not same type setting struct" (get-class self)))

       (let ((b1 (ref val  '_b))
             (n1 (ref val  '_pos))
             (b2 (ref self '_b))
             (n2 (ref self '_pos))
             (N  (ref self '_sz)))
         (let lp ((i 0) (j1 n1) (j2 n2))
           (if (< i N)
               (begin
                 (set-u8 b2 j2 (get-u8 b1 j1))
                 (lp (+ i 1) (+ j1 1) (+ j2 1)))))))))
  (define type
    (property
     (lambda (self)
       (let lp ((f (ref self '_fields_)) (tt #f) (off 0) (l '()))
         (match f
           (()
            (reverse l))
           
           (((s t) . ff)
            (lp ff tt 0 (cons t l)))
         
           (((s b t) . ff)
            (let ()
              (define (inc t off ll)
                (let ((off (+ b off)))
                  (if (>= off (* ((ref t '_size)) 8))
                      (lp (cons (list s b t) ff) tt 0 l)
                      (lp ff t off ll))))
            
              (define (init)
                (inc 0 t (cons t l)))
            
              (cond
               ((= off 0)
                (init))
               ((eq? t tt)
                (inc tt off l))
               (else
                (init))))))))))
       
  (define _as_parameter
    (lambda (self)
      (let lp ((l (ref self '_l_)))
        (if (pair? l)
            (let ((ct (car l)))
              (cond
               ((isinstance ct Structure)
                (append ((ref ct '_as_parameter)) (lp (cdr l))))
               (else
                (cons   ((ref ct '_as_parameter)) (lp (cdr l))))))
            '()))))
    
  (define __dir__
    (lambda (self)
      (let ((x (py-set ((ref object '__dir__) self))))
        (sort
         (to-list
          ((ref x 'union)
           (py-set
            (hash-fold
             (lambda (k v s) (cons k s))
             '()
             (ref self '_m_)))))
         string<)))))

(define-python-class Union (ctype)
  (define _fromlist
    (lambda (self x)
      ;; Set positional arguments
      (define m (ref self '_m_))
      (let lp ((x (to-list x)) (f (ref self '_fields_)))
        (match x
          ((x . xx)
           (match f
             (((s t) . ff)
              (if (pair? x)
                  ((ref (hash-ref m s) '_fromlist) x)
                  (set (hash-ref m s) 'value x))
              (lp xx ff))
           
             (()
              (raise TypeError "too many initializers"))))
          (()
           (values))))))

  (define __init__
    (lam (self (* x) (** xx))
         (let* ((m      (make-hash-table))
                (fields (to-list (ref self '_fields_)))
                (l
           
                 ;; Fill in the hashmap with defult values
                 (let lp ((f fields) (off 0) (tt #f) (ttt #f) (l '()))
                   (match f
                     (()
                      (reverse l))
               
                     (((s t) . ff)
                      (let ((tt (t)))
                        (rawset tt '_simple #t)
                        (hash-set! m s tt)
                        (lp ff 0 #f #f (cons tt l))))))))
                                       
           (rawset self '_fields_ fields)
           (rawset self '_m_ m)
           (rawset self '_l_ l)

           (let* ((sz ((ref self '_size)))
                  (b  (make-bytevector sz)))

             (rawset self '_pos 0)
             (rawset self '_b   b)
             (_de-serialize self)
             (let ((addr (pointer-address (bytevector->pointer b))))
               (rawset self '_value (mk-u64 addr))))

           (_fromlist self x)

           ;; Set keyword arguments
           (hash-for-each
            (lambda (s x)
              (aif it (hash-ref m s #f)
                   (if (pair? x)
                       ((ref it '_from_list) x)
                       (set it 'value x))
                   (raise TypeError "Missing key in struct" s)))
            xx))))

         

  (define _as_ptr
    (lambda (self)
      (pointer-address (bytevector->pointer (ref self '_b)))))

  (define _pack_ 4)
  (define _sz    #f)
  (define _size
    (lambda (self)
      (aif it (ref self '_sz #f)
           it
           (if (ref self '_l_)
               (let ((sz
                      (let* ((d (ref self '_pack_))
                             (l (ref self '_l_)))
                        (let lp ((l l) (n 0))
                          (if (pair? l)
                              (let* ((e ((ref (car l) '_size)))
                                     (f (modulo e d)))
                                (lp (cdr l) (max n
                                                 (+ e
                                                    (if (> f 0)
                                                        (- d f)
                                                        0)))))
                              n))))) 
                 (rawset self '_sz sz)
                 sz)
               (let* ((p (self))
                      (s ((ref p '_size))))
                 (set self '_sz s)
                 s)))))
      
  (define _serialize-into
    (lambda (self b n)
      (let* ((d (ref self '_pack_))
             (l (ref self '_l_)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                ((ref (car l) '_serialize-into) b n)
                (lp (cdr l)))
              ((ref self '_size)))))))

  (define _serialize-from
    (lambda (self b n)
      (set self '_pos n)
      (set self '_b   b)
      (let* ((d (ref self '_pack_))
             (l (ref self '_l_)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                ((ref (car l) '_serialize-into) b n)
                (lp (cdr l)))
              ((ref self '_size)))))))
  
  (define _serialize
    (lambda (self)
      ((ref self '_serialize-into) (ref self '_b) (ref self '_pos))))

  (define _de-serialize
    (lambda (self)
      ((ref self '_serialize-from) (ref self '_b) (ref self '_pos))))
       
  (define __getattr__
    (lambda (self k)
      (aif it (ref (hash-ref (ref self '_m_) k) 'value)
          it
          (raise (AttributeError "no attribute ~a" k)))))
  
  (define __setattr__
    (lambda (self k v)
      (aif it (hash-ref (ref self '_m_) k)
           (set it 'value v)
           (let ((it (ref self k fail)))
             (if (eq? it fail)
                 (raise (AttributeError
                         "Can anly set already present attributes ~a" k))
                 ((rawref object '__setattr__) self k v))))))
    
  (define __repr__
    (lambda (self)
      (let* ((m (ref self '_m_))
             (l (fold
                 (lambda (k s) (cons* 
                                (ref (hash-ref m (car k))  'value)
                                (car k)
                                s))
                 '()
                 (ref self '_fields_))))
        (format #f "~a(~{~a:~a|~}~a:~a)"
                (ref self '__name__)
                (reverse (cddr l))
                (cadr l)
                (car  l)))))

  (define value
    (property
     (lambda (self) self)
     (lambda (self val)
       (when (not (same-type (get-class self) (get-class val)))
         (raise ValueError "not same type setting struct" (get-class self)))
       (let ((b1 (ref val  '_b))
             (n1 (ref val  '_pos))
             (b2 (ref self '_b))
             (n2 (ref self '_pos))
             (N  (ref self '_sz)))
         (let lp ((i 0) (j1 n1) (j2 n2))
           (if (< i N)
               (begin
                 (set-u8 b2 j2 (get-u8 b1 j1))
                 (lp (+ i 1) (+ j1 1) (+ j2 1)))))))))
  (define type
    (property
     (lambda (self)
       (let lp ((f (ref self '_fields_)) (tt #f) (off 0) (l '()))
         (match f
           (()
            (reverse l))
           
           (((s t) . ff)
            (lp ff tt 0 (cons t l))))))))
  
  (define _as_parameter
    (lambda (self)
      (raise TypeError "UNion does not suport non pointer c argument")))
  
  (define __dir__
    (lambda (self)
      (let ((x (py-set ((ref object '__dir__) self))))
        (sort
         (to-list
          ((ref x 'union)
           (py-set
            (hash-fold
             (lambda (k v s) (cons k s))
             '()
             (ref self '_m_)))))
         string<)))))


(define-python-class c_array (ctype)
  (define _as_parameter
    (lambda (self)
      (make-pointer
       (+
        (pointer-address
         (bytevector->pointer (ref self '_b)))
        (ref self '_pos)))))

  (define _size
    (lambda (self)    
      (let* ((p (ref self '_parent))
             (x (* ((ref p '_size) p) (ref self '_len))))
        x)))
        

  (define _serialize-from
    (lambda (self b n)
      (set self '_b b)
      (set self '_pos n)
      (let* ((N (ref self '_len))
             (a (pointer-address (bytevector->pointer b)))
             (p (ref self '_parent))
             (s ((ref p '_size) p)))
        (let lp ((i 0) (j n) (l '()))
          (if (< i N)
              (lp
               (+ i 1)
               (+ j s)
               (cons
                (aif it (hash-ref *bpointers* (+ a j))
                     (if (same-type p (get-class it))
                         it
                         (let ((p (p)))
                           ((ref p '_serialize-from) b j)
                           (rawset p '_simple #t)
                           p))
                     (let ((p (p)))
                       ((ref p '_serialize-from) b j)
                       (rawset p '_simple #t)
                       (hash-set! *bpointers* (+ a j) b)
                       p))
                l))
              (set self '_guard (reverse l))))
        (+ n (* s N)))))

  (define _copy
    (lambda (self v)
      (let lp ((l (ref self '_guard)) (v v))
        (if (pair? v)
            (if (pair? l)
                (begin
                  (set (car l) 'value (car v))
                  (lp (cdr l) (cdr v)))
                (raise ValueError "too many arguments to c_array"))))))
  
  (define value
    (property
     (lambda (self) (ref self '_guard))
     _copy))

  (define __repr__
    (lambda (self)
      (format #f "<~a_Aray_~a>" (ref self '_parent) (ref self '_len))))

  (define __len__
    (lambda (self) (ref self '_len)))
  
  (define __getitem__
    (lambda (self i)
      (if (vector? i)
          (apply pylist-slice (ref self '_guard) (vector->list i))
          (pylist-ref (ref self '_guard) i))))

  (define __iter__
    (lambda (self)
      (ref self '_guard)))

    
  (define __init__
    (case-lambda
      ((self)
       (let* ((n ((ref self '_size)))
              (b (make-bytevector n)))
         (_serialize-from self b 0)))

      ((self v)
       (__init__ self)
       (cond
        ((and (isinstance v c_array)
              (same-type (ref self '_parent) (get-class v))
                (= (ref self '_len) (ref v '_len)))
         (_copy self (ref v '_guard)))
        (else
         (_copy self (list v)))))
           
      ((self . v)
       (__init__ self)
       (_copy self v)))))

(define (cast p P)
  (let ((q (P)))
    (cond
     ((isinstance q c_pointer)
      (cond
       ((isinstance p c_pointer)
        (set-u64 (ref q '_b) 0 (get-u64 (ref p '_b) 0)))
       ((isinstance p c_array)
        (set-u64 (ref q '_b) 0 ((ref p '_as_ptr))))
       (else
        (raise ValueError
               "cast: only c_array or c_pointer in first argument"))))
     (else
      (raise ValueError "cast: only c_pointer in second argument")))
    q))

(define-python-class c_fkn (ctype)
  (define _sz   8)
  (define _type '*)
 
  (define _as_parameter
    (lambda (self)      
      (make-pointer
       (get-u64 (ref self '_b) (ref self '_pos)))))

  (define _frombv
    (lambda (self bv i)
      (get-u64 bv i)))

  (define _tobv
    (lambda (self bv i val)
      (set-u64 bv i val)))

  (define _serialize-from
    (lambda (self b i)
      (set self '_b b)
      (set self '_pos i)
      (set self '_guard
           (pointer->procedure
            (ref (ref self '_rettype c_int) '_type)
            (make-pointer
             (get-u64 b i))
            (map (lambda (x) (ref x '_type))   
                 (ref self '_argtypes '()))))))

  (define __call__
    (lambda (self . l)
      ((ref self '_rettype)
       (apply
        (ref self '_guard)
        (map (lambda (t x) ((ref (t x) '_as_parameter)))
             (ref self '_argtypes)
             l)))))
        
  (define __init__
    (case-lambda
      ((self)
       (set self '_b (make-bytevector 8))
       (set self '_pos 0))

      ((self val)
       (cond
        ((isinstance val c_fkn)
         (__init__ self)
         (when (not (same-type (get-class self) (get-class val)))
           (raise "Not correct type in c_fkn"))
         (set self '_guard (ref val '_guard))
         (set-u64 (ref self '_b) (ref self '_pos)
                  (get-u64 (ref val '_b) (ref val '_pos))))
        
        ((procedure? val)
         (__init__ self)
         (let* ((at  (ref self '_argtypes '()))
                (rt  (ref self '_rettype c_int))
                (vag (lambda x
                       ((ref
                         (rt (apply val (map (lambda (t x) (t x))
                                             at x)))
                         '_as_parameter)))))
       
           (set self '_guard vag)
           (set-u64 (ref self '_b) (ref self '_pos)
                    (pointer-address
                     (procedure->pointer
                      (ref rt '_type)
                      vag 
                      (map (lambda (x) (ref x '_type)) at))))))
        
        ((pointer? val)
         (__init__ self)
         (set self '_guard
              (pointer->procedure
               (ref (ref self '_rettype c_int) '_type)
               val
               (map (lambda (x) (ref x '_type))   
                    (ref self '_argtypes '()))))

         (set-u64 (ref self '_b) (ref self '_pos)
                  (pointer-address val))))))))

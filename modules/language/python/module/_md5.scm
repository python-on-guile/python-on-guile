;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _md5)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (md5))

(define-python-class md5 (Summer)
  (define name     "md5")
  (define digest_size 16)
  
  (define _command "/usr/bin/md5sum"))
  

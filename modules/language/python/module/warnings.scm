;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module warnings)
  #:use-module ((oop pf-objects) #:select (ref set define-python-class))
  #:use-module (ice-9 match)
  #:use-module (language python def)
  #:use-module ((language python module _python) #:select (issubclass dir))
  #:use-module (language python exceptions)
  #:use-module (language python bool)
  #:use-module (language python dict)
  #:use-module (language python list)
  #:use-module (language python with)
  #:use-module (language python try)
  #:use-module (language python module collections)
  #:use-module ((language python module os path) #:select (abspath))
  #:use-module ((language python module re) #:select (fullmatch compile))
  #:use-module (language python module io)
  #:use-module ((language python compile) #:select (Ni-f Ni Mi Filei- Filei-f
                                                         Modulei- Modulei-f))
  #:replace (warn)
  #:export (warn-f filterwarnings simplefilter formatwarning catch_warnings))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define WarnContainer (namedtuple "WarnContainer"
                                  "args message filename lineno category"))

(define (warn- x)
  ((@ (guile) format) #t x))

(define (warn-f . x)
  (apply fwarn x (fluid-ref Ni-f)
         (fluid-ref Filei-f) (fluid-ref Modulei-f) x))

(define-syntax warn
  (lambda (x)
    (syntax-case x ()
      ((_ . x)
       #'(let ((line Ni      )
               (file Filei-  )
               (mo   Modulei-))
           (apply fwarn (list . x) line file mo (list . x))))

      (_
       #'(let ((line Ni)
               (file Filei-)
               (mo   Modulei-))
           (lambda l (apply fwarn l line file mo l)))))))

(def (fwarn args line file mo
            message (= category None) (= stacklevel 1) (= source None))
     (when (not (bool category)) (set! category UserWarning))
     (let lp ((l (fluid-ref filters)))
       (match l 
         (((and xx (action- message- category- module- lineno-)) . l)
          (let ((x (list line file mo message category xx)))
            (if 
             (and
              (bool (if (equal? message- "") #t (fullmatch message- message)))
              (bool (if (bool category) (issubclass category- category) #t))
              (bool (if (equal? module- "") #t (fullmatch module- mo)))
              (bool (if (= lineno- 0) #t (equal? lineno- line))))
             (apply dowarn args action- x)
             (lp l))))
         (() (dowarn args "always" line file mo message
                     (if (bool category) category UserWarning)
                     0)))))

(define warnlocs (make-hash-table))

(define collect (make-fluid (lambda (x) #f)))

(define (dowarn args action line file mo message category it)
  (let ((m (lambda ()
             (let ((r (formatwarning message category file line)))
               ((fluid-ref collect) (WarnContainer
                                     args
                                     (category message)
                                     file
                                     line
                                     category))
               r))))
    (cond
     ((equal? action "default")
      (aif it0 (hashq-ref warnlocs it)
           (aif it1 (hash-ref it0 mo)
                (aif it2 (hash-ref it1 line)
                     (values)
                     (begin
                       (hash-set! it1 line #t)
                       (warn- (m))))
                (let ((it1 (make-hash-table)))
                  (hash-set! it1 line #t)
                  (hash-set! it0 mo   it1)
                  (warn- (m))))
           (let ((it0 (make-hash-table))
                 (it1 (make-hash-table)))
             (hash-set! it1 line     #t)
             (hash-set! it0 mo       it1)
             (hashq-set! warnlocs it it0)
             (warn- (m)))))

     ((equal? action "module")
      (aif it0 (hashq-ref warnlocs it)           
           (aif it1 (hash-ref it0 mo)
                (values)
                (begin
                  (hash-set! it0 mo #t)
                  (warn- (m))))
           (let ((it0 (make-hash-table)))
             (hash-set! it0 mo #t)
             (hashq-set! warnlocs it it0)
             (warn- (m)))))
     
     ((equal? action "once")      
      (aif it0 (hashq-ref warnlocs it)
           (values)
           (begin
             (hashq-set! warnlocs it #t)
             (warn- (m)))))

     ((equal? action "error")
      (raise ((if (bool category)
                  category
                  Exception) (m))))

     ((equal? action "ignore")
      (values))

     ((equal? action "always")
      (warn- (m))))))
      

(define-python-class wWarn ()
  (define __init__
    (lambda (self record error)
      (set self '_record record)
      (set self '_error  error)
      (set self '_l      (py-list))))
  
  (define __enter__    
    (lambda (self)
      (set self 'filters (fluid-ref filters))
      (let ((col (fluid-ref collect)))
        (set self 'collect col)
        (fluid-set! collect
                    (lambda (x)
                      (pylist-append! (ref self '_l) x)
                      (col x))))
      (ref self '_l)))

  (define __exit__
    (lambda (self . x)
      (fluid-set! filters (ref self 'filters))
      (fluid-set! collect (ref self 'collect))
      (if (and (bool (car x)) (issubclass (car x) Warning))
          #t
          #f))))
  
(define filters (make-fluid '()))

(def (filterwarnings action
                     (= message  "")
                     (= category Warning)
                     (= module   "")
                     (= lineno   0)
                     (= append   #f))
     (let ((l (list action (compile message) category (compile module) lineno))
           (r (fluid-ref filters)))
       (fluid-set! filters
                   (if append
                       (append r (list l))
                       (cons l r)))))

(define (get-filenm filenm)
  (aif it (%search-load-path filenm)
       it
       filenm))

(define cache (dict))
(define (get-linestring file line)
  ((@ (guile) catch) #t
    (lambda ()
      (set! file (abspath file))
      (aif it (py-get cache file #f)
           (pylist-ref it (- line 1))
           (if file
               (with ((f (open (get-filenm file) "r")))
                     (let ((l ((ref f 'readlines))))
                       (pylist-set! cache file l)
                       (pylist-ref l (- line 1))))
               "")))
    (lambda x "")))

(def (formatwarning message category filename lineno (= line None))
     (if filename
         (format #f "~a:~a: ~a: ~a~%  ~a"
                 filename
                 lineno
                 (ref category '__name__)
                 message
                 (if (equal? line None)
                     (get-linestring filename lineno)
                     line))
         (format #f "NOFILE:~a: ~a:~a~%  ~a~%"
                 lineno
                 (ref category '__name__)
                 message
                 (if (equal? line None)
                     (get-linestring filename lineno)
                     line))))

(def (simplefilter action (= category UserWarning) (= lineno 0) (= append #f))
     (filterwarnings action #:category category #:lineno
                     lineno #:append append))

(define* (catch_warnings #:key (record #f))
  (wWarn record #t))

(filterwarnings "ignore"  #:category ResourceWarning)
(filterwarnings "ignore"  #:category ImportWarning)
(filterwarnings "ignore"  #:category PendingDeprecationWarning)
(filterwarnings "ignore"  #:category DeprecationWarning)
(filterwarnings "default" #:category DeprecationWarning #:module "__main__")

(define --export-macros #t)

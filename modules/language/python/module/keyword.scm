;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module keyword)
  #:use-module (language python string)
  #:use-module (language python set)
  #:use-module (oop pf-objects)
  
  #:export (kwlist iskeyword))

(define kwlist
  '("False"
    "None"
    "True"
    "and"
    "as"
    "assert"
    "break"
    "class"
    "continue"
    "def"
    "del"
    "elif"
    "else"
    "match"
    "except"
    "finally"
    "for"
    "from"
    "global"
    "if"
    "import"
    "in"
    "is"
    "lambda"
    "nonlocal"
    "not"
    "or"
    "pass"
    "raise"
    "return"
    "try"
    "while"
    "with"
    "yield"))


(define iskeyword (ref (py-set kwlist) '__contains__))
  
    

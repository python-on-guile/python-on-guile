;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module marshal)
  #:use-module (language python try)
  #:use-module (language python exceptions)
  #:export (load))

(define (load stream)
  (raise TypeError "reading binary files is unsupported"))

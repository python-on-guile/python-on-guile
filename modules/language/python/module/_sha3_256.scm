;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _sha3_256)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha3_256))

(define-python-class sha3_256 (Summer)
  (define name     "sha3_256")
  (define digest_size 32)
  
  (define _command "/usr/bin/sha3_256sum"))

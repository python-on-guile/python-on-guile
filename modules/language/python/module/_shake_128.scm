;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _shake_128)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (shake_128))

(define-python-class shake_128 (Summer)
  (define name     "shake_128")
  (define digest_size 0)
  
  (define _command "/usr/bin/shake_128sum"))

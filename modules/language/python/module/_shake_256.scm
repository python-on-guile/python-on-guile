;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _shake_256)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (shake_256))

(define-python-class shake_256 (Summer)
  (define name     "shake_256")
  (define digest_size 0)
  
  (define _command "/usr/bin/shake_256sum"))

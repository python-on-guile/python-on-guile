;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module threading)
  #:use-module (ice-9 threads)
  #:use-module (oop pf-objects)
  #:use-module (language python def)
  #:use-module (language python exceptions)
  #:use-module (language python list)
  #:export (RLock Lock start_new_thread allocate_lock Thread get_ident))

(define (get_ident)
  (current-thread))

(define-python-class RLock ()
  (define __init__
    (lambda (self)
      (set self '_lock (make-mutex 'recursive))))
  
  (define __enter__
    (lambda (self)
      (lock-mutex (ref self '_lock))
      self))

  (define __exit__
    (lambda (self . l)
      (unlock-mutex (ref self '_lock))
      #f))

  (define __leave__
    (lambda (self . l)
      (unlock-mutex (ref self '_lock))))

  (define acquire
    (lam (self (= blocking #t) (= timeout -1))
	 (if blocking
	     (if (< timeout 0)
		 (lock-mutex (ref self '_lock))
		 (let* ((cur (gettimeofday))
			(x   (+ (car cur) (/ (cdr cur) 1000000.0)))
			(y   (+ x timeout))
			(s   (floor y))
			(us  (floor (* (- y s) 1000000))))
		   (lock-mutex (ref self '_lock) (cons s us))))
	     (try-mutex (ref self '_lock)))))
  
  (define release __leave__))
    
(define allocate_lock
  (lambda () (RLock)))

(define Lock RLock)

(define (start_new_thread fkn args)
  (call-with-new-thread
   (lambda ()
     (apply fkn (to-list args)))))

(define (current_thread) (Thread #:ident (get_ident)))

(define-python-class Thread ()  
  (define __init__
    (lam (self (= target None) (= args '()) (= kwargs (make-hash-table))
               (= ident 'None))
         (set self 'id ident)
         (set self 'target  target)
         (set self 'args    args)
         (set self 'kwargs  kwargs)))

  (define name
    (lambda (self)
      'None))
  
  (define start
    (lambda (self)
      (call-with-new-thread
       (lambda ()
         (py-apply (ref self 'target)
                   (* (ref self 'args))
                   (** (ref self 'kwargs))))))))
  

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module copy)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (oop pf-objects)
  #:export (Error copy deepcopy))

(define Error 'CopyError)

(define (s x)
  (match x
    ((#:obj x) x)
    (x x)))

(define (tr o2 o)
  (when (is-a? o2 <py>)
    (struct-set! o2 4 (struct-ref o 4))
    (struct-set! o2 5 (struct-ref o 5))
    (struct-set! o2 6 (struct-ref o 6))
    (struct-set! o2 7 (struct-ref o 7))))

(define (copy     x)
  (let ((o2 (s ((@@ (persist persistance)      copy) x))))
    (tr o2 x)
    o2))

(define (deepcopy x)
  (let ((o2 (s ((@@ (persist persistance) deep-copy) x))))
    (tr o2 x)
    o2))
        

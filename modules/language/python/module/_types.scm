;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _types)
  #:use-module (oop goops)
  #:use-module (oop pf-objects)
  #:use-module (language python exceptions)
  #:use-module (language python def)
  #:use-module (language python code)
  #:use-module (language python bool)
  #:use-module (language python module)
  #:use-module (language python dict)
  #:use-module ((language python module python)
                #:select (getattr type object))

  #:export (MappingProxyType  FunctionType LambdaType ModuleType
                             MethodType BuiltinMethodType CodeType
                             AsyncGeneratorType CoroutineType GeneratorType
                             TracebackType FrameType WrapperDescriptorType
                             MethodWrapperType MethodDescriptorType
                             BuiltinFunctionType))
  
"""
Define names for built-in types that aren't directly accessible as a builtin.
"""
(define BuiltinFunctionType    #:BuiltinFunctionType)
(define MappingProxyType       dict)
(define FunctionType           #:Procedure)
(define LambdaType             #:Lambda)
(define MethodType             #:Method)
(define BuiltinMethodType      #:BuiltinMethodType)
(define CodeType               Code)
(define ModuleType             ModuleT)
(define AsyncGeneratorType     #:AsyncGeneratorType)
(define GeneratorType          #:GeneratorType)
(define CoroutineType          #:CoroutineType)
(define TracebackType          #:TracebackType)
(define FrameType              #:FrameType)
(define DynamicClassAttribute  #:DynamicClassAttribute)
(define ClassType              type)
(define InctanceType           object)
(define WrapperDescriptorType  #:Method)
(define MethodWrapperType      #:Method)
(define MethodDescriptorType   #:Method)
(define-python-class  DynamicClassAttribute ()
  "Route attribute access on a class to __getattr__.

  This is a descriptor, used to define attributes that act differently when
  accessed through an instance and through a class.  Instance access remains
  normal, but access to an attribute through a class will be routed to the
  class's __getattr__ method; this is done by raising AttributeError.

  This allows one to have properties active on an instance, and have virtual
  attributes on the class with the same name (see Enum for an example).
  "
  
  (define __init__
    (lam (self (= fget None) (= fset None) (= fdel None) (= doc None))
         (set self 'fget fget)
         (set self 'fset fset)
         (set self 'fdel fdel)
         
         ;; next two lines make DynamicClassAttribute act the same as property
         (set self '__doc__ (or (bool doc) (ref fget '__doc__)))
         (set self 'overwrite_doc  (eq? doc  None))
         
         ;; support for abstract methods
         (set self '__isabstractmethod__
              (bool (getattr fget '__isabstractmethod__' #f)))))

  (define __get__
    (lam (self instance (= ownerclass None))         
         (cond
          ((eq? instance None)
           (if (bool (ref self '__isabstractmethod__))
               self
               (raise (AttributeError "ownerclass"))))
          ((eq? (ref self 'fget) None)
           (raise (AttributeError "unreadable attribute")))
          (else
           ((ref self 'fget) instance)))))

  (define __set__
    (lambda (self instance value)
      (if (eq? (ref self 'fset) None)
          (raise (AttributeError "can't set attribute"))
          ((ref self 'fset) instance value))))

  (define __delete__
    (lambda (self instance)
      (if (eq? (ref self 'fdel) None)
          (raise (AttributeError "can't delete attribute"))
          ((ref self 'fdel) instance))))

  (define getter
    (lambda (self fget)
      (let* ((fdoc (if (ref self 'overwrite_doc)
                       (ref fget '__doc__)
                       None))
             (result ((type self)
                      fget
                      (ref self 'fset)
                      (ref self 'fdel)
                      (or (bool fdoc) (ref self '__doc__)))))
        
        (set result 'overwrite_doc (ref self 'overwrite_doc))
        result)))

  (define setter
    (lambda (self fset)
      (let ((result ((type self)
                     (ref self 'fget)
                     fset
                     (ref self 'fdel)
                     (ref self '__doc__))))
        
        (set result 'overwrite_doc (ref self 'overwrite_doc))
        result)))
  
  (define deleter
    (lambda (self fdel)
      (let ((result ((type self)
                     (ref self 'fget)
                     (ref self 'fset)
                     fdel
                     (ref self '__doc__))))
        
        (set result 'overwrite_doc (ref self 'overwrite_doc))
        result))))

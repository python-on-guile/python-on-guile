;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module _posixsubprocess)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python module python)
  #:use-module (language python bool)
  #:use-module (language python module os)
  #:use-module (language python module errno)
  #:use-module (language python list)
  #:use-module (ice-9 threads)
  #:use-module (language python exceptions)
  
  #:export (fork_exec))

(define (child_exec exec_array argv envp cwd
                    p2cread p2cwrite
                    c2pread c2pwrite
                    errread errwrite
                    errpipe_read errpipe_write
                    closed_fds restore_signals
                    call_setsid
                    fds_to_keep preexec_fn)

  (define errwrite #f)
  (define execmsg  "")
  (define msg      "")
  (define fds_to_close '())

  (catch #t
    (lambda ()

  (if (> p2cwrite 2)
      (close-fdes p2cwrite))
  (if (> c2pread 2)
      (close-fdes c2pread))
  (if (> errread 2)
      (close-fdes errread))
  (if (> errpipe_read 2)
      (close-fdes errpipe_read))

  (when (= c2pwrite 0)
    (set! c2pwrite (dup c2pwrite)))

  (when (> p2cread 0)
    (dup2 p2cread 0)
    (set_inheritable 0 #f))

  (when (> c2pwrite 1)
    (dup2 c2pwrite 1)
    (set_inheritable 1 #f))
  
  (if errwrite
      (when (> errwrite 2)
        (dup2 errwrite 2)
        (set_inheritable 2 #f)))
      

  (let lp ()
    (if errwrite
        (when (or (= errwrite 0) (= errwrite 1))
          (set! errwrite (dup errwrite))
          (lp))))

  (if (> p2cread 2)
      (close-fdes p2cread))

  (if (and c2pwrite (> c2pwrite 2) (not (= c2pwrite p2cread)))
      (close-fdes c2pwrite))
  
  (if (and errwrite (> errwrite 2) (not (= errwrite p2cread))
           (not (= errwrite c2pwrite)))
      (close-fdes errwrite))

  (if (bool cwd)
      (chdir cwd))

  (if (bool call_setsid)
      (setsid))

  (if (bool preexec_fn)
      (try
       (preexec_fn)
       (#:except #t (set! msg "Exception occured in preexec_fn"))))

  (if (bool closed_fds)
      (for ((fd : fds_to_close)) ()
           (close-fdes fd)))
  
  (let ((argv (to-list argv))
        (envp (if (bool envp) (to-list envp) envp)))
    (for ((e : exec_array)) ((ep #f))
         (try
          (lambda ()
            (if (bool envp)
                (apply execle e (append argv (list envp)))
                (apply execl  e argv)))
          (#:except
           #t
           =>
           (lambda x             
             (if (not execmsg)
                 (set! execmsg ""))
             (set! execmsg
               (+ execmsg ((@ (guile) format) #f " exec error: ~a~%" x))))))

         (let ((er (errno)))
           (if (and (not (= er ENOENT)) (not (=  er ENOTDIR)) (not ep))
               er
               ep))
             
         #:final
         (if ep (set_errno ep) (set_errno 0))))
  
  (if errwrite
      (write errpipe_write errwrite))

  (if execmsg
      (write errpipe_write execmsg))

  (if (errno)
      (write errpipe_write ((@ (guile) format) #f
                                "exec failed with errno ~a" (errno)))))
    (lambda x
      (values)))
  (primitive-exit 1))

     

  
  

(define (fork_exec process_args executable_list
                   close_fds fds_to_keep
                   cwd env_list
                   p2cread p2cwrite c2pread c2pwrite
                   errread errwrite
                   errpipe_read errpipe_write
                   restore_signals start_new_session preexec_fn)

  (define call_setsid #f)
  
  (if (and (bool close_fds) (< errpipe_read 3))
      (raise (ValueError "errpipe_write must be >= 3")))

  (for ((fd : fds_to_keep)) ()
       (if (not (isinstance fd int))
           (raise (ValueError "bad values(s) in fds_to_keep"))))

  (set_blocking      errpipe_read  #f)
  (set_blocking      errpipe_write #f)

  (let ((pid (primitive-fork)))
    (if (= pid 0)
        (child_exec executable_list
                    process_args
                    env_list
                    cwd
                    p2cread
                    p2cwrite
                    c2pread
                    c2pwrite
                    errread
                    errwrite
                    errpipe_read
                    errpipe_write
                    close_fds
                    restore_signals
                    call_setsid
                    fds_to_keep
                    preexec_fn)
        (begin
          #;(if (> c2pwrite 2)
              (close c2pwrite))
          #;(if (> errwrite 2)
              (close errwrite))
          #;(if (> p2cread 2)
              (close p2cread))
          pid))))

                      


;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python module string_)
  #:use-module (language python list)
  #:use-module (language python dict)
  #:use-module (language python for)
  #:use-module (language python def)
  #:use-module (language python string)
  #:use-module (language python property)
  #:use-module (language python exceptions)
  #:use-module ((ice-9 match) #:select (match))
  #:use-module (oop pf-objects)
  #:use-module ((language python module re) #:select
                ((compile    . re.compile)
                 (escape     . re.escape)
                 (VERBOSE    . re.VERBOSE)
                 (IGNORECASE . re.IGNORECASE))))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define build
  (lambda (prefix idpat bracepat error flags)
    (let ((pre (re.escape prefix)))
      (if (eq? error None) (set! error ""))
      (if (eq? bracepat None) (set! bracepat idpat))
      (set! idpat (ref idpat 'pattern))
      (set! bracepat (ref bracepat 'pattern))
      (re.compile
       (+ pre
          "(?:"
          (+ "(?P<escaped>"  pre      ")")
          "|"
          (+ "(?P<named>"    idpat    ")")
          "|"
          (+ "{(?P<braced>{" bracepat ")}")
          "|"
          (+ "(?P<invalid>"  error    "))"))
       (logior flags re.VERBOSE flags)))))

(define (build-it cls)
  (set cls '_pattern
       (build (ref cls '_delimiter)
              (ref cls '_idpattern)
              (ref cls '_braceidpattern)
              (ref cls '_invalid)
              (ref cls '_flags))))

(define-python-class TempType (type)
  (define __new__
    (lambda (cls name bases dict . l)
      (let ((d1 (make-hash-table))
            (d2 (make-hash-table)))
        (for ((k v : dict)) ()
             (if (in k '("pattern" "delimiter" "idpattern" "braceidpattern"
                         "flags"))
                 (hash-set! d1 k v)
                 (hash-set! d2 k v)))
        (let ((r (apply (rawref type '__new__) cls name bases d2 l)))
          (for ((k v : d1)) ()
               (set r k v))
          r))))
  
  (define delimiter
    (property
     (lambda (cls)
       (ref cls '_delimiter))
     (lambda (cls val)
       (if (> (string-length val) 1) (set cls '_adv #t))
       (set cls '_delimiter val)
       (build-it cls))))
  
  (define idpattern
    (property
     (lambda (cls)
       (ref cls '_idpattern))
     (lambda (cls r)
       (set cls '_adv #t)
       (set cls '_idpattern r)
       (build-it cls))))
    
  (define braceidpattern
    (property
     (lambda (cls)
       (ref cls '_braceidpattern))
     (lambda (cls r)
       (set cls '_adv #t)
       (set cls '_braceidpattern r)
       (build-it cls))))

  (define pattern
    (property
     (lambda (cls)
       (let ((pat (ref cls '_pattern)))
         (if (eq? pat None)
             (build
              (ref cls '_delimiter)
              (ref cls '_idpattern)
              (ref cls '_braceidpattern)
              (ref cls '_invalid)
              (ref cls '_flags))
             pat)))
     
     (lambda (cls r)
       (set cls '_adv #t)
       (set cls '_pattern r))))

  (define flags
    (property
     (lambda (cls)
       (ref cls '_flags))
     (lambda (cls f)
       (set cls '_adv   #t)
       (set cls '_flags f)
       (build-it cls)))))
  

(define adv-templating
  (lam (self p? (= mapping (dict)) (** kwds))      
       (let* ((cls (ref self '__class__))
              (r   (ref cls 'pattern))
              (i?  (> (logand re.IGNORECASE (ref cls 'flags)) 0)))
         
         (define (get s)
           (if i? (set! s (string-downcase s)))
           (aif it (py-get kwds s)
                (scm-str it)
                (py-get mapping s)))

         (let lp ((l (to-list ((ref r 'split) (ref self 'template))))
                  (r '()))
           (define (er1 s a l)
             (if p?
                 (lp l (cons* s a r))
                 (raise (KeyError (+ "missing key, " + a)))))
           
           (define (er2)
             (raise (ValueError "wrong format after delimiter")))
           
           (match l
             ((s q a b e . l)
              (cond
               ((not (eq? q None))
                (lp l (cons* q s r)))
               
               ((not (eq? a None))
                (aif it (get a)
                     (lp l (cons* it s r))
                     (er1 s a l)))
               
               ((not (eq? b None))
                (aif it (get a)
                     (lp l (cons* it s r))
                     (er1 s a l)))
               
               ((not (eq? e None))
                (er2))
               
               (else
                (lp l (cons s r)))))

             ((x)
              (lp '() (cons x r)))
             
             (()
              (string-join (reverse r) "")))))))
               

(define __substitute__
  (lam (self p? (= mapping (dict)) (** kwds))
       (let* ((cls (ref self '__class__))
              (ch (car (string->list (ref cls 'delimiter))))
              (ch? (lambda (x) (eqv? x ch)))
              (i? (> (logand re.IGNORECASE (ref cls 'flags)) 0)))

         (define (get s)
           (if i? (set! s (string-downcase s)))
           (aif it (py-get kwds s)
                (scm-str it)
                (py-get mapping s)))

         (let lp ((l (string->list (ref self 'template))) (r '()))
           (define (ok ch)
             (and (< (char->integer ch) 128)
                  (or
                   (char-alphabetic? ch)
                   (char-numeric? ch)
                   (eq? ch #\_))))
           (define (er1)
             (raise (TypeError
                     "char mot in [a-zA-Z_0-9] for template identifier")))
           
           (define (er2)
             (raise (TypeError
                     "char mot an ending } for template identifier")))
           
           (define (er3 m l s)
             (if p?
                 (lp l (append s r))
                 (raise (KeyError (+ "could not find template identifier "
                                     m)))))
           (match l
             (((? ch?) (? ch?) . l)
              (lp l (cons ch r)))
           
             (((? ch?) #\{ . l)
              (let* ((rr.l.s (let lp2 ((l l) (r '()) (s '(#\$ #\{)))
                               (match l
                                 ((#\} . l)
                                  (cons* (list->string (reverse r)) l
                                         (cons #\} s)))
                                 ((x . l)
                                  (if (ok x)
                                      (lp2 l (cons x r) (cons x s))
                                      (er1)))
                                 (() (er2)))))
                     (rr (car  rr.l.s))
                     (l  (cadr rr.l.s))
                     (s  (cddr rr.l.s)))
                
                (aif it (get rr)
                     (lp l (append (reverse (string->list (scm-str it))) r))
                     (er3 rr l s))))
             
             (((? ch?) . l)
              (let* ((rr.l.s (let lp2 ((l l) (r '()) (s '(#\$)))
                               (match l
                                 ((x . l)
                                  (if (ok x)
                                      (lp2 l (cons x r) (cons x s))
                                      (cons* (list->string (reverse r))
                                             (cons x l) s)))
                                 (()
                                  (cons* (list->string (reverse r)) '() s)))))
                     (rr (car  rr.l.s))
                     (l  (cadr rr.l.s))
                     (s  (cddr rr.l.s)))
                (aif it (get rr)
                     (lp l (append (reverse (string->list (scm-str it))) r))
                     (er3 rr l s))))
             
             ((x . l)
              (lp l (cons x r)))

             (()
              (list->string (reverse r))))))))

(define-python-class Template (#:metaclass TempType)
  (define  _adv            #f)
  
  (define  _idpattern      (re.compile "[_a-z][_a-z0-9]*"
                                       (logior
                                        re.IGNORECASE
                                        re.VERBOSE)))
  
  (define  _braceidpattern None)
  
  (define  _delimiter      "$")
  
  (define  _invalid        None)
  
  (define  _flags          re.IGNORECASE)

  (define _pattern None)
  
  (define __init__
    (lambda (self str)
      (set self 'template str)))

  (define substitute
    (lambda (self . x)
      (if (ref (ref self '__class__) '_adv)
          (apply adv-templating self #f x)
          (apply __substitute__ self #f x))))

  (define safe_substitute
    (lambda (self . x)
      (if (ref (ref self '__class__) '_adv)
          (apply adv-templating self #t x)
          (apply __substitute__ self #t x)))))

(set! (@ (language python module string) Template) Template)

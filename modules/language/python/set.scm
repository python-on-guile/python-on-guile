;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python set)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (language python type)
  #:use-module (language python exceptions)
  #:use-module (language python dict)
  #:use-module (language python for)
  #:use-module (language python try)
  #:use-module (language python bytes)
  #:use-module (language python list)
  #:use-module (language python yield)
  #:use-module (language python persist)
  #:use-module (language python bool)
  #:export (py-set frozenset weak-set <set>
                   fastlist-ref fastlist-set! fast-get fastlist-slice
		   set-ackumulator))

(define-class <set> () dict)
(name-object <set>)

(cpit <set>
      (o (lambda (o a)
	   (slot-set! o 'dict
		      (let ((h (make-py-hashtable)))
			(let lp ((a a))
			  (if (pair? a)
			      (begin
				(py-hash-set! h (caar a) (cdar a))
				(lp (cdr a))))))))
	 (list
	  (hash-fold (lambda (k v s) (cons (cons k v) s))
		     '()
		     (slot-ref o 'dict)))))


(define miss (list 'miss))

(define-method (< (o1 <set>) ( o2 <set>))
  (and (not (equal? o1 o2))
       (for-in ((k : o1)) ()
	  (if (in k o2)
	      (values)
	      (break #f))
	  #:final #t)))

(define-method (> (o1 <set>) ( o2 <set>))
  (and (not (equal? o1 o2))
       (for-in ((k : o2)) ()
            (if (in k o1)
                (values)
                (break #f))
            #:final #t)))

(define-method (<= (o1 <set>) ( o2 <set>))
  (for-in ((k : o1)) ()
       (if (in k o2)
           (values)
           (break #f))
       #:final #t))

(define-method (>= (o1 <set>) ( o2 <set>))
  (for-in ((k : o2)) ()
       (if (in k o1)
           (values)
           (break #f))
       #:final #t))

(define-method (in k (o <set>))
  (in k (slot-ref o 'dict)))

(define-method (len (o <set>))
  (for ((x : o)) ((n 0))
       (+ n 1)
       #:final n))
  
(define-python-class set (object <set>)
  (define __init__
    (case-lambda
      ((self)       
       (slot-set! self 'dict (make-py-hashtable)))
      ((self x)
       (let ((d (make-py-hashtable)))
         (slot-set! self 'dict d)
         (if (eq? x '())
             (values)
	     (for ((y : x)) ()
               (pylist-set! d y #t)))))))

  (define __bool__
    (lambda (self)
      (bool (slot-ref self 'dict))))
      
  (define pop
    (lambda (self)
      (call-with-values (lambda () (pylist-pop! (slot-ref self 'dict)))
        (lambda (k v) k))))
  
  (define  add
    (lambda (self k)
      (pylist-set! (slot-ref self 'dict) k #t)))

  (define copy
    (lambda (self)
      (let ((dict (py-copy (slot-ref self 'dict))))
        (set dict))))

  (define difference
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (catch #t
                  (lambda ()
                    (for ((x : (car l))) ()
                         (when (in x d)
                           (pylist-delete! r x))))
                  (lambda x
                    (raise (TypeError "Not a sequence object" (car l)))))
                (lp (cdr l)))))
        (set r))))

  (define difference_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x r)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (values))))
  
  (define discard
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (pylist-delete! r (car l))
                (lp (cdr l))))))))
  
  (define intersection
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                     (if (not (__contains__ y k))
                         (cons k dels)
                         dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l)))))
        (set r))))

  (define intersection_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                  (if (not (__contains__ y k))
                      (cons k dels)
                      dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l))))))))

  (define isdisjoint
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict))
             (n1 (fastlist-len r))
             (n2 (fastlist-len x)))
        (if (< n2 n1)
            (let ((xx x))
              (set! x r)
              (set! r xx)))
        (for ((k v : r)) ()
             (if (in k x)
                 (break #f))
             #:final
             #t))))
  
  (define issubset
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict)))
        (for ((k v : r)) ()
          (if (not (__contains__ x k))
              (break #f))
          #:final
          #t))))

  (define issuperset
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (for ((x v : r)) ()
          (if (not (in x r))
              (break #f))
          #:final
          #t))))

  (define remove
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (if (not (in x r))
            (raise KeyError "missing key in set at remove")
            (pylist-delete! r x)))))

  (define symmetric_difference
    (lambda (self x)
      (union (difference self x) (difference x self))))
  
  (define symmetric_difference_update
    (lambda (self x)
      (difference_update self x)
      (update self (difference x self))))

  (define union
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for-in ((k : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (set r))))))
  
  (define update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))             
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k v : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (values))))))
  
  (define __repr__
    (lambda (self)
      (let* ((r (py-keys (slot-ref self 'dict)))
             (n (fastlist-len r))
             (l (map (lambda (x) (print-scm x write)) (to-list r))))
        (cond
         ((= n 0)
          (format #f "set([])"))
         (else
          (format #f "set([~a~{, ~a~}])" (car l) (cdr l)))))))

  (define __str__
    (lambda (self)
      (let* ((r (py-keys (slot-ref self 'dict)))
             (n (fastlist-len r))
             (l (map (lambda (x) (print-scm x display)) (to-list r))))
        (cond
         ((= n 0)
          (format #f "set([])"))
         (else
          (format #f "set([~a~{, ~a~}])" (car l) (cdr l)))))))

  (define __contains__
    (lambda (self x)
      (let* ((d (slot-ref self 'dict))
             (t (slot-ref d    't)))
        (not (eq? miss (py-hash-ref t x miss))))))

  (define __and__
    (lambda (self op)
      (intersection self op)))

  (define __or__
    (lambda (self op)
      (union self op)))

  (define __sub__
    (lambda (self op)
      (difference self op)))

  (define __xor__
    (lambda (self op)
      (symmetric_difference self op)))

  (define __lt__
    (lambda (self other)
      (for ((k : self)) ()
           (if (in k other)
               (values)
               (break #f))
           #:final #t)))

  (define __le__
    (lambda (self other)
      (and (not (__eq__ self other))
           (__le__ self other))))
  
  (define __ge__
    (lambda (self other)
      (not (__lt__ other self))))

  (define __gt__
    (lambda (self other)
      (not (__le__ other self))))
    
  (define __eq__
    (lambda (self x)
      (and
       (__lt__ self x)
       (__lt__ x    self))))
  
  (define __iter__
    (lambda (self)
      ((make-generator
        (lambda (yield)
          (for ((k v : (slot-ref self 'dict))) ()
               (yield k)
               (values))))))))

(define-python-class weak-set (object <set>)
  (define __init__
    (case-lambda
      ((self)       
       (slot-set! self 'dict (make-py-weak-key-hashtable)))
      ((self x)
       (let ((d (make-py-weak-key-hashtable)))
         (slot-set! self 'dict d)
         (if (eq? x '())
             (values)
	     (for-in ((y : x)) ()
	       (pylist-set! d y #t)))))))

  (define __bool__
    (lambda (self)
      (bool (slot-ref self 'dict))))
      
  (define pop
    (lambda (self)
      (call-with-values (lambda () (pylist-pop! (slot-ref self 'dict)))
        (lambda (k v) k))))
  
  (define  add
    (lambda (self k)
      (pylist-set! (slot-ref self 'dict) k #t)))

  (define copy
    (lambda (self)
      (let ((dict (py-copy (slot-ref self 'dict))))
        (set dict))))

  (define difference
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x d)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (set r))))

  (define difference_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x r)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (values))))
  
  (define discard
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (pylist-delete! r (car l))
                (lp (cdr l))))))))
  
  (define intersection
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                     (if (not (__contains__ y k))
                         (cons k dels)
                         dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l)))))
        (set r))))

  (define intersection_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                  (if (not (__contains__ y k))
                      (cons k dels)
                      dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l))))))))

  (define isdisjoint
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict))
             (n1 (fastlist-len r))
             (n2 (fastlist-len x)))
        (if (< n2 n1)
            (let ((xx x))
              (set! x r)
              (set! r xx)))
        (for ((k v : r)) ()
             (if (in k x)
                 (break #f))
             #:final
             #t))))
  
  (define issubset
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict)))
        (for ((k v : r)) ()
          (if (not (__contains__ x k))
              (break #f))
          #:final
          #t))))

  (define issuperset
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (for ((x v : r)) ()
          (if (not (in x r))
              (break #f))
          #:final
          #t))))

  (define remove
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (if (not (in x r))
            (raise KeyError "missing key in set at remove")
            (pylist-delete! r x)))))

  (define symmetric_difference
    (lambda (self x)
      (union (difference self x) (difference x self))))
  
  (define symmetric_difference_update
    (lambda (self x)
      (difference_update self x)
      (update self (difference x self))))

  (define union
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (set r))))))
  
  (define update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))             
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k v : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (values))))))
  
  (define __repr__
    (lambda (self)
      (let* ((r (py-keys (slot-ref self 'dict)))
             (n (fastlist-len r))
             (l (to-list r)))
        (cond
         ((= n 0)
          (format #f "set([])"))
         (else
          (format #f "set([~a~{, ~a~}])" (car l) (cdr l)))))))

  (define __contains__
    (lambda (self x)
      (let* ((d (slot-ref self 'dict))
             (t (slot-ref d    't)))
        (not (eq? miss (py-hash-ref t x miss))))))

  (define __and__
    (lambda (self op)
      (intersection self op)))

  (define __or__
    (lambda (self op)
      (union self op)))

  (define __sub__
    (lambda (self op)
      (difference self op)))

  (define __xor__
    (lambda (self op)
      (symmetric_difference self op)))

  (define __lt__
    (lambda (self other)
      (for ((k : self)) ()
           (if (in k other)
               (values)
               (break #f))
           #:final #t)))

  (define __le__
    (lambda (self other)
      (and (not (__eq__ self other))
           (__le__ self other))))
  
  (define __ge__
    (lambda (self other)
      (not (__lt__ other self))))

  (define __gt__
    (lambda (self other)
      (not (__le__ other self))))
    
  (define __eq__
    (lambda (self x)
      (and
       (__lt__ self x)
       (__lt__ x    self))))
  
  (define __iter__
    (lambda (self)
      ((make-generator
	  (lambda (yield)
	    (for ((k v : (slot-ref self 'dict))) ()
		 (yield k)
		 (values))))))))

(define py-set set)
(define-python-class frozenset (set))

;;; Sequence fast methods (to be fast to basic datastructures)

(define <g-dict>  (ref dict       '__goops__))
(define <g-list>  (ref pylist     '__goops__))
(define <g-bytes> (ref bytes      '__goops__))
(define <g-bytea> (ref bytearray  '__goops__))

(define-syntax m
  (syntax-rules ()
    ((_ (a ...) (b ...))
     (b ... a ...))
    ((_ a       (b ...))
     (apply b ... a))))

;;; --------------------- MKFAST-SEQ ------------------------------------------
(define-syntax-rule (mkfast-seq fastlist-ref py-list-ref pylist-ref
                                vector-ref list-ref string-ref py-bytes-ref
                                py-bytea-ref l)
    (define (fastlist-ref x . l)
      (let ((xx x))
        (cond
         ((vector? xx)
          (m l (vector-ref xx)))
         
         ((struct? xx)
          (let ((cls (class-of xx)))
            (cond
             ((eq? cls <g-list>)
              (m l (py-list-ref xx)))
               
             ((eq? cls <g-dict>)
              (m l (py-dict-ref xx)))
           
             ((eq? cls <g-bytes>)
              (m l (py-bytes-ref xx)))

             ((eq? cls <g-bytea>)
              (m l (py-bytea-ref xx)))
           
             (else
              (m l (pylist-ref xx))))))

         ((string? xx)
          (m l (string-ref xx)))
         
         ((pair? xx)
          (m l (list-ref xx)))
         
         (else
          (m l (pylist-ref xx)))))))



(define py-list-slice
  (resolve-method-g pylist-slice `(,<py-list>      _ _ _)))
(define py-bytes-slice
  (resolve-method-g pylist-slice `(,<py-bytes>     _ _ _)))
(define py-bytea-slice
  (resolve-method-g pylist-slice `(,<py-bytearray> _ _ _)))
(define vector-slice
  (resolve-method-g pylist-slice `(,<vector>       _ _ _)))
(define list-slice
  (resolve-method-g pylist-slice `(,<pair>         _ _ _)))
(define string-slice
  (resolve-method-g pylist-slice `(,<string>       _ _ _)))

(mkfast-seq fastlist-slice py-list-slice pylist-slice
            vector-slice list-slice string-slice py-bytes-slice
            py-bytea-slice (i2 i3))

;;;-------------------------- MKFAST ----------------------------

(define-syntax-rule (mkfast fastlist-ref py-list-ref pylist-ref
                            vector-ref list-ref string-ref
                            py-dict-ref py-bytes-ref py-bytea-ref l)
  (define (fastlist-ref x i . l)
    (let ((xx x) (ii i))
      (if (number? ii)
          (cond
           ((vector? xx)
            (m l (vector-ref xx ii)))
           
           ((struct? xx)
            (let ((cls (class-of xx)))
              (cond
               ((eq? cls <g-list>)
                (m l (py-list-ref xx ii)))
               
               ((eq? cls <g-bytes>)
                (m l (py-bytes-ref xx ii)))

               ((eq? cls <g-bytea>)
                (m l (py-bytea-ref xx ii)))

               (else
                (m l (pylist-ref xx ii))))))

           ((string? xx)
            (m l (string-ref xx ii)))
         
           ((pair? xx)
            (m l (list-ref xx ii)))
         
           (else
            (m l (pylist-ref xx ii))))
        
          (cond
           ((struct? xx)
            (let ((cls (class-of xx)))
              (cond
               ((eq? cls <g-list>)
                (m l (py-list-ref xx ii)))
               
               ((eq? cls <g-bytes>)
                (m l (py-bytes-ref xx ii)))

               ((eq? cls <g-bytea>)
                (m l (py-bytea-ref xx ii)))

               (else
                (m l (pylist-ref xx ii))))))
         
           (else
            (m l (pylist-ref xx ii))))))))

(define py-dict-ref   (ref dict '__getitem__))
(define py-list-ref   (resolve-method-g pylist-ref `(,<py-list>      _)))
(define py-bytes-ref  (resolve-method-g pylist-ref `(,<py-bytes>     _)))
(define py-bytea-ref  (resolve-method-g pylist-ref `(,<py-bytearray> _)))
(define py-vector-ref (resolve-method-g pylist-ref `(,<vector>       _)))
(define py-string-ref (resolve-method-g pylist-ref `(,<string>       _)))
(define py-list-ref   (resolve-method-g pylist-ref `(,<pair>         _)))
(mkfast fastlist-ref py-list-ref pylist-ref
        py-vector-ref py-list-ref py-string-ref py-dict-ref
        py-bytes-ref py-bytea-ref ())



(define py-dict-get  (ref dict 'get))
(define py-list-get  (resolve-method-g py-get `(,<py-list>      <integer> . _)))
(define py-bytes-get (resolve-method-g py-get `(,<py-bytes>     <integer> . _)))
(define py-bytea-get (resolve-method-g py-get `(,<py-bytearray> <integer> . _)))
(define vector-get   (resolve-method-g py-get `(,<vector>       <integer> . _)))
(define list-get     (resolve-method-g py-get `(,<pair>         <integer> . _)))
(define string-get   (resolve-method-g py-get `(,<string>       <integer> . _)))

(mkfast fast-get py-list-get py-get
        vector-get list-get string-get py-dict-get py-bytes-get
        py-bytea-get l)



(define py-dict-set!   (ref dict '__setitem__))
(define py-list-set!   (resolve-method-g pylist-set! `(,<py-list>      _ _)))
(define py-bytes-set!  (resolve-method-g pylist-set! `(,<py-bytes>     _ _)))
(define py-bytea-set!  (resolve-method-g pylist-set! `(,<py-bytearray> _ _)))
(define py-vector-set! (resolve-method-g pylist-set! `(,<vector>       _ _)))
(define py-string-set! (resolve-method-g pylist-set! `(,<string>       _ _)))
(define py-list-set!   (resolve-method-g pylist-set! `(,<pair>         _ _)))

(mkfast fastlist-set! py-list-set! pylist-set!
        py-vector-set! py-list-set! py-string-set! py-dict-set!
        py-bytes-set! py-bytea-set! (val))


(define (set-ackumulator)
  (let ((df (dict-ackumulator)))
    (case-lambda
     ((k) (df k #t))
     (()
      (let ((s (set)))
	(slot-set! s 'dict (df))
	s)))))


;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python memoryview)
  #:use-module (oop pf-objects)
  #:use-module (language python exceptions)
  #:use-module (language python try)
  #:use-module ((language python module _python) #:select (isinstance bytes))
  #:export (memoryview))

(define-python-class memoryview ()
  (define __init__
    (lambda (self obj)
      (cond
       ((isinstance obj bytes)
        (begin
          (set self 'obj obj)
          (set self 'format "B")
          (set self 'ndim   1)))
       (else
        (raise (TypeError "not a supported memoryview object")))))))
      
            
            


  

;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python inspect)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  #:use-module (language python string)
  #:use-module (language python exceptions)
  #:use-module (language python def)
  #:use-module (language python for)
  #:use-module (language python module)
  #:use-module (language python tuple)
  #:use-module (language python list)
  #:use-module (language python number)
  #:use-module (language python bytes)
  #:use-module (language python dict)
  #:use-module (language python bool)
  #:use-module (language python property)
  #:use-module (system vm program)
  #:export (get-source-file
            get-module isinstance
            issubclass
            isinstance
            sequence?
            mapping?))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define scmlang     ((@@ (system base language) lookup-language)   'scheme))
(define scm-eval    ((@@ (system base language) language-evaluator) scmlang))
(define scm-reader  ((@@ (system base language) language-reader)    scmlang))
(define (scm-eval-string x m)
  (scm-eval (with-input-from-string x (lambda ()
                                        (catch #t
                                          (lambda () (scm-reader
                                                      (current-input-port)
                                                      m))
                                          (lambda x (pk x))))) m))

(define eval_scm
  (case-lambda
    ((s)   (scm-eval-string s (current-module)))
    ((s m) (scm-eval-string s
                            (catch #t
                              (lambda () (rawref m '_module))
                              (lambda x m))))))

(define-method (issubclass x y) #f)

(define-method (issubclass x (y <pair>))
  (or
   (issubclass x (car y))
   (let ((z (cdr y)))
     (if (pair? z)
         (issubclass x z)
         #t))))

(define-method (issubclass x (y <vector>))
  (let ((n (vector-length y)))
    (let lp ((i 0))
      (if (< i n)
          (if (issubclass x (vector-ref y i))
              #t
              (lp (+ i 1)))
          #f))))

(define-method (issubclass (sub <p>) (cls <p>))
  (aif it (ref-in-class cls  '__subclasscheck__ #f)
       (it cls sub)
       (if (eq? sub cls)
	   #t
	   (if (memq cls (ref sub '__mro__))
               #t
               #f))))


(define (isinstance x y)
  (let lp ((y y))
    (cond
     ((keyword? y)
      (cond
       ((eq? y #:Method)
        (and
         (is-a? x <procedure>)
         (ref x '__self__ #f)))
     
       ((eq? y #:Procedure) 
        (and
         (is-a? x <procedure>)
         (not (ref x '__self__ #f))))

       ((eq? y #:Lambda)
        #f)

       ((eq? y #:GeneratorType)
        (and
         (ref x '__next__)))

       ((eq? y #:DynamicClassAttribute)
        #t)
     
       (else
        #f)))
   
     ((or (-isinstance y tuple)
          (-isinstance y list))
      (for ((yy : y)) ()
           (when (lp yy)
             (break #t))
           #:final #f))
   
     ((eq? y static-method)
      (and (is-a? x <procedure>)
           (not (ref x '__kind__ #f))))

     ((eq? y object-method)
      (and (is-a? x <procedure>)
           (eq? object-method (ref x '__kind__ #f))))

     ((eq? y class-method)
      (and (is-a? x <procedure>)
           (eq? class-method (ref x '__kind__ #f))))

     ((and (procedure? x) (not (struct? x)))
      #f)
   
     (else
      (-isinstance x y)))))

(define-method (-isinstance i y)
  #f)

(define-method (-isinstance i (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y i)))
   (else
    (next-method))))

(define-method (-isinstance (i <null>) y)
  (if (issubclass y tuple)
      #t
      (next-method)))

(define li (py-list '()))
(define-method (-isinstance (i <py-list>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y li)))
   (else
    (next-method))))

(define-method (-isinstance (i <py-list>) y)
  (if (issubclass y list)
      #t
      (next-method)))

(define-method (-isinstance (i <vector>) y)
  (if (issubclass y tuple)
      #t
      (next-method)))

(define in (py-int 1 #f))
(define-method (-isinstance (i <integer>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y in)))
   (else
    (next-method))))

(define-method (-isinstance (i <integer>) y)
  (if (issubclass y py-int)
      #t
      (next-method)))

(define r (py-float 0.0 #f))
(define-method (-isinstance (i <real>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y r)))
   (else
    (next-method))))

(define-method (-isinstance (i <real>) y)
  (if (issubclass y py-float)
      #t
      (next-method)))

(define c (py-complex 0.0 #f))
(define-method (-isinstance (i <complex>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y c)))
   (else
    (next-method))))

(define-method (-isinstance (i <complex>) y)
  (if (issubclass y py-complex)
      #t
      (next-method)))

(define t (tuple '() #f))
(define-method (-isinstance (i <pair>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y t)))
   (else
    (next-method))))
(define-method (-isinstance (i <vector>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y t)))
   (else
    (next-method))))

(define-method (-isinstance (i <pair>) y)
  (if (issubclass y tuple)
      #t
      (next-method)))

(define s (py-str "" #f))
(define-method (-isinstance (i <string>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y s)))
   (else
    (next-method))))

(define-method (-isinstance (i <string>) y)
  (if (issubclass y py-str)
      #t
      (next-method)))

(define b (bytes ""))
(define-method (-isinstance (i <bytevector>) (y <p>))
  (cond
   ((ref-in-class y '__instancecheck__ #f) =>
    (lambda (it)
      (it y b)))
   (else
    (next-method))))

(define-method (-isinstance (i <bytevector>) y)
  (if (issubclass y bytes)
      #t
      (next-method)))

(define-method (-isinstance (i <py-bytes>) y)
  (if (issubclass y bytes)
      #t
      (next-method)))

  
(define-method (-isinstance (o <p>) (cl <p>))
  (cond
   ((ref-in-class cl '__instancecheck__ #f) =>
    (lambda (it)
      (it cl o)))
   
   (else
    (is-a? o (ref cl '__goops__)))))

(define (end f)
  (if (equal? f "python-shell")
      f
      (if (or (py-startswith f "/home") (py-startswith f "/root"))
          f
          (let ((ff (%search-load-path f)))
            (if ff
                ff
                ((eval_scm
                  "(@ (language python module os path) abspath)")             
                 f))))))
  

(define (get-source-file f)
  (cond
   ((procedure? f)
    (let ((ff (ref f '__file__ #f)))
      (if ff
          (end ff)
          (let* ((s (program-sources f))
                 (x (if (pair? s) (car s) #f)))
            (if x
                (if (> (length x) 1)
                    (let ((f (list-ref x 1)))
                      (if f
                          (end f)
                          'None))
                    'None)
                'None)))))
   ((struct? f)
    (let ((f (ref f '__file__)))
      (if f
          (end f)
          'None)))
   (else
    'None)))

(def (get-module obj (= _filename #f))
   (define (nm ff ext p?)
     (define (mod x)
       (string-append x ext))
     
     (define (modify l)
       (cons (mod (car l)) (cdr l)))

     ((if p? (lambda (x) x) end)
      (let lp ((l  (string-split ff #\.))
	       (r '("module" "python" "language")))
	(if (pair? l)
	    (lp (cdr l) (cons (car l) r))
	    (string-join (reverse (modify r)) "/")))))

  (define (f)       
    (let lp ((_filename _filename) (p? #t))                
      (if (bool _filename)	    
	  (for ((k v : modules)) ()
	    (let ((kk (nm k ".py" p?)))
	      (cond
	       ((equal? kk _filename) 
		(break (pylist-ref modules k)))
	      
	       ((equal? (end kk) _filename)
		(break (pylist-ref modules k)))

	       (else
		(let ((kk (nm k ".scm" p?)))
		  (cond
		   ((equal? kk _filename) 
		    (break (pylist-ref modules k)))
		   
		   ((equal? (end kk) _filename) 
		    (break (pylist-ref modules k)))

		   (else
		    (values)))))))

	    #:final None)
	  
	  (if p?
	      (lp ((eval_scm
		    "(@ (language python module os path) abspath)")
		   _filename) #f)
	      None))))
                        
  (if (isinstance obj Module)
      obj
      (aif it (ref obj '__module__ #f)
	   (aif it (pylist-ref modules it)
		it
		(f))
	   (f))))
                  

(define (get-source-span f) (list 0 0))

(define (sequence? x)
  (or
   (list? x)
   (vector? x)
   (is-a? x <py-list>)
   (isinstance x list)))

(define (mapping? x)
  (or
   (hash-table? x)
   (isinstance x dict)))

(set! (@ (oop dict) isinstance-) isinstance)
(set! (@ (oop dict) issubclass-) issubclass)


;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (language python yield)
  #:use-module (oop pf-objects)
  #:use-module (language python exceptions)
  #:use-module (persist persistance)
  #:use-module (oop goops)
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (language python persist)
  #:use-module (language python arg)
  #:replace (send)
  #:re-export (arg-empty produce-arg handle-arg)
  #:export (<yield> <stis-iter> yield
            in-yield define-generator
            make-generator
            sendException sendClose
            yield-from yield-f yield-raw send-g))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-class <stis-iter> () x l)

(define (make-stis-iter x)
  (let ((o (make <stis-iter>)))
    (struct-set! o 0 x)
    (struct-set! o 1 '())
    o))

(define stis-next #f)
(eval-when (compile eval load)
  (begin
    (define stis      #f)
    (when (and #t
	       (module-defined? (resolve-module '(ice-9 stis)) 'make-generator))
      (set! stis (resolve-module '(ice-9 stis)))
      (process-use-modules '(((ice-9 stis)
			      #:select
			      ((return         . stis-return        )
			       (next           . stis-next          )
			       (make-generator . stis-make-generator))))))))

(define in-yield (make-fluid #f))
(name-object in-yield)

(define-syntax-parameter YIELD (lambda (x) #f))

(define-syntax yield-raw
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)
       (if stis
	   #'(YIELD x ...)

	   #'(begin
	       (fluid-set! in-yield #t)
	       (abort-to-prompt YIELD x ...))))
      (x
       (if stis
	   #'(lambda xx
	       (apply YIELD xx))
       
	   #'(lambda xx
	       (fluid-set! in-yield #t)
	       (apply abort-to-prompt YIELD xx)))))))

(define (g-muck y)
  (syntax-parameterize ((YIELD (lambda (x) #'y)))
    (lambda (g . xx)
      (let ((ret (apply yield-raw xx)))
	(if ret
	    (if g
		(g   ret)
		(ret #f))
	    (when g
	      (g #f)))))))

(define (y-muck y)
  (syntax-parameterize ((YIELD (lambda (x)
				 (syntax-case x ()
				   ((_ a ...)
				    #'(y a ...))
				   (_ #'y)))))
    (lambda xx
      (let ((ret (apply yield-raw xx)))
	(when ret
	  (ret #f))))))

(define-syntax yield-g
  (lambda (x)
    (syntax-case x ()
      ((_ #f x ...)
       #'(let ((ret (yield-raw x ...)))
	   (when ret
	     (ret #f))))

      ((_ g x ...)
       #'(g (yield-raw x ...)))
               
      (x
       #'(g-muck YIELD)))))

(define-syntax yield
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)       
       #'(yield-g #f x ...))     
      (x
       #'(y-muck YIELD)))))

(define-syntax-rule (stis-run x it)
  (let ((ret (stis-return x it)))
    (if (eq? ret 'query-stis-stack)
	(stis-return x x)
	ret)))

(define-syntax-rule (stisser-mac x a ...)
  (stis-run x (produce-arg a ...)))

(define (stisser-muck x)
  (case-lambda
   ((a      ) (stisser-mac x a))
   ((a b    ) (stisser-mac x a b))
   ((       ) (stisser-mac x))
   ((a b . c)
    (stis-run x (apply produce-arg a b c)))))

(define-syntax-rule (stisser nm x)
  (define-syntax nm
    (lambda (xx)
      (syntax-case xx ()
	((_ a (... ...))
	 #'(stisser-mac x a (... ...)))
	
	(_
	 #'(stisser-muck x))))))

(define-syntax make-generator-stis
  (lambda (x)
    (syntax-case x ()
      ((_ (lambda (y a ...) code ...))
       (eq? (syntax->datum #'lambda) 'lambda)
       #'(lambda (a ...)
	   (make-stis-iter
	    ((stis-make-generator
	      (lambda (stack)
		(stisser y stack)
		code ...))))))

      ((_ f)
       #'(lambda args
	   (make-stis-iter 
	    (let ()
	      ((stis-make-generator
		(lambda (stack)
		  (stisser ss stack) 
		  (apply f ss args))))))))

      ((_ f _)
       #'(lambda args
	   (make-stis-iter 
	    (let ()
	      ((stis-make-generator
		(lambda (stack)
		  (stisser ss stack)
		  (syntax-parameterize ((YIELD
					 (lambda (x)
					   (syntax-case x ()
					     ((_ a (... ...))
					      #'(ss a (... ...)))
					     (_
					      #'ss)))))
		    (apply f args)))))))))
      
      ((_ . _)
       (error "make geneator takes one argument"))

      (_ (lambda (f) (make-generator-stis f))))))

	  
(define-syntax make-generator-delim
  (lambda (x)
    (syntax-case x ()
      ((_ (lambda (y a ...) code ...))
       (eq? (syntax->datum #'lambda) 'lambda)
       #'(lambda (a ...)
	   (let ()
	     (define obj   (make <yield>))
	     (define ab (make-prompt-tag))
	     (syntax-parameterize ((YIELD (lambda x #'ab)))
	       (slot-set! obj 'k #f)
	       (slot-set! obj 'closed #f)
	       (slot-set! obj 's
		 (lambda (a ...)
		   (call-with-prompt ab
		     (lambda ()
		       (let ((y yield))
			 (set-procedure-property! y 'yield-g yield-g)
			 code ...

			 (slot-set! obj 'closed #t)
			 (produce-arg arg-empty)))
		     
		     (letrec ((lam
			       (lambda (k . l)
				 (fluid-set! in-yield #f)
				 (struct-set! obj 1
					      (lambda (zz)
						(call-with-prompt ab
						  (lambda ()
						    (k zz))
						  lam)))
				 (apply produce-arg l))))
		       lam))))
	       obj))))

      
      ((_ f)
       #'(lambda args
	   (define obj (make <yield>))
	   (define ab  (make-prompt-tag))
	   (syntax-parameterize ((YIELD (lambda x #'ab)))
	      (slot-set! obj 'k #f)
	      (slot-set! obj 'closed #f)
	      (slot-set! obj 's
	        (lambda ()
		  (call-with-prompt ab
		    (lambda ()
		      (let ((y yield))
			(set-procedure-property! y 'yield-g yield-g)
			(apply f y args))
			
		      (slot-set! obj 'closed #t)
		      (produce-arg arg-empty))

		    (letrec ((lam
			      (lambda (k . l)
				(fluid-set! in-yield #f)
				(struct-set! obj 1
					     (lambda (zz)
					       (call-with-prompt ab
					         (lambda ()
						   (k zz))
						 lam)))
				(apply produce-arg l))))
		      lam)))))
	   obj))

      ((_ f _)
       #'(lambda args
	   (define obj (make <yield>))
	   (define ab  (make-prompt-tag))
	   (syntax-parameterize ((YIELD (lambda x #'ab)))
	      (slot-set! obj 'k #f)
	      (slot-set! obj 'closed #f)
	      (slot-set! obj 's
	        (lambda ()
		  (call-with-prompt ab
		    (lambda ()
		      (apply f args)
		      (slot-set! obj 'closed #t)
		      arg-empty)

		    (letrec ((lam
			      (lambda (k . l)
				(fluid-set! in-yield #f)
				(struct-set! obj 1
					     (lambda (zz)
					       (call-with-prompt ab
					         (lambda ()
						   (k zz))
						 lam)))
				(handle-arg l))))
		      lam)))))
	   obj)))))
       

(define-syntax make-generator
  (lambda (x)
    (syntax-case x ()
      ((_ f ...)
       (if stis
	   #'(make-generator-stis  f ...)
	   #'(make-generator-delim f ...))))))

(define-syntax define-generator
  (lambda (x)
    (syntax-case x ()
      ((_ (f . args) code ...)
       #'(define f
           (make-generator
            (lambda args
              code ...)))))))

(define-class <yield> () s k closed)
(cpit <yield> (o (lambda (obj s k closed)
		   (slot-set! obj 's      s     )
		   (slot-set! obj 'k      k     )
		   (slot-set! obj 'closed closed)
		   obj)
		 (list
		  (slot-ref o 's)
		  (slot-ref o 'k)
		  (slot-ref o 'closed))))

(name-object <yield>)

(define (maybe-resend u)
  (lambda (g)
    (if g 
	(apply send g u)
	(if (null? u)
	    '()
	    (apply produce-arg u)))))
  
(define-method (send (l <stis-iter>) . u)
  (define k   (struct-ref l 0))
  (define val (maybe-resend u))

  (handle-arg (stis-next k val) -1
	      (lambda (x)
		(throw 'python StopIteration (StopIteration x)))))

(define-method (send-g (l <stis-iter>) val)
  (define k   (struct-ref l 0))
      
  (handle-arg (stis-next k val) -1
	      (lambda (x)
		(throw 'python StopIteration (StopIteration x) None))))


(define-method (send (l <yield>) . u)
  (let ((k (struct-ref l 1))
	(s (struct-ref l 0))
	(c (struct-ref l 2)))
    (if (not c)
	(if k
	    (k (maybe-resend u))
            (if (or (null? u) (eq? (car u) 'None))
                (s)
                (throw
		 'python TypeError
		 (TypeError
		  "can't send a non-None value to a just-started generator"
		  ) #f)))            
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (send-g (l <yield>) g)
  (let ((k (struct-ref l 1))
        (s (struct-ref l 0))
        (c (struct-ref l 2)))
    (if (not c)
        (if k
            (k g)
            (throw 'python TypeError
                   (TypeError
                    "can't send a non-None value to a just-started generator"
                    )))
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (sendException (l <yield>) e o tr)
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if (not c)
        (if k           
            (k (lambda (g)
                 (if g
                     (sendException g e o tr)
                     (throw 'python e o tr))))
            (throw 'python Exception (Exception) tr))
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (sendClose (l <yield>))
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if c
        (values)
        (if k
            (begin
              (catch #t              
                (lambda ()
                  (k (lambda (g)
                       (if g
                           (sendClose g)
                           (throw 'python GeneratorExit GeneratorExit #f))))
                  (slot-set! l 'closed #t)
                  (throw 'python RuntimeError
                         (RuntimeError "sendClose") #f))
                (lambda (tag . v)
                  (slot-set! l 'closed #t)
                  (if (eq? tag 'python)
                      (match v
                        ((tag . l)
                         (if (eq? tag GeneratorExit)
                           (values)
                           (apply throw tag l))))
                      (apply throw tag v))))
              (slot-set! l 'closed #t))))))
            

(define-method (send (l <p>) . u)
  (apply (ref l '__send__) u))

(define-method (sendException (l <p>) . u)
  (apply (ref l 'throw) u))

(define-method (sendClose (l <p>))
  ((ref l '__close__)))

(define (yield-from a b) #f)                              

(set! (@@ (ice-9 stis) finished) arg-empty)

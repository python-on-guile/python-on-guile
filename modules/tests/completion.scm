;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


;;; Python on Guile --- Python implementation atop Guile
;;; Copyright © 2019 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This file is part of Python on Guile.
;;;
;;; This library is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module- (test-completion)
  #:use-module (language python completer)
  #:use-module (language python spec)
  #:use-module (ice-9 readline)
  #:use-module (srfi srfi-64))

(test-begin "completion")

(test-equal "completion for os.conc<TAB> works"
  "os.concat "
  (let ((pyeval (@@ (language python spec) python-eval))
        (port (mkstemp! "/tmp/pguile-readline.XXXXXX")))
    (with-output-to-port port
      (lambda _ (format #t "os.conc	")))
    (let ((file (port-filename port)))
      (close-port port)
      (parameterize ((current-language python))
        (pyeval "import os\n")
        (with-readline-completion-function
         (complete-python pyeval)
         (lambda ()
           (with-input-from-file file
             (lambda ()
               (set-readline-input-port! (current-input-port))
               (readline)))))))))

(test-end "completion")
